//
//  ModuleByTypeService.swift
//  EpcLensProject
//
//  Created by Bluehorse Mac mini on 24/07/18.
//  Copyright © 2018 Moumita Datta. All rights reserved.
//

import UIKit

class ModuleByTypeService: NSObject {
    
    var peopleResponse:Data = Data()
    var objSource : NSObject? = nil
    
    override init() {
        super.init()
    }
    
    // MARK:- Module By Type
    func getModuleByType( paramDict:[String:String], selectorFunction:Selector) {
        
        let urlString = EpicLensServiceConstant.BaseURL.appending(EpicLensServiceConstant.ModuleByType)
        let url = URL(string: urlString)!
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.httpMethod = "POST"
        request.timeoutInterval = 10
        
        let dataDictionary = NSMutableDictionary()
        
        for (theKey, theValue) in paramDict {
            dataDictionary.setValue(theValue.trim(), forKey: theKey.trim())
        }
        let postDictionary = NSMutableDictionary()
        postDictionary.setValue(dataDictionary, forKey: "data")
        
        let jsonData = try? JSONSerialization.data(withJSONObject: postDictionary)
        
        request.httpBody = jsonData
        
        let objConfiguration = URLSessionConfiguration.default
        objConfiguration.allowsCellularAccess = true
        let objSession = URLSession(configuration: objConfiguration)
        let task = objSession.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                return
            }
            
            // TODO:- Module By Type Response string
            let responseString = String(data: data, encoding: String.Encoding.utf8)
            print("Module By Type Response = \(responseString ?? "")")
            
            // TODO:- Search MarketPlace Data
            self.peopleResponse = data
            
            // TODO:- Callback function
            let meth = class_getInstanceMethod(object_getClass(self.objSource), selectorFunction)
            let imp = method_getImplementation(meth!)
            typealias ClosureType = @convention(c) (AnyObject, Selector) -> Void
            let callbackFunction : ClosureType = unsafeBitCast(imp, to: ClosureType.self)
            callbackFunction(self.objSource!,selectorFunction)
        }
        task.resume()
    }
    
}
