//
//  CompanyService.swift
//  EpcLensProject
//
//  Created by Surajit Ghosh on 06/05/18.
//  Copyright © 2018 Sudipta Biswas. All rights reserved.
//

import UIKit

class CompanyService: NSObject {
    
    var companyResponse:Data = Data()
    var objSource : NSObject? = nil
    
    override init() {
        super.init()
    }
    
    // MARK:- Company List
    func getCompanyList( paramDict:[String:String], selectorFunction:Selector) {
        
        let urlString = EpicLensServiceConstant.BaseURL.appending(EpicLensServiceConstant.CompanyList)
        let url = URL(string: urlString)!
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.httpMethod = "POST"
        request.timeoutInterval = 10
        
        let dataDictionary = NSMutableDictionary()
        
        for (theKey, theValue) in paramDict {
            dataDictionary.setValue(theValue.trim(), forKey: theKey.trim())
        }
        let postDictionary = NSMutableDictionary()
        postDictionary.setValue(dataDictionary, forKey: "data")
        
        let jsonData = try? JSONSerialization.data(withJSONObject: postDictionary)
        
        request.httpBody = jsonData
        
        let objConfiguration = URLSessionConfiguration.default
        objConfiguration.allowsCellularAccess = true
        let objSession = URLSession(configuration: objConfiguration)
        let task = objSession.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                return
            }
            
            // TODO:- Company List Response string
            let responseString = String(data: data, encoding: String.Encoding.utf8)
            print("Company List Response = \(responseString ?? "")")
            
            // TODO:- Search MarketPlace Data
            self.companyResponse = data
            
            // TODO:- Callback function
            let meth = class_getInstanceMethod(object_getClass(self.objSource), selectorFunction)
            let imp = method_getImplementation(meth!)
            typealias ClosureType = @convention(c) (AnyObject, Selector) -> Void
            let callbackFunction : ClosureType = unsafeBitCast(imp, to: ClosureType.self)
            callbackFunction(self.objSource!,selectorFunction)
        }
        task.resume()
    }
}
