//
//  MyLensListByCategoryDispatcher.swift
//  EpcLensProject
//
//  Created by Moumita Datta on 09/05/18.
//  Copyright © 2018 Moumita Datta. All rights reserved.
//

import UIKit

protocol MyLensListByCategoryProtocol {
    func didUpdateControllerWithObjMyLensListByCategory(myLensListByCategory:MyLensListByCategoryModel)
}


class MyLensListByCategoryDispatcher: NSObject {
    
    private var objMyLensByCategoryService = MyLensByCategoryService()
    var delegate: MyLensListByCategoryProtocol?
    
    override init() {
        super.init()
        objMyLensByCategoryService.objSource = self
    }
    
    // MARK:- Get MyLens List By Category
    func GetMyLensListByCategory(paramDict:[String:String]) {
        objMyLensByCategoryService.getMyLensListByCategory(paramDict: paramDict, selectorFunction: #selector(self.MylensListByCategoryResponse))
    }
    
    @objc func MylensListByCategoryResponse() {
        do {
            let objResponseModel = try JSONDecoder().decode(MyLensListByCategoryModel.self, from: objMyLensByCategoryService.myLensByCategoryResponse)
            print(objResponseModel.data.mylensdetailsbyslug.count)
            self.delegate?.didUpdateControllerWithObjMyLensListByCategory(myLensListByCategory: objResponseModel)
        } catch {
            print(error)
        }
    }
}
