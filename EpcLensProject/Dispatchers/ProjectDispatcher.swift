//
//  ProjectDispatcher.swift
//  EpcLensProject
//
//  Created by Moumita Datta on 27/11/18.
//  Copyright © 2018 Moumita Datta. All rights reserved.
//

import UIKit

protocol ProjectListProtocol {
    func didupdateControllerWithProjectList(projectList:ProjectListModel)
}

class ProjectDispatcher: NSObject {
    
    private var objProjectService = ProjectService()
    var delegate: ProjectListProtocol?
    
    override init() {
        super.init()
        objProjectService.objSource = self
    }
    
    // MARK:- Get Project List
    func GetProjectList(paramDict:[String:String]) {
        objProjectService.getProjectList(paramDict: paramDict, selectorFunction: #selector(self.ProjectListResponse))
    }
    
    @objc func ProjectListResponse() {
        do {
            let objResponseModel = try JSONDecoder().decode(ProjectListModel.self, from: objProjectService.projectResponse)
            print(objResponseModel.data.projectList.count)
            self.delegate?.didupdateControllerWithProjectList(projectList: objResponseModel)
        } catch {
            print(error)
        }
    }
    
}
