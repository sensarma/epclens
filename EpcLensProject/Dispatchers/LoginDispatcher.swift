//
//  LoginDispatcher.swift
//  EpcLensProject
//
//  Created by Sudipta Biswas on 19/04/18.
//  Copyright © 2018 Sudipta Biswas. All rights reserved.
//

import UIKit


@objc protocol LoginDispatchProtocol {    
    @objc optional func didupdateControllerFromLoginDispatch(_isSuccess:Bool)
    @objc optional func didupdateUserDetails(_isSuccess:Bool)
}

class LoginDispatcher: NSObject {
    
    
    private var objLoginService = LoginService()
    var delegate: LoginDispatchProtocol?
    
    override init() {
        super.init()
        objLoginService.objSource = self
    }
    
    // MARK:- User Login
    func userLogin(_emial:String,_password:String) {
        objLoginService.logInWith(_username: _emial, _password: _password, selectorFunction: #selector(LoginDispatcher.loginResponseData))
    }
    
    @objc func loginResponseData() {
        do {
            let objloginResponseModel = try JSONDecoder().decode(LoginResponseModel.self, from: objLoginService.loginResponse)
            if objloginResponseModel.data.verified == true {
                EpicLensConstantsVariables.accessToken = objloginResponseModel.data.accessToken!
                self.delegate?.didupdateControllerFromLoginDispatch!(_isSuccess: true)
            } else {
                self.delegate?.didupdateControllerFromLoginDispatch!(_isSuccess: false)
            }            
        } catch {
            print("loginResponseData error :: ",error)
        }
    }
    
    // MARK:- Get User Details
    func GetuserDetails() {
        objLoginService.getuserDetails(selectorFunction: #selector(self.UserDetailsResponse))
    }
    
    @objc func UserDetailsResponse() {
        do {
            let objResponseModel = try JSONDecoder().decode(UserdetailsResponseModel.self, from: objLoginService.UserdetailsResponse)
            if objResponseModel.data.success == true {
                EpicLensConstantsVariables.userDetailsData = objResponseModel.data
                self.delegate?.didupdateUserDetails!(_isSuccess: true)
            } else {
                self.delegate?.didupdateUserDetails!(_isSuccess: false)
            }
        } catch {
            print("UserDetailsResponse error :: ",error)
        }
    }
    
}
