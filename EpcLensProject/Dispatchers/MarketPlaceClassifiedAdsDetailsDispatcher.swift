//
//  MarketPlaceClassifiedAdsDetailsDispatcher.swift
//  EpcLensProject
//
//  Created by Moumita Datta on 05/07/18.
//  Copyright © 2018 Moumita Datta. All rights reserved.
//

import UIKit
protocol ClassifiedAdsDetailsProtocol {
    func didUpdateControllerWithObjMarketPlaceClassifiedAdsDetails(classifiedAdsDetails:MarketPlaceClassifiedAdsDetailsModel)
}


class MarketPlaceClassifiedAdsDetailsDispatcher: NSObject {
    
    private var objClassifiedAdsDetailsService = MarketPlaceClassifiedAdsDetailsService()
    var delegate: ClassifiedAdsDetailsProtocol?
    
    override init() {
        super.init()
        objClassifiedAdsDetailsService.objSource = self
    }
    
    // MARK:- Get MarketPlace
    func GetMarketPlaceClassifiedAdsDetails(paramDict:[String:String]) {
        objClassifiedAdsDetailsService.getMarketPlaceClassifiedAdsDetailsService(paramDict: paramDict, selectorFunction: #selector(self.ClassifiedAdsDetailsResponse))
    }
    
    @objc func ClassifiedAdsDetailsResponse() {
        do {
            let objResponseModel = try JSONDecoder().decode(MarketPlaceClassifiedAdsDetailsModel.self, from: objClassifiedAdsDetailsService.classifiedAdsDetailsResponse)
            self.delegate?.didUpdateControllerWithObjMarketPlaceClassifiedAdsDetails(classifiedAdsDetails: objResponseModel)
        } catch {
            print(error)
        }
    }
}
