//
//  LensDeskDispatcher.swift
//  EpcLensProject
//
//  Created by Moumita Datta on 26/06/18.
//  Copyright © 2018 Moumita Datta. All rights reserved.
//

import UIKit

protocol LensDeskListProtocol {
    func didUpdateControllerWithObjLensDeskMenuList(lensDeskMenuList:LensDeskMenuListModel)
    func didUpdateControllerWithObjLensDeskListDetailsByMenu(lensDeskListDetailsByMenu:LensDeskDetailsByMenuModel)
}

class LensDeskDispatcher: NSObject {
    
    private var objLensDeskService = LensDeskService()
    var delegate: LensDeskListProtocol?
    
    override init() {
        super.init()
        objLensDeskService.objSource = self
    }
    
    // MARK:- Get Lexicon Menu List
    func GetLensDeskMenuList(paramDict:[String:String]) {
        objLensDeskService.getLensDeskMenuList(paramDict: paramDict, selectorFunction: #selector(self.LensDeskMenuListResponse))
    }
    
    @objc func LensDeskMenuListResponse() {
        do {
            let objResponseModel = try JSONDecoder().decode(LensDeskMenuListModel.self, from: objLensDeskService.lensDeskResponse)
            //print("LexiconList data count:: ",objResponseModel.data.lexiconslist.count)
            self.delegate?.didUpdateControllerWithObjLensDeskMenuList(lensDeskMenuList: objResponseModel)
        } catch {
            print(error)
        }
    }
    
    // MARK:- Get LensDesk List Details By Menu
    func GetLensDeskListDetailsByMenu(paramDict:[String:String]) {
        objLensDeskService.getLensDeskListDetailsByMenu(paramDict: paramDict, selectorFunction: #selector(self.LensDeskListDetailsByMenuResponse))
    }
    
    @objc func LensDeskListDetailsByMenuResponse() {
        do {
            let objResponseModel = try JSONDecoder().decode(LensDeskDetailsByMenuModel.self, from: objLensDeskService.lensDeskResponse)
            //print("LexiconList data count:: ",objResponseModel.data.lexiconslist.count)
            self.delegate?.didUpdateControllerWithObjLensDeskListDetailsByMenu(lensDeskListDetailsByMenu: objResponseModel)
        } catch {
            print(error)
        }
    }
}
