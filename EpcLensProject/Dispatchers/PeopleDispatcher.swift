//
//  PeopleDispatcher.swift
//  EpcLensProject
//
//  Created by Surajit Ghosh on 06/05/18.
//  Copyright © 2018 Sudipta Biswas. All rights reserved.
//


import UIKit

protocol PeopleListProtocol {
    func didupdateControllerWithPeopleList(peopleList:PeopleListModel)
}

class PeopleDispatcher: NSObject {
    
    private var objPeopleService = PeopleService()
    var delegate: PeopleListProtocol?
    
    override init() {
        super.init()
        objPeopleService.objSource = self
    }
    
    // MARK:- Get People List
    func GetPeopleList(paramDict:[String:String]) {        
        objPeopleService.getPeopleList(paramDict: paramDict, selectorFunction: #selector(self.PeopleListResponse))
    }
    
    @objc func PeopleListResponse() {
        do {
            let objResponseModel = try JSONDecoder().decode(PeopleListModel.self, from: objPeopleService.peopleResponse)
            print(objResponseModel.data.peopleList.count)
            self.delegate?.didupdateControllerWithPeopleList(peopleList: objResponseModel)            
        } catch {
            print(error)
        }
    }

}
