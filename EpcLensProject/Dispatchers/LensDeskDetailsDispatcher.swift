//
//  LensDeskDetailsDispatcher.swift
//  EpcLensProject
//
//  Created by Moumita Datta on 10/07/18.
//  Copyright © 2018 Moumita Datta. All rights reserved.
//

import UIKit

protocol LensDeskDetailsProtocol {
    func didUpdateControllerWithObjLensDeskDetails(lensDeskDetails:LensDeskDetailsModel)
}



class LensDeskDetailsDispatcher: NSObject {
    
    private var objLensDeskDetailsService = LensDeskDetailsService()
    var delegate: LensDeskDetailsProtocol?
    
    override init() {
        super.init()
        objLensDeskDetailsService.objSource = self
    }
    
    // MARK:- Get LensDesk Details By ID
    func GetLensDeskDetailsByID(paramDict:[String:String]) {
        objLensDeskDetailsService.getLensDeskDetailsByID(paramDict: paramDict, selectorFunction: #selector(self.LensDeskDetailsResponse))
    }
    
    @objc func LensDeskDetailsResponse() {
        do {
            let objResponseModel = try JSONDecoder().decode(LensDeskDetailsModel.self, from: objLensDeskDetailsService.lensDeskDetailsResponse)
            self.delegate?.didUpdateControllerWithObjLensDeskDetails(lensDeskDetails: objResponseModel)
        } catch {
            print(error)
        }
    }
}
