//
//  CompanyDispatcher.swift
//  EpcLensProject
//
//  Created by Surajit Ghosh on 06/05/18.
//  Copyright © 2018 Sudipta Biswas. All rights reserved.
//


import UIKit

protocol CompanyListProtocol {
    func didUpdateControllerWithCompanyList(companylist:CompanyListModel)
}

class CompanyDispatcher: NSObject {
    
    private var objCompanyService = CompanyService()
    var delegate: CompanyListProtocol?
    
    override init() {
        super.init()
        objCompanyService.objSource = self
    }
    
    // MARK:- Get Company List
    func GetCompanyList(paramDict:[String:String]) {
        objCompanyService.getCompanyList(paramDict: paramDict, selectorFunction: #selector(self.CompanyistResponse))
    }
    
    @objc func CompanyistResponse() {
        do {
            let objResponseModel = try JSONDecoder().decode(CompanyListModel.self, from: objCompanyService.companyResponse)
            self.delegate?.didUpdateControllerWithCompanyList(companylist: objResponseModel)
        } catch {
            print(error)
        }
        
    }
}
