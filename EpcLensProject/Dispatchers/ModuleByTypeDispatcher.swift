//
//  ModuleByTypeDispatcher.swift
//  EpcLensProject
//
//  Created by Bluehorse Mac mini on 24/07/18.
//  Copyright © 2018 Moumita Datta. All rights reserved.
//

import UIKit

protocol ModuleByTypeProtocol {
    func didupdateControllerWithModuleByType(moduleByType:ModuleByTypeModel)
}

class ModuleByTypeDispatcher: NSObject {
    
    private var objModuleByTypeService = ModuleByTypeService()
    var delegate: ModuleByTypeProtocol?
    
    override init() {
        super.init()
        objModuleByTypeService.objSource = self
    }
    
    // MARK:- Get People List
    func GetModuleByType(paramDict:[String:String]) {
        objModuleByTypeService.getModuleByType(paramDict: paramDict, selectorFunction: #selector(self.ModuleByTypeResponse))
    }
    
    @objc func ModuleByTypeResponse() {
        do {
            let objResponseModel = try JSONDecoder().decode(ModuleByTypeModel.self, from: objModuleByTypeService.peopleResponse)
            print(objResponseModel.data.msg)
            self.delegate?.didupdateControllerWithModuleByType(moduleByType: objResponseModel)
        } catch {
            print(error)
        }
    }
    
}
