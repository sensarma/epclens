//
//  MyLensDispatcher.swift
//  EpcLensProject
//
//  Created by Moumita Datta on 09/05/18.
//  Copyright © 2018 Moumita Datta. All rights reserved.
//

import UIKit

protocol MyLensListProtocol {
    func didUpdateControllerWithObjMyLensList(myLensList:MyLensListModel)
}


class MyLensDispatcher: NSObject {
    
    private var objMyLensService = MyLensService()
    var delegate: MyLensListProtocol?
    
    override init() {
        super.init()
        objMyLensService.objSource = self
    }
    
    // MARK:- Get MyLens List
    func GetMyLensList(paramDict:[String:String]) {
        objMyLensService.getMyLensList(paramDict: paramDict, selectorFunction: #selector(self.MylensListResponse))
    }
    
    @objc func MylensListResponse() {
        do {
            let objResponseModel = try JSONDecoder().decode(MyLensListModel.self, from: objMyLensService.myLensResponse)
            print("myLensList data count:: ",objResponseModel.data.mylenslist.count)
            self.delegate?.didUpdateControllerWithObjMyLensList(myLensList: objResponseModel)
        } catch {
            print(error)
        }
    }
}
