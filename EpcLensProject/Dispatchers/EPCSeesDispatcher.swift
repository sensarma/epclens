//
//  EPCSeesDispatcher.swift
//  EpcLensProject
//
//  Created by Moumita Datta on 16/05/18.
//  Copyright © 2018 Moumita Datta. All rights reserved.
//

import UIKit

protocol EPCSeesListProtocol {
    func didUpdateControllerWithObjEPCSeesList(epcSeesList:EPCSeesListModel)
}


class EPCSeesDispatcher: NSObject {
    
    private var objEPCSeesService = EPCSeesService()
    var delegate: EPCSeesListProtocol?
    
    override init() {
        super.init()
        objEPCSeesService.objSource = self
    }
    
    // MARK:- Get EPCSees List
    func GetEPCSeesList(paramDict:[String:String]) {
        objEPCSeesService.getEpcSeesList(paramDict: paramDict, selectorFunction: #selector(self.EPCSeesListResponse))
    }
    
    @objc func EPCSeesListResponse() {
        do {
            let objResponseModel = try JSONDecoder().decode(EPCSeesListModel.self, from: objEPCSeesService.epcSeesResponse)
            print("EPCSeesList data count:: ",objResponseModel.data.itemList.count)
            self.delegate?.didUpdateControllerWithObjEPCSeesList(epcSeesList: objResponseModel)
        } catch {
            print(error)
        }
    }
}
