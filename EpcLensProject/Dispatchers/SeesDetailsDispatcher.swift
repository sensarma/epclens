//
//  SeesDetailsDispatcher.swift
//  EpcLensProject
//
//  Created by BlueHorse MacBook Pro on 23/07/18.
//  Copyright © 2018 Moumita Datta. All rights reserved.
//

import UIKit


protocol SeesDetailsProtocol {
    func didUpdateControllerWithSeesDetails(seesDetails:LensFlareListModel)
    func didUpdateControllerWithEPCSeesByGalleryID(seesByGalleryID:EPCSeesDetailsModel)
}

class SeesDetailsDispatcher:  NSObject {
    
    private var objSeesDetailsService = SeesDetailsService()
    var delegate: SeesDetailsProtocol?
    
    override init() {
        super.init()
        objSeesDetailsService.objSource = self
    }
    
    
    // MARK:- Get Sees Details
    func GetSeesDetails(paramDict:[String:String]) {
        objSeesDetailsService.getSeesDetails(paramDict: paramDict, selectorFunction: #selector(self.SeesDetailsResponse))
    }
    
    @objc func SeesDetailsResponse() {
        do {
            let objResponseModel = try JSONDecoder().decode(LensFlareListModel.self, from: objSeesDetailsService.lensFlareResponse)
            self.delegate?.didUpdateControllerWithSeesDetails(seesDetails:objResponseModel)
            //self.delegate?.didUpdateControllerWithSeesDetails(seeslist: objResponseModel)
        } catch {
            print(error)
        }
    }
    
    
    
    // MARK:- Get Sees Details By Gallery ID
    func GetSeesDetailsByGalleryId(paramDict:[String:String]) {
        objSeesDetailsService.getSeesDetailsByGalleryId(paramDict: paramDict, selectorFunction: #selector(self.SeesDetailsByGalleryIDResponse))
    }
    
    @objc func SeesDetailsByGalleryIDResponse() {
        do {
            let objResponseModel = try JSONDecoder().decode(EPCSeesDetailsModel.self, from: objSeesDetailsService.lensFlareResponse)
            self.delegate?.didUpdateControllerWithEPCSeesByGalleryID(seesByGalleryID: objResponseModel)
        } catch {
            print(error)
        }
    }
    
}
