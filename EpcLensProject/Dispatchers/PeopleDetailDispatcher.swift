//
//  PeopleDetailDispatcher.swift
//  EpcLensProject
//
//  Created by Surajit Ghosh on 08/05/18.
//  Copyright © 2018 Sudipta Biswas. All rights reserved.
//


import UIKit

protocol PeopleDetailProtocol {
    func didUpdateControllerWithPeopleData(peopleDetailsData:PeopleDetailModel)
}

class PeopleDetailDispatcher: NSObject {

    private var objPeopleDetailService = PeopleDetailService()
    var delegate: PeopleDetailProtocol?
    
    override init() {
        super.init()
        objPeopleDetailService.objSource = self
    }
    
    // MARK:- Get People detail
    func GetPeopleDetail(paramDict:[String:String]) {
        
        objPeopleDetailService.getPeopleDetail(paramDict: paramDict, selectorFunction: #selector(self.PeopleDetailResponse))
    }
    
    @objc func PeopleDetailResponse() {
        do{
            let objResponseModel = try JSONDecoder().decode(PeopleDetailModel.self, from: objPeopleDetailService.peopleDetailResponse)
            self.delegate?.didUpdateControllerWithPeopleData(peopleDetailsData: objResponseModel)
            
        }catch{
            print(error)
        }
    }
}
