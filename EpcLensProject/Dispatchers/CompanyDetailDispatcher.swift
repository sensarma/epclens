//
//  CompanyDetailDispatcher.swift
//  EpcLensProject
//
//  Created by Surajit Ghosh on 27/05/18.
//  Copyright © 2018 Sudipta Biswas. All rights reserved.
//


import UIKit
protocol CompanyDetailProtocol {
    
    func didUpdateControllerWithCompanyDetailsData(companyDetails:CompanyDetailsModel)
    func didUpdateControllerWithCompanyOverviewData(companyOverview:CompanyOverViewModel)
    func didUpdateControllerWithCompanyOperationData(companyOperation:CompanyOperationModel)
    func didUpdateControllerWithCompanyMetricsData(companyMetrics:CompanyMetricsModel)
    
}

class CompanyDetailDispatcher: NSObject {
    
    private var objCompanyDetailService = CompanyDetailService()
    var delegate: CompanyDetailProtocol?
    
    override init() {
        super.init()
        objCompanyDetailService.objSource = self
    }
    
    // MARK:- Get Company Detail
    func GetCompanyDetail(paramDict:[String:String]) {
        objCompanyDetailService.getComapnyDetail(paramDict: paramDict, selectorFunction: #selector(self.CompanyDetailResponse))
    }
    
    @objc func CompanyDetailResponse() {
        do {
            let objResponseModel = try JSONDecoder().decode(CompanyDetailsModel.self, from: objCompanyDetailService.companyDetailResponse)
            self.delegate?.didUpdateControllerWithCompanyDetailsData(companyDetails: objResponseModel)
        } catch {
            print(error)
        }
        
    }
    
    // MARK:- Get Company Overview
    func GetCompanyOverview(paramDict:[String:String]) {
        objCompanyDetailService.getComapnyOverview(paramDict: paramDict, selectorFunction: #selector(self.CompanyOverviewResponse))
    }
    
    @objc func CompanyOverviewResponse() {
        do {
            let objResponseModel = try JSONDecoder().decode(CompanyOverViewModel.self, from: objCompanyDetailService.companyOverViewResponse)
            self.delegate?.didUpdateControllerWithCompanyOverviewData(companyOverview: objResponseModel)
        } catch {
            print(error)
        }
        
    }
    
    // MARK:- Get Company Operation
    func GetCompanyOperation(paramDict:[String:String]) {
        objCompanyDetailService.getComapnyOperation(paramDict: paramDict, selectorFunction: #selector(self.CompanyOperationResponse))
    }
    
    @objc func CompanyOperationResponse() {
        do {
            let objResponseModel = try JSONDecoder().decode(CompanyOperationModel.self, from: objCompanyDetailService.companyOperationResponse)
            self.delegate?.didUpdateControllerWithCompanyOperationData(companyOperation: objResponseModel)
        } catch {
            print(error)
        }
    }
    
    // MARK:- Get Company Metrics
    func GetCompanyMetrics(paramDict:[String:String]) {
        objCompanyDetailService.getComapnyMetrics(paramDict: paramDict, selectorFunction: #selector(self.CompanyMetricsResponse))
    }
    
    @objc func CompanyMetricsResponse() {
        do {
           let objResponseModel = try JSONDecoder().decode(CompanyMetricsModel.self, from: objCompanyDetailService.companyMetricsResponse)
            self.delegate?.didUpdateControllerWithCompanyMetricsData(companyMetrics: objResponseModel)
        } catch {
            print(error)
        }
     }

}
