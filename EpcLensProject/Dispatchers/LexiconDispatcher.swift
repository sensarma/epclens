//
//  LexiconDispatcher.swift
//  EpcLensProject
//
//  Created by Moumita Datta on 14/05/18.
//  Copyright © 2018 Moumita Datta. All rights reserved.
//

import UIKit

protocol LexiconListProtocol {
    func didUpdateControllerWithObjLexiconMenuList(lexiconMenuList:LexiconMenuListModel)
    func didUpdateControllerWithObjLexiconListDetailsByMenu(lexiconListDetails:LexiconMenuDetailsModel)
}


class LexiconDispatcher: NSObject {
    
    private var objLexiconService = LexiconService()
    var delegate: LexiconListProtocol?
    
    override init() {
        super.init()
        objLexiconService.objSource = self
    }
    
    // MARK:- Get Lexicon Menu List
    func GetLexiconMenuList(paramDict:[String:String]) {
        objLexiconService.getLexiconMenuList(paramDict: paramDict, selectorFunction: #selector(self.LexiconMenuListResponse))
    }
    
    @objc func LexiconMenuListResponse() {
        do {
            let objResponseModel = try JSONDecoder().decode(LexiconMenuListModel.self, from: objLexiconService.lexiconResponse)
            //print("LexiconList data count:: ",objResponseModel.data.lexiconslist.count)
            self.delegate?.didUpdateControllerWithObjLexiconMenuList(lexiconMenuList: objResponseModel)
        } catch {
            print(error)
        }
    }
    
    // MARK:- Get Lexicon List Details
    func GetLexiconListDetailsByMenu(paramDict:[String:String]) {
        objLexiconService.getLexiconListDetailsByMenu(paramDict: paramDict, selectorFunction: #selector(self.LexiconListDetailsByMenuResponse))
    }
    
    @objc func LexiconListDetailsByMenuResponse() {
        do {
            let objResponseModel = try JSONDecoder().decode(LexiconMenuDetailsModel.self, from: objLexiconService.lexiconResponse)
            //print("LexiconList data count:: ",objResponseModel.data.lexiconslist.count)
            self.delegate?.didUpdateControllerWithObjLexiconListDetailsByMenu(lexiconListDetails: objResponseModel)
        } catch {
            print(error)
        }
    }
}
