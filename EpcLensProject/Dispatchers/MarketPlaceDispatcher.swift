//
//  MarketPlaceDispatcher.swift
//  EpcLensProject
//
//  Created by Moumita Datta on 20/06/18.
//  Copyright © 2018 Moumita Datta. All rights reserved.
//

import UIKit

protocol MarketPlaceProtocol {
    func didUpdateControllerWithObjMarketPlace(marketPlace:MarketPlaceModel)
}



class MarketPlaceDispatcher: NSObject {
    
    private var objMarketPlaceService = MarketPlaceService()
    var delegate: MarketPlaceProtocol?
    
    override init() {
        super.init()
        objMarketPlaceService.objSource = self
    }
    
    // MARK:- Get MarketPlace
    func GetMarketPlace(paramDict:[String:String]) {
        objMarketPlaceService.getMarketPlaceService(paramDict: paramDict, selectorFunction: #selector(self.MarketPlaceResponse))
    }
    
    @objc func MarketPlaceResponse() {
        do {
            let objResponseModel = try JSONDecoder().decode(MarketPlaceModel.self, from: objMarketPlaceService.marketPlaceResponse)
            self.delegate?.didUpdateControllerWithObjMarketPlace(marketPlace: objResponseModel)
        } catch {
            print(error)
        }
    }
}
