//
//  MyAccountActivitiesDetailsDispatcher.swift
//  EpcLensProject
//
//  Created by Moumita Datta on 15/06/18.
//  Copyright © 2018 Moumita Datta. All rights reserved.
//

import UIKit


protocol MyAccountActivitiesDetailsProtocol {
    func didUpdateControllerWithObjMyAccountActivitiesDetails(myAcctActivitiesDetails:MyAccountActivitiesDetailsModel)
}


class MyAccountActivitiesDetailsDispatcher: NSObject {
    
    private var objMyAccountActivitiesDetailsService = MyAccountActivitiesDetailsService()
    var delegate: MyAccountActivitiesDetailsProtocol?
    
    override init() {
        super.init()
        objMyAccountActivitiesDetailsService.objSource = self
    }
    
    // MARK:- Get Lexicon Menu List
    func GetMyAccountActivitiesDetails(paramDict:[String:String]) {
        objMyAccountActivitiesDetailsService.getMyAccountActivitiesDetails(paramDict: paramDict, selectorFunction: #selector(self.MyAccountActivitiesDetailsResponse))
    }
    
    @objc func MyAccountActivitiesDetailsResponse() {
        do {
            let objResponseModel = try JSONDecoder().decode(MyAccountActivitiesDetailsModel.self, from: objMyAccountActivitiesDetailsService.myAcctActivitiesResponse)
            self.delegate?.didUpdateControllerWithObjMyAccountActivitiesDetails(myAcctActivitiesDetails: objResponseModel)
        } catch {
            print(error)
        }
    }
}
