//
//  LexiconDetailsDispatcher.swift
//  EpcLensProject
//
//  Created by Moumita Datta on 07/06/18.
//  Copyright © 2018 Moumita Datta. All rights reserved.
//

import UIKit

protocol LexiconDetailsProtocol {
    func didUpdateControllerWithObjLexiconDetails(lexiconDetails:LexiconDetailsModel)
}



class LexiconDetailsDispatcher: NSObject {
    
    private var objLexiconDetailsService = LexiconDetailsService()
    var delegate: LexiconDetailsProtocol?
    
    override init() {
        super.init()
        objLexiconDetailsService.objSource = self
    }
    
    // MARK:- Get Lexicon Details By Category
    func GetLexiconDetailsByCategory(paramDict:[String:String]) {
        objLexiconDetailsService.getLexiconDetailsByCategory(paramDict: paramDict, selectorFunction: #selector(self.LexiconDetailsResponse))
    }
    
    @objc func LexiconDetailsResponse() {
        do {
            let objResponseModel = try JSONDecoder().decode(LexiconDetailsModel.self, from: objLexiconDetailsService.lexiconDetailsResponse)
            self.delegate?.didUpdateControllerWithObjLexiconDetails(lexiconDetails: objResponseModel)
        } catch {
            print(error)
        }
    }
}
