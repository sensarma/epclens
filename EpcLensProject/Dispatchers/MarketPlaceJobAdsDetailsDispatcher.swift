//
//  MarketPlaceJobAdsDetailsDispatcher.swift
//  EpcLensProject
//
//  Created by Moumita Datta on 10/07/18.
//  Copyright © 2018 Moumita Datta. All rights reserved.
//

import UIKit

protocol JobAdsDetailsProtocol {
    func didUpdateControllerWithObjMarketPlaceJobAdsDetails(jobAdsDetails:MarketPlaceJobAdsDetailsModel)
}


class MarketPlaceJobAdsDetailsDispatcher: NSObject {
    
    private var objJobAdsDetailsService = MarketPlaceJobAdsDetailsService()
    var delegate: JobAdsDetailsProtocol?
    
    override init() {
        super.init()
        objJobAdsDetailsService.objSource = self
    }
    
    // MARK:- Get MarketPlace Job
    func GetMarketPlaceJobAdsDetails(paramDict:[String:String]) {
        objJobAdsDetailsService.getMarketPlaceJobAdsDetailsService(paramDict: paramDict, selectorFunction: #selector(self.JobAdsDetailsResponse))
    }
    
    @objc func JobAdsDetailsResponse() {
        do {
            let objResponseModel = try JSONDecoder().decode(MarketPlaceJobAdsDetailsModel.self, from: objJobAdsDetailsService.jobAdsDetailsResponse)
            self.delegate?.didUpdateControllerWithObjMarketPlaceJobAdsDetails(jobAdsDetails: objResponseModel)
        } catch {
            print(error)
        }
    }
}
