//
//  MarketPlaceJobsListingDispatcher.swift
//  EpcLensProject
//
//  Created by Moumita Datta on 28/06/18.
//  Copyright © 2018 Moumita Datta. All rights reserved.
//

import UIKit

protocol JobsListingProtocol {
    func didUpdateControllerWithObjMarketPlaceJobsListing(jobsListing:MarketPlaceJobsListModel)
}


class MarketPlaceJobsListingDispatcher: NSObject {
    
    private var objJobsListService = MarketPlaceJobsListingService()
    var delegate: JobsListingProtocol?
    
    override init() {
        super.init()
        objJobsListService.objSource = self
    }
    
    // MARK:- Get MarketPlace Jobs
    func GetMarketPlaceJobsListing(paramDict:[String:String]) {
        objJobsListService.getMarketPlaceJobsListService(paramDict: paramDict, selectorFunction: #selector(self.JobsListingResponse))
    }
    
    @objc func JobsListingResponse() {
        do {
            let objResponseModel = try JSONDecoder().decode(MarketPlaceJobsListModel.self, from: objJobsListService.jobsListingResponse)
            self.delegate?.didUpdateControllerWithObjMarketPlaceJobsListing(jobsListing: objResponseModel)
        } catch {
            print(error)
        }
    }
}

