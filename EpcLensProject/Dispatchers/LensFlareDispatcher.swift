//
//  LensFlareDispatcher.swift
//  EpcLensProject
//
//  Created by Moumita Datta on 17/05/18.
//  Copyright © 2018 Moumita Datta. All rights reserved.
//

import UIKit

protocol LensFlareListProtocol {
    func didUpdateControllerWithLensFlareList(lensFlarelist:LensFlareListModel)
    func didUpdateControllerWithLensFlareItemsByID(lensFlarelistByID:LensFlareListModel)
}



class LensFlareDispatcher: NSObject {
    
    private var objLensFlareService = LensFlareService()
    var delegate: LensFlareListProtocol?
    
    override init() {
        super.init()
        objLensFlareService.objSource = self
    }
    
    // MARK:- Get LensFlare List
    func GetLensFlareMenuList(paramDict:[String:String]) {
        objLensFlareService.getLensFlareList(paramDict: paramDict, selectorFunction: #selector(self.LensFlareListResponse))
    }
    
    @objc func LensFlareListResponse() {
        do {
            let objResponseModel = try JSONDecoder().decode(LensFlareListModel.self, from: objLensFlareService.lensFlareResponse)
            self.delegate?.didUpdateControllerWithLensFlareList(lensFlarelist: objResponseModel)
        } catch {
            print(error)
        }
    }
    
    
    
    
    // MARK:- Get LensFlare List By ID
    func GetLensFlareListByID(paramDict:[String:String]) {
        objLensFlareService.getLensFlareListByID(paramDict: paramDict, selectorFunction: #selector(self.LensFlareListByIDResponse))
    }
    
    @objc func LensFlareListByIDResponse() {
        do {
            let objResponseModel = try JSONDecoder().decode(LensFlareListModel.self, from: objLensFlareService.lensFlareResponse)
            self.delegate?.didUpdateControllerWithLensFlareItemsByID(lensFlarelistByID: objResponseModel)
        } catch {
            print(error)
        }
    }
    
    
    
    
    
    
    /*// MARK:- Get Lexicon Details By Category
    func GetLexiconDetailsByCategory(paramDict:[String:String]) {
        objLensFlareService.getLexiconDetailsByCategory(paramDict: paramDict, selectorFunction: #selector(self.LexiconDetailsByCategoryResponse))
    }
    
    @objc func LexiconDetailsByCategoryResponse() {
        do {
            let objResponseModel = try JSONDecoder().decode(LensFlareListModel.self, from: objLensFlareService.lensFlareResponse)
            self.delegate?.didUpdateControllerWithLensFlareList(lensFlarelist: objResponseModel)
            //self.delegate?.didUpdateControllerWithSeesDetails(seeslist: objResponseModel)
        } catch {
            print(error)
        }
    }*/
    
}
