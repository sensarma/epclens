//
//  MarketPlaceClassifiedAdsListingDispatcher.swift
//  EpcLensProject
//
//  Created by Moumita Datta on 28/06/18.
//  Copyright © 2018 Moumita Datta. All rights reserved.
//

import UIKit
protocol ClassifiedAdsListingProtocol {
    func didUpdateControllerWithObjMarketPlaceClassifiedAdsListing(classifiedAdsListing:MarketPlaceClassifiedAdsListModel)
}


class MarketPlaceClassifiedAdsListingDispatcher: NSObject {
    
    private var objClassifiedAdsListService = MarketPlaceClassifiedAdsListingService()
    var delegate: ClassifiedAdsListingProtocol?
    
    override init() {
        super.init()
        objClassifiedAdsListService.objSource = self
    }
    
    // MARK:- Get MarketPlace
    func GetMarketPlaceClassifiedAdsListing(paramDict:[String:String]) {
        objClassifiedAdsListService.getMarketPlaceClassifiedAdsListService(paramDict: paramDict, selectorFunction: #selector(self.ClassifiedAdsListingResponse))
    }
    
    @objc func ClassifiedAdsListingResponse() {
        do {
            let objResponseModel = try JSONDecoder().decode(MarketPlaceClassifiedAdsListModel.self, from: objClassifiedAdsListService.classifiedAdsListingResponse)
            self.delegate?.didUpdateControllerWithObjMarketPlaceClassifiedAdsListing(classifiedAdsListing: objResponseModel)
        } catch {
            print(error)
        }
    }
}
