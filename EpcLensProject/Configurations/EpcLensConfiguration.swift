//
//  EpcLensConfiguration.swift
//  EpcLensProject
//
//  Created by Sudipta Biswas on 28/04/18.
//  Copyright © 2018 Sudipta Biswas. All rights reserved.
//

import UIKit

class EpicLensServiceConstant {
    //Old 28.04.2018
    //static let BaseURL = "http://52.14.15.185:3300/api/v2/"
    
    //New 6.6.2018
    //static let BaseURL = "http://18.217.188.228:3300/api/v2/"
    
    //New 14.11.2018
    static let BaseURL = "http://18.223.247.167:3300/api/v2/"
    
    //For Server error
    //static let BaseURL = "http://18.221.184.199:3300/api/v2/"
    
    
    static let Login        = "login"
    static let UserDetails  = "userdetails"
    static let ModuleByType = "setmodulebytype"
    
    
    //RightMenu
    static let PeopleList   = "getPeopleList"
    static let CompanyList  = "companylist"
    static let PeopleDetail = "getpeopledetails"
    static let ProjectList   = "projectlist"

    
    //LeftMenu
    static let Lensflarelist = "flarelists"
    static let LensflarelistByID = "flareItemsbyid"
    static let Seeslist = "seeslists"
    static let SeesDetails = "seesbyid"
    static let SeesByGalleryID = "seesbygalleryid"
    
    
    static let MyLensList = "mylensList"
    static let MyLensListByCategory = "mylensdetailsbyslug"
    static let MarketPlaceList = "marketplace"
    static let ClassifiedAdsList = "classifiedlisting"
    static let ClassifiedAdsDetails = "classifieddetails"
    static let JobsList = "joblisting"
    static let JobAdsDetails = "jobdetails"
    
    
    static let LexiconMenuList = "lexiconcategoryList"
    static let LexiconListDetailsByMenu = "lexicondetails"
    static let LexiconDetailsByCategory = "lexicondetailsbycat"
    static let MyAccountActivitiesDetails = "myactivities"
    static let LensDeskMenuList = "lensdeskcategoryList"
    static let LensDeskListDetailsByMenu = "lensdeskdetailsbycat"
    static let LensDeskDetails = "lensdeskdetails"
    
    
    //Company
    static let CompanyDetail = "companydetails"
    static let CompanyOverview = "companyoverview"
    static let CompanyOperation = "companyoperation"
    static let CompanyMetrics = "companymatrics"
}


class EpicLensConstantsVariables {
    static var accessToken = ""
    static var userDetailsData : UserDetailsData?
    static let IndicatorSize = UIDevice.current.iPhone == true ? CGSize(width: 30, height:30) : CGSize(width: 70, height:70)
    static let IndicatorValue = 14
}

