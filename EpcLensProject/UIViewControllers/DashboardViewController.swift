//
//  DashboardViewController.swift
//  EpcLensProject
//
//  Created by Sudipta Biswas on 28/04/18.
//  Copyright © 2018 Sudipta Biswas. All rights reserved.
//

import UIKit

class DashboardViewController: UIViewController {
    
    //MARK:: UIView
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:: IBAction
    @IBAction func btnLeftMenuDidTap(_ sender: Any) {
        let elDrawer = self.navigationController?.parent as! KYDrawerController
        elDrawer.setDrawerState(.opened, animated: true)
    }
    @IBAction func btnRightMenuDidTap(_ sender: UIButton) {
        
//        let originalTransform = sender.transform
//        let scaledTransform = originalTransform.scaledBy(x: 0.2, y: 0.2)
//        let scaledAndTranslatedTransform = scaledTransform.translatedBy(x: 0.0, y: -250.0)
//        UIView.animate(withDuration: 0.7, animations: {
//            sender.transform = scaledAndTranslatedTransform
//        })
        
        let objReportPopup = self.storyboard?.instantiateViewController(withIdentifier: "RightMenuController") as! RightMenuController
        objReportPopup.showPopup(_controller: self)
    }
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
