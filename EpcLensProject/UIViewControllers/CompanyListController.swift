//
//  CompanyListController.swift
//  EpcLensProject
//
//  Created by Surajit Ghosh on 06/05/18.
//  Copyright © 2018 Sudipta Biswas. All rights reserved.
//

import UIKit

class CompanyListController: UIViewController, CompanyListProtocol, ModuleByTypeProtocol {

    @IBOutlet weak var tableCompanyList: UITableView!
    var companyDispatcher = CompanyDispatcher()
    var companyListArr = [CompanyList]()
    
    var moduleByTypeDispatcher = ModuleByTypeDispatcher()
    var moduleByTypeStrMsg : String = ""
    
    var totalPageCount : Int = -1
    
    
    //Mark:: AnimatedLoader
    let loaderController = EpcLensLoaderViewController(nibName: "EpcLensLoaderViewController", bundle: nil)
    
    
    //MARK:: UIView
    override func viewDidLoad() {
        super.viewDidLoad()
        companyDispatcher.delegate = self
        var tempDict = [String:String]()
        tempDict["token"] = EpicLensConstantsVariables.accessToken
        tempDict["page"] = "0"
        companyDispatcher.GetCompanyList(paramDict: tempDict)
        
        // Do any additional setup after loading the view.
        
        // Start Loaded
        loaderController.showEpcLoader(_sourceView: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    

    //MARK:: IBAction
    @IBAction func btnLeftMenuDidTap(_ sender: Any) {
        let elDrawer = self.navigationController?.parent as! KYDrawerController
        elDrawer.setDrawerState(.opened, animated: true)
    }
    
    @IBAction func btnRightMenuDidTap(_ sender: UIButton) {
        let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
        let objReportPopup = storyboardMain.instantiateViewController(withIdentifier: "RightMenuController") as! RightMenuController
        objReportPopup.showPopup(_controller: self)
    }
    
    @IBAction func menuClicked(_ sender: Any) {
        print(self.navigationController?.viewControllers ?? "")
        let elDrawer = self.navigationController?.parent as! KYDrawerController
        elDrawer.setDrawerState(.opened, animated: true)
    }
    
    @IBAction func BtnSettingsDidTap(_ sender: Any) {
        let btn = sender as! UIButton
        let cell = btn.superview?.superview?.superview?.superview as! UITableViewCell
        let indexOfCell = self.tableCompanyList.indexPath(for: cell)
        print("Clicked Index : \(String(describing: indexOfCell?.row))")
        EpcLensController.sharedInstance.showToastMesage(_sourceController: self, _msg: "\(String(describing: indexOfCell?.row))")
        //if sender.tag == index {
        FTPopOverMenu.show(forSender: sender as! UIView, withMenuArray: ["see mentions","put on 'my radar"], imageArray: nil, doneBlock: { (index) in
            //print("Index of Menu : \(index)")
            
            if (index == 0) {
                //self.seeMentions(_index: (indexOfCell?.row)!)
                let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
                let searchVC = storyboardMain.instantiateViewController(withIdentifier: "SearchVC") as! SearchController
                self.navigationController?.pushViewController(searchVC, animated: true)
                
            } else if(index == 1) {
                //self.putOnMyRader(_index: (indexOfCell?.row)!)
                
                self.moduleByTypeDispatcher.delegate = self
                var tempDict = [String:String]()
                tempDict["token"] = EpicLensConstantsVariables.accessToken
                tempDict["id"] = String(self.companyListArr[(indexOfCell?.row)!].id)
                tempDict["type"] = "companyradar"
                self.moduleByTypeDispatcher.GetModuleByType(paramDict: tempDict)
            }
        }) {
            
        }
    }
    
    private func seeMentions(_index:Int) {
        
    }
    
    private func putOnMyRader(_index:Int) {
        
    }
    
    //MARK:: CompanyListProtocol
    func didUpdateControllerWithCompanyList(companylist:CompanyListModel) {
        DispatchQueue.main.async {
            if companylist.data.sucess {
                self.companyListArr = companylist.data.companyList!
                self.totalPageCount = companylist.data.totalPage
                self.tableCompanyList.reloadData()
                // Stop Loaded
                self.loaderController.hideEpcLoader()
            }
        }
    }
    
    func didupdateControllerWithModuleByType(moduleByType: ModuleByTypeModel) {
        DispatchQueue.main.async {
            if moduleByType.data.success {
                self.moduleByTypeStrMsg = moduleByType.data.msg!
                //print("moduleByTypeStrMsg :: \(self.moduleByTypeStrMsg)")
                EpcLensController.sharedInstance.showAlert(_sourceController: self, _msg: self.moduleByTypeStrMsg)
                // Stop Loaded
                self.loaderController.hideEpcLoader()
            }
        }
    }
    func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        //Bottom Refresh
        
        if scrollView == tableCompanyList{
            
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
            {
                for index in stride(from: 1, to: totalPageCount, by: 1) {
                var tempDict = [String:String]()
                tempDict["token"] = EpicLensConstantsVariables.accessToken
                tempDict["page"] = String(index)
                companyDispatcher.GetCompanyList(paramDict: tempDict)
                }
            }
        }
    }
    
    
}

extension CompanyListController : UITableViewDataSource,UITableViewDelegate {
    //MARK:: UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return companyListArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let idstr = "companycell"
        
        var cell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: idstr)
        }
        
        
        //Declare the outlet in our custom class
        let lblCompanyName = cell.viewWithTag(1) as! UILabel
        let lblPeopleName = cell.viewWithTag(4) as! UILabel
        let lblPeopleDesignation = cell.viewWithTag(5) as! UILabel
        let lblServicetype = cell.viewWithTag(6) as! UILabel
        let lblOpectypeName = cell.viewWithTag(40) as! UILabel
        
        
        if (companyListArr[indexPath.row].name == nil) || (companyListArr[indexPath.row].name! == "") || (companyListArr[indexPath.row].name! == "(NULL)") || (companyListArr[indexPath.row].name! == "<NULL>") || ((companyListArr[indexPath.row].name! == "<null>") || (companyListArr[indexPath.row].name! == "(null)") || (companyListArr[indexPath.row].name! == "")) {
            lblCompanyName.text = " "
        } else {
            lblCompanyName.text = companyListArr[indexPath.row].name?.lowercased()
        }
            
        if (companyListArr[indexPath.row].peopleName == nil) || (companyListArr[indexPath.row].peopleName! == "") || (companyListArr[indexPath.row].peopleName! == "(NULL)") || (companyListArr[indexPath.row].peopleName! == "<NULL>") || ((companyListArr[indexPath.row].peopleName! == "<null>") || (companyListArr[indexPath.row].peopleName! == "(null)") || (companyListArr[indexPath.row].peopleName! == "")) {
            lblPeopleName.text = " "
        } else {
            lblPeopleName.text = companyListArr[indexPath.row].peopleName?.lowercased()
        }
        
        if (companyListArr[indexPath.row].peopleDesignation == nil) || (companyListArr[indexPath.row].peopleDesignation! == "") || (companyListArr[indexPath.row].peopleDesignation! == "(NULL)") || (companyListArr[indexPath.row].peopleDesignation! == "<NULL>") || ((companyListArr[indexPath.row].peopleDesignation! == "<null>") || (companyListArr[indexPath.row].peopleDesignation! == "(null)") || (companyListArr[indexPath.row].peopleDesignation! == "")) {
            lblPeopleDesignation.text = " "
        } else {
            lblPeopleDesignation.text = companyListArr[indexPath.row].peopleDesignation?.lowercased()
        }
        
        if (companyListArr[indexPath.row].serviceType == nil) || (companyListArr[indexPath.row].serviceType!.rawValue == "") || (companyListArr[indexPath.row].serviceType!.rawValue == "(NULL)") || (companyListArr[indexPath.row].serviceType!.rawValue == "<NULL>") || ((companyListArr[indexPath.row].serviceType!.rawValue == "<null>") || (companyListArr[indexPath.row].serviceType!.rawValue == "(null)") || (companyListArr[indexPath.row].serviceType!.rawValue == "")) {
            lblServicetype.text = " "
        } else {
            lblServicetype.text = (companyListArr[indexPath.row].serviceType)?.rawValue
        }
        
        if (companyListArr[indexPath.row].opectypeName == nil) || (companyListArr[indexPath.row].opectypeName! == "") || (companyListArr[indexPath.row].opectypeName! == "(NULL)") || (companyListArr[indexPath.row].opectypeName! == "<NULL>") || ((companyListArr[indexPath.row].opectypeName! == "<null>") || (companyListArr[indexPath.row].opectypeName! == "(null)") || (companyListArr[indexPath.row].opectypeName! == "")) {
            lblOpectypeName.text = " "
        } else {
            lblOpectypeName.text = companyListArr[indexPath.row].opectypeName?.lowercased()
        }
        

        let lblO = cell.viewWithTag(7) as! UILabel
        let lblE = cell.viewWithTag(8) as! UILabel
        let lblP = cell.viewWithTag(9) as! UILabel
        let lblC = cell.viewWithTag(10) as! UILabel
        
        print("companyList[indexPath.row]:: \(indexPath)")
        
        let arrProjectPhase = [companyListArr[indexPath.row].projectphaseName]
        if arrProjectPhase.count > 0 {
            for index in stride(from: 0, to: arrProjectPhase.count, by: 1) {
                print("arrProjectPhase[indexPath.row]:: \(String(describing: arrProjectPhase[index]))")
                let arrProjectPhaseIndex = arrProjectPhase[index]!
                for ix in stride(from: 0, to: arrProjectPhaseIndex.count, by: 1) {
                    print("arrProjectPhaseIndex:: \(arrProjectPhaseIndex)")
                    if arrProjectPhaseIndex[ix].optionLabel.rawValue == "Engineering" {
                        lblE.textColor = UIColor.black
                    } else if arrProjectPhaseIndex[ix].optionLabel.rawValue == "Construction" {
                        lblC.textColor = UIColor.black
                    } else if arrProjectPhaseIndex[ix].optionLabel.rawValue == "Owner" {
                        lblO.textColor = UIColor.black
                    } else if arrProjectPhaseIndex[ix].optionLabel.rawValue == "Procurement" {
                        lblP.textColor = UIColor.black
                    }
                }
            }
        }
        /*if companyListArr[indexPath.row].projectphaseName! == "Engineering" {
            lblO.textColor = UIColor.white
            lblE.textColor = UIColor.black
            lblP.textColor = UIColor.white
            lblC.textColor = UIColor.white
        } else if companyListArr[indexPath.row].projectphaseName! == "Construction" {
            lblO.textColor = UIColor.white
            lblE.textColor = UIColor.white
            lblP.textColor = UIColor.white
            lblC.textColor = UIColor.black
        } else if companyListArr[indexPath.row].projectphaseName! == "Owner" {
            lblO.textColor = UIColor.black
            lblE.textColor = UIColor.white
            lblP.textColor = UIColor.white
            lblC.textColor = UIColor.white
        } else if companyListArr[indexPath.row].projectphaseName! == "Procurement" {
            lblO.textColor = UIColor.white
            lblE.textColor = UIColor.white
            lblP.textColor = UIColor.black
            lblC.textColor = UIColor.white
        }*/
        
       
        //Declare the outlet in our custom class
        let imgPower = cell.viewWithTag(11) as! UIImageView
        let imgOil = cell.viewWithTag(12) as! UIImageView
        let imgMidstr = cell.viewWithTag(13) as! UIImageView
        let imgPline = cell.viewWithTag(14) as! UIImageView
        let imgGas = cell.viewWithTag(15) as! UIImageView
        let imgInfrast = cell.viewWithTag(16) as! UIImageView
        let imgRefining = cell.viewWithTag(17) as! UIImageView
        let imgMM = cell.viewWithTag(18) as! UIImageView
        let imgInstCom = cell.viewWithTag(19) as! UIImageView

      
        imgPower.image = #imageLiteral(resourceName: "status_white")
        imgOil.image = #imageLiteral(resourceName: "status_white")
        imgMidstr.image = #imageLiteral(resourceName: "status_white")
        imgPline.image = #imageLiteral(resourceName: "status_white")
        imgGas.image = #imageLiteral(resourceName: "status_white")
        imgInfrast.image = #imageLiteral(resourceName: "status_white")
        imgRefining.image = #imageLiteral(resourceName: "status_white")
        imgMM.image = #imageLiteral(resourceName: "status_white")
        imgInstCom.image = #imageLiteral(resourceName: "status_white")
        
        //print("[companyList[indexPath.row].companyCategory].count :: \([companyList[indexPath.row].companyCategory].count)")
        
        let arrCompanyCategory = [companyListArr[indexPath.row].companyCategory]
       
        if arrCompanyCategory.count > 0 {
            for index in stride(from: 0, to: arrCompanyCategory.count, by: 1) {
                for i in arrCompanyCategory[index]! {
                    //print("index :: \(i)")
                    if i.categoryName == "Power" {
                        imgPower.image = #imageLiteral(resourceName: "status_yellow")
                    } else if i.categoryName == "Oil" {
                        imgOil.image = #imageLiteral(resourceName: "status_yellow")
                    } else if i.categoryName == "" {
                        imgMidstr.image = #imageLiteral(resourceName: "status_yellow")
                    } else if i.categoryName == "P-line" {
                        imgPline.image = #imageLiteral(resourceName: "status_yellow")
                    } else if i.categoryName == "Gas" {
                        imgGas.image = #imageLiteral(resourceName: "status_yellow")
                    } else if i.categoryName == "Infrast" {
                        imgInfrast.image = #imageLiteral(resourceName: "status_yellow")
                    } else if i.categoryName == "" {
                        imgRefining.image = #imageLiteral(resourceName: "status_yellow")
                    } else if i.categoryName == "" {
                        imgMM.image = #imageLiteral(resourceName: "status_yellow")
                    } else if i.categoryName == "Inst/com" {
                        imgInstCom.image = #imageLiteral(resourceName: "status_yellow")
                    }
                }
            }
        }
        
        let lblYT = cell.viewWithTag(20) as! UILabel
        let lblNT = cell.viewWithTag(21) as! UILabel
        let lblNU = cell.viewWithTag(22) as! UILabel
        let lblBC = cell.viewWithTag(23) as! UILabel
        let lblAB = cell.viewWithTag(24) as! UILabel
        let lblSK = cell.viewWithTag(25) as! UILabel
        let lblMB = cell.viewWithTag(26) as! UILabel
        
        
        let arrCompanyReg = [companyListArr[indexPath.row].compReg]
        
        if (arrCompanyReg.count) > 0 {
            for index in stride(from: 0, to: arrCompanyReg.count, by: 1) {
                for i in arrCompanyReg[index]! {
                    if i.regionName == "yt" {
                        lblYT.textColor = UIColor.black
                    } else if i.regionName == "nt" {
                        lblNT.textColor = UIColor.black
                    } else if i.regionName == "nu" {
                        lblNU.textColor = UIColor.black
                    } else if i.regionName == "bc" {
                        lblBC.textColor = UIColor.black
                    } else if i.regionName == "ab" {
                        lblAB.textColor = UIColor.black
                    } else if i.regionName == "sk" {
                        lblSK.textColor = UIColor.black
                    } else if i.regionName == "mb" {
                        lblMB.textColor = UIColor.black
                    }
                    
                }
            }
        }
        
        //Declare the outlet in our custom class
        let lblStaff5 = cell.viewWithTag(27) as! UILabel
        let lblStaff50 = cell.viewWithTag(28) as! UILabel
        let lblStaff250 = cell.viewWithTag(29) as! UILabel
        let lblStaff250Plus = cell.viewWithTag(30) as! UILabel
        
        let lblTotal5 = cell.viewWithTag(31) as! UILabel
        let lblTotal50 = cell.viewWithTag(32) as! UILabel
        let lblTotal250 = cell.viewWithTag(33) as! UILabel
        let lblTotal250Plus = cell.viewWithTag(34) as! UILabel
        let imgCompanyProfile = cell.viewWithTag(3) as! UIImageView

         lblStaff5.textColor = UIColor.white
         lblStaff50.textColor = UIColor.white
         lblStaff250.textColor = UIColor.white
         lblStaff250Plus.textColor = UIColor.white
        
        if companyListArr[indexPath.row].noOfStuff! > 250 {
            lblStaff250Plus.textColor = UIColor.black
        } else if companyListArr[indexPath.row].noOfStuff! < 5 {
            lblStaff5.textColor = UIColor.black
        } else if companyListArr[indexPath.row].noOfStuff! < 50 {
             lblStaff50.textColor = UIColor.black
        } else if companyListArr[indexPath.row].noOfStuff! < 250 {
            lblStaff250.textColor = UIColor.black
        }
        
         lblTotal5.textColor = UIColor.white
         lblTotal50.textColor = UIColor.white
         lblTotal250.textColor = UIColor.white
         lblTotal250Plus.textColor = UIColor.white
        
        
        if (companyListArr[indexPath.row].companySize == nil) || (companyListArr[indexPath.row].companySize! == "") || (companyListArr[indexPath.row].companySize! == "(NULL)") || (companyListArr[indexPath.row].companySize! == "<NULL>") || ((companyListArr[indexPath.row].companySize! == "<null>") || (companyListArr[indexPath.row].companySize! == "(null)") || (companyListArr[indexPath.row].companySize! == "")) {
            lblTotal250Plus.textColor = UIColor.white
            lblTotal250.textColor = UIColor.white
            lblTotal50.textColor = UIColor.white
            lblTotal5.textColor = UIColor.black
        } else {
            //print("companySize :: \(String(describing: companyList[indexPath.row].companySize))")
            let totalStaff : Int = Int(companyListArr[indexPath.row].companySize!)!
            if totalStaff > 250 {
                lblTotal250Plus.textColor = UIColor.black
            } else if totalStaff > 50 && totalStaff < 250 {
                lblTotal250.textColor = UIColor.black
            } else if totalStaff > 5 && totalStaff < 50  {
                lblTotal50.textColor = UIColor.black
            } else if totalStaff < 5 {
                lblTotal5.textColor = UIColor.black
            }
        }
        
        //imgCompanyProfile.image = UIImage(named:companyListArr[indexPath.row].logo!)
        
        if companyListArr[indexPath.row].logo!.isEmpty == false && companyListArr[indexPath.row].logo != "" {
            let escapedString = companyListArr[indexPath.row].logo?.replacingOccurrences(of: " ", with: "%20")
            Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgCompanyProfile)
        } else {
            imgCompanyProfile.image = UIImage(named: "AppIcon")
        }
        
//        if let imgUrl = URL.init(string: companyListArr[indexPath.row].logo!) {
//            Manager.shared.loadImage(with: imgUrl, into: imgCompanyProfile)
//        }

        let btnSettings = cell.viewWithTag(2) as! UIButton
        
        btnSettings.addTarget(self, action:#selector(self.BtnSettingsDidTap(_:)), for: UIControlEvents.touchUpInside)
        
        let viewBack = cell.viewWithTag(35)!
        viewBack.layer.cornerRadius = 10.0
        viewBack.clipsToBounds = true
        
        let viewBackName = cell.viewWithTag(36)!
        viewBackName.layer.cornerRadius = 10.0
        viewBackName.clipsToBounds = true
        
        let viewBackSettings = cell.viewWithTag(37)!
        viewBackSettings.layer.cornerRadius = 10.0
        viewBackSettings.clipsToBounds = true
        
        let viewBtm = cell.viewWithTag(38)!
        viewBtm.layer.cornerRadius = 7.0
        viewBtm.clipsToBounds = true
        
        return cell
    }
    
    //MARK:: UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 225
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboardCompany = UIStoryboard(name: "Company", bundle: nil)
        let companyDetailVC = storyboardCompany.instantiateViewController(withIdentifier: "CompanyDetailController") as! CompanyDetailController
        companyDetailVC.companyId = String.init(describing: companyListArr[indexPath.row].id)
        companyDetailVC.selectedOptionMenuID = 0
        self.navigationController?.pushViewController(companyDetailVC, animated: true)
     }
}

