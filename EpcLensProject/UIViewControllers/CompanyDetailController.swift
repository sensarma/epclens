//
//  CompanyDetailController.swift
//  EpcLensProject
//
//  Created by Moumita Datta on 27/05/18.
//  Copyright © 2018 Sudipta Biswas. All rights reserved.
//

import UIKit
import GoogleMaps

//class MapLocationCell: UITableViewCell {
//
//    @IBOutlet weak var mapSatellite: GMSMapView!
//    //@IBOutlet weak var mapviewObj: GMSMapView!
//
//}


class CompanyDetailController: UIViewController, CompanyDetailProtocol {
    
     private enum CompanyView:Int {
        case overview = 0
        case operation = 1
        case metrics = 2
    }
    
    @IBOutlet weak var tableCompanyDetail: UITableView!
    
    var companyDetailsDispatcher = CompanyDetailDispatcher()
    var companyId : String = ""
    var companyDetailArr = [CompanyDetailNode]()
    var companyDetailMetricsArr = [CompanyDetailMetrics]()
    var companyClienteleMetricsArr = [CompanyClientaleMetrics]()
    var companyServiceMetricsArr = [CompanyServicePortfolio]()
    
    var companyOperationArr = [CompanyOperationDetail]()
    var companyOperationProjectArr = [CompanyOperationProject]()
    var companyOperationStoryArr = [CompanyOperationStory]()
    var companyOperationResultArr = [OperationResultArray]()
    var companyOperationImageArr = [OperationResultArray]()
    var companyOperationVideoArr = [OperationResultArray]()
    
    var companyOverViewArr = [CompanyDetailOverView]()
    
    var selectedOptionMenuID : Int = -1
    
    private var companyViewType:CompanyView = CompanyView.overview
    private let locationManager = CLLocationManager()
    
    
    //Mark:: AnimatedLoader
    let loaderController = EpcLensLoaderViewController(nibName: "EpcLensLoaderViewController", bundle: nil)
    
    
    //MARK:: UIView
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //For GoogleMap
        locationManager.delegate = self as? CLLocationManagerDelegate
        locationManager.requestWhenInUseAuthorization()
        
        
        companyDetailsDispatcher.delegate = self
        
        
        var tempDict = [String:String]()
        tempDict["token"] = EpicLensConstantsVariables.accessToken
        tempDict["id"] = companyId
        companyDetailsDispatcher.GetCompanyDetail(paramDict: tempDict)
        
        
        if selectedOptionMenuID == 0 {
            var tempDict = [String:String]()
            tempDict["token"] = EpicLensConstantsVariables.accessToken
            tempDict["id"] = companyId
            
            
            companyDetailsDispatcher.GetCompanyOverview(paramDict: tempDict)
        }
        // Start Loaded
        loaderController.showEpcLoader(_sourceView: self)
        
        
    }
    
    // Set the status bar style to complement night-mode.
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    //MARK:: IBAction
    @IBAction func btnBackDidTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func BtnSettingsDidTap(_ sender: Any) {
        print(self.navigationController?.viewControllers ?? "")
        let elDrawer = self.navigationController?.parent as! KYDrawerController
        elDrawer.setDrawerState(.opened, animated: true)
    }
    
    
    
    //MARK:: CompanyDetailProtocol
    func didUpdateControllerWithCompanyDetailsData(companyDetails:CompanyDetailsModel) {
        DispatchQueue.main.async {
            if companyDetails.data.sucess {
                self.companyDetailArr = companyDetails.data.companyDetails!
                self.tableCompanyDetail.reloadData()
                // Stop Loaded
                self.loaderController.hideEpcLoader()
            }
        }
    }
    
    func didUpdateControllerWithCompanyOverviewData(companyOverview:CompanyOverViewModel) {
        DispatchQueue.main.async {
            if companyOverview.data.sucess {
                if let resultArr = companyOverview.data.companyDetails {
                    self.companyOverViewArr = resultArr
                } else {
                    self.companyOverViewArr.removeAll()
                }
                
                self.tableCompanyDetail.reloadData()
                //self.tableCompanyDetail.reloadSections(NSIndexSet(index: 2) as IndexSet, with: .automatic)
                // Stop Loaded
                self.loaderController.hideEpcLoader()
            }
        }
        /*if companyOverview.errNode.errCode == "0" {
            companyOverViewArr = companyOverview.data.companyDetails!
            tableCompanyDetail.reloadData()
            // Stop Loaded
            self.loaderController.hideEpcLoader()
        }*/
    }
    
    func didUpdateControllerWithCompanyOperationData(companyOperation:CompanyOperationModel) {
        DispatchQueue.main.async {
            if companyOperation.data.sucess {
                self.companyOperationArr = companyOperation.data.companyDetails!
                self.companyOperationProjectArr = companyOperation.data.companyprojects!
                self.companyOperationStoryArr = companyOperation.data.companyStory!
                self.companyOperationResultArr = companyOperation.data.resultArray!
                
                self.tableCompanyDetail.reloadData()
                //self.tableCompanyDetail.reloadSections(NSIndexSet(index: 1) as IndexSet, with: .automatic)
                // Stop Loaded
                self.loaderController.hideEpcLoader()
            }
        }
    }
    
    func didUpdateControllerWithCompanyMetricsData(companyMetrics:CompanyMetricsModel) {
        DispatchQueue.main.async {
            if companyMetrics.data.sucess {
                self.companyDetailMetricsArr = companyMetrics.data.companyDetails!
                self.companyClienteleMetricsArr = companyMetrics.data.companyClientale!
                self.companyServiceMetricsArr = companyMetrics.data.companyServicePortfolio!
                
                //self.tableCompanyDetail.reloadSections(NSIndexSet(index: 2) as IndexSet, with: .automatic)
                self.tableCompanyDetail.reloadData()
                // Stop Loaded
                self.loaderController.hideEpcLoader()
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}


extension CompanyDetailController : UITableViewDataSource,UITableViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, CLLocationManagerDelegate {
    //MARK:: UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else if section == 1 {
            return 1
        } else {
            if selectedOptionMenuID == 0 { //companyViewType == CompanyView.overview {
                return 3
            } else if selectedOptionMenuID == 1 {//companyViewType == CompanyView.operation {
                return companyOperationStoryArr.count+4
            } else {
                return 6
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let idstr = "CompanyDetailHeaderCell"
            var companyDetailHeaderCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
            if companyDetailHeaderCell == nil {
                companyDetailHeaderCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
            }
            
            //Declare the outlet in our custom class
            let lblCompanyName = companyDetailHeaderCell.viewWithTag(1) as! UILabel
            let lblOwnership = companyDetailHeaderCell.viewWithTag(2) as! UILabel
            let lblCompanyServiceType = companyDetailHeaderCell.viewWithTag(3) as! UILabel
            let lblOpectypeName = companyDetailHeaderCell.viewWithTag(4) as! UILabel
            let viewBackHeader = companyDetailHeaderCell.viewWithTag(100)
            let imgComanyLogo = companyDetailHeaderCell.viewWithTag(105) as! UIImageView
            let imgComanyBanner = companyDetailHeaderCell.viewWithTag(110) as! UIImageView
            
            
            viewBackHeader?.layer.cornerRadius = 10.0
            viewBackHeader?.clipsToBounds = true
            
            
            //print(companyDetailArr)
            if companyDetailArr.count > 0 {
                if (companyDetailArr[indexPath.row].name == nil) || (companyDetailArr[indexPath.row].name! == "") || (companyDetailArr[indexPath.row].name! == "(NULL)") || (companyDetailArr[indexPath.row].name! == "<NULL>") || ((companyDetailArr[indexPath.row].name! == "<null>") || (companyDetailArr[indexPath.row].name! == "(null)") || (companyDetailArr[indexPath.row].name! == "")) {
                    lblCompanyName.text = " "
                } else {
                    lblCompanyName.text = companyDetailArr[indexPath.row].name?.lowercased()
                }
                
                if (companyDetailArr[indexPath.row].ownership == nil) || (companyDetailArr[indexPath.row].ownership! == "") || (companyDetailArr[indexPath.row].ownership! == "(NULL)") || (companyDetailArr[indexPath.row].ownership! == "<NULL>") || ((companyDetailArr[indexPath.row].ownership! == "<null>") || (companyDetailArr[indexPath.row].ownership! == "(null)") || (companyDetailArr[indexPath.row].ownership! == "")) {
                    lblOwnership.text = " "
                } else {
                    lblOwnership.text = companyDetailArr[indexPath.row].ownership?.lowercased()
                }
                
                if (companyDetailArr[indexPath.row].companyServiceType == nil) || (companyDetailArr[indexPath.row].companyServiceType! == "") || (companyDetailArr[indexPath.row].companyServiceType! == "(NULL)") || (companyDetailArr[indexPath.row].companyServiceType! == "<NULL>") || ((companyDetailArr[indexPath.row].companyServiceType! == "<null>") || (companyDetailArr[indexPath.row].name! == "(null)") || (companyDetailArr[indexPath.row].companyServiceType! == "")) {
                    lblCompanyServiceType.text = " "
                } else {
                    lblCompanyServiceType.text = companyDetailArr[indexPath.row].companyServiceType?.lowercased()
                }
                
                if (companyDetailArr[indexPath.row].opectypeName == nil) || (companyDetailArr[indexPath.row].opectypeName! == "") || (companyDetailArr[indexPath.row].opectypeName! == "(NULL)") || (companyDetailArr[indexPath.row].opectypeName! == "<NULL>") || ((companyDetailArr[indexPath.row].opectypeName! == "<null>") || (companyDetailArr[indexPath.row].opectypeName! == "(null)") || (companyDetailArr[indexPath.row].opectypeName! == "")) {
                    lblOpectypeName.text = " "
                } else {
                    lblOpectypeName.text = companyDetailArr[indexPath.row].opectypeName?.lowercased()
                }
                
                
                if companyDetailArr[indexPath.item].logo?.isEmpty == false && companyDetailArr[indexPath.item].logo != "" {
                    let escapedString = companyDetailArr[indexPath.item].logo?.replacingOccurrences(of: " ", with: "%20")
                    Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgComanyLogo)
                } else {
                    imgComanyLogo.image = UIImage(named: "AppIcon")
                }
                
                if companyDetailArr[indexPath.item].companyBanner?.isEmpty == false && companyDetailArr[indexPath.item].companyBanner != "" {
                    let escapedString = companyDetailArr[indexPath.item].companyBanner?.replacingOccurrences(of: " ", with: "%20")
                    Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgComanyBanner)
                } else {
                    imgComanyBanner.image = UIImage(named: "AppIcon")
                }
                
                //Declare the outlet in our custom class
                let lblO = companyDetailHeaderCell.viewWithTag(5) as! UILabel
                let lblE = companyDetailHeaderCell.viewWithTag(6) as! UILabel
                let lblP = companyDetailHeaderCell.viewWithTag(7) as! UILabel
                let lblC = companyDetailHeaderCell.viewWithTag(8) as! UILabel
                
                if companyDetailArr[indexPath.row].projectphaseName! == "Engineering" {
                    lblO.textColor = UIColor.black
                    lblE.textColor = UIColor.black
                    lblP.textColor = UIColor.white
                    lblC.textColor = UIColor.white
                } else if companyDetailArr[indexPath.row].projectphaseName! == "Construction" {
                    lblO.textColor = UIColor.white
                    lblE.textColor = UIColor.white
                    lblP.textColor = UIColor.white
                    lblC.textColor = UIColor.black
                } else if companyDetailArr[indexPath.row].projectphaseName! == "Owner" {
                    lblO.textColor = UIColor.black
                    lblE.textColor = UIColor.white
                    lblP.textColor = UIColor.white
                    lblC.textColor = UIColor.white
                } else if companyDetailArr[indexPath.row].projectphaseName! == "Procurement" {
                    lblO.textColor = UIColor.white
                    lblE.textColor = UIColor.white
                    lblP.textColor = UIColor.black
                    lblC.textColor = UIColor.white
                }
            }
            return companyDetailHeaderCell
            
        } else if indexPath.section == 1 {
            let idstr = "CompanyDetailOptionCell"
            var companyDetailOptionCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
            if companyDetailOptionCell == nil {
                companyDetailOptionCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
            }
            
            //Declare the outlet in our custom class
            let collectionCompanyDetailOption = companyDetailOptionCell.viewWithTag(20) as! UICollectionView
            
            
            
            collectionCompanyDetailOption.dataSource = self
            collectionCompanyDetailOption.delegate = self
            collectionCompanyDetailOption.reloadData()

            return companyDetailOptionCell
        } else {
            if selectedOptionMenuID == 0 {
            //if companyViewType == CompanyView.overview {
                print("companyOverViewArr::\(companyOverViewArr)")
                if indexPath.row == 0 {
                    let idstr = "overviewcell"
                    var overviewCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
                    if overviewCell == nil {
                        overviewCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
                    }
                    
                    //Declare the outlet in our custom class
                    let lblParentCompanyName = overviewCell.viewWithTag(1) as! UILabel
                    let lblNaicsCode = overviewCell.viewWithTag(2) as! UILabel
                    let lblSubsidiaries = overviewCell.viewWithTag(3) as! UILabel
                    let lblAffiliations = overviewCell.viewWithTag(4) as! UILabel
                    let lblStartingOperations = overviewCell.viewWithTag(5) as! UILabel
                    let lblPresidentName = overviewCell.viewWithTag(6) as! UILabel
                    let lblPhoneNumber = overviewCell.viewWithTag(7) as! UILabel
                    let lblEmail = overviewCell.viewWithTag(8) as! UILabel
                    let lblHeadquaterAddress = overviewCell.viewWithTag(9) as! UILabel
                    let lblWebsiteAddress = overviewCell.viewWithTag(10) as! UILabel
                    let lblBusinessDescription = overviewCell.viewWithTag(11) as! UILabel
                    let lblMissionStatement = overviewCell.viewWithTag(12) as! UILabel
                    let imgLeftQuote = overviewCell.viewWithTag(13) as! UIImageView
                    let imgRightQuote = overviewCell.viewWithTag(14) as! UIImageView
                    
                    if companyOverViewArr.count > 0 {
                        if (companyOverViewArr[indexPath.row].parentCompany == nil) || (companyOverViewArr[indexPath.row].parentCompany! == "") || (companyOverViewArr[indexPath.row].parentCompany! == "(NULL)") || (companyOverViewArr[indexPath.row].parentCompany! == "<NULL>") || ((companyOverViewArr[indexPath.row].parentCompany! == "<null>") || (companyOverViewArr[indexPath.row].parentCompany! == "(null)") || (companyOverViewArr[indexPath.row].parentCompany! == "")) {
                            lblParentCompanyName.text = " "
                        } else {
                            lblParentCompanyName.text = companyOverViewArr[indexPath.row].parentCompany!
                        }
                        
                        if (companyOverViewArr[indexPath.row].naicsCode == nil) || (companyOverViewArr[indexPath.row].naicsCode! == "") || (companyOverViewArr[indexPath.row].naicsCode! == "(NULL)") || (companyOverViewArr[indexPath.row].naicsCode! == "<NULL>") || ((companyOverViewArr[indexPath.row].naicsCode! == "<null>") || (companyOverViewArr[indexPath.row].naicsCode! == "(null)") || (companyOverViewArr[indexPath.row].naicsCode! == "")) {
                            lblNaicsCode.text = " "
                        } else {
                            lblNaicsCode.text = companyOverViewArr[indexPath.row].naicsCode!
                        }
                        
                        if (companyOverViewArr[indexPath.row].subsidiries == nil) || (companyOverViewArr[indexPath.row].subsidiries! == "") || (companyOverViewArr[indexPath.row].subsidiries! == "(NULL)") || (companyOverViewArr[indexPath.row].subsidiries! == "<NULL>") || ((companyOverViewArr[indexPath.row].subsidiries! == "<null>") || (companyOverViewArr[indexPath.row].subsidiries! == "(null)") || (companyOverViewArr[indexPath.row].subsidiries! == "")) {
                            lblSubsidiaries.text = " "
                        } else {
                            lblSubsidiaries.text = companyOverViewArr[indexPath.row].subsidiries!
                        }
                        
                        if (companyOverViewArr[indexPath.row].affilations == nil) || (companyOverViewArr[indexPath.row].affilations! == "") || (companyOverViewArr[indexPath.row].affilations! == "(NULL)") || (companyOverViewArr[indexPath.row].affilations! == "<NULL>") || ((companyOverViewArr[indexPath.row].affilations! == "<null>") || (companyOverViewArr[indexPath.row].affilations! == "(null)") || (companyOverViewArr[indexPath.row].affilations! == "")) {
                            lblAffiliations.text = " "
                        } else {
                            lblAffiliations.text = companyOverViewArr[indexPath.row].affilations!
                        }
                        
                        if (companyOverViewArr[indexPath.row].yearOfStartingOps == nil) || (companyOverViewArr[indexPath.row].yearOfStartingOps! == "") || (companyOverViewArr[indexPath.row].yearOfStartingOps! == "(NULL)") || (companyOverViewArr[indexPath.row].yearOfStartingOps! == "<NULL>") || ((companyOverViewArr[indexPath.row].yearOfStartingOps! == "<null>") || (companyOverViewArr[indexPath.row].yearOfStartingOps! == "(null)") || (companyOverViewArr[indexPath.row].yearOfStartingOps! == "")) {
                            lblStartingOperations.text = " "
                        } else {
                            lblStartingOperations.text = companyOverViewArr[indexPath.row].yearOfStartingOps!
                        }
                        
                        if (companyOverViewArr[indexPath.row].presidentCeo == nil) || (companyOverViewArr[indexPath.row].presidentCeo! == "") || (companyOverViewArr[indexPath.row].presidentCeo! == "(NULL)") || (companyOverViewArr[indexPath.row].presidentCeo! == "<NULL>") || ((companyOverViewArr[indexPath.row].presidentCeo! == "<null>") || (companyOverViewArr[indexPath.row].presidentCeo! == "(null)") || (companyOverViewArr[indexPath.row].presidentCeo! == "")) {
                            lblPresidentName.text = " "
                        } else {
                            lblPresidentName.text = companyOverViewArr[indexPath.row].presidentCeo!
                        }
                        
                        if (companyOverViewArr[indexPath.row].phoneNo == nil) || (companyOverViewArr[indexPath.row].phoneNo! == "") || (companyOverViewArr[indexPath.row].phoneNo! == "(NULL)") || (companyOverViewArr[indexPath.row].phoneNo! == "<NULL>") || ((companyOverViewArr[indexPath.row].phoneNo! == "<null>") || (companyOverViewArr[indexPath.row].phoneNo! == "(null)") || (companyOverViewArr[indexPath.row].phoneNo! == "")) {
                            lblPhoneNumber.text = " "
                        } else {
                            lblPhoneNumber.text = companyOverViewArr[indexPath.row].countryCode! + companyOverViewArr[indexPath.row].phoneNo!
                        }
                        
                        if (companyOverViewArr[indexPath.row].email == nil) || (companyOverViewArr[indexPath.row].email! == "") || (companyOverViewArr[indexPath.row].email! == "(NULL)") || (companyOverViewArr[indexPath.row].email! == "<NULL>") || ((companyOverViewArr[indexPath.row].email! == "<null>") || (companyOverViewArr[indexPath.row].email! == "(null)") || (companyOverViewArr[indexPath.row].email! == "")) {
                            lblEmail.text = " "
                        } else {
                            lblEmail.text = companyOverViewArr[indexPath.row].email!
                        }
                        
                        if (companyOverViewArr[indexPath.row].headquarterAddress == nil) || (companyOverViewArr[indexPath.row].headquarterAddress! == "") || (companyOverViewArr[indexPath.row].headquarterAddress! == "(NULL)") || (companyOverViewArr[indexPath.row].headquarterAddress! == "<NULL>") || ((companyOverViewArr[indexPath.row].headquarterAddress! == "<null>") || (companyOverViewArr[indexPath.row].headquarterAddress! == "(null)") || (companyOverViewArr[indexPath.row].headquarterAddress! == "")) {
                            lblHeadquaterAddress.text = " "
                        } else {
                            lblHeadquaterAddress.text = companyOverViewArr[indexPath.row].headquarterAddress!
                        }
                        
                        if (companyOverViewArr[indexPath.row].website == nil) || (companyOverViewArr[indexPath.row].website! == "") || (companyOverViewArr[indexPath.row].website! == "(NULL)") || (companyOverViewArr[indexPath.row].website! == "<NULL>") || ((companyOverViewArr[indexPath.row].website! == "<null>") || (companyOverViewArr[indexPath.row].website! == "(null)") || (companyOverViewArr[indexPath.row].website! == "")) {
                            lblWebsiteAddress.text = " "
                        } else {
                            lblWebsiteAddress.text = companyOverViewArr[indexPath.row].website!
                        }
                        
                        if (companyOverViewArr[indexPath.row].businessDesc == nil) || (companyOverViewArr[indexPath.row].businessDesc! == "") || (companyOverViewArr[indexPath.row].businessDesc! == "(NULL)") || (companyOverViewArr[indexPath.row].businessDesc! == "<NULL>") || ((companyOverViewArr[indexPath.row].businessDesc! == "<null>") || (companyOverViewArr[indexPath.row].businessDesc! == "(null)") || (companyOverViewArr[indexPath.row].businessDesc! == "")) {
                            lblBusinessDescription.text = " "
                        } else {
                            lblBusinessDescription.attributedText = companyOverViewArr[indexPath.row].businessDesc!.attributedHtmlString
                        }
                        
                        if (companyOverViewArr[indexPath.row].missionStatement == nil) || (companyOverViewArr[indexPath.row].missionStatement! == "") || (companyOverViewArr[indexPath.row].missionStatement! == "(NULL)") || (companyOverViewArr[indexPath.row].missionStatement! == "<NULL>") || ((companyOverViewArr[indexPath.row].missionStatement! == "<null>") || (companyOverViewArr[indexPath.row].missionStatement! == "(null)") || (companyOverViewArr[indexPath.row].missionStatement! == "")) {
                            lblMissionStatement.text = " "
                            imgLeftQuote.isHidden = true
                            imgRightQuote.isHidden = true
                        } else {
                            lblMissionStatement.text = companyOverViewArr[indexPath.row].missionStatement!
                            imgLeftQuote.isHidden = false
                            imgRightQuote.isHidden = false
                        }
                    }
                    
                    return overviewCell
                } else if indexPath.row == 1 {
                    let idstr = "MapLocationCell"
                    //let mapLocationCell = tableCompanyDetail.dequeueReusableCell(withIdentifier: idstr) as! MapLocationCell
                    
                    var mapLocationCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)
                    if mapLocationCell == nil {
                        mapLocationCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
                    }
                    
                    //let mapSatelliteOfc = tableView.dequeueReusableCell(withIdentifier: "MapLocationCell") as! MapLocationCell
                    
                    //Declare the outlet in our custom class
                    let mapSatelliteOfc = mapLocationCell.viewWithTag(25) as! GMSMapView
                    let lblSatelliteOfc = mapLocationCell.viewWithTag(20) as! UILabel
                    
                    print("companyOverViewArr:: ",companyOverViewArr)
                    
                    lblSatelliteOfc.text = "satellite office location"
                    
                    let marker = GMSMarker()
                    
                    if companyOverViewArr.count > 0 {
                        let satoffAddresslat : String = companyOverViewArr[0].satoffAddresslat!
                        let satoffAddresslng : String = companyOverViewArr[0].satoffAddresslng!
                        
                        //let lat = Double(satoffAddresslat)//Double("13.063754")
                        //let long = Double("80.24358699999993")
                        marker.position = CLLocationCoordinate2DMake(Double(satoffAddresslat)!,Double(satoffAddresslng)!)
                    }
                    
                    ///View for Marker
                    let DynamicView = UIView(frame: CGRect(x:0, y:0, width:50, height:50))
                    DynamicView.backgroundColor = UIColor.clear
                    
                    //Pin image view for Custom Marker
                    let imageView = UIImageView()
                    imageView.frame  = CGRect(x:0, y:0, width:35, height:35)
                    imageView.image = UIImage(named:"MapPin")
                    
                    //Adding pin image to view for Custom Marker
                    DynamicView.addSubview(imageView)
                    
                    UIGraphicsBeginImageContextWithOptions(DynamicView.frame.size, false, UIScreen.main.scale)
                    DynamicView.layer.render(in: UIGraphicsGetCurrentContext()!)
                    let imageConverted: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
                    UIGraphicsEndImageContext()
                    
                    marker.icon = imageConverted
                    marker.map = mapSatelliteOfc //mapLocationCell.mapSatelliteOfc
                    
                    mapSatelliteOfc.camera = GMSCameraPosition.camera(withTarget: marker.position, zoom: 12)
                    
                    mapSatelliteOfc.mapType = .normal
                    mapSatelliteOfc.settings.zoomGestures = true
                    
                    do {
                        // Set the map style by passing the URL of the local file.
                        if let styleURL = Bundle.main.url(forResource: "style", withExtension: "json") {
                            mapSatelliteOfc.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
                        } else {
                            NSLog("Unable to find style.json")
                        }
                    } catch {
                        NSLog("One or more of the map styles failed to load. \(error)")
                    }
                    
//                    mapLocationCell.mapSatellite.delegate = self
//                    mapLocationCell.mapSatellite.mapType = .hybrid
//                    mapLocationCell.mapSatellite.isMyLocationEnabled = true
//                    mapLocationCell.mapSatellite.settings.myLocationButton = true
//
//                    let position = CLLocationCoordinate2D(latitude: 10, longitude: 10)
//                    let marker = GMSMarker(position: position)
//                    marker.title = "Hello World"
//                    marker.map = mapLocationCell.mapSatellite
                    
                    
                    
                    return mapLocationCell
                } else {
                    let idstr = "MapLocationCell"
                    
                    
                    var mapLocationCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)
                    if mapLocationCell == nil {
                        mapLocationCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
                    }
                    
                    
                    //Declare the outlet in our custom class
                    let mapOperatingRegion = mapLocationCell.viewWithTag(25) as! GMSMapView
                    let lblOperatingRegion = mapLocationCell.viewWithTag(20) as! UILabel
                    
                    print("companyOverViewArr:: ",companyOverViewArr)
                    
                    lblOperatingRegion.text = "operating regions"
                    
                    
                    
                    let marker = GMSMarker()
                    
                    if companyOverViewArr.count > 0 {
                        let operegAddresslat : String = companyOverViewArr[0].operegAddresslat!
                        let operegAddresslng : String = companyOverViewArr[0].operegAddresslng!
                        
                        marker.position = CLLocationCoordinate2DMake(Double(operegAddresslat)!,Double(operegAddresslng)!)
                    }
                    
                    ///View for Marker
                    let DynamicView = UIView(frame: CGRect(x:0, y:0, width:50, height:50))
                    DynamicView.backgroundColor = UIColor.clear
                    
                    //Pin image view for Custom Marker
                    let imageView = UIImageView()
                    imageView.frame  = CGRect(x:0, y:0, width:35, height:35)
                    imageView.image = UIImage(named:"MapPin")
                    
                    //Adding pin image to view for Custom Marker
                    DynamicView.addSubview(imageView)
                    
                    UIGraphicsBeginImageContextWithOptions(DynamicView.frame.size, false, UIScreen.main.scale)
                    DynamicView.layer.render(in: UIGraphicsGetCurrentContext()!)
                    let imageConverted: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
                    UIGraphicsEndImageContext()
                    
                    marker.icon = imageConverted
                    marker.map = mapOperatingRegion
                    
                    mapOperatingRegion.camera = GMSCameraPosition.camera(withTarget: marker.position, zoom: 11)
                    
                    mapOperatingRegion.mapType = .normal
                    
                    
                    
                    return mapLocationCell
                }
                
            } else if selectedOptionMenuID == 1 {
                print("companyOperationArr::\(companyOperationArr)")
                if indexPath.row == 0 {
                    let idstr = "OperationCell"
                    var overviewCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
                    if overviewCell == nil {
                        overviewCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
                    }
                    
                    //Declare the outlet in our custom class
                    let imgLeftQuote = overviewCell.viewWithTag(1) as! UIImageView
                    let lblMissionStatement = overviewCell.viewWithTag(2) as! UILabel
                    let imgRightQuote = overviewCell.viewWithTag(3) as! UIImageView
                    let lblCompanynSize = overviewCell.viewWithTag(4) as! UILabel
                    let lblFTEsUnit = overviewCell.viewWithTag(5) as! UILabel
                    let lblStaffUnit = overviewCell.viewWithTag(6) as! UILabel
                    let lblRegisteredProducts = overviewCell.viewWithTag(7) as! UILabel
                    
                    
                    if companyOperationArr.count > 0 {
                        if (companyOperationArr[indexPath.row].missionStatement == nil) || (companyOperationArr[indexPath.row].missionStatement! == "") || (companyOperationArr[indexPath.row].missionStatement! == "(NULL)") || (companyOperationArr[indexPath.row].missionStatement! == "<NULL>") || ((companyOperationArr[indexPath.row].missionStatement! == "<null>") || (companyOperationArr[indexPath.row].missionStatement! == "(null)") || (companyOperationArr[indexPath.row].missionStatement! == "")) {
                            lblMissionStatement.text = " "
                            imgLeftQuote.isHidden = true
                            imgRightQuote.isHidden = true
                        } else {
                            lblMissionStatement.text = companyOverViewArr[indexPath.row].missionStatement!
                            imgLeftQuote.isHidden = false
                            imgRightQuote.isHidden = false
                        }
                        
                        if (companyOperationArr[indexPath.row].companySize == nil) || (companyOperationArr[indexPath.row].companySize! == "") || (companyOperationArr[indexPath.row].companySize! == "(NULL)") || (companyOperationArr[indexPath.row].companySize! == "<NULL>") || ((companyOperationArr[indexPath.row].companySize! == "<null>") || (companyOperationArr[indexPath.row].companySize! == "(null)") || (companyOperationArr[indexPath.row].companySize! == "")) {
                            lblCompanynSize.text = " "
                        } else {
                            lblCompanynSize.text = companyOperationArr[indexPath.row].companySize!
                        }
                        
                        if (companyOperationArr[indexPath.row].ftesBusinessUnit == nil) || (companyOperationArr[indexPath.row].ftesBusinessUnit! == "") || (companyOperationArr[indexPath.row].ftesBusinessUnit! == "(NULL)") || (companyOperationArr[indexPath.row].ftesBusinessUnit! == "<NULL>") || ((companyOperationArr[indexPath.row].ftesBusinessUnit! == "<null>") || (companyOperationArr[indexPath.row].ftesBusinessUnit! == "(null)") || (companyOperationArr[indexPath.row].ftesBusinessUnit! == "")) {
                            lblFTEsUnit.text = " "
                        } else {
                            lblFTEsUnit.text = companyOperationArr[indexPath.row].ftesBusinessUnit!
                        }
                        
                        if (companyOperationArr[indexPath.row].noOfStuff == nil) {
                            lblStaffUnit.text = " "
                        } else {
                            lblStaffUnit.text = String(companyOperationArr[indexPath.row].noOfStuff!)
                        }
                        
                        if (companyOperationArr[indexPath.row].registeredLisencedProducts == nil) || (companyOperationArr[indexPath.row].registeredLisencedProducts! == "") || (companyOperationArr[indexPath.row].registeredLisencedProducts! == "(NULL)") || (companyOperationArr[indexPath.row].registeredLisencedProducts == "<NULL>") || ((companyOperationArr[indexPath.row].registeredLisencedProducts! == "<null>") || (companyOperationArr[indexPath.row].registeredLisencedProducts! == "(null)") || (companyOperationArr[indexPath.row].registeredLisencedProducts! == "")) {
                            lblRegisteredProducts.text = " "
                        } else {
                            lblRegisteredProducts.text = companyOperationArr[indexPath.row].registeredLisencedProducts!
                        }
                    }
                    
                    return overviewCell
                } else if indexPath.row == 1 {
                    let idstr = "ServiceCell"
                    var serviceCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
                    if serviceCell == nil {
                        serviceCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
                    }
                    
                    //Declare the outlet in our custom class
                    let lblCapitalProject = serviceCell.viewWithTag(110) as! UILabel
                    let collectionCapitalProject = serviceCell.viewWithTag(115) as! UICollectionView
                    
                    lblCapitalProject.text = "capital projects list:"
                    
                    collectionCapitalProject.dataSource = self
                    collectionCapitalProject.delegate = self
                    collectionCapitalProject.reloadData()
                    
                    
                    return serviceCell
                } else if indexPath.row == 2 {
                    let idstr = "KeyClienteleCell"
                    var keyClienteleCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
                    if keyClienteleCell == nil {
                        keyClienteleCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
                    }
                    
                    //Declare the outlet in our custom class
                    let lblOperationImages = keyClienteleCell.viewWithTag(90) as! UILabel
                    let collectionOperationImages = keyClienteleCell.viewWithTag(95) as! UICollectionView
                    
                    lblOperationImages.text = "images:"
                    
                    collectionOperationImages.dataSource = self
                    collectionOperationImages.delegate = self
                    collectionOperationImages.reloadData()
                    
                    return keyClienteleCell
                } else if indexPath.row == 3 {
                    let idstr = "OperationVideoCell"
                    var operationVideoCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
                    if operationVideoCell == nil {
                        operationVideoCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
                    }
                    
                    //Declare the outlet in our custom class
                    let lblOperationVideoImages = operationVideoCell.viewWithTag(80) as! UILabel
                    let collectionOperationVideo = operationVideoCell.viewWithTag(85) as! UICollectionView
                    
                    lblOperationVideoImages.text = "videos:"
                    
                    collectionOperationVideo.dataSource = self
                    collectionOperationVideo.delegate = self
                    collectionOperationVideo.reloadData()
                    
                    return operationVideoCell
                } else {
                    let idstr = "StoryCell"
                    var storyCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
                    if storyCell == nil {
                        storyCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
                    }
                    
                    //Declare the outlet in our custom class
                    let lblStoryName = storyCell.viewWithTag(61) as! UILabel
                    let lblStoryDesc = storyCell.viewWithTag(62) as! UILabel
                    
                    let i : Int = indexPath.row - 4
                    print("companyOperationStoryArr indexpath::\(i)")
                    print("companyOperationStoryArr::\(companyOperationStoryArr[i])")
                    if companyOperationStoryArr.count > 0 {
                        if (companyOperationStoryArr[i].storyTitle == nil) || (companyOperationStoryArr[i].storyTitle! == "") || (companyOperationStoryArr[i].storyTitle! == "(NULL)") || (companyOperationStoryArr[i].storyTitle! == "<NULL>") || ((companyOperationStoryArr[i].storyTitle! == "<null>") || (companyOperationStoryArr[i].storyTitle! == "(null)") || (companyOperationStoryArr[i].storyTitle! == "")) {
                            lblStoryName.text = " "
                        } else {
                            lblStoryName.text = companyOperationStoryArr[i].storyTitle
                        }
                        
                        if (companyOperationStoryArr[i].storyDesc == nil) || (companyOperationStoryArr[i].storyDesc! == "") || (companyOperationStoryArr[i].storyDesc! == "(NULL)") || (companyOperationStoryArr[i].storyDesc! == "<NULL>") || ((companyOperationStoryArr[i].storyDesc! == "<null>") || (companyOperationStoryArr[i].storyDesc! == "(null)") || (companyOperationStoryArr[i].storyDesc! == "")) {
                            lblStoryDesc.text = " "
                        } else {
                            lblStoryDesc.attributedText = companyOperationStoryArr[i].storyDesc?.attributedHtmlString
                        }
                    }
                    return storyCell
                }
                
            } else {
                if indexPath.row == 0 {
                    let idstr = "MetricsCell"
                    var metricsCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
                    if metricsCell == nil {
                        metricsCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
                    }
                    
                    //Declare the outlet in our custom class
                    let lblCapitalBudget = metricsCell.viewWithTag(1) as! UILabel
                    let lblGrossRevenue = metricsCell.viewWithTag(2) as! UILabel
                    
                    if companyDetailMetricsArr.count > 0 {
                        if (companyDetailMetricsArr[indexPath.row].annualBudget == nil) || (companyDetailMetricsArr[indexPath.row].annualBudget! == "") || (companyDetailMetricsArr[indexPath.row].annualBudget! == "(NULL)") || (companyDetailMetricsArr[indexPath.row].annualBudget! == "<NULL>") || ((companyDetailMetricsArr[indexPath.row].annualBudget! == "<null>") || (companyDetailMetricsArr[indexPath.row].annualBudget! == "(null)") || (companyDetailMetricsArr[indexPath.row].annualBudget! == "")) {
                            lblCapitalBudget.text = " "
                        } else {
                            lblCapitalBudget.text = companyDetailMetricsArr[indexPath.row].annualBudget
                        }
                        
                        if (companyDetailMetricsArr[indexPath.row].revnue == nil) || (companyDetailMetricsArr[indexPath.row].revnue! == "") || (companyDetailMetricsArr[indexPath.row].revnue! == "(NULL)") || (companyDetailMetricsArr[indexPath.row].revnue! == "<NULL>") || ((companyDetailMetricsArr[indexPath.row].revnue! == "<null>") || (companyDetailMetricsArr[indexPath.row].revnue! == "(null)") || (companyDetailMetricsArr[indexPath.row].revnue! == "")) {
                            lblGrossRevenue.text = " "
                        } else {
                            lblGrossRevenue.text = companyDetailMetricsArr[indexPath.row].revnue
                        }
                    }
                    
                    return metricsCell
                } else if indexPath.row == 1 {
                    let idstr = "KeyClienteleCell"
                    var keyClienteleCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
                    if keyClienteleCell == nil {
                        keyClienteleCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
                    }
                    
                    //Declare the outlet in our custom class
                    let lblKeyClientele = keyClienteleCell.viewWithTag(90) as! UILabel
                    let collectionKeyClientele = keyClienteleCell.viewWithTag(95) as! UICollectionView
                    
                    lblKeyClientele.text = "key clientele:"
                    
                    collectionKeyClientele.dataSource = self
                    collectionKeyClientele.delegate = self
                    collectionKeyClientele.reloadData()
                    
                    return keyClienteleCell
                } else if indexPath.row == 2 {
                    let idstr = "ImageContractCell"
                    var imageContractCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
                    if imageContractCell == nil {
                        imageContractCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
                    }
                    
                    //Declare the outlet in our custom class
                    let lblKeyClientele = imageContractCell.viewWithTag(100) as! UILabel
                    let imgContractRisk = imageContractCell.viewWithTag(105) as! UIImageView
                    
                    
                    lblKeyClientele.text = "contract risk composition:"
                    
                    if companyDetailMetricsArr.count > 0 {
                        if companyDetailMetricsArr[0].contractrisk?.isEmpty == false && companyDetailMetricsArr[0].contractrisk != "" {
                            let escapedString = companyDetailMetricsArr[0].contractrisk?.replacingOccurrences(of: " ", with: "%20")
                            Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgContractRisk)
                        } else {
                            imgContractRisk.image = UIImage(named: "AppIcon")
                        }
                    }
                    
                    return imageContractCell
                } else if indexPath.row == 3 {
                    let idstr = "ServiceCell"
                    var serviceCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
                    if serviceCell == nil {
                        serviceCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
                    }
                    
                    //Declare the outlet in our custom class
                    let lblServicePortfolio = serviceCell.viewWithTag(110) as! UILabel
                    let collectionServicePortfolio = serviceCell.viewWithTag(115) as! UICollectionView
                    
                    lblServicePortfolio.text = "services portfolio:"
                    
                    collectionServicePortfolio.dataSource = self
                    collectionServicePortfolio.delegate = self
                    collectionServicePortfolio.reloadData()
                    
                    
                    return serviceCell
                } else if indexPath.row == 4 {
                    let idstr = "StoryCell"
                    var storyCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
                    if storyCell == nil {
                        storyCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
                    }
                    
                    //Declare the outlet in our custom class
                    let lblProjectRelatedSoftware = storyCell.viewWithTag(61) as! UILabel
                    let lblProjectSoftwareName = storyCell.viewWithTag(62) as! UILabel
                    
                    lblProjectRelatedSoftware.text = "project related software:"
                    
                    if companyDetailMetricsArr.count > 0 {
                        if (companyDetailMetricsArr[0].projectSoftware == nil) || (companyDetailMetricsArr[0].projectSoftware! == "") || (companyDetailMetricsArr[0].projectSoftware! == "(NULL)") || (companyDetailMetricsArr[0].projectSoftware! == "<NULL>") || ((companyDetailMetricsArr[0].projectSoftware! == "<null>") || (companyDetailMetricsArr[0].projectSoftware! == "(null)") || (companyDetailMetricsArr[0].projectSoftware! == "")) {
                            lblProjectSoftwareName.text = " "
                        } else {
                            lblProjectSoftwareName.text = companyDetailMetricsArr[0].projectSoftware
                        }
                    }
                    return storyCell
                } else {
                    let idstr = "StoryCell"
                    var storyCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
                    if storyCell == nil {
                        storyCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
                    }
                    
                    //Declare the outlet in our custom class
                    let lblRegistationReference = storyCell.viewWithTag(61) as! UILabel
                    let lblRegistationReferenceIsNet = storyCell.viewWithTag(62) as! UILabel
                    
                    lblRegistationReference.text = "isnet registration reference:"
                    
                    if companyDetailMetricsArr.count > 0 {
                        if (companyDetailMetricsArr[0].isnetRegistrationReference == nil) || (companyDetailMetricsArr[0].isnetRegistrationReference! == "") || (companyDetailMetricsArr[0].isnetRegistrationReference! == "(NULL)") || (companyDetailMetricsArr[0].isnetRegistrationReference! == "<NULL>") || ((companyDetailMetricsArr[0].isnetRegistrationReference! == "<null>") || (companyDetailMetricsArr[0].isnetRegistrationReference! == "(null)") || (companyDetailMetricsArr[0].isnetRegistrationReference! == "")) {
                            lblRegistationReferenceIsNet.text = " "
                        } else {
                            lblRegistationReferenceIsNet.text = companyDetailMetricsArr[0].isnetRegistrationReference
                        }
                    }
                    return storyCell
                }
            }
        }
        
    }
    
    //MARK:: UITableViewDelegate 
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 230
        } else if indexPath.section == 1 {
            return 50
        } else {
            if selectedOptionMenuID == 0 {
                if indexPath.row == 0 {
                    return UITableViewAutomaticDimension
                } else {
                    return 300
                }
            } else if selectedOptionMenuID == 1 {
                if indexPath.row == 0 {
                    return UITableViewAutomaticDimension
                } else if indexPath.row == 1 {
                    let i : Int = companyOperationProjectArr.count
                    let height : CGFloat = CGFloat((30*i) + 50)
                    return height
                } else if indexPath.row == 2 {
                    return 135
                } else if indexPath.row == 3 {
                    return 150
                } else {
                    return UITableViewAutomaticDimension
                }
            } else if selectedOptionMenuID == 2 {
                if indexPath.row == 0 {
                    return 100
                } else if indexPath.row == 1 {
                    return 135
                } else if indexPath.row == 2 {
                    return 250
                } else if indexPath.row == 3 {
                    let i : Int = (companyServiceMetricsArr.count/2) + 1
                    let height : CGFloat = CGFloat((25*i) + 35)
                    return height
                } else {
                    return 55
                }
            }
            return 230.0
        }
    }
    
    
    /*// MARK: - CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard status == .authorizedWhenInUse else {
            return
        }
        
        locationManager.startUpdatingLocation()
//        MapLocationCell.mapSatellite.isMyLocationEnabled = true
//        MapLocationCell.mapSatellite.settings.myLocationButton = true
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        
//        mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
//        locationManager.stopUpdatingLocation()
//        fetchNearbyPlaces(coordinate: location.coordinate)
    }
    */
    
    // MARK: - UICollectionViewDataSource protocol
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if selectedOptionMenuID == 0 {
            return 3
        } else if selectedOptionMenuID == 1 {
            if collectionView.tag == 20 {
                return 3
            } else if collectionView.tag == 95 {
                var number : Int = 0
                for index in stride(from: 0, to: companyOperationResultArr.count, by: 1) {
                    if companyOperationResultArr[index].type == "image" {
                        number = number + 1
                        companyOperationImageArr.append(companyOperationResultArr[index])
                    }
                }
                print("image count ::\(number)")
                return number
            } else if collectionView.tag == 85 {
                var number : Int = 0
                for index in stride(from: 0, to: companyOperationResultArr.count, by: 1) {
                    if companyOperationResultArr[index].type == "video" {
                        number = number + 1
                        companyOperationVideoArr.append(companyOperationResultArr[index])
                    }
                }
                print("video count ::\(number)")
                return number
            } else {
                return companyOperationProjectArr.count
            }
        } else if selectedOptionMenuID == 2 {
            if collectionView.tag == 20 {
                return 3
            } else if collectionView.tag == 95 {
                print("KeyCleintele ::\(companyClienteleMetricsArr.count)")
                return companyClienteleMetricsArr.count
            } else {
                print("Service ::\(companyServiceMetricsArr.count)")
                return companyServiceMetricsArr.count
            }
        }
        return 3
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if selectedOptionMenuID == 0 {
            let idstr = "CompanyDetailsOptionMenuCell"
            
            // get a reference to our storyboard cell
            let optionMenuCell : UICollectionViewCell! = collectionView.dequeueReusableCell(withReuseIdentifier: idstr, for: indexPath)
            
            
            // Use the outlet in our custom class to get a reference to the UI Elements in the cell
            let imgMenuBackground = optionMenuCell.viewWithTag(21) as! UIImageView
            let lblMenuTitle = optionMenuCell.viewWithTag(22) as! UILabel
            let imgMenu = optionMenuCell.viewWithTag(23) as! UIImageView
            let imgSelectedMenuBackground = optionMenuCell.viewWithTag(24) as! UIImageView
            
            
            // Use the outlet in our custom class to get a reference to the UILabel in the cell
            if indexPath.item == 0 {
                imgMenuBackground.isHidden = true
                imgSelectedMenuBackground.image = UIImage(named: "BrownBackgroundSelected")
                imgSelectedMenuBackground.isHidden = false
                lblMenuTitle.text = "overview"
                imgMenu.image = UIImage(named: "tab_icon1")
            } else if indexPath.item == 1 {
                imgMenuBackground.image = UIImage(named: "BlueBackground")
                imgMenuBackground.isHidden = false
                imgSelectedMenuBackground.isHidden = true
                lblMenuTitle.text = "operation"
                imgMenu.image = UIImage(named: "tab_icon2")
            } else {
                imgMenuBackground.image = UIImage(named: "AshBackground")
                imgMenuBackground.isHidden = false
                imgSelectedMenuBackground.isHidden = true
                lblMenuTitle.text = "matrics"
                imgMenu.image = UIImage(named: "tab_icon3")
            }
            
            return optionMenuCell
        } else if selectedOptionMenuID == 1 {
            if collectionView.tag == 20 {
                let idstr = "CompanyDetailsOptionMenuCell"
                
                // get a reference to our storyboard cell
                let optionMenuCell : UICollectionViewCell! = collectionView.dequeueReusableCell(withReuseIdentifier: idstr, for: indexPath)
                
                
                // Use the outlet in our custom class to get a reference to the UI Elements in the cell
                let imgMenuBackground = optionMenuCell.viewWithTag(21) as! UIImageView
                let lblMenuTitle = optionMenuCell.viewWithTag(22) as! UILabel
                let imgMenu = optionMenuCell.viewWithTag(23) as! UIImageView
                let imgSelectedMenuBackground = optionMenuCell.viewWithTag(24) as! UIImageView
                
                
                // Use the outlet in our custom class to get a reference to the UILabel in the cell
                if indexPath.item == 0 {
                    imgMenuBackground.image = UIImage(named: "BrownBackground")
                    imgMenuBackground.isHidden = false
                    imgSelectedMenuBackground.isHidden = true
                    lblMenuTitle.text = "overview"
                    imgMenu.image = UIImage(named: "tab_icon1")
                } else if indexPath.item == 1 {
                    imgMenuBackground.isHidden = true
                    imgSelectedMenuBackground.image = UIImage(named: "BlueBackgroundSelected")
                    imgSelectedMenuBackground.isHidden = false
                    lblMenuTitle.text = "operation"
                    imgMenu.image = UIImage(named: "tab_icon2")
                } else {
                    imgMenuBackground.isHidden = false
                    imgMenuBackground.image = UIImage(named: "AshBackground")
                    imgSelectedMenuBackground.isHidden = true
                    lblMenuTitle.text = "matrics"
                    imgMenu.image = UIImage(named: "tab_icon3")
                }
                
                return optionMenuCell
            } else if collectionView.tag == 95 {
                let idstr = "KeyClienteleCollectionCell"
                
                // get a reference to our storyboard cell
                let keyClienteleCell : UICollectionViewCell! = collectionView.dequeueReusableCell(withReuseIdentifier: idstr, for: indexPath)
                
                // Use the outlet in our custom class to get a reference to the UI Elements in the cell
                let imgKeyClientele = keyClienteleCell.viewWithTag(25) as! UIImageView
                let imgViewOperationImage = keyClienteleCell.viewWithTag(30) as! UIImageView
                let lblOperationImageName = keyClienteleCell.viewWithTag(20) as! UILabel
                
                imgKeyClientele.isHidden = true
                imgViewOperationImage.isHidden = false
                lblOperationImageName.isHidden = false
                
                if companyOperationImageArr.count > 0 {
                    if companyOperationImageArr[indexPath.item].mediaURL?.isEmpty == false && companyOperationImageArr[indexPath.item].mediaURL != "" {
                        let escapedString = companyOperationImageArr[indexPath.item].mediaURL?.replacingOccurrences(of: " ", with: "%20")
                        Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgViewOperationImage)
                    } else {
                        imgViewOperationImage.image = UIImage(named: "AppIcon")
                    }
                    
                    if (companyOperationImageArr[indexPath.item].title == nil) || (companyOperationImageArr[indexPath.item].title! == "") || (companyOperationImageArr[indexPath.item].title! == "(NULL)") || (companyOperationImageArr[indexPath.item].title! == "<NULL>") || ((companyOperationImageArr[indexPath.item].title! == "<null>") || (companyOperationImageArr[indexPath.item].title! == "(null)") || (companyOperationImageArr[indexPath.item].title! == "")) {
                        lblOperationImageName.text = " "
                    } else {
                        lblOperationImageName.text = companyOperationImageArr[indexPath.item].title
                    }
                }
                
                
                return keyClienteleCell
            } else if collectionView.tag == 85 {
                let idstr = "OperationVideoCollectionCell"
                
                // get a reference to our storyboard cell
                let operationVideoCollectionCell : UICollectionViewCell! = collectionView.dequeueReusableCell(withReuseIdentifier: idstr, for: indexPath)
                
                // Use the outlet in our custom class to get a reference to the UI Elements in the cell
                let lblOperationVideoName = operationVideoCollectionCell.viewWithTag(20) as! UILabel
                let imgViewOperationVideo = operationVideoCollectionCell.viewWithTag(10) as! UIImageView
                
                if companyOperationVideoArr.count > 0 {
                    if companyOperationVideoArr[indexPath.item].thumbnailShort?.isEmpty == false && companyOperationVideoArr[indexPath.item].thumbnailShort != "" {
                        let escapedString = companyOperationVideoArr[indexPath.item].thumbnailShort?.replacingOccurrences(of: " ", with: "%20")
                        Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgViewOperationVideo)
                    } else {
                        imgViewOperationVideo.image = UIImage(named: "AppIcon")
                    }
                    
                    if (companyOperationVideoArr[indexPath.item].title == nil) || (companyOperationVideoArr[indexPath.item].title! == "") || (companyOperationVideoArr[indexPath.item].title! == "(NULL)") || (companyOperationVideoArr[indexPath.item].title! == "<NULL>") || ((companyOperationVideoArr[indexPath.item].title! == "<null>") || (companyOperationVideoArr[indexPath.item].title! == "(null)") || (companyOperationVideoArr[indexPath.item].title! == "")) {
                        lblOperationVideoName.text = " "
                    } else {
                        lblOperationVideoName.text = companyOperationVideoArr[indexPath.item].title
                    }
                }
                
                return operationVideoCollectionCell
            } else {
                let idstr = "ServiceCollectionCell"
                
                // get a reference to our storyboard cell
                let serviceCollectionCell : UICollectionViewCell! = collectionView.dequeueReusableCell(withReuseIdentifier: idstr, for: indexPath)
                
                // Use the outlet in our custom class to get a reference to the UI Elements in the cell
                let lblOperationProjectName = serviceCollectionCell.viewWithTag(10) as! UILabel
                let imgViewService = serviceCollectionCell.viewWithTag(5) as! UIImageView
                
                imgViewService.layer.cornerRadius = 4.0
                imgViewService.clipsToBounds = true
                
                print(companyOperationProjectArr)
                if companyOperationProjectArr.count > 0 {
                    if (companyOperationProjectArr[indexPath.item].name == nil) || (companyOperationProjectArr[indexPath.item].name! == "") || (companyOperationProjectArr[indexPath.item].name! == "(NULL)") || (companyOperationProjectArr[indexPath.item].name! == "<NULL>") || ((companyOperationProjectArr[indexPath.item].name! == "<null>") || (companyOperationProjectArr[indexPath.item].name! == "(null)") || (companyOperationProjectArr[indexPath.item].name! == "")) {
                        lblOperationProjectName.text = " "
                    } else {
                        lblOperationProjectName.text = companyOperationProjectArr[indexPath.item].name
                    }
                    
                    if companyOperationProjectArr[indexPath.item].projectStatus == "execution" {
                        imgViewService.backgroundColor = UIColor(red: CGFloat((0.0 / 255.0)), green: CGFloat((230.0 / 255.0)), blue: CGFloat((118.0 / 255.0)), alpha: 1.0)
                    } else if companyOperationProjectArr[indexPath.item].projectStatus == "plan" {
                        imgViewService.backgroundColor = UIColor.white
                    } else if companyOperationProjectArr[indexPath.item].projectStatus == "stopped" || companyOperationProjectArr[indexPath.item].projectStatus == "completed" {
                        imgViewService.backgroundColor = UIColor(red: CGFloat((247.0 / 255.0)), green: CGFloat((10.0 / 255.0)), blue: CGFloat((18.0 / 255.0)), alpha: 1.0)
                    } else {
                        imgViewService.backgroundColor = UIColor(red: CGFloat((242.0 / 255.0)), green: CGFloat((185.0 / 255.0)), blue: CGFloat((92.0 / 255.0)), alpha: 1.0)
                    }
                }
                
                return serviceCollectionCell
            }
        } else {
            if collectionView.tag == 20 {
                let idstr = "CompanyDetailsOptionMenuCell"
                
                // get a reference to our storyboard cell
                let optionMenuCell : UICollectionViewCell! = collectionView.dequeueReusableCell(withReuseIdentifier: idstr, for: indexPath)
                
                
                // Use the outlet in our custom class to get a reference to the UI Elements in the cell
                let imgMenuBackground = optionMenuCell.viewWithTag(21) as! UIImageView
                let lblMenuTitle = optionMenuCell.viewWithTag(22) as! UILabel
                let imgMenu = optionMenuCell.viewWithTag(23) as! UIImageView
                let imgSelectedMenuBackground = optionMenuCell.viewWithTag(24) as! UIImageView
                
                
                // Use the outlet in our custom class to get a reference to the UILabel in the cell
                if indexPath.item == 0 {
                    imgMenuBackground.image = UIImage(named: "BrownBackground")
                    imgMenuBackground.isHidden = false
                    imgSelectedMenuBackground.isHidden = true
                    lblMenuTitle.text = "overview"
                    imgMenu.image = UIImage(named: "tab_icon1")
                } else if indexPath.item == 1 {
                    imgMenuBackground.image = UIImage(named: "BlueBackground")
                    imgMenuBackground.isHidden = false
                    imgSelectedMenuBackground.isHidden = true
                    lblMenuTitle.text = "operation"
                    imgMenu.image = UIImage(named: "tab_icon2")
                } else {
                    imgMenuBackground.isHidden = true
                    imgSelectedMenuBackground.image = UIImage(named: "AshBackgroundSelected")
                    imgSelectedMenuBackground.isHidden = false
                    lblMenuTitle.text = "matrics"
                    imgMenu.image = UIImage(named: "tab_icon3")
                }
                
                return optionMenuCell
            } else if collectionView.tag == 95 {
                let idstr = "KeyClienteleCollectionCell"
                
                // get a reference to our storyboard cell
                let keyClienteleCell : UICollectionViewCell! = collectionView.dequeueReusableCell(withReuseIdentifier: idstr, for: indexPath)
                
                // Use the outlet in our custom class to get a reference to the UI Elements in the cell
                let imgKeyClientele = keyClienteleCell.viewWithTag(25) as! UIImageView
                let imgViewOperationImage = keyClienteleCell.viewWithTag(30) as! UIImageView
                let lblOperationImageName = keyClienteleCell.viewWithTag(20) as! UILabel
                
                imgKeyClientele.isHidden = false
                imgViewOperationImage.isHidden = true
                lblOperationImageName.isHidden = true
                
                
                if companyClienteleMetricsArr.count > 0 {
                    print(companyClienteleMetricsArr)
                    if companyClienteleMetricsArr[indexPath.item].image?.isEmpty == false && companyClienteleMetricsArr[indexPath.item].image != "" {
                        let escapedString = companyClienteleMetricsArr[indexPath.row].image?.replacingOccurrences(of: " ", with: "%20")
                        Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgKeyClientele)
                    } else {
                        imgKeyClientele.image = UIImage(named: "AppIcon")
                    }
                }
                
                
                return keyClienteleCell
            } else {
                let idstr = "ServiceCollectionCell"
                
                // get a reference to our storyboard cell
                let serviceCollectionCell : UICollectionViewCell! = collectionView.dequeueReusableCell(withReuseIdentifier: idstr, for: indexPath)
                
                // Use the outlet in our custom class to get a reference to the UI Elements in the cell
                let lblService = serviceCollectionCell.viewWithTag(10) as! UILabel
                let imgViewService = serviceCollectionCell.viewWithTag(5) as! UIImageView
                
                imgViewService.layer.cornerRadius = 4.0
                imgViewService.clipsToBounds = true
                imgViewService.backgroundColor = UIColor(red: CGFloat((242.0 / 255.0)), green: CGFloat((185.0 / 255.0)), blue: CGFloat((92.0 / 255.0)), alpha: 1.0)
                
                if companyServiceMetricsArr.count > 0 {
                    if (companyServiceMetricsArr[indexPath.item].serviceText == nil) || (companyServiceMetricsArr[indexPath.item].serviceText! == "") || (companyServiceMetricsArr[indexPath.item].serviceText! == "(NULL)") || (companyServiceMetricsArr[indexPath.item].serviceText! == "<NULL>") || ((companyServiceMetricsArr[indexPath.item].serviceText! == "<null>") || (companyServiceMetricsArr[indexPath.item].serviceText! == "(null)") || (companyServiceMetricsArr[indexPath.item].serviceText! == "")) {
                        lblService.text = " "
                    } else {
                        lblService.text = companyServiceMetricsArr[indexPath.item].serviceText
                    }
                }
                
                return serviceCollectionCell
            }
        }
    }
    
    
    // MARK:: UICollectionViewDelegate protocol
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if selectedOptionMenuID == 0 {
            let cellWidth = (self.view.frame.size.width-60)/3
            return CGSize(width: cellWidth, height: 40)
        } else if selectedOptionMenuID == 1 {
            if collectionView.tag == 20 {
                let cellWidth = (self.view.frame.size.width-60)/3
                return CGSize(width: cellWidth, height: 40)
            } else if collectionView.tag == 95 {
                return CGSize(width: 90, height: 90)
            } else if collectionView.tag == 85 {
                return CGSize(width: 95, height: 95)
            } else {
                let cellWidth = (self.view.frame.size.width-50)
                return CGSize(width: cellWidth, height: 25)
            }
        } else {
            if collectionView.tag == 20 {
                let cellWidth = (self.view.frame.size.width-60)/3
                return CGSize(width: cellWidth, height: 40)
            } else if collectionView.tag == 95 {
                return CGSize(width: 90, height: 90)
            } else {
                let cellWidth = (self.view.frame.size.width-60)/2
                return CGSize(width: cellWidth, height: 20)
            }
        }
    }
    
    /*func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView.tag == 95 {
            return 5.0
        }
        return 10.0
    }*/
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 20 {
            if indexPath.row == 0 {
                selectedOptionMenuID = 0
                
                var tempDict = [String:String]()
                tempDict["token"] = EpicLensConstantsVariables.accessToken
                tempDict["id"] = companyId
                companyDetailsDispatcher.GetCompanyOverview(paramDict: tempDict)
                
                // Start Loaded
                loaderController.showEpcLoader(_sourceView: self)
                tableCompanyDetail.reloadData()
            } else if indexPath.row == 1 {
                selectedOptionMenuID = 1
                
                var tempDict = [String:String]()
                tempDict["token"] = EpicLensConstantsVariables.accessToken
                tempDict["id"] = companyId
                companyDetailsDispatcher.GetCompanyOperation(paramDict: tempDict)
                
                // Start Loaded
                loaderController.showEpcLoader(_sourceView: self)
                tableCompanyDetail.reloadData()
            } else if indexPath.row == 2 {
                selectedOptionMenuID = 2
                
                var tempDict = [String:String]()
                tempDict["token"] = EpicLensConstantsVariables.accessToken
                tempDict["id"] = companyId
                companyDetailsDispatcher.GetCompanyMetrics(paramDict: tempDict)
                
                // Start Loaded
                loaderController.showEpcLoader(_sourceView: self)
                tableCompanyDetail.reloadData()
            }
            
            print("selectedOptionMenuID :: \(selectedOptionMenuID)")
            collectionView.reloadData()
        } else if collectionView.tag == 85 {
            if selectedOptionMenuID == 1 {
                //let selectedVideo = self.companyOperationVideoArr[indexPath.item]
                let storyboardPeople = UIStoryboard(name: "People", bundle: nil)
                let videoVC = storyboardPeople.instantiateViewController(withIdentifier: "VideoViewController") as! VideoViewController
                
                var videoURLStr : String = ""
                if (self.companyOperationVideoArr[indexPath.item].thumbnailURL == nil) || (self.companyOperationVideoArr[indexPath.item].thumbnailURL! == "") || (self.companyOperationVideoArr[indexPath.item].thumbnailURL! == "(NULL)") || (self.companyOperationVideoArr[indexPath.item].thumbnailURL! == "<NULL>") || ((self.companyOperationVideoArr[indexPath.item].thumbnailURL! == "<null>") || (self.companyOperationVideoArr[indexPath.item].thumbnailURL! == "(null)") || (self.companyOperationVideoArr[indexPath.item].thumbnailURL! == "")) {
                    videoURLStr = self.companyOperationVideoArr[indexPath.item].mediaURL!
                } else {
                    videoURLStr = self.companyOperationVideoArr[indexPath.item].thumbnailURL!
                }
                
                videoVC.videoURLStr = videoURLStr
                self.navigationController?.pushViewController(videoVC, animated: true)
            }
        } else if collectionView.tag == 115 {
            if selectedOptionMenuID == 1 {
                let storyboardProject = UIStoryboard(name: "Project", bundle: nil)
                let projectVC = storyboardProject.instantiateViewController(withIdentifier: "ProjectDetailVC") as! ProjectDetailController
                projectVC.projectID = self.companyOperationProjectArr[indexPath.item].id!
                self.navigationController?.pushViewController(projectVC, animated: true)
            }
        }
    }
}



// MARK: - GMSMapViewDelegate
/*extension CompanyDetailController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        reverseGeocodeCoordinate(position.target)
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        addressLabel.lock()
        
        if (gesture) {
            mapCenterPinImage.fadeIn(0.25)
            mapView.selectedMarker = nil
        }
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoContents marker: GMSMarker) -> UIView? {
        guard let placeMarker = marker as? PlaceMarker else {
            return nil
        }
        guard let infoView = UIView.viewFromNibName("MarkerInfoView") as? MarkerInfoView else {
            return nil
        }
        
        infoView.nameLabel.text = placeMarker.place.name
        if let photo = placeMarker.place.photo {
            infoView.placePhoto.image = photo
        } else {
            infoView.placePhoto.image = UIImage(named: "generic")
        }
        
        return infoView
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        mapCenterPinImage.fadeOut(0.25)
        return false
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        mapCenterPinImage.fadeIn(0.25)
        mapView.selectedMarker = nil
        return false
    }
}*/
