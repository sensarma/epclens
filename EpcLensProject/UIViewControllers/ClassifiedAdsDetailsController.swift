//
//  ClassifiedAdsDetailsController.swift
//  EpcLensProject
//
//  Created by Moumita Datta on 04/07/18.
//  Copyright © 2018 Moumita Datta. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation


class ClassifiedAdsDetailsController: UIViewController, ClassifiedAdsDetailsProtocol {
    
    @IBOutlet weak var tableClassifiedAdsDetails: UITableView!
    @IBOutlet var constraintClassifiedAdsDetailsTableBottom: NSLayoutConstraint!
    
    var classifiedDetailsDispatcher = MarketPlaceClassifiedAdsDetailsDispatcher()
    var classifiedDetailsArr = [ClassifiedDetails]()
    var classifiedGalleryArr = [DefaultGallery]()
    var isSavedClassified: Int = 0
    
    
    var classifiedAdsId:Int = 0
    var isGalleryImgCalled : Bool = false
    var selectedAdsImgIndex : Int = -1
    let isFeatured : Bool = false
    
    //Mark:: AnimatedLoader
    let loaderController = EpcLensLoaderViewController(nibName: "EpcLensLoaderViewController", bundle: nil)
    
    
    
    //MARK:: UIView
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        classifiedDetailsDispatcher.delegate = self
        var tempDict = [String:String]()
        tempDict["token"] = EpicLensConstantsVariables.accessToken
        tempDict["id"] = String(classifiedAdsId)
        classifiedDetailsDispatcher.GetMarketPlaceClassifiedAdsDetails(paramDict: tempDict)
        
        
        // Start Loaded
        loaderController.showEpcLoader(_sourceView: self)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:: IBAction
    @IBAction func btnBackDidTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:: ClassifiedAdsDetailsProtocol
    func didUpdateControllerWithObjMarketPlaceClassifiedAdsDetails(classifiedAdsDetails: MarketPlaceClassifiedAdsDetailsModel) {
        DispatchQueue.main.async {
            if classifiedAdsDetails.data.sucess {
                self.classifiedDetailsArr = classifiedAdsDetails.data.classifieddetails!
                self.classifiedGalleryArr = classifiedAdsDetails.data.defaultGalleries!
                self.isSavedClassified = classifiedAdsDetails.data.isSavedClassified as Int
                
                self.tableClassifiedAdsDetails.delegate = self
                self.tableClassifiedAdsDetails.dataSource = self
                self.tableClassifiedAdsDetails.reloadData()
                // Stop Loaded
                self.loaderController.hideEpcLoader()
            }
        }
    }
    
}



extension ClassifiedAdsDetailsController : UITableViewDataSource, UITableViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    //MARK:: UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else if section == 1 {
            return 1
        } else if section == 2 {
            return 1
        } else if section == 3 {
            return 1
        } else if section == 4 {
            return 3
        } else if section == 5 {
            return 1
        } else if section == 6 {
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.section == 0) {
            let idstr = "ClassifiedAdsTitleTableCell"
            
            // get a reference to our storyboard cell
            var classifiedAdsTitleCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
            if classifiedAdsTitleCell == nil {
                classifiedAdsTitleCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
            }
            
            
            //Declare the outlet in our custom class
            let viewClassifiedAds = classifiedAdsTitleCell.viewWithTag(10)
            let viewTitleClassifiedAds = classifiedAdsTitleCell.viewWithTag(11)
            let viewClassifiedAdsIsFeature = classifiedAdsTitleCell.viewWithTag(100)
            let lblClassifiedAdsIsFeature = classifiedAdsTitleCell.viewWithTag(105) as! UILabel
            let imgViewClassifiedAds = classifiedAdsTitleCell.viewWithTag(15) as! UIImageView
            let lblClassifiedTitle = classifiedAdsTitleCell.viewWithTag(12) as! UILabel
            let lblClassifiedCategory = classifiedAdsTitleCell.viewWithTag(13) as! UILabel
            let lblClassifiedLocation = classifiedAdsTitleCell.viewWithTag(14) as! UILabel
            let lblClassifiedShortDesc = classifiedAdsTitleCell.viewWithTag(16) as! UILabel
            let lblIsSavedClassified = classifiedAdsTitleCell.viewWithTag(17) as! UILabel
            let viewIsSavedClassified = classifiedAdsTitleCell.viewWithTag(18)
            
            //print("classifiedDetailsArr :: \(classifiedDetailsArr)")
            
            viewClassifiedAds?.layer.cornerRadius = 10
            viewClassifiedAds?.clipsToBounds = true
            viewTitleClassifiedAds?.layer.cornerRadius = 10
            viewTitleClassifiedAds?.clipsToBounds = true
            viewClassifiedAdsIsFeature?.layer.cornerRadius = 5.0
            viewClassifiedAdsIsFeature?.clipsToBounds = true
            
            
            if classifiedDetailsArr.count > 0 {
                if (classifiedDetailsArr[0].title?.isEmpty)! {
                    lblClassifiedTitle.text = " "
                } else {
                    lblClassifiedTitle.text = classifiedDetailsArr[indexPath.row].title!
                }
                
                let isFeatured : String = classifiedDetailsArr[indexPath.row].isFeatured!
                
                print("isFeatured :: \(isFeatured)")
                
                if isFeatured == "yes" {
                    viewClassifiedAdsIsFeature?.isHidden = false
                    lblClassifiedAdsIsFeature.text = "Featured"
                } else {
                    viewClassifiedAdsIsFeature?.isHidden = true
                }
                
                imgViewClassifiedAds.layer.cornerRadius = 5.0
                imgViewClassifiedAds.clipsToBounds = true
                
                if classifiedDetailsArr[indexPath.row].mediaURL?.isEmpty == false && classifiedDetailsArr[0].mediaURL != "" {
                    let escapedString = classifiedDetailsArr[0].mediaURL?.replacingOccurrences(of: " ", with: "%20")
                    Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgViewClassifiedAds)
                } else {
                    imgViewClassifiedAds.image = UIImage(named: "AppIcon")
                }
                
                if (classifiedDetailsArr[0].optionLabel?.isEmpty)! {
                    lblClassifiedCategory.text = " "
                } else {
                    lblClassifiedCategory.text = "categories : " + classifiedDetailsArr[0].optionLabel!
                }
                
                if (classifiedDetailsArr[0].location?.isEmpty)! {
                    lblClassifiedLocation.text = " "
                } else {
                    lblClassifiedLocation.text = classifiedDetailsArr[0].location!
                }
                
                if (classifiedDetailsArr[0].shortDesc?.isEmpty)! {
                    lblClassifiedShortDesc.text = " "
                } else {
                    lblClassifiedShortDesc.text = classifiedDetailsArr[0].shortDesc!
                }
                
                if isSavedClassified == 0 {
                    lblIsSavedClassified.text = "save this classified"
                    viewIsSavedClassified?.backgroundColor = UIColor.black
                } else {
                    lblIsSavedClassified.text = "saved classified"
                    viewIsSavedClassified?.backgroundColor = UIColor(red: CGFloat((255.0 / 255.0)), green: CGFloat((165.0 / 255.0)), blue: CGFloat((3.0 / 255.0)), alpha: 1.0)
                }
            }
            
            classifiedAdsTitleCell.selectionStyle = UITableViewCellSelectionStyle.none
            return classifiedAdsTitleCell
        } else if (indexPath.section == 1) {
            let idstr = "ClassifiedAdvertisementsGalleryTableCell"
            
            // get a reference to our storyboard cell
            var classifiedAdsGalleryCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
            if classifiedAdsGalleryCell == nil {
                classifiedAdsGalleryCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
            }
            
            //Declare the outlet in our custom class
            let imgViewClassifiedAdsGallery = classifiedAdsGalleryCell.viewWithTag(22) as! UIImageView
            let collectionClassifiedAdsGallery = classifiedAdsGalleryCell.viewWithTag(25) as! UICollectionView
            
            
            classifiedAdsGalleryCell.viewWithTag(21)?.layer.cornerRadius = 10.0
            classifiedAdsGalleryCell.viewWithTag(21)?.clipsToBounds = true
            
            if isGalleryImgCalled == true {
                if classifiedGalleryArr[selectedAdsImgIndex].galleryType == "image" {
                    if classifiedGalleryArr[selectedAdsImgIndex].galleryThumbnail?.isEmpty == false && classifiedGalleryArr[selectedAdsImgIndex].galleryThumbnail != "" {
                        let escapedString = classifiedGalleryArr[selectedAdsImgIndex].galleryThumbnail?.replacingOccurrences(of: " ", with: "%20")
                        Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgViewClassifiedAdsGallery)
                    } else {
                        imgViewClassifiedAdsGallery.image = UIImage(named: "AppIcon")
                    }
                } else {
                    let videoURL = URL(string: classifiedGalleryArr[selectedAdsImgIndex].youtubeIframe!)
                    if videoURL != nil {
                        let player = AVPlayer(url: videoURL!)
                        let playerLayer = AVPlayerLayer(player: player)
                        playerLayer.frame = self.view.bounds
                        self.view.layer.addSublayer(playerLayer)
                        player.play()
                    } else {
                        EpcLensController.sharedInstance.showAlert(_sourceController: self, _msg: EpcLensMessages.InVaild_VideoUrl)
                    }
                }
            }
            
            
            //selectedID = 1
            
            collectionClassifiedAdsGallery.dataSource = self
            collectionClassifiedAdsGallery.delegate = self
            collectionClassifiedAdsGallery.reloadData()
            
            
            
            classifiedAdsGalleryCell.selectionStyle = UITableViewCellSelectionStyle.none
            return classifiedAdsGalleryCell
        } else if (indexPath.section == 2) {
            let idstr = "ClassifiedAdsDescriptionTableCell"
            
            // get a reference to our storyboard cell
            var classifiedAdsDescriptionCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
            if classifiedAdsDescriptionCell == nil {
                classifiedAdsDescriptionCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
            }
            
            //Declare the outlet in our custom class
            //let viewClassifiedAdsBtnTop = classifiedAdsBtnCell.viewWithTag(70)
            let lblClassifiedAdsDescription = classifiedAdsDescriptionCell.viewWithTag(32) as! UILabel
            if classifiedDetailsArr.count > 0 {
                if (classifiedDetailsArr[0].description?.isEmpty)! {
                    lblClassifiedAdsDescription.text = " "
                } else {
                    lblClassifiedAdsDescription.attributedText = classifiedDetailsArr[0].description!.attributedHtmlString
                }
            }
            
            classifiedAdsDescriptionCell.selectionStyle = UITableViewCellSelectionStyle.none
            return classifiedAdsDescriptionCell
        } else if (indexPath.section == 3) {
            let idstr = "ClassifiedAdsContactTableCell"
            
            // get a reference to our storyboard cell
            var classifiedAdsContactCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
            if classifiedAdsContactCell == nil {
                classifiedAdsContactCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
            }
            
            //Declare the outlet in our custom class
            let lblClassifiedAdsContactNo = classifiedAdsContactCell.viewWithTag(41) as! UILabel
            let lblClassifiedAdsEmailId = classifiedAdsContactCell.viewWithTag(42) as! UILabel
            let lblClassifiedAdsFax = classifiedAdsContactCell.viewWithTag(43) as! UILabel
            if classifiedDetailsArr.count > 0 {
                if (classifiedDetailsArr[0].contactNo?.isEmpty)! {
                    lblClassifiedAdsContactNo.text = " "
                } else {
                    lblClassifiedAdsContactNo.text = classifiedDetailsArr[0].countryCode! + " " + classifiedDetailsArr[0].contactNo! + " /" + classifiedDetailsArr[0].countryCode1!
                }
                
                if (classifiedDetailsArr[0].emailID?.isEmpty)! {
                    lblClassifiedAdsEmailId.text = " "
                } else {
                    lblClassifiedAdsEmailId.text = classifiedDetailsArr[0].emailID!
                }
                
                if (classifiedDetailsArr[0].fax?.isEmpty)! {
                    lblClassifiedAdsFax.text = " "
                } else {
                    lblClassifiedAdsFax.text = classifiedDetailsArr[0].fax!
                }
            }
            
            classifiedAdsContactCell.selectionStyle = UITableViewCellSelectionStyle.none
            return classifiedAdsContactCell
        } else if (indexPath.section == 4) {
            let idstr = "ClassifiedAdsInfoTableCell"
            
            // get a reference to our storyboard cell
            var classifiedAdsInfoCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
            if classifiedAdsInfoCell == nil {
                classifiedAdsInfoCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
            }
            
            //Declare the outlet in our custom class
            let imgviewClassifiedAdsContactInfo = classifiedAdsInfoCell.viewWithTag(52) as! UIImageView
            let txtFieldClassifiedAdsContactInfo = classifiedAdsInfoCell.viewWithTag(53) as! UITextField
            
            
            classifiedAdsInfoCell.viewWithTag(51)?.layer.cornerRadius = 10.0
            classifiedAdsInfoCell.viewWithTag(51)?.clipsToBounds = true
            classifiedAdsInfoCell.viewWithTag(51)?.layer.borderColor = UIColor.black.cgColor
            classifiedAdsInfoCell.viewWithTag(51)?.layer.borderWidth = 0.5
            
            
            if indexPath.row == 0 {
                imgviewClassifiedAdsContactInfo.image = UIImage(named: "MarketPlace_Person")
                txtFieldClassifiedAdsContactInfo.placeholder = "your name"
            } else if indexPath.row == 1 {
                imgviewClassifiedAdsContactInfo.image = UIImage(named: "MarketPlace_Phone")
                txtFieldClassifiedAdsContactInfo.placeholder = "phone number"
            } else if indexPath.row == 2 {
                imgviewClassifiedAdsContactInfo.image = UIImage(named: "MarketPlace_Write_Message")
                txtFieldClassifiedAdsContactInfo.placeholder = "message"
            }
            
            
            
            classifiedAdsInfoCell.selectionStyle = UITableViewCellSelectionStyle.none
            
            return classifiedAdsInfoCell
        } else if (indexPath.section == 5) {
            let idstr = "ClassifiedAdsSaveCopyTableCell"
            
            // get a reference to our storyboard cell
            var classifiedAdsSaveCopyCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
            if classifiedAdsSaveCopyCell == nil {
                classifiedAdsSaveCopyCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
            }
            
            /*//Declare the outlet in our custom class
            //let viewClassifiedAdsBtnTop = classifiedAdsBtnCell.viewWithTag(70)
            let lblClassifiedAdsDescription = classifiedAdsInfoCell.viewWithTag(32) as! UILabel
            if classifiedDetailsArr.count > 0 {
                if (classifiedDetailsArr[0].description?.isEmpty)! {
                    lblClassifiedAdsDescription.text = " "
                } else {
                    lblClassifiedAdsDescription.attributedText = classifiedDetailsArr[0].description!.htmlToAttributedString
                }
            }*/
            
            
            return classifiedAdsSaveCopyCell
        } else {
            let idstr = "ClassifiedAdsSendRequestBtnTableCell"
            
            // get a reference to our storyboard cell
            var classifiedAdsBtnCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
            if classifiedAdsBtnCell == nil {
                classifiedAdsBtnCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
            }
            
            //Declare the outlet in our custom class
            //let viewClassifiedAdsBtnTop = classifiedAdsBtnCell.viewWithTag(70)
            let viewClassifiedAdsBtn = classifiedAdsBtnCell.viewWithTag(75)
            let btnclassifiedAdsSendRequest = classifiedAdsBtnCell.viewWithTag(71) as! UIButton
            
            
            viewClassifiedAdsBtn?.layer.cornerRadius = 10
            viewClassifiedAdsBtn?.clipsToBounds = true
            
            btnclassifiedAdsSendRequest.layer.cornerRadius = 10.0
            
            //classifiedAdsBtnCell.selectionStyle = UITableViewCellSelectionStyle.none
            return classifiedAdsBtnCell
        }
    }
    
    
    //MARK:: UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.section == 0) {
            return 175
        } else if (indexPath.section == 1) {
            if classifiedGalleryArr.count > 0 {
                return 300
            } else {
                return 210
            }
        } else if (indexPath.section == 2) {
            return UITableViewAutomaticDimension
        } else if (indexPath.section == 3) {
            return 115
        } else if (indexPath.section == 4) {
            return 45
        } else if (indexPath.section == 5) {
            return 45
        } else {
            return 75
        }
    }
    
    
    
    
    // MARK:: UICollectionViewDataSource protocol
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //print("classifiedGalleryArr.count ::\(classifiedGalleryArr.count)")
        return classifiedGalleryArr.count
    }
    
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let idstr = "ClassifiedAdsGalleryCell"
        
        // get a reference to our storyboard cell
        let classifiedAdsGalleryCell : UICollectionViewCell! = collectionView.dequeueReusableCell(withReuseIdentifier: idstr, for: indexPath)
        
        // Use the outlet in our custom class to get a reference to the UI Elements in the cell
        let imgViewClassifiedAdsGallery = classifiedAdsGalleryCell.viewWithTag(3120) as! UIImageView
        let imgViewVideoIcon = classifiedAdsGalleryCell.viewWithTag(3125) as! UIImageView
        
        
        
        if classifiedGalleryArr[indexPath.item].galleryType == "image" {
            imgViewVideoIcon.isHidden = true
            if classifiedGalleryArr[indexPath.item].galleryThumbnail?.isEmpty == false && classifiedGalleryArr[indexPath.item].galleryThumbnail != "" {
                let escapedString = classifiedGalleryArr[indexPath.row].galleryThumbnail?.replacingOccurrences(of: " ", with: "%20")
                Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgViewClassifiedAdsGallery)
            } else {
                imgViewClassifiedAdsGallery.image = UIImage(named: "AppIcon")
            }
        } else {
            imgViewVideoIcon.isHidden = false
        }
        
        
        return classifiedAdsGalleryCell
    }
    
    
    
    // MARK:: UICollectionViewDelegate protocol
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 90)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        isGalleryImgCalled = true
        selectedAdsImgIndex = indexPath.item
        //print("selectedAdsImgIndex :: \(selectedAdsImgIndex)")
        tableClassifiedAdsDetails.reloadData()
    }
    
}

