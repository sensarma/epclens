//
//  LexiconListController.swift
//  EpcLensProject
//
//  Created by Moumita Datta on 14/05/18.
//  Copyright © 2018 Moumita Datta. All rights reserved.
//

import UIKit

class LexiconListController: UIViewController, LexiconListProtocol {
    
    @IBOutlet weak var tableLexicon: UITableView!
    
    var lexiconDispatcher = LexiconDispatcher()
    var lexiconsMenuListArr = [LexiconsMenuList]()
    var lexiconMenuResultArr = [LexiconMenuResult]()
    var lexiconMenuHelpusArr = [LexiconMenuHelpus]()
    
    //var objSelectedItem:LexiconsMenuList? = nil
    
    
    var selectedID : Int = -1
    private var isFirstTimeLoadMenuDetails = false
    
    
    //Mark:: AnimatedLoader
    let loaderController = EpcLensLoaderViewController(nibName: "EpcLensLoaderViewController", bundle: nil)
    
    
    //MARK:: UIView
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        
//        lexiconDispatcher.delegate = self
//        var tempDict = [String:String]()
//        tempDict["token"] = EpicLensConstantsVariables.accessToken
//        lexiconDispatcher.GetLexiconMenuList(paramDict: tempDict)
//        
        
        //selectedID = 1
        //let indexPath = IndexPath(item: 0, section: 0) //Here the first value is set to the first index of the collection view
        //self.postCategoryCollectionView.selectItem(at: indexPath, animated: true, scrollPosition: [])
        
        
        
        
        lexiconDispatcher.delegate = self
        var tempDict = [String:String]()
        tempDict["token"] = EpicLensConstantsVariables.accessToken
        lexiconDispatcher.GetLexiconMenuList(paramDict: tempDict)
        
        
        // Start Loaded
        loaderController.showEpcLoader(_sourceView: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:: IBAction
    @IBAction func btnLeftMenuDidTap(_ sender: Any) {
        let elDrawer = self.navigationController?.parent as! KYDrawerController
        elDrawer.setDrawerState(.opened, animated: true)
    }
    
    @IBAction func btnRightMenuDidTap(_ sender: UIButton) {
        let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
        let objReportPopup = storyboardMain.instantiateViewController(withIdentifier: "RightMenuController") as! RightMenuController
        objReportPopup.showPopup(_controller: self)
    }
    
    @objc func btnAddDidTap(_ sender: Any) {
        let storyboardLexicon = UIStoryboard(name: "Lexicon", bundle: nil)
        let helpUsAddVC = storyboardLexicon.instantiateViewController(withIdentifier: "HelpLexiconVC") as! HelpLexiconController
        self.navigationController?.pushViewController(helpUsAddVC, animated: false)
    }
    
    //MARK:: LexiconListProtocol
    func didUpdateControllerWithObjLexiconMenuList(lexiconMenuList: LexiconMenuListModel) {
        DispatchQueue.main.async {
            if lexiconMenuList.data.sucess {
                self.lexiconsMenuListArr = lexiconMenuList.data.lexiconslist
                self.tableLexicon.delegate = self
                self.tableLexicon.dataSource = self
                self.tableLexicon.reloadData()
                if self.isFirstTimeLoadMenuDetails == false && self.lexiconsMenuListArr.count > 0 {
                    self.isFirstTimeLoadMenuDetails = true
                    self.selectedID = self.lexiconsMenuListArr[0].id
                    var tempDict = [String:String]()
                    tempDict["token"] = EpicLensConstantsVariables.accessToken
                    tempDict["id"] = String(self.selectedID)
                    self.lexiconDispatcher.GetLexiconListDetailsByMenu(paramDict: tempDict)
                } else {
                    // Stop Loaded
                    self.loaderController.hideEpcLoader()
                }
            }
        }
    }
    
    
    func didUpdateControllerWithObjLexiconListDetailsByMenu(lexiconListDetails: LexiconMenuDetailsModel) {
        DispatchQueue.main.async {
            if lexiconListDetails.data.sucess {
                if let resultArr = lexiconListDetails.data.result {
                    self.lexiconMenuResultArr = resultArr
                } else {
                    self.lexiconMenuResultArr.removeAll()
                }
                
                if let helpusArr = lexiconListDetails.data.helpus {
                    self.lexiconMenuHelpusArr = helpusArr
                } else {
                    self.lexiconMenuHelpusArr.removeAll()
                }
                self.tableLexicon.delegate = self
                self.tableLexicon.dataSource = self
                self.tableLexicon.reloadData()
                // Stop Loaded
                self.loaderController.hideEpcLoader()
            }
        }
    }
    
}


extension LexiconListController : UITableViewDataSource, UITableViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    //MARK:: UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        if selectedID == 1 {
            return 2
        } else { //if UserDefaults.standard.bool(forKey: "isEncyclopedia") {
            return 3
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else if section == 1 {
            print("lexiconMenuResultArr.count :: \(lexiconMenuResultArr.count)")
            return lexiconMenuResultArr.count
        } else {// if section == 2 {
            print("lexiconMenuHelpusArr.count :: \(lexiconMenuHelpusArr.count)")
            return lexiconMenuHelpusArr.count
        } 
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.section == 0) {
            let idstr = "LexiconMenuTableCell"
            
            // get a reference to our storyboard cell
            var lexiconMenuCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
            if lexiconMenuCell == nil {
                lexiconMenuCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
            }
            
            // Use the outlet in our custom class to get a reference to the UI Elements in the cell
            let collectionLexiconMenu = lexiconMenuCell.viewWithTag(1) as! UICollectionView
            
            collectionLexiconMenu.dataSource = self
            collectionLexiconMenu.delegate = self
            collectionLexiconMenu.reloadData()
            
            lexiconMenuCell.selectionStyle = .none
            return lexiconMenuCell
        } else if (indexPath.section == 1) {
            let idstr = "LexiconTableCell"
            
            // get a reference to our storyboard cell
            var lexiconCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
            if lexiconCell == nil {
                lexiconCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
            }
            
            // Use the outlet in our custom class to get a reference to the UI Elements in the cell
            let lblDetails = lexiconCell.viewWithTag(4) as! UILabel
            
            if lexiconMenuResultArr.count > 0 {
                lblDetails.text = lexiconMenuResultArr[indexPath.row].title
            }
            
            if (indexPath.row == (lexiconMenuResultArr.count-1)) {
                lexiconCell.viewWithTag(32)?.layer.cornerRadius = 10
            }
            
            if lexiconMenuResultArr[indexPath.row].type == "video" {
                lexiconCell.viewWithTag(5)?.isHidden = true
            } else {
                lexiconCell.viewWithTag(5)?.isHidden = false
            }
            
            
            lexiconCell.selectionStyle = .none
            return lexiconCell
        } else {
            let idstr = "LexiconTableCell"
            
            // get a reference to our storyboard cell
            var lexiconCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
            if lexiconCell == nil {
                lexiconCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
            }
            
            if lexiconMenuHelpusArr.count > 0 {
                let lblDetails = lexiconCell.viewWithTag(4) as! UILabel
                lblDetails.text = lexiconMenuHelpusArr[indexPath.row].title
            }
            
            if (indexPath.row == (lexiconMenuHelpusArr.count-1)) {
                lexiconCell.viewWithTag(32)?.layer.cornerRadius = 10
            }
            
            lexiconCell.selectionStyle = .none
            return lexiconCell
        }
    }
    
    
    //MARK:: UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 100
        } else {
            return 35
        }
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 {
            let lexiconHeaderCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: "LexiconHeaderView")!
            
            
            let lblHeaderTitle = lexiconHeaderCell.viewWithTag(13) as! UILabel
            
            lblHeaderTitle.text = "new and trending"
            lexiconHeaderCell.viewWithTag(14)?.isHidden = true
            
            return lexiconHeaderCell
        } else if section == 2 {
            let lexiconHeaderCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: "LexiconHeaderView")!
            
            let lblHeaderTitle = lexiconHeaderCell.viewWithTag(13) as! UILabel
            let btnAdd = lexiconHeaderCell.viewWithTag(20) as! UIButton
            
            
            lblHeaderTitle.text = "help us"
            lexiconHeaderCell.viewWithTag(12)?.isHidden = true
            lexiconHeaderCell.viewWithTag(14)?.isHidden = false
            lexiconHeaderCell.viewWithTag(14)?.layer.cornerRadius = 10
            lexiconHeaderCell.viewWithTag(14)?.layer.borderWidth = 0.5
            lexiconHeaderCell.viewWithTag(14)?.layer.borderColor = UIColor.white.cgColor
            
            
            btnAdd.addTarget(self, action: #selector(btnAddDidTap), for: .touchUpInside)
            return lexiconHeaderCell
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if (section == 0) {
            return 0
        } else if (section == 1) {
            return 35
        } else if (section == 2) {
            return 35
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 15
    }
    
    
    
    //    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
    //        if indexPath.section == 0 {
    //            return 150
    //        } else {
    //            return 35
    //        }
    //    }
    
    
    /*func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
     return 85.0
     }*/
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //print("Menu ::",lexiconMenuResultArr[indexPath.row].type!)
        if (indexPath.section == 1) {
            if lexiconMenuResultArr[indexPath.row].type == "video" {
                print("VideoList id :: \(String(describing: self.lexiconMenuResultArr[indexPath.item].id))")
                let storyboardLensflare = UIStoryboard(name: "Lensflare", bundle: nil)
                let lensFlareVC = storyboardLensflare.instantiateViewController(withIdentifier: "LensFlareVC") as! LensFlareController
                lensFlareVC.id = lexiconMenuResultArr[indexPath.item].id
                //lensFlareVC.isLensFlareLoadType = 2
                //UserDefaults.standard.set(false, forKey: "isLensflare")
                self.navigationController?.pushViewController(lensFlareVC, animated: false)
            } else if lexiconMenuResultArr[indexPath.row].lexiconcategoryTitle == "encyclopedia" {
                let storyboardLensflare = UIStoryboard(name: "Lexicon", bundle: nil)
                let lexiconDetailsVC = storyboardLensflare.instantiateViewController(withIdentifier: "LexiconDetailsVC") as! LexiconDetailsController
                lexiconDetailsVC.selectedLexiconID = 2
                lexiconDetailsVC.selectedLexiconItemID = lexiconMenuResultArr[indexPath.item].id
                lexiconDetailsVC.navigationTitleStr = lexiconMenuResultArr[indexPath.row].lexiconcategoryTitle!//"encyclopedia"
                self.navigationController?.pushViewController(lexiconDetailsVC, animated: false)
            } else if lexiconMenuResultArr[indexPath.row].lexiconcategoryTitle == "glossary" {
                let storyboardLensflare = UIStoryboard(name: "Lexicon", bundle: nil)
                let lexiconDetailsVC = storyboardLensflare.instantiateViewController(withIdentifier: "LexiconDetailsVC") as! LexiconDetailsController
                lexiconDetailsVC.selectedLexiconID = 3
                lexiconDetailsVC.selectedLexiconItemID = lexiconMenuResultArr[indexPath.row].id
                lexiconDetailsVC.navigationTitleStr = "glossary"
                self.navigationController?.pushViewController(lexiconDetailsVC, animated: false)
            } else if lexiconMenuResultArr[indexPath.row].lexiconcategoryTitle == "regulations" {
                let storyboardLensflare = UIStoryboard(name: "Lexicon", bundle: nil)
                let lexiconDetailsVC = storyboardLensflare.instantiateViewController(withIdentifier: "LexiconDetailsVC") as! LexiconDetailsController
                //print("selectedLexiconItemID :: ",lexiconMenuResultArr[indexPath.item].id)
                lexiconDetailsVC.selectedLexiconID = 4
                lexiconDetailsVC.selectedLexiconItemID = lexiconMenuResultArr[indexPath.row].id
                lexiconDetailsVC.navigationTitleStr = "regulations"
                self.navigationController?.pushViewController(lexiconDetailsVC, animated: false)
            } else if lexiconMenuResultArr[indexPath.row].lexiconcategoryTitle == "tools" {
                let storyboardLensflare = UIStoryboard(name: "Lexicon", bundle: nil)
                let lexiconDetailsVC = storyboardLensflare.instantiateViewController(withIdentifier: "LexiconDetailsVC") as! LexiconDetailsController
                lexiconDetailsVC.selectedLexiconID = 5
                lexiconDetailsVC.selectedLexiconItemID = lexiconMenuResultArr[indexPath.row].id
                lexiconDetailsVC.navigationTitleStr = "tools"
                self.navigationController?.pushViewController(lexiconDetailsVC, animated: false)
            }
        }
    }
    
    
    // MARK:: UICollectionViewDataSource protocol
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("LexiconList data count:: ",lexiconsMenuListArr.count)
        return lexiconsMenuListArr.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let idstr = "LexiconMenuCell"
        
        // get a reference to our storyboard cell
        let lexiconMenuCell : UICollectionViewCell! = collectionView.dequeueReusableCell(withReuseIdentifier: idstr, for: indexPath)
        
        // Use the outlet in our custom class to get a reference to the UI Elements in the cell
        let lblMenuTitle = lexiconMenuCell.viewWithTag(2) as! UILabel
        let imgMenuThumbnail = lexiconMenuCell.viewWithTag(3) as! UIImageView
        
        
        lblMenuTitle.text = lexiconsMenuListArr[indexPath.row].title
        if lexiconMenuCell.isSelected == true {
            lblMenuTitle.textColor = UIColor.black
        }
        
        
        let menuImgUrlString = lexiconsMenuListArr[indexPath.row].image
        
        if menuImgUrlString?.isEmpty == false && menuImgUrlString != "" {
            let escapedString = menuImgUrlString?.replacingOccurrences(of: " ", with: "%20")            
            Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgMenuThumbnail)
        } else {
            imgMenuThumbnail.image = UIImage(named: "AppIcon")
        }
        
        /// Text Color Change based on Selection
        if selectedID == lexiconsMenuListArr[indexPath.row].id {
            lblMenuTitle.textColor = UIColor.black
        } else {
            lblMenuTitle.textColor = UIColor.white
        }
        
        /// background Color Change of Items
        if indexPath.row == 0 {
            lexiconMenuCell?.backgroundColor = UIColor(red: CGFloat((191.0 / 255.0)), green: CGFloat((146.0 / 255.0)), blue: CGFloat((126.0 / 255.0)), alpha: 1.0)
        } else if indexPath.row == 1 {
            lexiconMenuCell?.backgroundColor = UIColor(red: CGFloat((110.0 / 255.0)), green: CGFloat((156.0 / 255.0)), blue: CGFloat((178.0 / 255.0)), alpha: 1.0)
        } else if indexPath.row == 2 {
            lexiconMenuCell?.backgroundColor = UIColor(red: CGFloat((140.0 / 255.0)), green: CGFloat((161.0 / 255.0)), blue: CGFloat((179.0 / 255.0)), alpha: 1.0)
        } else if indexPath.row == 3 {
            lexiconMenuCell?.backgroundColor = UIColor(red: CGFloat((175.0 / 255.0)), green: CGFloat((163.0 / 255.0)), blue: CGFloat((149.0 / 255.0)), alpha: 1.0)
        } else {
            lexiconMenuCell?.backgroundColor = UIColor(red: CGFloat((165.0 / 255.0)), green: CGFloat((160.0 / 255.0)), blue: CGFloat((166.0 / 255.0)), alpha: 1.0)
        }
        
        
        return lexiconMenuCell!
    }
    
    // MARK:: UICollectionViewDelegate protocol
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth = (self.view.frame.size.width-50)/3
        return CGSize(width: cellWidth, height: 35)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("objSelectedItem :: \(String(describing: self.lexiconsMenuListArr[indexPath.item]))")
        selectedID = self.lexiconsMenuListArr[indexPath.item].id
        
        
        var tempDict = [String:String]()
        tempDict["token"] = EpicLensConstantsVariables.accessToken
        tempDict["id"] = String(selectedID)
        lexiconDispatcher.GetLexiconListDetailsByMenu(paramDict: tempDict)
        
        tableLexicon.reloadData()        
    }
    
}
