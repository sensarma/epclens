//
//  ProjectListController.swift
//  EpcLensProject
//
//  Created by BlueHorse MacBook Pro on 23/07/18.
//  Copyright © 2018 Moumita Datta. All rights reserved.
//

import UIKit

class ProjectListController: UIViewController, ProjectListProtocol {
    
    var projectDispatcher = ProjectDispatcher()
    var projectListArr = [ProjectList]()
    @IBOutlet weak var tableProjectList: UITableView!
    
    //Mark:: AnimatedLoader
    let loaderController = EpcLensLoaderViewController(nibName: "EpcLensLoaderViewController", bundle: nil)
    
    var projectID : Int = -1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        projectDispatcher.delegate = self
        var tempDict = [String:String]()
        tableProjectList.separatorStyle = .none
        tempDict["token"] = EpicLensConstantsVariables.accessToken
        tempDict["page"] = "0"
        projectDispatcher.GetProjectList(paramDict: tempDict)
        
        
        // Start Loaded
        loaderController.showEpcLoader(_sourceView: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didupdateControllerWithProjectList(projectList: ProjectListModel) {
        DispatchQueue.main.async {
            if projectList.data.sucess {
                self.projectListArr = projectList.data.projectList
                self.tableProjectList.reloadData()
                
                // Stop Loaded
                self.loaderController.hideEpcLoader()
            }
        }
    }
    
    //MARK:: IBAction
    @IBAction func btnLeftMenuDidTap(_ sender: Any) {
        let elDrawer = self.navigationController?.parent as! KYDrawerController
        elDrawer.setDrawerState(.opened, animated: true)
    }
    
    @IBAction func btnRightMenuDidTap(_ sender: UIButton) {
        
        //        let originalTransform = sender.transform
        //        let scaledTransform = originalTransform.scaledBy(x: 0.2, y: 0.2)
        //        let scaledAndTranslatedTransform = scaledTransform.translatedBy(x: 0.0, y: -250.0)
        //        UIView.animate(withDuration: 0.7, animations: {
        //            sender.transform = scaledAndTranslatedTransform
        //        })
        
        let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
        let objReportPopup = storyboardMain.instantiateViewController(withIdentifier: "RightMenuController") as! RightMenuController
        objReportPopup.showPopup(_controller: self)
    }

}

extension ProjectListController : UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return projectListArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let idstr = "projectCell"
        
        var cell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: idstr)
        }
        
        // Use the outlet in our custom class to get a reference to the UI Elements in the cell
        /*let lblName = cell.viewWithTag(1) as! UILabel
        let imgPeopleProfile = cell.viewWithTag(2) as! UIImageView
        let lblDesignation = cell.viewWithTag(3) as! UILabel
        let lblAddress = cell.viewWithTag(4) as! UILabel
        let lblJobTypes = cell.viewWithTag(5) as! UILabel
        let viewBack = cell.viewWithTag(6)!
        let viewNameBack = cell.viewWithTag(7)!
        let viewJobTypesBack = cell.viewWithTag(8)!
        
        lblName.text =  projectListArr[indexPath.row].peopleName
        
        imgPeopleProfile.layer.cornerRadius = 33.5
        imgPeopleProfile.layer.borderColor = UIColor.darkGray.cgColor
        imgPeopleProfile.layer.borderWidth = 1.0
        imgPeopleProfile.clipsToBounds = true
        
        if projectListArr[indexPath.row].img?.isEmpty == false && peopleListArr[indexPath.row].img != "" {
            let escapedString = projectListArr[indexPath.row].img?.replacingOccurrences(of: " ", with: "%20")
            Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgPeopleProfile)
        } else {
            imgPeopleProfile.image = UIImage(named: "AppIcon")
        }
        
        if (projectListArr[indexPath.row].designation == nil) || (projectListArr[indexPath.row].designation! == "") || (projectListArr[indexPath.row].designation! == "(NULL)") || (projectListArr[indexPath.row].designation! == "<NULL>") || ((projectListArr[indexPath.row].designation! == "<null>") || (projectListArr[indexPath.row].designation! == "(null)") || (projectListArr[indexPath.row].designation! == "")) {
            lblDesignation.text = " "
        } else {
            lblDesignation.text =  projectListArr[indexPath.row].designation ?? "" + " at " +  projectListArr[indexPath.row].companyName!
        }
        
        if (projectListArr[indexPath.row].location == nil) || (projectListArr[indexPath.row].location! == "") || (projectListArr[indexPath.row].location! == "(NULL)") || (projectListArr[indexPath.row].location! == "<NULL>") || ((projectListArr[indexPath.row].location! == "<null>") || (projectListArr[indexPath.row].location! == "(null)") || (projectListArr[indexPath.row].location! == "")) {
            lblAddress.text = " "
        } else {
            lblAddress.text =  projectListArr[indexPath.row].location ?? "no address"
        }
        
        if (projectListArr[indexPath.row].jobTypes == nil) || (projectListArr[indexPath.row].jobTypes! == "") || (projectListArr[indexPath.row].jobTypes! == "(NULL)") || (projectListArr[indexPath.row].jobTypes! == "<NULL>") || ((projectListArr[indexPath.row].jobTypes! == "<null>") || (projectListArr[indexPath.row].jobTypes! == "(null)") || (projectListArr[indexPath.row].jobTypes! == "")) {
            lblJobTypes.text = " "
        } else {
            lblJobTypes.text =  projectListArr[indexPath.row].jobTypes
        }
        
        //btnContact.addTarget(self, action:#selector(self.btnContactPressed(_sender:)), for: UIControlEvents.touchUpInside)
        viewJobTypesBack.layer.cornerRadius = 5.0
        viewJobTypesBack.clipsToBounds = true
        viewJobTypesBack.layer.borderColor = UIColor.darkGray.cgColor
        viewJobTypesBack.layer.borderWidth = 0.5
        
        viewBack.layer.cornerRadius = 10.0
        viewBack.clipsToBounds = true
        
        viewNameBack.layer.cornerRadius = 10.0
        viewNameBack.clipsToBounds = true*/
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 133.0
    }
    
    /*func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboardPeople = UIStoryboard(name: "People", bundle: nil)
        let peopleVC = storyboardPeople.instantiateViewController(withIdentifier: "PeopleDetailController") as! PeopleDetailController
        peopleVC.peopleId = projectListArr[indexPath.row].id
        self.navigationController?.pushViewController(peopleVC, animated: true)
    }*/
}
