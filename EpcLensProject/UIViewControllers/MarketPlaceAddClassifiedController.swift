//
//  MarketPlaceAddClassifiedController.swift
//  EpcLensProject
//
//  Created by Moumita Datta on 02/07/18.
//  Copyright © 2018 Moumita Datta. All rights reserved.
//

import UIKit
import QuartzCore


class MarketPlaceAddClassifiedController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableAddClassified: UITableView!
    
    var adClassifiedFeaturesArr  = ["$99 for 15 days", "$199 for 30 days", "$389 for 60 days", "post for free"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tableAddClassified.rowHeight = UITableViewAutomaticDimension
        tableAddClassified.estimatedRowHeight = 2000
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:: IBAction
    @IBAction func btnBackDidTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:: UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 3
        } else if section == 1 {
            return adClassifiedFeaturesArr.count
        } else if section == 2 {
            return 2
        } else {
            return 1
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let idstr = "AddClassifiedCategoryTableCell"
                
                // get a reference to our storyboard cell
                var addClassifiedCategoryCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
                if addClassifiedCategoryCell == nil {
                    addClassifiedCategoryCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
                }
                
                
                //Declare the outlet in our custom class
                let viewCategories = addClassifiedCategoryCell.viewWithTag(20)
               /* //let viewClassifiedAds = classifiedAdsListCell.viewWithTag(21)
                let lblClassifiedAdsTitle = classifiedAdsListCell.viewWithTag(22) as! UILabel
                let lblClassifiedAdsCategory = classifiedAdsListCell.viewWithTag(23) as! UILabel
                let lblClassifiedAdsLocation = classifiedAdsListCell.viewWithTag(24) as! UILabel
                let lblClassifiedAdsShortDesc = classifiedAdsListCell.viewWithTag(26) as! UILabel
                
                classifiedAdsListCell.viewWithTag(20)?.layer.cornerRadius = 10.0
                classifiedAdsListCell.viewWithTag(20)?.clipsToBounds = true
                classifiedAdsListCell.viewWithTag(21)?.layer.cornerRadius = 10.0
                classifiedAdsListCell.viewWithTag(21)?.clipsToBounds = true
                imgViewClassifiedAds.layer.cornerRadius = 5.0
                imgViewClassifiedAds.clipsToBounds = true
                */
                
                viewCategories?.layer.cornerRadius = 8.0
                viewCategories?.layer.borderWidth = 0.5
                viewCategories?.layer.borderColor = UIColor.darkGray.cgColor
                
                return addClassifiedCategoryCell
            } else if indexPath.row == 1 {
                let idstr = "AddClassifiedTitleTableCell"
                
                // get a reference to our storyboard cell
                var addClassifiedTitleCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
                if addClassifiedTitleCell == nil {
                    addClassifiedTitleCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
                }
                
                //Declare the outlet in our custom class
                let viewTitle = addClassifiedTitleCell.viewWithTag(30)
                
                viewTitle?.layer.cornerRadius = 8.0
                viewTitle?.layer.borderWidth = 0.5
                viewTitle?.layer.borderColor = UIColor.darkGray.cgColor
                
                return addClassifiedTitleCell
            } else {
                let idstr = "AddClassifiedDescriptionTableCell"
                
                // get a reference to our storyboard cell
                var addClassifiedDescriptionCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
                if addClassifiedDescriptionCell == nil {
                    addClassifiedDescriptionCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
                }
                
                //Declare the outlet in our custom class
                let viewDescription = addClassifiedDescriptionCell.viewWithTag(40)
                
                viewDescription?.layer.cornerRadius = 8.0
                viewDescription?.layer.borderWidth = 0.5
                viewDescription?.layer.borderColor = UIColor.darkGray.cgColor
                
                return addClassifiedDescriptionCell
            }
        } else if indexPath.section == 1 {
            let idstr = "AddClassifiedFeatureTableCell"
            
            // get a reference to our storyboard cell
            var addClassifiedFeatureCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
            if addClassifiedFeatureCell == nil {
                addClassifiedFeatureCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
            }
            
            //Declare the outlet in our custom class
            let lblFeatures = addClassifiedFeatureCell.viewWithTag(62) as! UILabel
            
            lblFeatures.text = adClassifiedFeaturesArr[indexPath.row]
            
            return addClassifiedFeatureCell
        } else if indexPath.section == 2 {
            if indexPath.row == 0 {
                let idstr = "AddClassifiedTitleTableCell"
                
                // get a reference to our storyboard cell
                var addClassifiedTitleCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
                if addClassifiedTitleCell == nil {
                    addClassifiedTitleCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
                }
                
                //Declare the outlet in our custom class
                let viewTitle = addClassifiedTitleCell.viewWithTag(30)
                
                viewTitle?.layer.cornerRadius = 8.0
                viewTitle?.layer.borderWidth = 0.5
                viewTitle?.layer.borderColor = UIColor.darkGray.cgColor
                return addClassifiedTitleCell
            } else {
                let idstr = "AddClassifiedPhoneTableCell"
                
                // get a reference to our storyboard cell
                var addClassifiedPhoneCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
                if addClassifiedPhoneCell == nil {
                    addClassifiedPhoneCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
                }
                
                //Declare the outlet in our custom class
                let viewPhoneLeft = addClassifiedPhoneCell.viewWithTag(70)
                let viewPhoneRight = addClassifiedPhoneCell.viewWithTag(71)
                
                viewPhoneLeft?.layer.cornerRadius = 8.0
                viewPhoneLeft?.layer.borderWidth = 0.5
                viewPhoneLeft?.layer.borderColor = UIColor.darkGray.cgColor
                
                viewPhoneRight?.layer.cornerRadius = 8.0
                viewPhoneRight?.layer.borderWidth = 0.5
                viewPhoneRight?.layer.borderColor = UIColor.darkGray.cgColor
                
                return addClassifiedPhoneCell
            }
        } else if indexPath.section == 3 {
            let idstr = "AddClassifiedImageBrowsingTableCell"
            
            // get a reference to our storyboard cell
            var addClassifiedImageBrowsingCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
            if addClassifiedImageBrowsingCell == nil {
                addClassifiedImageBrowsingCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
            }
            
            //Declare the outlet in our custom class
            let viewImgBrowser = addClassifiedImageBrowsingCell.viewWithTag(80)
            let viewImgBrowserInside = addClassifiedImageBrowsingCell.viewWithTag(81)
            
            
            let yourViewBorder = CAShapeLayer()
            yourViewBorder.strokeColor = UIColor.darkGray.cgColor
            yourViewBorder.lineDashPattern = [5, 5] //[width,gap]
            yourViewBorder.frame = (viewImgBrowserInside?.bounds)!
            yourViewBorder.fillColor = nil
            //yourViewBorder.cornerRadius = 10.0
            yourViewBorder.path = UIBezierPath(rect: (viewImgBrowserInside?.bounds)!).cgPath
            viewImgBrowserInside?.layer.addSublayer(yourViewBorder)
            viewImgBrowserInside?.layer.cornerRadius = 10.0
            viewImgBrowserInside?.clipsToBounds = true
            
            viewImgBrowser?.layer.cornerRadius = 10.0
            viewImgBrowser?.clipsToBounds = true
            
            return addClassifiedImageBrowsingCell
        } else { //if indexPath.section == 4 {
            let idstr = "AddClassifiedPostAdTableCell"
            
            // get a reference to our storyboard cell
            var addClassifiedPostAdCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
            if addClassifiedPostAdCell == nil {
                addClassifiedPostAdCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
            }
            
            return addClassifiedPostAdCell
        }
    }
    
    
    //MARK:: UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            if indexPath.row == 2 {
                return UITableViewAutomaticDimension
            } else {
                return 45
            }
        } else if indexPath.section == 1 {
            return 30
        } else if indexPath.section == 2 {
            return 45
        } else if indexPath.section == 3 {
            return 225
        } else if indexPath.section == 4 {
            return 45
        }
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            // get a reference to our storyboard cell
            let addClassifiedHeaderCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: "AddClassifiedHeaderCell")!
            
//            //Declare the outlet in our custom class
//            let lblHeaderTitle = addClassifiedHeaderCell.viewWithTag(10) as! UILabel
//
//            if isClassifiedAds == 0 {
//                lblHeaderTitle.font = lblHeaderTitle.font.withSize(16)
//                lblHeaderTitle.text = "classified ads"
//
//            } else {
//                lblHeaderTitle.font = lblHeaderTitle.font.withSize(16)
//                lblHeaderTitle.text = "job ads"
//            }
            return addClassifiedHeaderCell
        } else if section == 1 {
            // get a reference to our storyboard cell
            let addClassifiedFeatureHeaderCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: "AddClassifiedFeatureHeaderCell")!
            
            return addClassifiedFeatureHeaderCell
        }
        return nil
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 45
        } else if section == 1 {
            return 65
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }
    
}
