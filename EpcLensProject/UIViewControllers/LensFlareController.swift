//
//  LensFlareController.swift
//  EpcLensProject
//
//  Created by Moumita Datta on 15/05/18.
//  Copyright © 2018 Moumita Datta. All rights reserved.
//

import UIKit



class LensFlareController: UIViewController, LensFlareListProtocol {//}, LexiconDetailsProtocol {
    
    //Navigation Menu
    @IBOutlet var navitionTitle: UILabel!
    @IBOutlet var leftMenuBtn: UIButton!
    @IBOutlet var leftMenuImg: UIImageView!
    
    
    @IBOutlet weak var tableLensFlare: UITableView!
    private var isPlayVideo = false
    
    
    var lensFlareDispatcher = LensFlareDispatcher()
    var lensFlareVideoArr = [LensFlareItemList]()
//    var lexiconDetailsDispatcher = LexiconDetailsDispatcher()
//    var lexiconDetailsVideoArr = [LexiconDetailsResult]()
//    var objSelectedLexiconItem:LexiconDetailsResult? = nil
    
    
    var lensFlareVideoDetailsArr = [LensFlareItemList]()
    //var lensFlareFeatureArr = [LensFlareItemList]()
    //var objSelectedLensFlareItem:LensFlareItemList? = nil
    
    
    var id:Int = 0
    var videoURLStr : String = ""
    
    @IBOutlet var collectionViewHeight: NSLayoutConstraint!    
    @IBOutlet var lblSponsorHeight: NSLayoutConstraint!
    
    
   // var sponsorsArr = [Array<Any>].self
    
    
    //var isLensflare: Bool = true
    //var isLensFlareLoadType = 0
    
    //Mark:: AnimatedLoader
    let loaderController = EpcLensLoaderViewController(nibName: "EpcLensLoaderViewController", bundle: nil)
    
    
    
    //MARK:: UIView
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        lensFlareDispatcher.delegate = self
        //lexiconDetailsDispatcher.delegate = self
        
        
        navitionTitle.text = "lensflare"
        leftMenuBtn.setImage(UIImage(named: "Navigation_Left_Menu_icon"), for: UIControlState.normal)
        leftMenuBtn.addTarget(self, action: #selector(self.btnLeftMenuDidTap(_:)), for: UIControlEvents.touchUpInside)
        leftMenuImg.isHidden = true
        
        
        var tempDict = [String:String]()
        tempDict["token"] = EpicLensConstantsVariables.accessToken
        tempDict["page"] = "0"
        lensFlareDispatcher.GetLensFlareMenuList(paramDict: tempDict)
        
        /*if isLensFlareLoadType == 0 {
            navitionTitle.text = "lensflare"
            leftMenuBtn.setImage(UIImage(named: "Navigation_Left_Menu_icon"), for: UIControlState.normal)
            leftMenuBtn.addTarget(self, action: #selector(self.btnLeftMenuDidTap(_:)), for: UIControlEvents.touchUpInside)
            leftMenuImg.isHidden = true
            
            
            var tempDict = [String:String]()
            tempDict["token"] = EpicLensConstantsVariables.accessToken
            tempDict["page"] = "0"
            lensFlareDispatcher.GetLensFlareMenuList(paramDict: tempDict)
        } else if isLensFlareLoadType == 2 {
            navitionTitle.text = "video"
            leftMenuBtn.setImage(UIImage(named: "BlankImage"), for: UIControlState.normal)
            leftMenuBtn.addTarget(self, action: #selector(self.btnBackDidTap), for: UIControlEvents.touchUpInside)
            leftMenuImg.image = UIImage(named:"Navigation_Back_Button_icon")!
            
            
            var tempDict = [String:String]()
            tempDict["token"] = EpicLensConstantsVariables.accessToken
            tempDict["id"] = String(1)
            tempDict["itemid"] = String(id)
            lexiconDetailsDispatcher.GetLexiconDetailsByCategory(paramDict: tempDict)
        }*/
        
        
        // Start Loaded
        loaderController.showEpcLoader(_sourceView: self)
        
    }    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //isLensflare = UserDefaults.standard.bool(forKey: "isLensflare")
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:: IBAction
    @IBAction func btnLeftMenuDidTap(_ sender: Any) {
        let elDrawer = self.navigationController?.parent as! KYDrawerController
        elDrawer.setDrawerState(.opened, animated: true)
    }
    
    @IBAction func btnRightMenuDidTap(_ sender: UIButton) {
        
        //        let originalTransform = sender.transform
        //        let scaledTransform = originalTransform.scaledBy(x: 0.2, y: 0.2)
        //        let scaledAndTranslatedTransform = scaledTransform.translatedBy(x: 0.0, y: -250.0)
        //        UIView.animate(withDuration: 0.7, animations: {
        //            sender.transform = scaledAndTranslatedTransform
        //        })
        let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
        let objReportPopup = storyboardMain.instantiateViewController(withIdentifier: "RightMenuController") as! RightMenuController
        objReportPopup.showPopup(_controller: self)
    }
    
    @IBAction func btnBackDidTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnShareDidTap(sender: UIButton) {
        let textToShare = "Swift is awesome!  Check out this website about it!"
        
        if let myWebsite = NSURL(string: "http://18.217.188.228/#/lens-flare") {
            // set up activity view controller
            let textToShare = [ myWebsite ]
            let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
            // present the view controller
            self.present(activityViewController, animated: true, completion: nil)
        }
    }
    
    //MARK:: LensFlareListProtocol
    func didUpdateControllerWithLensFlareList(lensFlarelist: LensFlareListModel) {
        DispatchQueue.main.async {
            if lensFlarelist.data.sucess {
                self.lensFlareVideoArr = lensFlarelist.data.itemList!
                if self.lensFlareVideoArr.count > 0 {
                    //self.objSelectedLensFlareItem = self.lensFlareVideoArr[0]
                    self.tableLensFlare.delegate = self
                    self.tableLensFlare.dataSource = self
                    self.tableLensFlare.reloadData()
                }
                // Stop Loaded
                self.loaderController.hideEpcLoader()
            }
        }
    }
    
    func didUpdateControllerWithLensFlareItemsByID(lensFlarelistByID: LensFlareListModel) {
        DispatchQueue.main.async {
            if lensFlarelistByID.data.sucess {
                self.lensFlareVideoDetailsArr = lensFlarelistByID.data.itemList!
                
                if self.lensFlareVideoDetailsArr.count > 0 {
                    print("lensFlareVideoDetailsArr ::",self.lensFlareVideoDetailsArr)
                    //self.objSelectedLensFlareItem = self.lensFlareVideoDetailsArr[0]
                    //self.objSelectedItem = self.lensFlareVideoDetailsArr[0]
                    self.tableLensFlare.delegate = self
                    self.tableLensFlare.dataSource = self
                    self.tableLensFlare.reloadData()
                }
                
//                let cell = self.tableLensFlare.cellForRow(at: IndexPath.init(row: 0, section: 0))
//                let sponsorCell = cell?.viewWithTag(55) as! UICollectionView
//                sponsorCell.delegate = self
//                sponsorCell.dataSource = self
//                sponsorCell.reloadData()
                
                // Stop Loaded
                self.loaderController.hideEpcLoader()
            }
        }
    }
    
    /*func didUpdateControllerWithSeesDetails(seeslist: LensFlareListModel) {
        DispatchQueue.main.async {
            if seeslist.data.sucess {
                self.lensFlareVideoArr = seeslist.data.itemList
                if self.lensFlareVideoArr.count > 0 {
                    self.objSelectedItem = self.lensFlareVideoArr[0]
                    self.tableLensFlare.delegate = self
                    self.tableLensFlare.dataSource = self
                    self.tableLensFlare.reloadData()
                }
                // Stop Loaded
                self.stopAnimating()
            }
        }
    }*/
    
    /*func didUpdateControllerWithLensFlareListByID(lensFlarelistByID:LensFlareListModel) {
        DispatchQueue.main.async {
            if lensFlarelistByID.data.sucess {
                self.lensFlareVideoDetailsArr = lensFlarelistByID.data.itemList
                
                if self.lensFlareVideoDetailsArr.count > 0 {
                    print("lensFlareVideoDetailsArr ::",self.lensFlareVideoDetailsArr)
                    //self.objSelectedLensFlareItem = self.lensFlareVideoArr[0]
                    //self.objSelectedItem = self.lensFlareVideoDetailsArr[0]
                    self.tableLensFlare.delegate = self
                    self.tableLensFlare.dataSource = self
                    self.tableLensFlare.reloadData()
                }
                /*self.tableLensFlare.delegate = self
                self.tableLensFlare.dataSource = self
                self.tableLensFlare.reloadData()*/
                // Stop Loaded
                self.stopAnimating()
            }
        }
    }*/
    
    
    //MARK:: LexiconDetailsProtocol
    func didUpdateControllerWithObjLexiconDetails(lexiconDetails: LexiconDetailsModel) {
        DispatchQueue.main.async {
            if lexiconDetails.data.sucess {
                //self.lexiconDetailsVideoArr = lexiconDetails.data.result
                self.tableLensFlare.delegate = self
                self.tableLensFlare.dataSource = self
                self.tableLensFlare.reloadData()
                // Stop Loaded
                self.loaderController.hideEpcLoader()
            }
        }
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension LensFlareController : UITableViewDataSource, UITableViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    //MARK:: UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.section == 0) {
            let idstr = "LensFlareCell"
            
            // get a reference to our storyboard cell
            var cell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
            if cell == nil {
                cell = UITableViewCell(style: .default, reuseIdentifier: idstr)
            }
            
            cell.selectionStyle = .none
            
            
            cell.viewWithTag(10)?.layer.masksToBounds = true
            
            // Use the outlet in our custom class to get a reference to the UI Elements in the cell
            let lblPublishedVideoDate = cell.viewWithTag(2) as! UILabel
            let lblVideoDetails = cell.viewWithTag(3) as! UILabel
            let webViewVideo = cell.viewWithTag(75) as! UIWebView
            let lblPlayerView = cell.viewWithTag(66) as! UILabel
            let collectionLensFlareSponsors = cell.viewWithTag(77) as! UICollectionView
            let lblLensflareVideoTitle = cell.viewWithTag(50) as! UILabel
            let btnShare = cell.viewWithTag(70) as! UIButton
            
            
            print(id)
            btnShare.isHidden = true
            
            
            
            cell.viewWithTag(60)?.isHidden = false
            
            
            if lensFlareVideoDetailsArr.count > 0 {
                btnShare.isHidden = false
                btnShare.addTarget(self, action:#selector(self.btnShareDidTap(sender:)), for: UIControlEvents.touchUpInside)
                
                if ((lensFlareVideoDetailsArr[indexPath.row].dateTime)?.isEmpty)! {
                    lblPublishedVideoDate.text = "Published on: "
                } else {
                    lblPublishedVideoDate.text = "Published on: " + (lensFlareVideoDetailsArr[indexPath.row].dateTime)!
                }
                
                if ((lensFlareVideoDetailsArr[indexPath.row].videoTitle)?.isEmpty)! {
                    lblLensflareVideoTitle.text = " "
                } else {
                    lblLensflareVideoTitle.text = lensFlareVideoDetailsArr[indexPath.row].videoTitle
                }
                
                if (lensFlareVideoDetailsArr[indexPath.row].videoDescription?.isEmpty)! {
                    lblVideoDetails.text = " "
                } else {
                    lblVideoDetails.text = lensFlareVideoDetailsArr[indexPath.row].videoDescription
                }
                
                if (lensFlareVideoDetailsArr[indexPath.row].videoURL == nil) || (lensFlareVideoDetailsArr[indexPath.row].videoURL! == "") || (lensFlareVideoDetailsArr[indexPath.row].videoURL! == "(NULL)") || (lensFlareVideoDetailsArr[indexPath.row].videoURL! == "<NULL>") || ((lensFlareVideoDetailsArr[indexPath.row].videoURL! == "<null>") || (lensFlareVideoDetailsArr[indexPath.row].videoURL! == "(null)") || (lensFlareVideoDetailsArr[indexPath.row].videoURL! == "")) {
                    videoURLStr = lensFlareVideoDetailsArr[indexPath.row].videoUrl1!
                } else {
                    videoURLStr = lensFlareVideoDetailsArr[indexPath.row].videoURL!
                }
                
                
                let videoURL = NSURL (string: videoURLStr)
                print("LensFlare videoURL:: \(String(describing: videoURL)))")
                
                //lensFlareFeatureArr = lensFlareVideoDetailsArr[indexPath.row].featuredBy
                collectionLensFlareSponsors.dataSource = self
                collectionLensFlareSponsors.delegate = self
                collectionLensFlareSponsors.reloadData()
                
                if self.isPlayVideo == true {
                    // initialize the video player with the url
                    webViewVideo.loadRequest(URLRequest(url: videoURL! as URL) as URLRequest)
                } else {
                    lblPlayerView.isHidden = false
                }
            }
            
            return cell
            
            /*if id == self.objSelectedLensFlareItem?.id {
             if ((self.objSelectedLensFlareItem?.dateTime)?.isEmpty)! {
             lblPublishedVideoDate.text = "Published on: "
             } else {
             lblPublishedVideoDate.text = "Published on: " + (self.objSelectedLensFlareItem?.dateTime)!
             }
             
             
             if ((self.objSelectedLensFlareItem?.videoDescription)?.isEmpty)! {
             lblVideoDetails.text = " "
             } else {
             lblVideoDetails.text = self.objSelectedLensFlareItem?.videoDescription
             }
             
             
             
             let videoURL = NSURL (string: (self.objSelectedLensFlareItem?.videoURL)!)
             print("LensFlare videoURL:: \(String(describing: videoURL)))")
             
             
             collectionLensFlareSponsors.dataSource = self
             collectionLensFlareSponsors.delegate = self
             collectionLensFlareSponsors.reloadData()
             
             if self.isPlayVideo == true {
             // initialize the video player with the url
             if (self.objSelectedLensFlareItem?.videoURL?.isEmpty)! {
             lblPlayerView.isHidden = false
             } else {
             lblPlayerView.isHidden = true
             webViewVideo.loadRequest(URLRequest(url: videoURL! as URL) as URLRequest)
             //self.player = AVPlayer(url: URL.init(string: (self.objSelectedLensFlareItem?.videoURL)!)!)
             }
             
             
             //                        avplayerController.player = self.player
             //                        avplayerController.view.frame = CGRect(x: 0, y: 0, width: (playerView?.frame.size.width)!, height: (playerView?.frame.size.height)!)
             //                        avplayerController.showsPlaybackControls = true
             //                        playerView?.addSubview(avplayerController.view)
             //                        self.player?.play()
             } else {
             if (playerView?.subviews.count)! > 0 {
             for objView in (playerView?.subviews)! {
             objView.removeFromSuperview()
             }
             }
             }
             }*/
           
            /*if isLensFlareLoadType == 0  else if isLensFlareLoadType == 1 {
                let idstr = "EPCSeesCell"
                
                // get a reference to our storyboard cell
                var cell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
                if cell == nil {
                    cell = UITableViewCell(style: .default, reuseIdentifier: idstr)
                }
                
                cell.selectionStyle = .none
                
                
                cell.viewWithTag(10)?.layer.masksToBounds = true
                
                // Use the outlet in our custom class to get a reference to the UI Elements in the cell
//                let lblPublishedVideoDate = cell.viewWithTag(2) as! UILabel
//                let lblVideoDetails = cell.viewWithTag(3) as! UILabel
                //let imageViewVideo = cell.viewWithTag(85) as! UIImageView
                let playerView = cell.viewWithTag(65)
                let lblPlayerView = cell.viewWithTag(66) as! UILabel
//                let collectionLensFlareSponsors = cell.viewWithTag(77) as! UICollectionView
                let lblSeesVideoTitle = cell.viewWithTag(51) as! UILabel
                let lblSeesVideoDate = cell.viewWithTag(52) as! UILabel
                let lblSeesVideoDetails = cell.viewWithTag(53) as! UILabel
                let collectionSeesSponsors = cell.viewWithTag(55) as! UICollectionView
                
                
                print(id)
                
//                ===============
                cell.viewWithTag(50)?.isHidden = false
                cell.viewWithTag(60)?.isHidden = true
                if id == self.objSelectedLensFlareItem?.id {
                    print(lensFlareVideoDetailsArr)
                    if lensFlareVideoDetailsArr.count > 0 {
                        if ((self.objSelectedLensFlareItem?.videoTitle)?.isEmpty)! {
                            lblSeesVideoTitle.text = " "
                        } else {
                            lblSeesVideoTitle.text = self.objSelectedLensFlareItem?.videoTitle
                        }
                        /*if ((lensFlareVideoDetailsArr[indexPath.row].publishedDate!).isEmpty) {
                            lblSeesVideoDate.text = "Published on: "
                        } else {
                            lblSeesVideoDate.text = "Published on: " + (lensFlareVideoDetailsArr[indexPath.row].publishedDate!)
                        }*/
                        
                        
                        if ((self.objSelectedLensFlareItem?.videoDescription)?.isEmpty)! {
                            lblSeesVideoDetails.text = " "
                        } else {
                            lblSeesVideoDetails.text = self.objSelectedLensFlareItem?.videoDescription
                        }
                        
                        cell.contentView.bringSubview(toFront: collectionSeesSponsors)
                        collectionSeesSponsors.dataSource = self
                        collectionSeesSponsors.delegate = self
                        collectionSeesSponsors.reloadData()
                        collectionSeesSponsors.backgroundColor = UIColor.red
                        
                        
                        let videoURL = NSURL (string: (lensFlareVideoDetailsArr[indexPath.row].videoURL)!)
                        print("EpcSees videoURL:: \(String(describing: videoURL)))")
                        
                        if self.isPlayVideo == true {
                            // initialize the video player with the url
                            if (lensFlareVideoDetailsArr[indexPath.row].videoURL?.isEmpty)! {
                                lblPlayerView.isHidden = false
                            } else {
                                lblPlayerView.isHidden = true
                                self.player = AVPlayer(url: URL.init(string: (lensFlareVideoDetailsArr[indexPath.row].videoURL)!)!)
                            }
                            //self.player = AVPlayer(url: URL.init(string: (lensFlareVideoDetailsArr[indexPath.row].videoURL)!)!)
                            
                            avplayerController.player = self.player
                            avplayerController.view.frame = CGRect(x: 0, y: 0, width: (playerView?.frame.size.width)!, height: (playerView?.frame.size.height)!)
                            avplayerController.showsPlaybackControls = true
                            playerView?.addSubview(avplayerController.view)
                            self.player?.play()
                        } else {
                            if (playerView?.subviews.count)! > 0 {
                                for objView in (playerView?.subviews)! {
                                    objView.removeFromSuperview()
                                }
                            }
                        }
                    }
                }
                return cell
            } else { //if isLensFlareLoadType == 2 {
                let idstr = "EPCSeesCell"
                
                // get a reference to our storyboard cell
                var cell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
                if cell == nil {
                    cell = UITableViewCell(style: .default, reuseIdentifier: idstr)
                }
                
                cell.selectionStyle = .none
                
                
                cell.viewWithTag(10)?.layer.masksToBounds = true
                
                // Use the outlet in our custom class to get a reference to the UI Elements in the cell
//                let lblPublishedVideoDate = cell.viewWithTag(2) as! UILabel
//                let lblVideoDetails = cell.viewWithTag(3) as! UILabel
                //let imageViewVideo = cell.viewWithTag(85) as! UIImageView
                let playerView = cell.viewWithTag(65)
                let lblPlayerView = cell.viewWithTag(66) as! UILabel
//                let collectionLensFlareSponsors = cell.viewWithTag(77) as! UICollectionView
                let lblSeesVideoTitle = cell.viewWithTag(51) as! UILabel
                let lblSeesVideoDate = cell.viewWithTag(52) as! UILabel
                let lblSeesVideoDetails = cell.viewWithTag(53) as! UILabel
                let collectionSeesSponsors = cell.viewWithTag(55) as! UICollectionView
                
                
                print(id)
                
//                ==========
                
                cell.viewWithTag(50)?.isHidden = false
                cell.viewWithTag(60)?.isHidden = true
                if id == self.objSelectedLexiconItem?.id {
                    print(lexiconDetailsVideoArr)
                    if lexiconDetailsVideoArr.count > 0 {
                        if ((self.objSelectedLexiconItem?.title!)?.isEmpty)! {
                            lblSeesVideoTitle.text = " "
                        } else {
                            lblSeesVideoTitle.text = self.objSelectedLexiconItem?.title!
                        }
                        if ((lexiconDetailsVideoArr[indexPath.row].publishedDate!).isEmpty) {
                            lblSeesVideoDate.text = "Published on: "
                        } else {
                            if (self.objSelectedLexiconItem?.peopleFrom?.isEmpty)! {
                                lblSeesVideoDate.text = "Published on: " + (lexiconDetailsVideoArr[indexPath.row].publishedDate!)
                            } else {
                                lblSeesVideoDate.text = "from " + (self.objSelectedLexiconItem?.peopleFrom!)! + "Published on: " + (lexiconDetailsVideoArr[indexPath.row].publishedDate!)
                            }
                        }
                        
                        
                        if ((self.objSelectedLexiconItem?.description!)?.isEmpty)! {
                            lblSeesVideoDetails.text = " "
                        } else {
                            lblSeesVideoDetails.text = self.objSelectedLexiconItem?.description!
                        }
                        
                        
                        collectionSeesSponsors.dataSource = self
                        collectionSeesSponsors.delegate = self
                        collectionSeesSponsors.reloadData()
                        
                        
                        let videoURL = NSURL (string: (lexiconDetailsVideoArr[indexPath.row].mediaURL)!)
                        print("Lexicon videoURL:: \(String(describing: videoURL)))")
                        
                        if self.isPlayVideo == true {
                            // initialize the video player with the url
                            if (lexiconDetailsVideoArr[indexPath.row].mediaURL?.isEmpty)! {
                                lblPlayerView.isHidden = false
                            } else {
                                lblPlayerView.isHidden = true
                                self.player = AVPlayer(url: URL.init(string: (lexiconDetailsVideoArr[indexPath.row].mediaURL)!)!)
                            }
                            //self.player = AVPlayer(url: URL.init(string: (lensFlareVideoDetailsArr[indexPath.row].videoURL)!)!)
                            
                            avplayerController.player = self.player
                            avplayerController.view.frame = CGRect(x: 0, y: 0, width: (playerView?.frame.size.width)!, height: (playerView?.frame.size.height)!)
                            avplayerController.showsPlaybackControls = true
                            playerView?.addSubview(avplayerController.view)
                            self.player?.play()
                        } else {
                            if (playerView?.subviews.count)! > 0 {
                                for objView in (playerView?.subviews)! {
                                    objView.removeFromSuperview()
                                }
                            }
                        }
                    }
                }
                return cell
            }*/
        } else {
            let idstr = "LensFlareCollectionCell"
            
            // get a reference to our storyboard cell
            var cell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
            if cell == nil {
                cell = UITableViewCell(style: .default, reuseIdentifier: idstr)
            }
            
            let collectionLensFlareVideo = cell.viewWithTag(7) as! UICollectionView
            
            
            // Do any additional setup after loading the view, typically from a nib.
            /*let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            layout.sectionInset = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
            layout.itemSize = CGSize(width: 115, height: 115)
            layout.minimumInteritemSpacing = 10
            collectionLensFlareVideo.collectionViewLayout = layout*/
            
            
            collectionLensFlareVideo.dataSource = self
            collectionLensFlareVideo.delegate = self
            collectionLensFlareVideo.reloadData()
            
            return cell
        }
    }
    
    
    //MARK:: UITableViewDelegate
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return UITableViewAutomaticDimension
        } else {
            return 150
        }
    }
    
    
    /*func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
     return 85.0
     }*/
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    // MARK:: UICollectionViewDataSource protocol
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        /*if isLensFlareLoadType == 0 {
            if collectionView.tag == 7 {
                return lensFlareVideoArr.count
            } else if collectionView.tag == 77 {
                //print("Sponsors No:: \(String(describing: self.objSelectedItem?.sponsorsBy.count))")
                return (self.objSelectedLensFlareItem?.sponsorsBy!.count)!
            }
        } else if isLensFlareLoadType == 1 {
            if collectionView.tag == 7 {
                print("lensFlareVideoArr.count:: ",lensFlareVideoArr.count)
                return lensFlareVideoArr.count
            } else if collectionView.tag == 55 {
                print("Epc Sees.sponsorsBy List ::",self.lensFlareVideoDetailsArr[0].sponsorsBy)
                print("Epc Sees.sponsorsBy Listcount ::",self.lensFlareVideoDetailsArr[0].sponsorsBy?.count as Any)
                return (self.lensFlareVideoDetailsArr[0].sponsorsBy!.count)
            }
        } else if isLensFlareLoadType == 2 {
            if collectionView.tag == 7 {
                print("lexiconDetailsVideoArr.count:: ",lexiconDetailsVideoArr.count)
                return lexiconDetailsVideoArr.count
            } else if collectionView.tag == 55 {
                print(String(describing: self.objSelectedLexiconItem?.sponsorsBy?.count))
                return (self.objSelectedLexiconItem?.sponsorsBy?.count)!
            }
        }*/
        if collectionView.tag == 7 {
            return lensFlareVideoArr.count
        } else if collectionView.tag == 77 {
            //print("Sponsors No:: \(String(describing: self.objSelectedItem?.sponsorsBy.count))")
            return (lensFlareVideoDetailsArr[0].featuredBy?.count)!//(self.objSelectedLensFlareItem?.featuredBy!.count)!
        }
        return 0
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 7 {
            let idstr = "LensFlareVideoCollectionCell"
            
            // get a reference to our storyboard cell
            let lensFlareVideoCell : UICollectionViewCell! = collectionView.dequeueReusableCell(withReuseIdentifier: idstr, for: indexPath)
            
            //Declare the outlet in our custom class to get a reference to the UI Elements in the cell
            let imgVideoThumbnail = lensFlareVideoCell.viewWithTag(1) as! UIImageView
            let lblVideoTitle = lensFlareVideoCell.viewWithTag(2) as! UILabel
            
            let videoImgUrlString = lensFlareVideoArr[indexPath.item].videoImg
            print("videoImgUrlString :: \(String(describing: videoImgUrlString))")
            
            if (videoImgUrlString == nil || videoImgUrlString == "") {
                imgVideoThumbnail.image = UIImage(named: "AppIcon")
            } else {
                let escapedString = videoImgUrlString?.replacingOccurrences(of: " ", with: "%20")
                
                Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgVideoThumbnail)
            }
//            if ((videoImgUrlString?.isEmpty)! || videoImgUrlString != nil) {
//                imgVideoThumbnail.image = UIImage(named: "AppIcon")
//            } else {//if let videoImgUrlString = lensFlareVideoArr[indexPath.row].videoImg {
//                let escapedString = videoImgUrlString?.replacingOccurrences(of: " ", with: "%20")
//
//                Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgVideoThumbnail)
//            }
            
            
            let videoTitleString = lensFlareVideoArr[indexPath.item].videoTitle
            if (videoTitleString?.isEmpty)!  {
                lblVideoTitle.text =  " "
            } else {//if let videoImgUrlString = lensFlareVideoArr[indexPath.row].videoImg {
                lblVideoTitle.text =  videoTitleString
            }
            
            return lensFlareVideoCell!
        } else {//if collectionView.tag == 77 {
            let idstr = "LensFlareSponsorsCollectionCell"
            
            // get a reference to our storyboard cell
            let lensFlareSponsorCell : UICollectionViewCell! = collectionView.dequeueReusableCell(withReuseIdentifier: idstr, for: indexPath)
            
            //Declare the outlet in our custom class
            let imgSponsorThumbnail = lensFlareSponsorCell.viewWithTag(6) as! UIImageView
            
            let sponsorImgUrlString = lensFlareVideoDetailsArr[0].featuredBy?[indexPath.item].compLogo
            //print("profileImgUrlString :: \(String(describing: sponsorImgUrlString))")
            
            if sponsorImgUrlString?.isEmpty == false && sponsorImgUrlString != "" {
                let escapedString = sponsorImgUrlString?.replacingOccurrences(of: " ", with: "%20")
                
                Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgSponsorThumbnail)
            } else {
                imgSponsorThumbnail.image = UIImage(named: "AppIcon")
            }
            
            
            
            return lensFlareSponsorCell!
        }
        /*if isLensFlareLoadType == 0 {
            if collectionView.tag == 7 {
                let idstr = "LensFlareVideoCollectionCell"
                
                // get a reference to our storyboard cell
                let lensFlareVideoCell : UICollectionViewCell! = collectionView.dequeueReusableCell(withReuseIdentifier: idstr, for: indexPath)
                
                //Declare the outlet in our custom class to get a reference to the UI Elements in the cell
                let imgVideoThumbnail = lensFlareVideoCell.viewWithTag(1) as! UIImageView
                let lblVideoTitle = lensFlareVideoCell.viewWithTag(2) as! UILabel
                
                
                
                let videoImgUrlString = lensFlareVideoArr[indexPath.item].videoImg
                if (videoImgUrlString?.isEmpty)!  {
                    imgVideoThumbnail.image = UIImage(named: "AppIcon")
                } else {//if let videoImgUrlString = lensFlareVideoArr[indexPath.row].videoImg {
                    let escapedString = videoImgUrlString?.replacingOccurrences(of: " ", with: "%20")
                    
                    Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgVideoThumbnail)
                }
                
                
                let videoTitleString = lensFlareVideoArr[indexPath.item].videoTitle
                if (videoTitleString?.isEmpty)!  {
                    lblVideoTitle.text =  " "
                } else {//if let videoImgUrlString = lensFlareVideoArr[indexPath.row].videoImg {
                    lblVideoTitle.text =  videoTitleString
                }
                
                return lensFlareVideoCell!
            } else {//if collectionView.tag == 77 {
                let idstr = "LensFlareSponsorsCollectionCell"
                
                // get a reference to our storyboard cell
                let lensFlareSponsorCell : UICollectionViewCell! = collectionView.dequeueReusableCell(withReuseIdentifier: idstr, for: indexPath)
                
                //Declare the outlet in our custom class
                let imgSponsorThumbnail = lensFlareSponsorCell.viewWithTag(6) as! UIImageView
                
                let sponsorImgUrlString = self.objSelectedLensFlareItem?.sponsorsBy![indexPath.item].compLogo
                //print("profileImgUrlString :: \(String(describing: sponsorImgUrlString))")
                
                if sponsorImgUrlString?.isEmpty == false && sponsorImgUrlString != "" {
                    let escapedString = sponsorImgUrlString?.replacingOccurrences(of: " ", with: "%20")
                    
                    Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgSponsorThumbnail)
                } else {
                    imgSponsorThumbnail.image = UIImage(named: "AppIcon")
                }
                
                
                
                return lensFlareSponsorCell!
            }
        } else if isLensFlareLoadType == 1 {
            print("CollectionView tag ============:\(collectionView.tag)")
            if collectionView.tag == 7 {
                let idstr = "LensFlareVideoCollectionCell"
                
                // get a reference to our storyboard cell
                let lensFlareVideoCell : UICollectionViewCell! = collectionView.dequeueReusableCell(withReuseIdentifier: idstr, for: indexPath)
                
                //Declare the outlet in our custom class
                let imgVideoThumbnail = lensFlareVideoCell.viewWithTag(1) as! UIImageView
                let lblVideoTitle = lensFlareVideoCell.viewWithTag(2) as! UILabel
                
                let videoImgUrlString = lensFlareVideoArr[indexPath.item].videoImg
                if (videoImgUrlString?.isEmpty)!  {
                    imgVideoThumbnail.image = UIImage(named: "AppIcon")
                } else {//if let videoImgUrlString = lensFlareVideoArr[indexPath.row].videoImg {
                    let escapedString = videoImgUrlString?.replacingOccurrences(of: " ", with: "%20")
                    
                    Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgVideoThumbnail)
                }
                
                
                let videoTitleString = lensFlareVideoArr[indexPath.item].videoTitle
                if (videoTitleString?.isEmpty)!  {
                    lblVideoTitle.text =  " "
                } else {//if let videoImgUrlString = lensFlareVideoArr[indexPath.row].videoImg {
                    lblVideoTitle.text =  videoTitleString
                }
                return lensFlareVideoCell
            } else {
                let idstr = "SeesDetailsSponsorsCollectionCell"
                
                // get a reference to our storyboard cell
                let lensFlareSponsorCell : UICollectionViewCell! = collectionView.dequeueReusableCell(withReuseIdentifier: idstr, for: indexPath)
                
                //Declare the outlet in our custom class
                let imgSponsorThumbnail = lensFlareSponsorCell.viewWithTag(6) as! UIImageView
                
                let sponsorImgUrlString = self.objSelectedLensFlareItem?.sponsorsBy![indexPath.item].compLogo
                print("sponsorImgUrlString :: \(String(describing: sponsorImgUrlString))")
                
                if sponsorImgUrlString?.isEmpty == false && sponsorImgUrlString != "" {
                    let escapedString = sponsorImgUrlString?.replacingOccurrences(of: " ", with: "%20")
                    
                    Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgSponsorThumbnail)
                } else {
                    imgSponsorThumbnail.image = UIImage(named: "AppIcon")
                }
                return lensFlareSponsorCell!
            }
            
        } else {
            if collectionView.tag == 7 {
                let idstr = "LensFlareVideoCollectionCell"
                
                // get a reference to our storyboard cell
                let lensFlareVideoCell : UICollectionViewCell! = collectionView.dequeueReusableCell(withReuseIdentifier: idstr, for: indexPath)
                
                //Declare the outlet in our custom class
                let imgVideoThumbnail = lensFlareVideoCell.viewWithTag(1) as! UIImageView
                let lblVideoTitle = lensFlareVideoCell.viewWithTag(2) as! UILabel
                
                
                let videoImgUrlString = lexiconDetailsVideoArr[indexPath.item].thumbnailShort
                if (videoImgUrlString?.isEmpty)!  {
                    imgVideoThumbnail.image = UIImage(named: "AppIcon")
                } else {//if let videoImgUrlString = lensFlareVideoArr[indexPath.row].videoImg {
                    let escapedString = videoImgUrlString?.replacingOccurrences(of: " ", with: "%20")
                    
                    Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgVideoThumbnail)
                }
                
                let videoTitleString = lexiconDetailsVideoArr[indexPath.item].title
                if (videoTitleString?.isEmpty)!  {
                    lblVideoTitle.text =  " "
                } else {//if let videoImgUrlString = lensFlareVideoArr[indexPath.row].videoImg {
                    lblVideoTitle.text =  videoTitleString
                }
                return lensFlareVideoCell
            } else {
                let idstr = "SeesDetailsSponsorsCollectionCell"
                
                // get a reference to our storyboard cell
                let lensFlareSponsorCell : UICollectionViewCell! = collectionView.dequeueReusableCell(withReuseIdentifier: idstr, for: indexPath)
                
                //Declare the outlet in our custom class
                let imgSponsorThumbnail = lensFlareSponsorCell.viewWithTag(6) as! UIImageView
                
                let sponsorImgUrlString = self.objSelectedLensFlareItem?.sponsorsBy![indexPath.item].compLogo
                print("sponsorImgUrlString :: \(String(describing: sponsorImgUrlString))")
                
                if sponsorImgUrlString?.isEmpty == false && sponsorImgUrlString != "" {
                    let escapedString = sponsorImgUrlString?.replacingOccurrences(of: " ", with: "%20")
                    
                    Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgSponsorThumbnail)
                } else {
                    imgSponsorThumbnail.image = UIImage(named: "AppIcon")
                }
                return lensFlareSponsorCell!
            }
            
        }*/
        
    }
    
    
    /*func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView.tag == 7 {
            let idstr = "LensFlareVideoCollectionCell"
            
            // get a reference to our storyboard cell
            let lensFlareVideoCell : UICollectionViewCell! = collectionView.dequeueReusableCell(withReuseIdentifier: idstr, for: indexPath)
            
            //Declare
            let imgVideoThumbnail = lensFlareVideoCell.viewWithTag(1) as! UIImageView
            let lblVideoTitle = lensFlareVideoCell.viewWithTag(2) as! UILabel
            
            
            
            if isLensFlareLoadType == 0 {
                let videoImgUrlString = lensFlareVideoArr[indexPath.item].videoImg
                if (videoImgUrlString?.isEmpty)!  {
                    imgVideoThumbnail.image = UIImage(named: "AppIcon")
                } else {//if let videoImgUrlString = lensFlareVideoArr[indexPath.row].videoImg {
                    let escapedString = videoImgUrlString?.replacingOccurrences(of: " ", with: "%20")
                    
                    Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgVideoThumbnail)
                }
                
                
                let videoTitleString = lensFlareVideoArr[indexPath.item].videoTitle
                if (videoTitleString?.isEmpty)!  {
                    lblVideoTitle.text =  " "
                } else {//if let videoImgUrlString = lensFlareVideoArr[indexPath.row].videoImg {
                    lblVideoTitle.text =  videoTitleString
                }
            } else if isLensFlareLoadType == 1 {
                let videoImgUrlString = lensFlareVideoArr[indexPath.item].videoImg
                if (videoImgUrlString?.isEmpty)!  {
                    imgVideoThumbnail.image = UIImage(named: "AppIcon")
                } else {//if let videoImgUrlString = lensFlareVideoArr[indexPath.row].videoImg {
                    let escapedString = videoImgUrlString?.replacingOccurrences(of: " ", with: "%20")
                    
                    Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgVideoThumbnail)
                }
                
                
                let videoTitleString = lensFlareVideoArr[indexPath.item].videoTitle
                if (videoTitleString?.isEmpty)!  {
                    lblVideoTitle.text =  " "
                } else {//if let videoImgUrlString = lensFlareVideoArr[indexPath.row].videoImg {
                    lblVideoTitle.text =  videoTitleString
                }
            } else if isLensFlareLoadType == 2 {
                let videoImgUrlString = lexiconDetailsVideoArr[indexPath.item].thumbnailShort
                if (videoImgUrlString?.isEmpty)!  {
                    imgVideoThumbnail.image = UIImage(named: "AppIcon")
                } else {//if let videoImgUrlString = lensFlareVideoArr[indexPath.row].videoImg {
                    let escapedString = videoImgUrlString?.replacingOccurrences(of: " ", with: "%20")
                    
                    Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgVideoThumbnail)
                }
                
                
                let videoTitleString = lexiconDetailsVideoArr[indexPath.item].title
                if (videoTitleString?.isEmpty)!  {
                    lblVideoTitle.text =  " "
                } else {//if let videoImgUrlString = lensFlareVideoArr[indexPath.row].videoImg {
                    lblVideoTitle.text =  videoTitleString
                }
            }
            
            return lensFlareVideoCell!
        }
        else if collectionView.tag == 77 {
            let idstr = "LensFlareSponsorsCollectionCell"
            
            // get a reference to our storyboard cell
            let lensFlareSponsorCell : UICollectionViewCell! = collectionView.dequeueReusableCell(withReuseIdentifier: idstr, for: indexPath)
            
            
            let imgSponsorThumbnail = lensFlareSponsorCell.viewWithTag(6) as! UIImageView
            
            let sponsorImgUrlString = self.objSelectedLensFlareItem?.sponsorsBy![indexPath.item].compLogo
            //print("profileImgUrlString :: \(String(describing: sponsorImgUrlString))")
            
            if sponsorImgUrlString?.isEmpty == false && sponsorImgUrlString != "" {
                let escapedString = sponsorImgUrlString?.replacingOccurrences(of: " ", with: "%20")
                
                Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgSponsorThumbnail)
            } else {
                imgSponsorThumbnail.image = UIImage(named: "AppIcon")
            }
            
            
            
            return lensFlareSponsorCell!
        } else {           //if collectionView.tag == 55
            let idstr = "SeesDetailsSponsorsCollectionCell"
            
            // get a reference to our storyboard cell
            let lensFlareSponsorCell : UICollectionViewCell! = collectionView.dequeueReusableCell(withReuseIdentifier: idstr, for: indexPath)
            
            
            if isLensFlareLoadType == 2 {
                let imgSponsorThumbnail = lensFlareSponsorCell.viewWithTag(6) as! UIImageView
                
                let sponsorImgUrlString = lexiconDetailsVideoArr[0].sponsorsBy![indexPath.item].compLogo
                print("sponsorImgUrlString :: \(String(describing: sponsorImgUrlString))")
                
                if sponsorImgUrlString?.isEmpty == false && sponsorImgUrlString != "" {
                    let escapedString = sponsorImgUrlString?.replacingOccurrences(of: " ", with: "%20")
                    
                    Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgSponsorThumbnail)
                } else {
                    imgSponsorThumbnail.image = UIImage(named: "AppIcon")
                }
            } else if isLensFlareLoadType == 1 {
                let imgSponsorThumbnail = lensFlareSponsorCell.viewWithTag(6) as! UIImageView
                
                let sponsorImgUrlString = self.objSelectedLensFlareItem?.sponsorsBy![indexPath.item].compLogo
                print("sponsorImgUrlString :: \(String(describing: sponsorImgUrlString))")
                
                if sponsorImgUrlString?.isEmpty == false && sponsorImgUrlString != "" {
                    let escapedString = sponsorImgUrlString?.replacingOccurrences(of: " ", with: "%20")
                    
                    Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgSponsorThumbnail)
                } else {
                    imgSponsorThumbnail.image = UIImage(named: "AppIcon")
                }
            }
            return lensFlareSponsorCell!
            
        }
    }*/
    
    // MARK:: UICollectionViewDelegate protocol
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 77 {
            return CGSize(width: 45, height: 45)
        } else if collectionView.tag == 7 {
            //let cellWidth = (self.view.frame.size.width-50)/2
            return CGSize(width: 125, height: 125)
        }
        return CGSize(width: 0, height: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 7 {
            /*if isLensFlareLoadType == 0 {
                self.objSelectedLensFlareItem = self.lensFlareVideoArr[indexPath.item]
                id = (self.objSelectedLensFlareItem?.id)!
                print("selected id:: ",String(id))
            } else if isLensFlareLoadType == 2 {
                self.objSelectedLexiconItem = self.lexiconDetailsVideoArr[indexPath.item]
                id = (self.objSelectedLexiconItem?.id)!
                print("selected id:: ",String(id))
            }*/
            //self.objSelectedLensFlareItem = self.lensFlareVideoArr[indexPath.item]
            id = self.lensFlareVideoArr[indexPath.item].id //(self.objSelectedLensFlareItem?.id)!
            print("selected id:: ",String(id))
            
            
            var tempDict = [String:String]()
            tempDict["token"] = EpicLensConstantsVariables.accessToken
            tempDict["id"] = String(id)
            lensFlareDispatcher.GetLensFlareListByID(paramDict: tempDict)
            
            self.isPlayVideo = true
            //self.tableLensFlare.reloadData()
            self.tableLensFlare.reloadRows(at: [IndexPath.init(row: 0, section: 0)], with: .none)
            /*let cell = self.tableLensFlare.cellForRow(at: indexPath)
            let collectionSponsor = cell?.viewWithTag(55) as! UICollectionView
            collectionSponsor.reloadData()*/
            /* var tempDict = [String:String]()
             tempDict["token"] = EpicLensConstantsVariables.accessToken
             tempDict["id"] = String(id)
             lensFlareDispatcher.GetLensFlareListByID(paramDict: tempDict)*/
            //self.objLoginDispatcher.GetuserDetails()*/
        }
    }
}
