//
//  EPCSeesDetailsController.swift
//  EpcLensProject
//
//  Created by BlueHorse MacBook Pro on 23/07/18.
//  Copyright © 2018 Moumita Datta. All rights reserved.
//

import UIKit

class EPCSeesDetailsController: UIViewController, SeesDetailsProtocol {
    
    //Navigation Menu
    @IBOutlet var leftMenuBtn: UIButton!
    @IBOutlet weak var tableSeesDetails: UITableView!
    
    private var isPlayVideo = false
    
    
    var seesDetailsDispatcher = SeesDetailsDispatcher()
    var seesVideoArr = [LensFlareItemList]()
    var seesDetailsVideoArr = [EPCSeesDetailsItemList]()
   
    
    //var lensFlareFeatureArr = [LensFlareItemList]()
    
    
    var id:Int = 0
    var videoURLStr : String = ""
    
    @IBOutlet var collectionViewHeight: NSLayoutConstraint!
    @IBOutlet var lblSponsorHeight: NSLayoutConstraint!
    
    
    //Mark:: AnimatedLoader
    let loaderController = EpcLensLoaderViewController(nibName: "EpcLensLoaderViewController", bundle: nil)
    
    
    
    //MARK:: UIView
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        seesDetailsDispatcher.delegate = self
        
        
        var tempDict = [String:String]()
        tempDict["token"] = EpicLensConstantsVariables.accessToken
        tempDict["id"] = String(id)
        seesDetailsDispatcher.GetSeesDetails(paramDict: tempDict)
        
        
        
        // Start Loaded
        loaderController.showEpcLoader(_sourceView: self)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:: IBAction
    @IBAction func btnBackDidTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnShareDidTap(sender: UIButton) {
        let textToShare = "Swift is awesome!  Check out this website about it!"
        
        if let myWebsite = NSURL(string: "http://www.codingexplorer.com/") {
            let objectsToShare = [textToShare, myWebsite] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            //New Excluded Activities Code
            activityVC.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.addToReadingList]
            
            
            activityVC.popoverPresentationController?.sourceView = sender
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    
    //MARK:: LensFlareListProtocol
    func didUpdateControllerWithSeesDetails(seesDetails: LensFlareListModel) {
        DispatchQueue.main.async {
            if seesDetails.data.sucess {
                self.seesVideoArr = seesDetails.data.itemList!
                if self.seesVideoArr.count > 0 {
                    self.tableSeesDetails.delegate = self
                    self.tableSeesDetails.dataSource = self
                    self.tableSeesDetails.reloadData()
                }
                // Stop Loaded
                self.loaderController.hideEpcLoader()
            }
        }
    }
    
    func didUpdateControllerWithEPCSeesByGalleryID(seesByGalleryID :EPCSeesDetailsModel) {
        DispatchQueue.main.async {
            if seesByGalleryID.data.sucess {
                self.seesDetailsVideoArr = seesByGalleryID.data.itemList!
                
                if self.seesDetailsVideoArr.count > 0 {
                    print("lensFlareVideoDetailsArr ::",self.seesDetailsVideoArr)
                    self.tableSeesDetails.delegate = self
                    self.tableSeesDetails.dataSource = self
                    self.tableSeesDetails.reloadData()
                }
                
                // Stop Loaded
                self.loaderController.hideEpcLoader()
            }
        }
    }
    
}



extension EPCSeesDetailsController : UITableViewDataSource, UITableViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    //MARK:: UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.section == 0) {
            let idstr = "SeesCell"
            
            // get a reference to our storyboard cell
            var seesCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
            if seesCell == nil {
                seesCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
            }
            
            seesCell.selectionStyle = .none
            
            
            seesCell.viewWithTag(10)?.layer.masksToBounds = true
            
            // Use the outlet in our custom class to get a reference to the UI Elements in the cell
            let lblPublishedVideoDate = seesCell.viewWithTag(2) as! UILabel
            let lblVideoDetails = seesCell.viewWithTag(3) as! UILabel
            let webViewVideo = seesCell.viewWithTag(75) as! UIWebView
            let lblPlayerView = seesCell.viewWithTag(66) as! UILabel
            let collectionSeesSponsors = seesCell.viewWithTag(77) as! UICollectionView
            let lblLensflareVideoTitle = seesCell.viewWithTag(50) as! UILabel
            let btnShare = seesCell.viewWithTag(70) as! UIButton
            
            seesCell.viewWithTag(60)?.isHidden = false
            
            //print(id)
            
            btnShare.addTarget(self, action:#selector(self.btnShareDidTap(sender:)), for: UIControlEvents.touchUpInside)
            
            
            
            if seesDetailsVideoArr.count > 0 {
                if ((seesDetailsVideoArr[indexPath.row].videoTitle)?.isEmpty)! {
                    lblPublishedVideoDate.text = ""
                } else {
                    lblPublishedVideoDate.text = seesDetailsVideoArr[indexPath.row].videoTitle!
                }
                
                if (seesDetailsVideoArr[indexPath.row].publishedDate?.isEmpty)! {
                    lblLensflareVideoTitle.text = "Published on: "
                } else {
                    lblLensflareVideoTitle.text = "published on: " + seesDetailsVideoArr[indexPath.row].publishedDate!
                }
                
                if (seesDetailsVideoArr[indexPath.row].videoDescription?.isEmpty)! {
                    lblVideoDetails.text = " "
                } else {
                    lblVideoDetails.text = seesDetailsVideoArr[indexPath.row].videoDescription
                }
                
                if (seesDetailsVideoArr[indexPath.row].videoURL == nil) || (seesDetailsVideoArr[indexPath.row].videoURL! == "") || (seesDetailsVideoArr[indexPath.row].videoURL! == "(NULL)") || (seesDetailsVideoArr[indexPath.row].videoURL! == "<NULL>") || ((seesDetailsVideoArr[indexPath.row].videoURL! == "<null>") || (seesDetailsVideoArr[indexPath.row].videoURL! == "(null)") || (seesDetailsVideoArr[indexPath.row].videoURL! == "")) {
                    videoURLStr = seesDetailsVideoArr[indexPath.row].videoUrl1!
                } else {
                    videoURLStr = seesDetailsVideoArr[indexPath.row].videoURL!
                }
                
                
                let videoURL = NSURL (string: videoURLStr)
                print("LensFlare videoURL:: \(String(describing: videoURL)))")
                
                
                collectionSeesSponsors.dataSource = self
                collectionSeesSponsors.delegate = self
                collectionSeesSponsors.reloadData()
                
                if self.isPlayVideo == true {
                    // initialize the video player with the url
                    webViewVideo.loadRequest(URLRequest(url: videoURL! as URL) as URLRequest)
                } else {
                    lblPlayerView.isHidden = false
                }
            }
            
            return seesCell
        } else {
            let idstr = "SeesCollectionCell"
            
            // get a reference to our storyboard cell
            var seesCollectionCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
            if seesCollectionCell == nil {
                seesCollectionCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
            }
            
            //Declare the outlet in our custom class
            let collectionSeesVideo = seesCollectionCell.viewWithTag(7) as! UICollectionView
            
            
            
            collectionSeesVideo.dataSource = self
            collectionSeesVideo.delegate = self
            collectionSeesVideo.reloadData()
            
            return seesCollectionCell
        }
    }
    
    
    //MARK:: UITableViewDelegate
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return UITableViewAutomaticDimension
        } else {
            return 150
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    // MARK:: UICollectionViewDataSource protocol
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 7 {
            return seesVideoArr.count
        } else if collectionView.tag == 77 {
            return (seesDetailsVideoArr[0].sponsorsBy?.count)!
        }
        return 0
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 7 {
            let idstr = "SeesVideoCollectionCell"
            
            // get a reference to our storyboard cell
            let lensFlareVideoCell : UICollectionViewCell! = collectionView.dequeueReusableCell(withReuseIdentifier: idstr, for: indexPath)
            
            //Declare the outlet in our custom class to get a reference to the UI Elements in the cell
            let imgVideoThumbnail = lensFlareVideoCell.viewWithTag(1) as! UIImageView
            let lblVideoTitle = lensFlareVideoCell.viewWithTag(2) as! UILabel
            
            
            
            let videoImgUrlString = seesVideoArr[indexPath.item].videoImg
            
            if (videoImgUrlString?.isEmpty)!  {
                imgVideoThumbnail.image = UIImage(named: "AppIcon")
            } else {
                let escapedString = videoImgUrlString?.replacingOccurrences(of: " ", with: "%20")
                Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgVideoThumbnail)
            }
            
            
            let videoTitleString = seesVideoArr[indexPath.item].videoTitle
            if (videoTitleString?.isEmpty)!  {
                lblVideoTitle.text =  " "
            } else {//if let videoImgUrlString = lensFlareVideoArr[indexPath.row].videoImg {
                lblVideoTitle.text =  videoTitleString
            }
            
            return lensFlareVideoCell!
        } else {//if collectionView.tag == 77 {
            let idstr = "SeesSponsorsCollectionCell"
            
            // get a reference to our storyboard cell
            let lensFlareSponsorCell : UICollectionViewCell! = collectionView.dequeueReusableCell(withReuseIdentifier: idstr, for: indexPath)
            
            //Declare the outlet in our custom class
            let imgSponsorThumbnail = lensFlareSponsorCell.viewWithTag(6) as! UIImageView
            
            
            if (seesDetailsVideoArr[0].sponsorsBy?.count)! > 0 {
                let sponsorImgUrlString = seesDetailsVideoArr[0].sponsorsBy?[indexPath.item].compLogo
                
                if sponsorImgUrlString?.isEmpty == false && sponsorImgUrlString != "" {
                    let escapedString = sponsorImgUrlString?.replacingOccurrences(of: " ", with: "%20")
                    Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgSponsorThumbnail)
                } else {
                    imgSponsorThumbnail.image = UIImage(named: "AppIcon")
                }
            }
            
            return lensFlareSponsorCell!
        }
    }
    
    // MARK:: UICollectionViewDelegate protocol
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 77 {
            return CGSize(width: 45, height: 45)
        } else if collectionView.tag == 7 {
            return CGSize(width: 125, height: 125)
        }
        return CGSize(width: 0, height: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 7 {
            id = self.seesVideoArr[indexPath.item].id
            print("selected id:: ",String(id))
            
            
            var tempDict = [String:String]()
            tempDict["token"] = EpicLensConstantsVariables.accessToken
            tempDict["id"] = String(id)
            seesDetailsDispatcher.GetSeesDetailsByGalleryId(paramDict: tempDict)
            
            self.isPlayVideo = true
            self.tableSeesDetails.reloadRows(at: [IndexPath.init(row: 0, section: 0)], with: .none)
        }
    }
}

