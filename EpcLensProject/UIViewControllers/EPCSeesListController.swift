//
//  EPCSeesListController.swift
//  EpcLensProject
//
//  Created by Moumita Datta on 15/05/18.
//  Copyright © 2018 Moumita Datta. All rights reserved.
//

import UIKit

class EPCSeesListController: UIViewController, EPCSeesListProtocol {
    
    @IBOutlet weak var collectionEPCSees: UICollectionView!
    
    var epcSeesDispatcher = EPCSeesDispatcher()
    var epcSeesListArr = [EPCSeesItemList]()
    
    
    //Mark:: AnimatedLoader
    let loaderController = EpcLensLoaderViewController(nibName: "EpcLensLoaderViewController", bundle: nil)
    
    //MARK:: UIView
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        epcSeesDispatcher.delegate = self
        var tempDict = [String:String]()
        tempDict["token"] = EpicLensConstantsVariables.accessToken
        tempDict["page"] = "0"
        epcSeesDispatcher.GetEPCSeesList(paramDict: tempDict)
        
        
        
        // Start Loaded
        loaderController.showEpcLoader(_sourceView: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:: IBAction
    @IBAction func btnLeftMenuDidTap(_ sender: Any) {
        let elDrawer = self.navigationController?.parent as! KYDrawerController
        elDrawer.setDrawerState(.opened, animated: true)
    }
    
    @IBAction func btnRightMenuDidTap(_ sender: UIButton) {
        
        //        let originalTransform = sender.transform
        //        let scaledTransform = originalTransform.scaledBy(x: 0.2, y: 0.2)
        //        let scaledAndTranslatedTransform = scaledTransform.translatedBy(x: 0.0, y: -250.0)
        //        UIView.animate(withDuration: 0.7, animations: {
        //            sender.transform = scaledAndTranslatedTransform
        //        })
        
        let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
        let objReportPopup = storyboardMain.instantiateViewController(withIdentifier: "RightMenuController") as! RightMenuController
        objReportPopup.showPopup(_controller: self)
    }
    
    
    
    //MARK:: EPCSeesListProtocol
    func didUpdateControllerWithObjEPCSeesList(epcSeesList: EPCSeesListModel) {
        DispatchQueue.main.async {
            if epcSeesList.data.sucess {
                self.epcSeesListArr = epcSeesList.data.itemList
                self.collectionEPCSees.delegate = self
                self.collectionEPCSees.dataSource = self
                self.collectionEPCSees.reloadData()
                // Stop Loaded
                self.loaderController.hideEpcLoader()
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension EPCSeesListController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    // MARK:: UICollectionViewDataSource protocol
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //return epcSeesVideoArr.count
        return epcSeesListArr.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let idstr = "EPCSeesCell"
        // get a reference to our storyboard cell
        let epcSeesCell : UICollectionViewCell! = collectionView.dequeueReusableCell(withReuseIdentifier: idstr, for: indexPath)
        
        
        // Use the outlet in our custom class to get a reference to the UI Elements in the cell
        let imgVideoThumbnail = epcSeesCell.viewWithTag(1) as! UIImageView
        let lblDescription = epcSeesCell.viewWithTag(2) as! UILabel
        let lblTitle = epcSeesCell.viewWithTag(3) as! UILabel
        
        if epcSeesListArr[indexPath.item].image?.isEmpty == false && epcSeesListArr[indexPath.item].image != "" {
            let escapedString = epcSeesListArr[indexPath.item].image?.replacingOccurrences(of: " ", with: "%20")
            Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgVideoThumbnail)
        } else {
            imgVideoThumbnail.image = UIImage(named: "AppIcon")
        }
        
        
        if (epcSeesListArr[indexPath.item].title?.isEmpty)! {
            lblTitle.text =  " "
        } else {
            lblTitle.text =  epcSeesListArr[indexPath.item].title
        }
        
        if (epcSeesListArr[indexPath.item].description?.isEmpty)! {
            lblDescription.text = " "
        } else {
            lblDescription.text = epcSeesListArr[indexPath.item].description
        }
        
        return epcSeesCell!
    }
    
    
    // MARK:: UICollectionViewDelegate protocol
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth = (self.view.frame.size.width-30)/2
        return CGSize(width: cellWidth, height: 225)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
        print("You epcSeesListArr[indexPath.row].id cell #\(epcSeesListArr[indexPath.item].id)!")
        
        let storyboardLensflare = UIStoryboard(name: "EPCSees", bundle: nil)
        let ePCSeesDetailsVC = storyboardLensflare.instantiateViewController(withIdentifier: "EPCSeesDetailsVC") as! EPCSeesDetailsController
        ePCSeesDetailsVC.id = epcSeesListArr[indexPath.item].id
        //lensFlareVC.isLensFlareLoadType = 1
        //UserDefaults.standard.set(false, forKey: "isLensflare")
        self.navigationController?.pushViewController(ePCSeesDetailsVC, animated: false)
    }
    
    
}
