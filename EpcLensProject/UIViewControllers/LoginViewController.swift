//
//  LoginViewController.swift
//  EpcLensProject
//
//  Created by Sudipta Biswas on 28/04/18.
//  Copyright © 2018 Sudipta Biswas. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    let objLoginDispatcher = LoginDispatcher()
    
    @IBOutlet weak var viewLoginBack: UIView!
    
    @IBOutlet weak var btnLogin: UIButton!
    
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBOutlet weak var txtPassword: UITextField!
    
    //Mark:: AnimatedLoader
    let loaderController = EpcLensLoaderViewController(nibName: "EpcLensLoaderViewController", bundle: nil)
    
    
    //MARK:: UIView
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureLogin()
    }
    
    func configureLogin() {
        txtEmail.text = "abdul.barik@gmail.com"
        txtPassword.text = "0000"
        /*txtEmail.text = "admin@2webdesign.com"
        txtPassword.text = "passw0rd"*/
        
        
        objLoginDispatcher.delegate = self
        viewLoginBack.layer.cornerRadius = 5.0
        btnLogin.layer.cornerRadius = 12.0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- User Login
    @IBAction func login(_ sender: Any) {
        self.view.endEditing(true)
        if (txtEmail.text?.trim().isEmpty)! {
            EpcLensController.sharedInstance.showAlert(_sourceController: self, _msg: EpcLensMessages.login_InvalidEmail)
        } else if (txtPassword.text?.trim().isEmpty)! {
            EpcLensController.sharedInstance.showAlert(_sourceController: self, _msg: EpcLensMessages.login_blankPassword)
        } else {
            // Start Loaded
            loaderController.showEpcLoader(_sourceView: self)
            objLoginDispatcher.userLogin(_emial: (txtEmail.text?.trim())!, _password: (txtPassword.text?.trim())!)
        }
    }
    
    // MARK:- Background Touch
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}

extension LoginViewController:UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension LoginViewController:LoginDispatchProtocol {
    func didupdateControllerFromLoginDispatch(_isSuccess: Bool) {
        if _isSuccess {
            print("Successfull logged In")
            DispatchQueue.main.async {
                // Stop Loaded
                self.loaderController.hideEpcLoader()
                
                let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
                self.navigationController?.pushViewController((storyboardMain.instantiateViewController(withIdentifier: "KYDrawerController")), animated: true)
            }
            self.objLoginDispatcher.GetuserDetails()
        } else {
            print("Failed to logged in")
        }
    }
    
    func didupdateUserDetails(_isSuccess: Bool) {
        if _isSuccess  {
            print("Got user Details")
            print("User name : \(EpicLensConstantsVariables.userDetailsData?.userName ?? "")")
            DispatchQueue.main.async {
                // Stop Loaded
                self.loaderController.hideEpcLoader()
            }
        } else {
            print("Failed to retrived user details")
        }
    }
}

