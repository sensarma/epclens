//
//  HelpLexiconController.swift
//  EpcLensProject
//
//  Created by Moumita Datta on 28/05/18.
//  Copyright © 2018 Moumita Datta. All rights reserved.
//

import UIKit

class HelpLexiconController: UIViewController {
    
    @IBOutlet weak var tableHelpLexicon: UITableView!
    
//    var lexiconDispatcher = LexiconDispatcher()
//    var lexiconsMenuListArr = [LexiconsMenuList]()
    
    
    //Mark:: AnimatedLoader
    let loaderController = EpcLensLoaderViewController(nibName: "EpcLensLoaderViewController", bundle: nil)
    
    
    
    //MARK:: UIView
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:: IBAction
    @IBAction func btnBackDidTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension HelpLexiconController : UITableViewDataSource, UITableViewDelegate {
    
    //MARK:: UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else if section == 1 {
            return 5
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.section == 0) {
            // get a reference to our storyboard cell
            var helpLexiconHeaderCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: "HelpLexiconHeaderCell")!
            if helpLexiconHeaderCell == nil {
                helpLexiconHeaderCell = UITableViewCell(style: .default, reuseIdentifier: "HelpLexiconHeaderCell")
            }
            
            return helpLexiconHeaderCell
        } else if (indexPath.section == 1) {
            // get a reference to our storyboard cell
            var helpLexiconCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: "HelpLexiconTableCell")!
            if helpLexiconCell == nil {
                helpLexiconCell = UITableViewCell(style: .default, reuseIdentifier: "HelpLexiconTableCell")
            }
            
            // Use the outlet in our custom class to get a reference to the UI Elements in the cell
            let lblTitleField = helpLexiconCell.viewWithTag(10) as! UILabel
            let textView = helpLexiconCell.viewWithTag(20) as! UITextView
            
            
            if indexPath.row == 0 {
                lblTitleField.text = "title"
            } else if indexPath.row == 1 {
                lblTitleField.text = "description"
//                let newFrame = CGRect(x: 15, y: 40, width: self.view.frame.size.width - 30, height: 70)
//                textView.frame = newFrame
            } else if indexPath.row == 2 {
                lblTitleField.text = "companies in airticle"
            } else if indexPath.row == 3 {
                lblTitleField.text = "projects in airticle"
            } else {
                lblTitleField.text = "people in airticle"
            }
            
            
            textView.layer.borderColor = UIColor.black.cgColor
            textView.layer.borderWidth = 0.5
            
            
            return helpLexiconCell
        } else {
            // get a reference to our storyboard cell
            var helpLexiconButtonCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: "HelpLexiconButtonTableCell")!
            if helpLexiconButtonCell == nil {
                helpLexiconButtonCell = UITableViewCell(style: .default, reuseIdentifier: "HelpLexiconButtonTableCell")
            }
            
//            if lexiconMenuHelpusArr.count > 0 {
//                let lblDetails = lexiconCell.viewWithTag(4) as! UILabel
//                lblDetails.text = lexiconMenuHelpusArr[indexPath.row].title
//            }
//
//            if (indexPath.row == (lexiconMenuHelpusArr.count-1)) {
//                lexiconCell.viewWithTag(32)?.layer.cornerRadius = 10
//            }
            return helpLexiconButtonCell
        }
    }
    
    
    //MARK:: UITableViewDelegate
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if indexPath.section == 0 {
//            return 50
//        } else {
//            return 35
//        }
//    }
    

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 50
        } else if indexPath.section == 1 {
            return UITableViewAutomaticDimension
//            if indexPath.row == 1 {
//                return 120
//            } else {
//                return 90
//            }
        } else {
            return 50
        }
    }


/*func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
 return 85.0
 }*/
}
