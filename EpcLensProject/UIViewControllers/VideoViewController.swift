//
//  VideoViewController.swift
//  EpcLensProject
//
//  Created by BlueHorse MacBook Pro on 23/07/18.
//  Copyright © 2018 Moumita Datta. All rights reserved.
//

import UIKit

class VideoViewController: UIViewController {
    var videoURLStr : String = ""
    
    @IBOutlet var webviewPlayer: UIWebView!
    
    
    //UIView
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //let videoURL = NSURL (string: videoURLStr)
        print("LensFlare videoURL:: \(String(describing: videoURLStr)))")
        let videoURL = NSURL (string: videoURLStr)
        webviewPlayer.loadRequest(URLRequest(url: videoURL! as URL) as URLRequest)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK:: IBAction
    @IBAction func btnBackDidTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}
