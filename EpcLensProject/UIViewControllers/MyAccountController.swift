//
//  MyAccountController.swift
//  EpcLensProject
//
//  Created by Moumita Datta on 11/06/18.
//  Copyright © 2018 Moumita Datta. All rights reserved.
//

import UIKit

class MyAccountController: UIViewController, MyAccountActivitiesDetailsProtocol {
    
    @IBOutlet weak var collectionMyAccountMenu: UICollectionView!
    @IBOutlet weak var tableMyAccount: UITableView!
    @IBOutlet var constraintTableMyAccountBottom: NSLayoutConstraint!
    
    
    
    
    var activitiesDetailsDispatcher = MyAccountActivitiesDetailsDispatcher()
    var activityAdsArr = [AdvertisementResults]()
    var activityClassifiedArr = [ClassifiedResults]()
    var activityJobsArr = [JobResults]()
    var activityLexiconArr = [LexiconResults]()
    var activityRadarArr = [ClassifiedResults]()
    
    
    var menuItems  = [Any]()
    var menuActivityItemsArr  = ["ads", "classifieds adds", "jobs", "radar", "lexicon"]
    
    var selectedID : Int = -1
    var selectedActivity : Int = -1
    
    
    //Mark:: AnimatedLoader
    let loaderController = EpcLensLoaderViewController(nibName: "EpcLensLoaderViewController", bundle: nil)
    
    
    
    
    //MARK:: UIView
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        //NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        
        
        //Menu Items
        var menuItemsDic = [AnyHashable: Any]()
        menuItemsDic["MenuImage_path"] = "MyAccount_Membership_Icon"
        menuItemsDic["MenuName"] = "membership info"
        menuItemsDic["MenuSelectedID"] = 1
        menuItems.append(menuItemsDic)
        
        var menuItemsDic1 = [AnyHashable: Any]()
        menuItemsDic1["MenuImage_path"] = "MyAccount_Inbox_Icon"
        menuItemsDic1["MenuName"] = "inbox"
        menuItemsDic1["MenuSelectedID"] = 2
        menuItems.append(menuItemsDic1)
        
        var menuItemsDic2 = [AnyHashable: Any]()
        menuItemsDic2["MenuImage_path"] = "MyAccount_Tuner_Icon"
        menuItemsDic2["MenuName"] = "tuner default settings"
        menuItemsDic2["MenuSelectedID"] = 3
        menuItems.append(menuItemsDic2)
        
        var menuItemsDic3 = [AnyHashable: Any]()
        menuItemsDic3["MenuImage_path"] = "MyAccount_Payment_Icon"
        menuItemsDic3["MenuName"] = "payment info"
        menuItemsDic3["MenuSelectedID"] = 4
        menuItems.append(menuItemsDic3)
        
        var menuItemsDic4 = [AnyHashable: Any]()
        menuItemsDic4["MenuImage_path"] = "MyAccount_Licensing_Icon"
        menuItemsDic4["MenuName"] = "licensing info"
        menuItemsDic4["MenuSelectedID"] = 5
        menuItems.append(menuItemsDic4)
        
        var menuItemsDic5 = [AnyHashable: Any]()
        menuItemsDic5["MenuImage_path"] = "MyAccount_Activities_Icon"
        menuItemsDic5["MenuName"] = "my activities"
        menuItemsDic5["MenuSelectedID"] = 6
        menuItems.append(menuItemsDic5)
        
        print("menuItems ::",menuItems)
        
        
        
        
        activitiesDetailsDispatcher.delegate = self
        
        self.tableMyAccount.estimatedRowHeight = 95.0
        self.tableMyAccount.rowHeight = UITableViewAutomaticDimension
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWasShown), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeHidden), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- Keyboard show hide notification
    @objc func keyboardWasShown(aNotification: NSNotification) {
        
        if let keyboardSize = (aNotification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            self.constraintTableMyAccountBottom.constant = -keyboardSize.height
            self.view.layoutIfNeeded()
            
            
        }
    }
    
    @objc func keyboardWillBeHidden(aNotification: NSNotification) {
        self.constraintTableMyAccountBottom.constant = 0.0
        self.view.layoutIfNeeded()
    }
    
    //MARK:: IBAction
    @IBAction func btnLeftMenuDidTap(_ sender: Any) {
        let elDrawer = self.navigationController?.parent as! KYDrawerController
        elDrawer.setDrawerState(.opened, animated: true)
    }
    
    @IBAction func btnRightMenuDidTap(_ sender: UIButton) {
        let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
        let objReportPopup = storyboardMain.instantiateViewController(withIdentifier: "RightMenuController") as! RightMenuController
        objReportPopup.showPopup(_controller: self)
    }
    
    
    
    //MARK :: MyAccountActivitiesDetailsProtocol
    func didUpdateControllerWithObjMyAccountActivitiesDetails(myAcctActivitiesDetails: MyAccountActivitiesDetailsModel) {
        DispatchQueue.main.async {
            if myAcctActivitiesDetails.data.sucess {
                self.activityAdsArr = myAcctActivitiesDetails.data.advertisement!
                self.activityClassifiedArr = myAcctActivitiesDetails.data.classified!
                self.activityJobsArr = myAcctActivitiesDetails.data.jobs!
                self.activityRadarArr = myAcctActivitiesDetails.data.rader!
                self.activityLexiconArr = myAcctActivitiesDetails.data.lexicon!
                self.tableMyAccount.delegate = self
                self.tableMyAccount.dataSource = self
                self.tableMyAccount.reloadData()
                // Stop Loaded
                self.loaderController.hideEpcLoader()
            }
        }
    }
    

}


extension MyAccountController : UITableViewDataSource, UITableViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    //MARK:: UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        if selectedID == 1 {
            return 2
        } else if selectedID == 6 {
            return 2
        } else { //if UserDefaults.standard.bool(forKey: "isEncyclopedia") {
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectedID == 1 {
            if section == 0 {
                return 6
            } else {
                return 1
            }
        } else if selectedID == 6 {
            if section == 0 {
                return 1
            } else {
                if selectedActivity == 0 {
                    //print("activityAdsArr:: \(activityAdsArr)")
                    print("activityAdsArr:: \(activityAdsArr.count)")
                    return activityAdsArr.count
                } else if selectedActivity == 1 {
                    //print("activityClassifiedArr:: \(activityClassifiedArr)")
                    print("activityClassifiedArr:: \(activityClassifiedArr.count)")
                    return activityClassifiedArr.count
                } else if selectedActivity == 2 {
                    //print("activityJobsArr:: \(activityJobsArr)")
                    print("activityJobsArr:: \(activityJobsArr.count)")
                    return activityJobsArr.count
                } else if selectedActivity == 3 {
                    //print("activityRadarArr:: \(activityRadarArr)")
                    print("activityRadarArr:: \(activityRadarArr.count)")
                    return activityRadarArr.count
                } else if selectedActivity == 4 {
                    //print("activityLexiconArr:: \(activityLexiconArr)")
                    print("activityLexiconArr:: \(activityLexiconArr.count)")
                    return activityLexiconArr.count
                }
                return 1
            }
        } else {
            if section == 0 {
                return 1
            } else {
                return 0
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if selectedID == 1 {
            if (indexPath.section == 0) {
                let idstr = "MyAccountMemberShipTableCell"
                
                // get a reference to our storyboard cell
                var myAccountMemberShipCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
                if myAccountMemberShipCell == nil {
                    myAccountMemberShipCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
                }
                myAccountMemberShipCell.selectionStyle = UITableViewCellSelectionStyle.none
                
                
                //Declare the outlet in our custom class
                let memberDetailsView = myAccountMemberShipCell.viewWithTag(20)
                let imgViewMemberShip = myAccountMemberShipCell.viewWithTag(21) as! UIImageView
                let txtFieldMemberShip = myAccountMemberShipCell.viewWithTag(22) as! UITextField
                
                
                memberDetailsView?.layer.cornerRadius = 8
                memberDetailsView?.layer.borderColor = UIColor.darkGray.cgColor
                memberDetailsView?.layer.borderWidth = 0.5
                
                if indexPath.row == 0 {
                    imgViewMemberShip.image = UIImage(named:"Acc_Member_Email_Icon")!
                    txtFieldMemberShip.placeholder = "email addresss"
                    txtFieldMemberShip.keyboardType = UIKeyboardType.emailAddress
                } else if indexPath.row == 1 {
                    imgViewMemberShip.image = UIImage(named:"Acc_Member_Password_Icon")!
                    txtFieldMemberShip.placeholder = "password"
                    txtFieldMemberShip.keyboardType = UIKeyboardType.default
                } else if indexPath.row == 2 {
                    imgViewMemberShip.image = UIImage(named:"Acc_Member_Phone_Icon")!
                    txtFieldMemberShip.placeholder = "phone"
                    txtFieldMemberShip.keyboardType = UIKeyboardType.phonePad
                } else if indexPath.row == 3 {
                    imgViewMemberShip.image = UIImage(named:"Acc_Member_Language_Icon")!
                    txtFieldMemberShip.placeholder = "language"
                    txtFieldMemberShip.keyboardType = UIKeyboardType.default
                } else if indexPath.row == 4 {
                    imgViewMemberShip.image = UIImage(named:"Acc_Member_Date_Icon")!
                    txtFieldMemberShip.placeholder = "date / number format"
                    txtFieldMemberShip.keyboardType = UIKeyboardType.phonePad
                } else if indexPath.row == 5 {
                    imgViewMemberShip.image = UIImage(named:"Acc_Member_Signature_Icon")!
                    txtFieldMemberShip.placeholder = "signature"
                    txtFieldMemberShip.keyboardType = UIKeyboardType.default
                }
                
                txtFieldMemberShip.delegate = self
                return myAccountMemberShipCell
            } else {
                let idstr = "MyAccountBtnTableCell"
                
                // get a reference to our storyboard cell
                var myAccountBtnTableCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
                if myAccountBtnTableCell == nil {
                    myAccountBtnTableCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
                }
                
                myAccountBtnTableCell.selectionStyle = UITableViewCellSelectionStyle.none
                
                return myAccountBtnTableCell
            }
        } else if selectedID == 2 {
            if (indexPath.section == 0) {
                let idstr = "MyAccountActivityMenuTableCell"
                
                // get a reference to our storyboard cell
                var myAccountActivityCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
                if myAccountActivityCell == nil {
                    myAccountActivityCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
                }
                
                myAccountActivityCell.selectionStyle = UITableViewCellSelectionStyle.none
                
                
                return myAccountActivityCell
            } else {
                let idstr = "MyAccountBtnTableCell"
                
                // get a reference to our storyboard cell
                var myAccountBtnTableCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
                if myAccountBtnTableCell == nil {
                    myAccountBtnTableCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
                }
                myAccountBtnTableCell.selectionStyle = UITableViewCellSelectionStyle.none
                return myAccountBtnTableCell
            }
        } else if selectedID == 3 {
            if (indexPath.section == 0) {
                let idstr = "MyAccountActivityMenuTableCell"
                
                // get a reference to our storyboard cell
                var myAccountActivityCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
                if myAccountActivityCell == nil {
                    myAccountActivityCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
                }
                
                
                /*//Declare the outlet in our custom class
                let collectionMyAccountActvityMenu = myAccountActivityCell.viewWithTag(60) as! UICollectionView
                
                
                 
                collectionMyAccountActvityMenu.dataSource = self
                collectionMyAccountActvityMenu.delegate = self
                collectionMyAccountActvityMenu.reloadData()*/
                
                
                return myAccountActivityCell
            } else {
                let idstr = "MyAccountBtnTableCell"
                
                // get a reference to our storyboard cell
                var myAccountBtnTableCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
                if myAccountBtnTableCell == nil {
                    myAccountBtnTableCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
                }
                
                return myAccountBtnTableCell
            }
        } else if selectedID == 4 {
            if (indexPath.section == 0) {
                let idstr = "MyAccountActivityMenuTableCell"
                
                // get a reference to our storyboard cell
                var myAccountActivityCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
                if myAccountActivityCell == nil {
                    myAccountActivityCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
                }
                
                
                /*//Declare the outlet in our custom class
                let collectionMyAccountActvityMenu = myAccountActivityCell.viewWithTag(60) as! UICollectionView
                
                
                 
                collectionMyAccountActvityMenu.dataSource = self
                collectionMyAccountActvityMenu.delegate = self
                collectionMyAccountActvityMenu.reloadData()*/
                
                
                return myAccountActivityCell
            } else {
                let idstr = "MyAccountBtnTableCell"
                
                // get a reference to our storyboard cell
                var myAccountBtnTableCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
                if myAccountBtnTableCell == nil {
                    myAccountBtnTableCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
                }
                
                return myAccountBtnTableCell
            }
        } else if selectedID == 5 {
            if (indexPath.section == 0) {
                let idstr = "MyAccountActivityMenuTableCell"
                
                // get a reference to our storyboard cell
                var myAccountActivityCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
                if myAccountActivityCell == nil {
                    myAccountActivityCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
                }
                
                /*//Declare the outlet in our custom class
                let collectionMyAccountActvityMenu = myAccountActivityCell.viewWithTag(60) as! UICollectionView
                
                
                
                collectionMyAccountActvityMenu.dataSource = self
                collectionMyAccountActvityMenu.delegate = self
                collectionMyAccountActvityMenu.reloadData()*/
                
                
                return myAccountActivityCell
            } else {
                let idstr = "MyAccountBtnTableCell"
                
                // get a reference to our storyboard cell
                var myAccountBtnTableCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
                if myAccountBtnTableCell == nil {
                    myAccountBtnTableCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
                }
                
                return myAccountBtnTableCell
            }
        } else {
            if (indexPath.section == 0) {
                let idstr = "MyAccountActivityMenuTableCell"
                
                // get a reference to our storyboard cell
                var myAccountActivityCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
                if myAccountActivityCell == nil {
                    myAccountActivityCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
                }
                
                
                //Declare the outlet in our custom class
                let collectionMyAccountActvityMenu = myAccountActivityCell.viewWithTag(60) as! UICollectionView
                
                
                collectionMyAccountActvityMenu.dataSource = self
                collectionMyAccountActvityMenu.delegate = self
                collectionMyAccountActvityMenu.reloadData()
                
                myAccountActivityCell.selectionStyle = UITableViewCellSelectionStyle.none
                return myAccountActivityCell
            } else {
                if selectedActivity == 0 {
                    let idstr = "MyAccountAdsTableCell"
                    
                    // get a reference to our storyboard cell
                    var activityAdsCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
                    if activityAdsCell == nil {
                        activityAdsCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
                    }
                    
                    //Declare the outlet in our custom class
                    let viewBackName = activityAdsCell.viewWithTag(6311)
                    let lblAdsTitle = activityAdsCell.viewWithTag(6312) as! UILabel
                    let imgAdsProfile = activityAdsCell.viewWithTag(6313) as! UIImageView
                    let lblAdsShortDesc = activityAdsCell.viewWithTag(6314) as! UILabel
                    
                    
                    viewBackName?.layer.cornerRadius = 10.0
                    viewBackName?.clipsToBounds = true
                    
                    
                    if (activityAdsArr[indexPath.row].title?.isEmpty)! {
                        lblAdsTitle.isHidden =  true
                    } else {
                        lblAdsTitle.text =  activityAdsArr[indexPath.row].title
                    }
                    
                    if activityAdsArr[indexPath.row].mediaURL?.isEmpty == false && activityAdsArr[indexPath.row].mediaURL != "" {
                        let escapedString = activityAdsArr[indexPath.row].mediaURL?.replacingOccurrences(of: " ", with: "%20")
                        Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgAdsProfile)
                    } else {
                        imgAdsProfile.image = UIImage(named: "AppIcon")
                    }
                    
                    if (activityAdsArr[indexPath.row].shortDesc?.isEmpty)! {
                        lblAdsShortDesc.text =  " "
                    } else {
                        lblAdsShortDesc.text =  activityAdsArr[indexPath.row].shortDesc
                    }
                    
                    activityAdsCell.selectionStyle = UITableViewCellSelectionStyle.none
                    return activityAdsCell
                } else if selectedActivity == 1 {
                    let idstr = "MyAccountClassifiedTableCell"
                    
                    // get a reference to our storyboard cell
                    var activityClassifiedCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
                    if activityClassifiedCell == nil {
                        activityClassifiedCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
                    }
                    
                    //Declare the outlet in our custom class
                    let imgClassifiedProfile = activityClassifiedCell.viewWithTag(6321) as! UIImageView
                    let lblTitle = activityClassifiedCell.viewWithTag(6322) as! UILabel
                    let lblFeatured = activityClassifiedCell.viewWithTag(6323) as! UILabel
                    let lblPrice = activityClassifiedCell.viewWithTag(6324) as! UILabel
                    
                    imgClassifiedProfile.layer.cornerRadius = 10.0
                    imgClassifiedProfile.clipsToBounds = true
                    
                    if activityClassifiedArr[indexPath.row].mediaURL?.isEmpty == false && activityClassifiedArr[indexPath.row].mediaURL != "" {
                        let escapedString = activityClassifiedArr[indexPath.row].mediaURL?.replacingOccurrences(of: " ", with: "%20")
                        Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgClassifiedProfile)
                    } else {
                        imgClassifiedProfile.image = UIImage(named: "AppIcon")
                    }
                    
                    if (activityClassifiedArr[indexPath.row].title?.isEmpty)! {
                        lblTitle.isHidden =  true
                    } else {
                        lblTitle.text =  activityClassifiedArr[indexPath.row].title
                    }
                    
                    let isFeatured : Bool = (activityClassifiedArr[indexPath.row].isFeatured != nil)
                    if isFeatured == false {
                        lblFeatured.text =  " "
                    } else {
                        lblFeatured.text =  "featured"
                    }
                    
                    if (activityClassifiedArr[indexPath.row].price?.isEqual(to: 0))! {
                        lblPrice.text =  " "
                    } else {
                        lblPrice.text =  "$ " + String(activityClassifiedArr[indexPath.row].price!)
                    }
                    
                    activityClassifiedCell.selectionStyle = UITableViewCellSelectionStyle.none
                    return activityClassifiedCell
                } else if selectedActivity == 2 {
                    let idstr = "MyAccountJobsTableCell"
                    
                    // get a reference to our storyboard cell
                    var activityJobsCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
                    if activityJobsCell == nil {
                        activityJobsCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
                    }
                    
                    //Declare the outlet in our custom class
                    let imgJobProfile = activityJobsCell.viewWithTag(6331) as! UIImageView
                    let lblTitle = activityJobsCell.viewWithTag(6332) as! UILabel
                    let lblTime = activityJobsCell.viewWithTag(6333) as! UILabel
                    let lblFeatured = activityJobsCell.viewWithTag(6334) as! UILabel
                    let lblOwnership = activityJobsCell.viewWithTag(6335) as! UILabel
                    let lblLocation = activityJobsCell.viewWithTag(6336) as! UILabel
                    
                    
                    imgJobProfile.layer.cornerRadius = 10.0
                    imgJobProfile.clipsToBounds = true
                    
                    if activityJobsArr[indexPath.row].companyLogo?.isEmpty == false && activityJobsArr[indexPath.row].companyLogo != "" {
                        let escapedString = activityJobsArr[indexPath.row].companyLogo?.replacingOccurrences(of: " ", with: "%20")
                        Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgJobProfile)
                    } else {
                        imgJobProfile.image = UIImage(named: "AppIcon")
                    }
                    
                    if (activityJobsArr[indexPath.row].title?.isEmpty)! {
                        lblTitle.isHidden =  true
                    } else {
                        lblTitle.text =  activityJobsArr[indexPath.row].title
                    }
                    
                    if (activityJobsArr[indexPath.row].timeAgo?.isEmpty)! {
                        lblTime.text =  " "
                    } else {
                        lblTime.text =  activityJobsArr[indexPath.row].timeAgo
                    }
                    
                    let isFeatured : Bool = (activityJobsArr[indexPath.row].isFeatured != nil)
                    if isFeatured == false {
                        lblFeatured.text =  " "
                    } else {
                        lblFeatured.text =  "featured"
                    }
                    
                    if (activityJobsArr[indexPath.row].ownership?.isEmpty)! {
                        lblOwnership.isHidden =  true
                    } else {
                        lblOwnership.text =  activityJobsArr[indexPath.row].ownership
                    }
                    
                    if (activityJobsArr[indexPath.row].location?.isEmpty)! {
                        lblLocation.text =  " "
                    } else {
                        lblLocation.text =  activityJobsArr[indexPath.row].location
                    }
                    
                    activityJobsCell.selectionStyle = UITableViewCellSelectionStyle.none
                    return activityJobsCell
                } else if selectedActivity == 3 {
                    let idstr = "MyAccountRadarTableCell"
                    
                    // get a reference to our storyboard cell
                    var activityRadarCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
                    if activityRadarCell == nil {
                        activityRadarCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
                    }
                    
                    //Declare the outlet in our custom class
                    let imgRadarProfile = activityRadarCell.viewWithTag(6341) as! UIImageView
                    let viewIndustry = activityRadarCell.viewWithTag(6345)
                    let lblIndustry = activityRadarCell.viewWithTag(6342) as! UILabel
                    let lblTitle = activityRadarCell.viewWithTag(6343) as! UILabel
                    let lblShortDesc = activityRadarCell.viewWithTag(6344) as! UILabel
                    
                    
                    imgRadarProfile.layer.cornerRadius = 10.0
                    imgRadarProfile.clipsToBounds = true
                    
                    if activityRadarArr[indexPath.row].mediaURL?.isEmpty == false && activityRadarArr[indexPath.row].mediaURL != "" {
                        let escapedString = activityRadarArr[indexPath.row].mediaURL?.replacingOccurrences(of: " ", with: "%20")
                        Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgRadarProfile)
                    } else {
                        imgRadarProfile.image = UIImage(named: "AppIcon")
                    }
                    
                    if (activityRadarArr[indexPath.row].industry?.isEmpty)! {
                        viewIndustry?.isHidden =  true
                    } else {
                        lblIndustry.text =  activityRadarArr[indexPath.row].industry
                    }
                    
                    if (activityRadarArr[indexPath.row].title?.isEmpty)! {
                        lblTitle.text =  " "
                    } else {
                        lblTitle.text =  activityRadarArr[indexPath.row].title
                    }
                    
                    if (activityRadarArr[indexPath.row].shortDesc?.isEmpty)! {
                        lblShortDesc.text =  " "
                    } else {
                        lblShortDesc.text =  activityRadarArr[indexPath.row].shortDesc
                    }
                    
                    activityRadarCell.selectionStyle = UITableViewCellSelectionStyle.none
                    return activityRadarCell
                } else {
                    let idstr = "MyAccountLexiconTableCell"
                    
                    // get a reference to our storyboard cell
                    var activityLexiconCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
                    if activityLexiconCell == nil {
                        activityLexiconCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
                    }
                    
                    //Declare the outlet in our custom class
                    let lblTitle = activityLexiconCell.viewWithTag(6351) as! UILabel
                    
                    
                    if (activityLexiconArr[indexPath.row].title?.isEmpty)! {
                        lblTitle.text =  " "
                    } else {
                        lblTitle.text =  activityLexiconArr[indexPath.row].title
                    }
                    
                    activityLexiconCell.selectionStyle = UITableViewCellSelectionStyle.none
                    return activityLexiconCell
                }
            }
        }
    }
    
    
    //MARK:: UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if selectedID == 1 {
            return 45
         } else if selectedID == 6 {
            if (indexPath.section == 0) {
                return 50
            } else {
//                if selectedActivity == 2 {
//                    return 150
//                }
                return UITableViewAutomaticDimension
            }
        } else {
            if (indexPath.section == 0) {
                return 60
            } else {
                return 45
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let myAccountHeaderCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: "MyAccountHeaderCell")!
        
        let lblHeaderTitle = myAccountHeaderCell.viewWithTag(20) as! UILabel
        
        if selectedID == 1 {
            if section == 0 {
                
                lblHeaderTitle.text = "membership"
            }
        } else if selectedID == 6 {
            if section == 1 {
                if selectedActivity == 0 {
                    
                    lblHeaderTitle.font = lblHeaderTitle.font.withSize(16)
                    lblHeaderTitle.text = "ads"
                } else if selectedActivity == 1 {
                    lblHeaderTitle.font = lblHeaderTitle.font.withSize(16)
                    lblHeaderTitle.text = "classifieds"
                } else if selectedActivity == 2 {
                    lblHeaderTitle.font = lblHeaderTitle.font.withSize(16)
                    lblHeaderTitle.text = "jobs"
                } else if selectedActivity == 3 {
                    
                    lblHeaderTitle.font = lblHeaderTitle.font.withSize(16)
                    lblHeaderTitle.text = "radar"
                } else {
                    
                    lblHeaderTitle.font = lblHeaderTitle.font.withSize(16)
                    lblHeaderTitle.text = "lexicon"
                    
                }
            }
        }
        return myAccountHeaderCell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if selectedID == 1 {
            if (section == 0) {
                return 35
            } else {
                return 0
            }
        } else if selectedID == 6 {
            if section == 1 {
                return 30
            } else {
                return 0
            }
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }
    
    
    
    
    
    // MARK:: UICollectionViewDataSource protocol
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if selectedID == 6 {
            if collectionView == collectionMyAccountMenu {
                //print("MyAccount menuItems count:: ",menuItems.count)
                return menuItems.count
            } else if collectionView.tag == 60 {
                return menuActivityItemsArr.count
            } else {
                return 0
            }
        } else {
            if collectionView == collectionMyAccountMenu {
                return menuItems.count
                //        } else if collectionView.tag == 60 {
                //            return 5
            } else {
                return 0
            }
        }
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if selectedID == 1 {
            if collectionView == collectionMyAccountMenu {
                let idstr = "MyAccountMenuCell"
                
                // get a reference to our storyboard cell
                let myAccountMenuCell : UICollectionViewCell! = collectionView.dequeueReusableCell(withReuseIdentifier: idstr, for: indexPath)
                
                
                // Use the outlet in our custom class to get a reference to the UI Elements in the cell
                let lblMenuTitle = myAccountMenuCell.viewWithTag(12) as! UILabel
                let imgMenu = myAccountMenuCell.viewWithTag(13) as! UIImageView
                
                
                var menuItemsDic = [AnyHashable: Any]()
                menuItemsDic = menuItems[indexPath.row] as! [AnyHashable : Any]
                
                // Use the outlet in our custom class to get a reference to the UILabel in the cell
                if let aKey = menuItemsDic["MenuImage_path"] as? String {
                    imgMenu.image = UIImage(named: aKey)
                }
                if let aKey = menuItemsDic["MenuName"] as? String {
                    lblMenuTitle.text = aKey
                }
                
                
                //cell.backgroundColor = UIColor.cyan // make cell more visible in our example project
                lblMenuTitle.textColor = UIColor.white
                myAccountMenuCell?.layer.cornerRadius = 5
                if indexPath.row == 0 {
                    myAccountMenuCell.viewWithTag(11)?.backgroundColor = UIColor(red: CGFloat((191.0 / 255.0)), green: CGFloat((146.0 / 255.0)), blue: CGFloat((126.0 / 255.0)), alpha: 1.0)
                    lblMenuTitle.textColor = UIColor.black
                } else if indexPath.row == 1 {
                    myAccountMenuCell.viewWithTag(11)?.backgroundColor = UIColor(red: CGFloat((110.0 / 255.0)), green: CGFloat((156.0 / 255.0)), blue: CGFloat((178.0 / 255.0)), alpha: 1.0)
                } else if indexPath.row == 2 {
                    myAccountMenuCell.viewWithTag(11)?.backgroundColor = UIColor(red: CGFloat((165.0 / 255.0)), green: CGFloat((160.0 / 255.0)), blue: CGFloat((166.0 / 255.0)), alpha: 1.0)
                } else if indexPath.row == 3 {
                    myAccountMenuCell.viewWithTag(11)?.backgroundColor = UIColor(red: CGFloat((110.0 / 255.0)), green: CGFloat((156.0 / 255.0)), blue: CGFloat((178.0 / 255.0)), alpha: 1.0)
                } else if indexPath.row == 4 {
                    myAccountMenuCell.viewWithTag(11)?.backgroundColor = UIColor(red: CGFloat((140.0 / 255.0)), green: CGFloat((161.0 / 255.0)), blue: CGFloat((179.0 / 255.0)), alpha: 1.0)
                } else {
                    myAccountMenuCell.viewWithTag(11)?.backgroundColor = UIColor(red: CGFloat((175.0 / 255.0)), green: CGFloat((163.0 / 255.0)), blue: CGFloat((149.0 / 255.0)), alpha: 1.0)
                }
                
                return myAccountMenuCell
            } else {
                let idstr = "MyAccountMenuCell"
                
                // get a reference to our storyboard cell
                let myAccountMenuCell : UICollectionViewCell! = collectionView.dequeueReusableCell(withReuseIdentifier: idstr, for: indexPath)
                
                return myAccountMenuCell
            }
        } else if selectedID == 2 {
            if collectionView == collectionMyAccountMenu {
                let idstr = "MyAccountMenuCell"
                
                // get a reference to our storyboard cell
                let myAccountMenuCell : UICollectionViewCell! = collectionView.dequeueReusableCell(withReuseIdentifier: idstr, for: indexPath)
                
                
                // Use the outlet in our custom class to get a reference to the UI Elements in the cell
                let lblMenuTitle = myAccountMenuCell.viewWithTag(12) as! UILabel
                let imgMenu = myAccountMenuCell.viewWithTag(13) as! UIImageView
                
                
                var menuItemsDic = [AnyHashable: Any]()
                menuItemsDic = menuItems[indexPath.row] as! [AnyHashable : Any]
                
                // Use the outlet in our custom class to get a reference to the UILabel in the cell
                if let aKey = menuItemsDic["MenuImage_path"] as? String {
                    imgMenu.image = UIImage(named: aKey)
                }
                if let aKey = menuItemsDic["MenuName"] as? String {
                    lblMenuTitle.text = aKey
                }
                
                
                //cell.backgroundColor = UIColor.cyan // make cell more visible in our example project
                lblMenuTitle.textColor = UIColor.white
                myAccountMenuCell?.layer.cornerRadius = 5
                if indexPath.row == 0 {
                    myAccountMenuCell.viewWithTag(11)?.backgroundColor = UIColor(red: CGFloat((191.0 / 255.0)), green: CGFloat((146.0 / 255.0)), blue: CGFloat((126.0 / 255.0)), alpha: 1.0)
                } else if indexPath.row == 1 {
                    myAccountMenuCell.viewWithTag(11)?.backgroundColor = UIColor(red: CGFloat((110.0 / 255.0)), green: CGFloat((156.0 / 255.0)), blue: CGFloat((178.0 / 255.0)), alpha: 1.0)
                    lblMenuTitle.textColor = UIColor.black
                } else if indexPath.row == 2 {
                    myAccountMenuCell.viewWithTag(11)?.backgroundColor = UIColor(red: CGFloat((165.0 / 255.0)), green: CGFloat((160.0 / 255.0)), blue: CGFloat((166.0 / 255.0)), alpha: 1.0)
                } else if indexPath.row == 3 {
                    myAccountMenuCell.viewWithTag(11)?.backgroundColor = UIColor(red: CGFloat((110.0 / 255.0)), green: CGFloat((156.0 / 255.0)), blue: CGFloat((178.0 / 255.0)), alpha: 1.0)
                } else if indexPath.row == 4 {
                    myAccountMenuCell.viewWithTag(11)?.backgroundColor = UIColor(red: CGFloat((140.0 / 255.0)), green: CGFloat((161.0 / 255.0)), blue: CGFloat((179.0 / 255.0)), alpha: 1.0)
                } else {
                    myAccountMenuCell.viewWithTag(11)?.backgroundColor = UIColor(red: CGFloat((175.0 / 255.0)), green: CGFloat((163.0 / 255.0)), blue: CGFloat((149.0 / 255.0)), alpha: 1.0)
                }
                
                return myAccountMenuCell
            } else {
                let idstr = "MyAccountMenuCell"
                
                // get a reference to our storyboard cell
                let myAccountMenuCell : UICollectionViewCell! = collectionView.dequeueReusableCell(withReuseIdentifier: idstr, for: indexPath)
                
                return myAccountMenuCell
            }
        } else if selectedID == 3 {
            if collectionView == collectionMyAccountMenu {
                let idstr = "MyAccountMenuCell"
                
                // get a reference to our storyboard cell
                let myAccountMenuCell : UICollectionViewCell! = collectionView.dequeueReusableCell(withReuseIdentifier: idstr, for: indexPath)
                
                
                // Use the outlet in our custom class to get a reference to the UI Elements in the cell
                let lblMenuTitle = myAccountMenuCell.viewWithTag(12) as! UILabel
                let imgMenu = myAccountMenuCell.viewWithTag(13) as! UIImageView
                
                
                var menuItemsDic = [AnyHashable: Any]()
                menuItemsDic = menuItems[indexPath.row] as! [AnyHashable : Any]
                
                // Use the outlet in our custom class to get a reference to the UILabel in the cell
                if let aKey = menuItemsDic["MenuImage_path"] as? String {
                    imgMenu.image = UIImage(named: aKey)
                }
                if let aKey = menuItemsDic["MenuName"] as? String {
                    lblMenuTitle.text = aKey
                }
                
                
                //cell.backgroundColor = UIColor.cyan // make cell more visible in our example project
                lblMenuTitle.textColor = UIColor.white
                myAccountMenuCell?.layer.cornerRadius = 5
                if indexPath.row == 0 {
                    myAccountMenuCell.viewWithTag(11)?.backgroundColor = UIColor(red: CGFloat((191.0 / 255.0)), green: CGFloat((146.0 / 255.0)), blue: CGFloat((126.0 / 255.0)), alpha: 1.0)
                } else if indexPath.row == 1 {
                    myAccountMenuCell.viewWithTag(11)?.backgroundColor = UIColor(red: CGFloat((110.0 / 255.0)), green: CGFloat((156.0 / 255.0)), blue: CGFloat((178.0 / 255.0)), alpha: 1.0)
                } else if indexPath.row == 2 {
                    myAccountMenuCell.viewWithTag(11)?.backgroundColor = UIColor(red: CGFloat((165.0 / 255.0)), green: CGFloat((160.0 / 255.0)), blue: CGFloat((166.0 / 255.0)), alpha: 1.0)
                    lblMenuTitle.textColor = UIColor.black
                } else if indexPath.row == 3 {
                    myAccountMenuCell.viewWithTag(11)?.backgroundColor = UIColor(red: CGFloat((110.0 / 255.0)), green: CGFloat((156.0 / 255.0)), blue: CGFloat((178.0 / 255.0)), alpha: 1.0)
                } else if indexPath.row == 4 {
                    myAccountMenuCell.viewWithTag(11)?.backgroundColor = UIColor(red: CGFloat((140.0 / 255.0)), green: CGFloat((161.0 / 255.0)), blue: CGFloat((179.0 / 255.0)), alpha: 1.0)
                } else {
                    myAccountMenuCell.viewWithTag(11)?.backgroundColor = UIColor(red: CGFloat((175.0 / 255.0)), green: CGFloat((163.0 / 255.0)), blue: CGFloat((149.0 / 255.0)), alpha: 1.0)
                }
                
                return myAccountMenuCell
            } else {
                let idstr = "MyAccountMenuCell"
                
                // get a reference to our storyboard cell
                let myAccountMenuCell : UICollectionViewCell! = collectionView.dequeueReusableCell(withReuseIdentifier: idstr, for: indexPath)
                
                return myAccountMenuCell
            }
        } else if selectedID == 4 {
            if collectionView == collectionMyAccountMenu {
                let idstr = "MyAccountMenuCell"
                
                // get a reference to our storyboard cell
                let myAccountMenuCell : UICollectionViewCell! = collectionView.dequeueReusableCell(withReuseIdentifier: idstr, for: indexPath)
                
                
                // Use the outlet in our custom class to get a reference to the UI Elements in the cell
                let lblMenuTitle = myAccountMenuCell.viewWithTag(12) as! UILabel
                let imgMenu = myAccountMenuCell.viewWithTag(13) as! UIImageView
                
                
                var menuItemsDic = [AnyHashable: Any]()
                menuItemsDic = menuItems[indexPath.row] as! [AnyHashable : Any]
                
                // Use the outlet in our custom class to get a reference to the UILabel in the cell
                if let aKey = menuItemsDic["MenuImage_path"] as? String {
                    imgMenu.image = UIImage(named: aKey)
                }
                if let aKey = menuItemsDic["MenuName"] as? String {
                    lblMenuTitle.text = aKey
                }
                
                
                //cell.backgroundColor = UIColor.cyan // make cell more visible in our example project
                lblMenuTitle.textColor = UIColor.white
                myAccountMenuCell?.layer.cornerRadius = 5
                if indexPath.row == 0 {
                    myAccountMenuCell.viewWithTag(11)?.backgroundColor = UIColor(red: CGFloat((191.0 / 255.0)), green: CGFloat((146.0 / 255.0)), blue: CGFloat((126.0 / 255.0)), alpha: 1.0)
                } else if indexPath.row == 1 {
                    myAccountMenuCell.viewWithTag(11)?.backgroundColor = UIColor(red: CGFloat((110.0 / 255.0)), green: CGFloat((156.0 / 255.0)), blue: CGFloat((178.0 / 255.0)), alpha: 1.0)
                } else if indexPath.row == 2 {
                    myAccountMenuCell.viewWithTag(11)?.backgroundColor = UIColor(red: CGFloat((165.0 / 255.0)), green: CGFloat((160.0 / 255.0)), blue: CGFloat((166.0 / 255.0)), alpha: 1.0)
                } else if indexPath.row == 3 {
                    myAccountMenuCell.viewWithTag(11)?.backgroundColor = UIColor(red: CGFloat((110.0 / 255.0)), green: CGFloat((156.0 / 255.0)), blue: CGFloat((178.0 / 255.0)), alpha: 1.0)
                    lblMenuTitle.textColor = UIColor.black
                } else if indexPath.row == 4 {
                    myAccountMenuCell.viewWithTag(11)?.backgroundColor = UIColor(red: CGFloat((140.0 / 255.0)), green: CGFloat((161.0 / 255.0)), blue: CGFloat((179.0 / 255.0)), alpha: 1.0)
                } else {
                    myAccountMenuCell.viewWithTag(11)?.backgroundColor = UIColor(red: CGFloat((175.0 / 255.0)), green: CGFloat((163.0 / 255.0)), blue: CGFloat((149.0 / 255.0)), alpha: 1.0)
                }
                
                return myAccountMenuCell
            } else {
                let idstr = "MyAccountMenuCell"
                
                // get a reference to our storyboard cell
                let myAccountMenuCell : UICollectionViewCell! = collectionView.dequeueReusableCell(withReuseIdentifier: idstr, for: indexPath)
                
                return myAccountMenuCell
            }
        } else if selectedID == 5 {
            if collectionView == collectionMyAccountMenu {
                let idstr = "MyAccountMenuCell"
                
                // get a reference to our storyboard cell
                let myAccountMenuCell : UICollectionViewCell! = collectionView.dequeueReusableCell(withReuseIdentifier: idstr, for: indexPath)
                
                
                // Use the outlet in our custom class to get a reference to the UI Elements in the cell
                let lblMenuTitle = myAccountMenuCell.viewWithTag(12) as! UILabel
                let imgMenu = myAccountMenuCell.viewWithTag(13) as! UIImageView
                
                
                var menuItemsDic = [AnyHashable: Any]()
                menuItemsDic = menuItems[indexPath.row] as! [AnyHashable : Any]
                
                // Use the outlet in our custom class to get a reference to the UILabel in the cell
                if let aKey = menuItemsDic["MenuImage_path"] as? String {
                    imgMenu.image = UIImage(named: aKey)
                }
                if let aKey = menuItemsDic["MenuName"] as? String {
                    lblMenuTitle.text = aKey
                }
                
                
                //cell.backgroundColor = UIColor.cyan // make cell more visible in our example project
                lblMenuTitle.textColor = UIColor.white
                myAccountMenuCell?.layer.cornerRadius = 5
                if indexPath.row == 0 {
                    myAccountMenuCell.viewWithTag(11)?.backgroundColor = UIColor(red: CGFloat((191.0 / 255.0)), green: CGFloat((146.0 / 255.0)), blue: CGFloat((126.0 / 255.0)), alpha: 1.0)
                } else if indexPath.row == 1 {
                    myAccountMenuCell.viewWithTag(11)?.backgroundColor = UIColor(red: CGFloat((110.0 / 255.0)), green: CGFloat((156.0 / 255.0)), blue: CGFloat((178.0 / 255.0)), alpha: 1.0)
                } else if indexPath.row == 2 {
                    myAccountMenuCell.viewWithTag(11)?.backgroundColor = UIColor(red: CGFloat((165.0 / 255.0)), green: CGFloat((160.0 / 255.0)), blue: CGFloat((166.0 / 255.0)), alpha: 1.0)
                } else if indexPath.row == 3 {
                    myAccountMenuCell.viewWithTag(11)?.backgroundColor = UIColor(red: CGFloat((110.0 / 255.0)), green: CGFloat((156.0 / 255.0)), blue: CGFloat((178.0 / 255.0)), alpha: 1.0)
                } else if indexPath.row == 4 {
                    myAccountMenuCell.viewWithTag(11)?.backgroundColor = UIColor(red: CGFloat((140.0 / 255.0)), green: CGFloat((161.0 / 255.0)), blue: CGFloat((179.0 / 255.0)), alpha: 1.0)
                    lblMenuTitle.textColor = UIColor.black
                } else {
                    myAccountMenuCell.viewWithTag(11)?.backgroundColor = UIColor(red: CGFloat((175.0 / 255.0)), green: CGFloat((163.0 / 255.0)), blue: CGFloat((149.0 / 255.0)), alpha: 1.0)
                }
                
                return myAccountMenuCell
            } else {
                let idstr = "MyAccountMenuCell"
                
                // get a reference to our storyboard cell
                let myAccountMenuCell : UICollectionViewCell! = collectionView.dequeueReusableCell(withReuseIdentifier: idstr, for: indexPath)
                
                return myAccountMenuCell
            }
        } else {
            if collectionView == collectionMyAccountMenu {
                let idstr = "MyAccountMenuCell"
                
                // get a reference to our storyboard cell
                let myAccountMenuCell : UICollectionViewCell! = collectionView.dequeueReusableCell(withReuseIdentifier: idstr, for: indexPath)
                
                
                // Use the outlet in our custom class to get a reference to the UI Elements in the cell
                let lblMenuTitle = myAccountMenuCell.viewWithTag(12) as! UILabel
                let imgMenu = myAccountMenuCell.viewWithTag(13) as! UIImageView
                
                
                var menuItemsDic = [AnyHashable: Any]()
                menuItemsDic = menuItems[indexPath.row] as! [AnyHashable : Any]
                
                // Use the outlet in our custom class to get a reference to the UILabel in the cell
                if let aKey = menuItemsDic["MenuImage_path"] as? String {
                    imgMenu.image = UIImage(named: aKey)
                }
                if let aKey = menuItemsDic["MenuName"] as? String {
                    lblMenuTitle.text = aKey
                }
                
                
                //cell.backgroundColor = UIColor.cyan // make cell more visible in our example project
                lblMenuTitle.textColor = UIColor.white
                myAccountMenuCell?.layer.cornerRadius = 5
                if indexPath.row == 0 {
                    myAccountMenuCell.viewWithTag(11)?.backgroundColor = UIColor(red: CGFloat((191.0 / 255.0)), green: CGFloat((146.0 / 255.0)), blue: CGFloat((126.0 / 255.0)), alpha: 1.0)
                } else if indexPath.row == 1 {
                    myAccountMenuCell.viewWithTag(11)?.backgroundColor = UIColor(red: CGFloat((110.0 / 255.0)), green: CGFloat((156.0 / 255.0)), blue: CGFloat((178.0 / 255.0)), alpha: 1.0)
                } else if indexPath.row == 2 {
                    myAccountMenuCell.viewWithTag(11)?.backgroundColor = UIColor(red: CGFloat((165.0 / 255.0)), green: CGFloat((160.0 / 255.0)), blue: CGFloat((166.0 / 255.0)), alpha: 1.0)
                } else if indexPath.row == 3 {
                    myAccountMenuCell.viewWithTag(11)?.backgroundColor = UIColor(red: CGFloat((110.0 / 255.0)), green: CGFloat((156.0 / 255.0)), blue: CGFloat((178.0 / 255.0)), alpha: 1.0)
                } else if indexPath.row == 4 {
                    myAccountMenuCell.viewWithTag(11)?.backgroundColor = UIColor(red: CGFloat((140.0 / 255.0)), green: CGFloat((161.0 / 255.0)), blue: CGFloat((179.0 / 255.0)), alpha: 1.0)
                } else {
                    myAccountMenuCell.viewWithTag(11)?.backgroundColor = UIColor(red: CGFloat((175.0 / 255.0)), green: CGFloat((163.0 / 255.0)), blue: CGFloat((149.0 / 255.0)), alpha: 1.0)
                    lblMenuTitle.textColor = UIColor.black
                }
                
                return myAccountMenuCell
            } else {
                let idstr = "MyAccountActivityMenuCell"
                
                // get a reference to our storyboard cell
                let activityMenuCell : UICollectionViewCell! = collectionView.dequeueReusableCell(withReuseIdentifier: idstr, for: indexPath)
                
                
                activityMenuCell.layer.cornerRadius = 10.0
                // Use the outlet in our custom class to get a reference to the UI Elements in the cell
                let lblMenuTitle = activityMenuCell.viewWithTag(65) as! UILabel
                
                
                lblMenuTitle.text = menuActivityItemsArr[indexPath.item]
                
                
                if selectedActivity == 0 {
                    if indexPath.row == 0 {
                        lblMenuTitle.textColor = UIColor.white
                        activityMenuCell.backgroundColor = UIColor(red: CGFloat((255.0 / 255.0)), green: CGFloat((165.0 / 255.0)), blue: CGFloat((3.0 / 255.0)), alpha: 1.0)
                    } else {
                        lblMenuTitle.textColor = UIColor.black
                        activityMenuCell.backgroundColor = UIColor.white
                    }
                } else if selectedActivity == 1 {
                    if indexPath.row == 1 {
                        lblMenuTitle.textColor = UIColor.white
                        activityMenuCell.backgroundColor = UIColor(red: CGFloat((255.0 / 255.0)), green: CGFloat((165.0 / 255.0)), blue: CGFloat((3.0 / 255.0)), alpha: 1.0)
                    } else {
                        lblMenuTitle.textColor = UIColor.black
                        activityMenuCell.backgroundColor = UIColor.white
                    }
                } else if selectedActivity == 2 {
                    if indexPath.row == 2 {
                        lblMenuTitle.textColor = UIColor.white
                        activityMenuCell.backgroundColor = UIColor(red: CGFloat((255.0 / 255.0)), green: CGFloat((165.0 / 255.0)), blue: CGFloat((3.0 / 255.0)), alpha: 1.0)
                    } else {
                        lblMenuTitle.textColor = UIColor.black
                        activityMenuCell.backgroundColor = UIColor.white
                    }
                } else if selectedActivity == 3 {
                    if indexPath.row == 3 {
                        lblMenuTitle.textColor = UIColor.white
                        activityMenuCell.backgroundColor = UIColor(red: CGFloat((255.0 / 255.0)), green: CGFloat((165.0 / 255.0)), blue: CGFloat((3.0 / 255.0)), alpha: 1.0)
                    } else {
                        lblMenuTitle.textColor = UIColor.black
                        activityMenuCell.backgroundColor = UIColor.white
                    }
                } else if selectedActivity == 4 {
                    if indexPath.row == 4 {
                        lblMenuTitle.textColor = UIColor.white
                        activityMenuCell.backgroundColor = UIColor(red: CGFloat((255.0 / 255.0)), green: CGFloat((165.0 / 255.0)), blue: CGFloat((3.0 / 255.0)), alpha: 1.0)
                    } else {
                        lblMenuTitle.textColor = UIColor.black
                        activityMenuCell.backgroundColor = UIColor.white
                    }
                }
                
                return activityMenuCell
            }
        }
    }
    
    
    
    // MARK:: UICollectionViewDelegate protocol
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if selectedID == 1 {
            let cellWidth = (self.view.frame.size.width-50)/3
            return CGSize(width: cellWidth, height: 35)
        } else if selectedID == 6 {
            if collectionView == collectionMyAccountMenu {
                let cellWidth = (self.view.frame.size.width-50)/3
                return CGSize(width: cellWidth, height: 35)
            } else {
                let cellWidth = (self.view.frame.size.width-60)/5
                return CGSize(width: cellWidth, height: 28)
            }
        } else {
            let cellWidth = (self.view.frame.size.width-50)/3
            return CGSize(width: cellWidth, height: 35)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionMyAccountMenu {
            selectedID = indexPath.item+1
            print("selectedID :: \(selectedID)")
            collectionMyAccountMenu.reloadData()
            
            if selectedID == 6 {
                var tempDict = [String:String]()
                tempDict["token"] = EpicLensConstantsVariables.accessToken
                activitiesDetailsDispatcher.GetMyAccountActivitiesDetails(paramDict: tempDict)
                
                // Start Loaded
                loaderController.showEpcLoader(_sourceView: self)
                selectedActivity = 0
            } else {
                
            }
            tableMyAccount.reloadData()
        } else {
            selectedActivity = indexPath.item
            print("selectedActivity :: \(selectedActivity)")
            tableMyAccount.reloadData()
        }
        
        
    }
    
}


extension MyAccountController : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if selectedID == 1 {
            self.tableMyAccount.scrollToRow(at: IndexPath.init(row: 5, section: 0), at: .bottom, animated: true)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let cell = textField.superview?.superview?.superview?.superview as? UITableViewCell
        guard let _cell = cell else {
            return false
        }
        let indexpath = self.tableMyAccount.indexPath(for: _cell)
        if selectedID == 1 {
            if indexpath?.section == 0 {
                textField.resignFirstResponder()
                if indexpath?.row == 0 {
                    let cellTemp = self.tableMyAccount.cellForRow(at: IndexPath.init(row: 1, section: 0))
                    let textfieldCell = cellTemp?.viewWithTag(22) as! UITextField
                    textfieldCell.becomeFirstResponder()
                } else if indexpath?.row == 1 {
                    let cellTemp = self.tableMyAccount.cellForRow(at: IndexPath.init(row: 2, section: 0))
                    let textfieldCell = cellTemp?.viewWithTag(22) as! UITextField
                    textfieldCell.becomeFirstResponder()
                } else if indexpath?.row == 2 {
                    let cellTemp = self.tableMyAccount.cellForRow(at: IndexPath.init(row: 3, section: 0))
                    let textfieldCell = cellTemp?.viewWithTag(22) as! UITextField
                    textfieldCell.becomeFirstResponder()
                } else if indexpath?.row == 3 {
                    let cellTemp = self.tableMyAccount.cellForRow(at: IndexPath.init(row: 4, section: 0))
                    let textfieldCell = cellTemp?.viewWithTag(22) as! UITextField
                    textfieldCell.becomeFirstResponder()
                } else if indexpath?.row == 4 {
                    let cellTemp = self.tableMyAccount.cellForRow(at: IndexPath.init(row: 5, section: 0))
                    let textfieldCell = cellTemp?.viewWithTag(22) as! UITextField
                    textfieldCell.becomeFirstResponder()
                } else {
                    textField.resignFirstResponder()
                }
            }
        }
        textField.resignFirstResponder()
        return true
    }
}
