//
//  MarketPlaceController.swift
//  EpcLensProject
//
//  Created by Moumita Datta on 28/06/18.
//  Copyright © 2018 Moumita Datta. All rights reserved.
//

import UIKit

class MarketPlaceController: UIViewController, MarketPlaceProtocol {
    
    @IBOutlet weak var collectionMarketPlaceMenu: UICollectionView!
    @IBOutlet weak var tableMarketPlace: UITableView!
    
    
    var marketPlaceDispatcher = MarketPlaceDispatcher()
    var marketPlaceAdsArr = [AdverMarketPlaceList]()
    var marketPlaceClassifiedArr = [ClassifiedMarketPlaceList]()
    var marketPlaceJobsArr = [JobsMarketPlaceList]()
    
    var marketPlaceMenuArr  = ["ads", "classifieds", "jobs"]
    
    //var selectedID : Int = -1
    var selectedAdsImgIndex : Int = -1
    
    var isAdsImg : Bool = false
    
    //Mark:: AnimatedLoader
    let loaderController = EpcLensLoaderViewController(nibName: "EpcLensLoaderViewController", bundle: nil)
    
    
    
    //MARK:: UIView
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        marketPlaceDispatcher.delegate = self
        var tempDict = [String:String]()
        tempDict["token"] = EpicLensConstantsVariables.accessToken
        marketPlaceDispatcher.GetMarketPlace(paramDict: tempDict)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    //MARK:: IBAction
    @IBAction func btnLeftMenuDidTap(_ sender: Any) {
        let elDrawer = self.navigationController?.parent as! KYDrawerController
        elDrawer.setDrawerState(.opened, animated: true)
    }
    
    @IBAction func btnRightMenuDidTap(_ sender: UIButton) {
        let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
        let objReportPopup = storyboardMain.instantiateViewController(withIdentifier: "RightMenuController") as! RightMenuController
        objReportPopup.showPopup(_controller: self)
    }
    
    //MARK:: UIButton Functions
    @objc func btnViewAllClassifiedAdsTap(_ sender: Any) {
        let storyboardMarketPlace = UIStoryboard(name: "MarketPlace", bundle: nil)
        let marketPlaceListVC = storyboardMarketPlace.instantiateViewController(withIdentifier: "MarketPlaceListVC") as! MarketPlaceListController
        marketPlaceListVC.isClassifiedAds = 0
        self.navigationController?.pushViewController(marketPlaceListVC, animated: false)
    }
    
    @objc func btnViewAllJobsAdsTap(_ sender: Any) {
        let storyboardMarketPlace = UIStoryboard(name: "MarketPlace", bundle: nil)
        let marketPlaceListVC = storyboardMarketPlace.instantiateViewController(withIdentifier: "MarketPlaceListVC") as! MarketPlaceListController
        marketPlaceListVC.isClassifiedAds = 1
        self.navigationController?.pushViewController(marketPlaceListVC, animated: false)
    }
    
    //MARK:: MarketPlaceListProtocol
    func didUpdateControllerWithObjMarketPlace(marketPlace: MarketPlaceModel) {
        DispatchQueue.main.async {
            if marketPlace.data.sucess {
                self.marketPlaceAdsArr = marketPlace.data.adver!
                self.marketPlaceClassifiedArr = marketPlace.data.classified!
                self.marketPlaceJobsArr = marketPlace.data.jobs!
                self.tableMarketPlace.delegate = self
                self.tableMarketPlace.dataSource = self
                self.tableMarketPlace.reloadData()
                // Stop Loaded
                self.loaderController.hideEpcLoader()
            }
        }
    }
}


extension MarketPlaceController : UITableViewDataSource, UITableViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    //MARK:: UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.section == 0) {
            let idstr = "MarketPlaceAdvertisementsTableCell"
            
            // get a reference to our storyboard cell
            var marktPlaceAdsCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
            if marktPlaceAdsCell == nil {
                marktPlaceAdsCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
            }
            
            
            //Declare the outlet in our custom class
            let imgViewMarktPlaceAds = marktPlaceAdsCell.viewWithTag(311) as! UIImageView
            let collectionMarktPlaceAds = marktPlaceAdsCell.viewWithTag(312) as! UICollectionView
            
            if isAdsImg == true {
                if marketPlaceAdsArr[selectedAdsImgIndex].mediaURL?.isEmpty == false && marketPlaceAdsArr[selectedAdsImgIndex].mediaURL != "" {
                    let escapedString = marketPlaceAdsArr[selectedAdsImgIndex].mediaURL?.replacingOccurrences(of: " ", with: "%20")
                    Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgViewMarktPlaceAds)
                } else {
                    imgViewMarktPlaceAds.image = UIImage(named: "AppIcon")
                }
            }
            
            
            //selectedID = 1
            
            collectionMarktPlaceAds.dataSource = self
            collectionMarktPlaceAds.delegate = self
            collectionMarktPlaceAds.reloadData()
            
            
            
            //marktPlaceAdsCell.selectionStyle = UITableViewCellSelectionStyle.none
            return marktPlaceAdsCell
        } else if (indexPath.section == 1) {
            let idstr = "MarketPlaceClassifiedTableCell"
            
            // get a reference to our storyboard cell
            var marktPlaceClassifiedCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
            if marktPlaceClassifiedCell == nil {
                marktPlaceClassifiedCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
            }
            
            //Declare the outlet in our custom class
            let collectionMarktPlaceClassifiedAds = marktPlaceClassifiedCell.viewWithTag(40) as! UICollectionView
            
            
            collectionMarktPlaceClassifiedAds.dataSource = self
            collectionMarktPlaceClassifiedAds.delegate = self
            collectionMarktPlaceClassifiedAds.reloadData()
            
            
            //selectedID = 2
            
            //marktPlaceClassifiedCell.selectionStyle = UITableViewCellSelectionStyle.none
            return marktPlaceClassifiedCell
        } else if (indexPath.section == 2) {
            let idstr = "MarketPlaceJobsTableCell"
            
            // get a reference to our storyboard cell
            var marktPlaceJobsCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
            if marktPlaceJobsCell == nil {
                marktPlaceJobsCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
            }
            
            //Declare the outlet in our custom class
            let collectionMarktPlaceJobs = marktPlaceJobsCell.viewWithTag(50) as! UICollectionView
            
            
            collectionMarktPlaceJobs.dataSource = self
            collectionMarktPlaceJobs.delegate = self
            collectionMarktPlaceJobs.reloadData()
            
            
            //selectedID = 3
            
            //marktPlaceJobsCell.selectionStyle = UITableViewCellSelectionStyle.none
            return marktPlaceJobsCell
        } else {
            let idstr = "MarketPlaceJobsTableCell"
            
            // get a reference to our storyboard cell
            var marktPlaceJobsCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
            if marktPlaceJobsCell == nil {
                marktPlaceJobsCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
            }
            
            marktPlaceJobsCell.selectionStyle = UITableViewCellSelectionStyle.none
            return marktPlaceJobsCell
        }
    }
    
    
    //MARK:: UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.section == 0) {
            return 270
        } else if (indexPath.section == 1) {
            return 90
        } else if (indexPath.section == 2) {
            return 275
        } else {
            return 0
        }
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        // get a reference to our storyboard cell
        let marketPlaceHeaderCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: "MarketPlaceHeaderCell")!
        
        //Declare the outlet in our custom class
        let lblHeaderTitle = marketPlaceHeaderCell.viewWithTag(20) as! UILabel
        let viewAll = marketPlaceHeaderCell.viewWithTag(21)
        let btnViewAll = marketPlaceHeaderCell.viewWithTag(25) as! UIButton
        
        if section == 0 {
            lblHeaderTitle.font = lblHeaderTitle.font.withSize(16)
            lblHeaderTitle.text = "advertisements"
            viewAll?.isHidden = true
        } else if section == 1 {
            lblHeaderTitle.font = lblHeaderTitle.font.withSize(16)
            lblHeaderTitle.text = "classified ads"
            viewAll?.isHidden = false
            viewAll?.layer.borderWidth = 0.5
            viewAll?.layer.borderColor = UIColor.black.cgColor
            
            btnViewAll.addTarget(self, action: #selector(btnViewAllClassifiedAdsTap), for: .touchUpInside)
        } else if section == 2 {
            lblHeaderTitle.font = lblHeaderTitle.font.withSize(16)
            lblHeaderTitle.text = "jobs"
            viewAll?.isHidden = false
            viewAll?.layer.borderWidth = 0.5
            viewAll?.layer.borderColor = UIColor.black.cgColor
            
            btnViewAll.addTarget(self, action: #selector(btnViewAllJobsAdsTap), for: .touchUpInside)
        }
        return marketPlaceHeaderCell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }
    
    
    
    
    // MARK:: UICollectionViewDataSource protocol
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionMarketPlaceMenu {
            //print("marketPlaceMenuArr.count ::\(marketPlaceMenuArr.count)")
            return marketPlaceMenuArr.count
        } else if collectionView.tag == 312 {//selectedID == 1 {
            //print("marketPlaceAdsArr.count ::\(marketPlaceAdsArr.count)")
            return marketPlaceAdsArr.count
        } else if collectionView.tag == 40 {//selectedID == 2 {
            //print("marketPlaceAdsArr.count ::\(marketPlaceAdsArr.count)")
            return marketPlaceClassifiedArr.count
        } else if collectionView.tag == 50 {//selectedID == 3 {
            //print("marketPlaceAdsArr.count ::\(marketPlaceAdsArr.count)")
            return marketPlaceJobsArr.count
        }
        return 0
    }
    
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionMarketPlaceMenu {
            let idstr = "MarketPlaceMenuCell"
            
            // get a reference to our storyboard cell
            let marketPlaceMenuCell : UICollectionViewCell! = collectionView.dequeueReusableCell(withReuseIdentifier: idstr, for: indexPath)
            
            
            // Use the outlet in our custom class to get a reference to the UI Elements in the cell
            let lblMenuTitle = marketPlaceMenuCell.viewWithTag(12) as! UILabel
            
            
            // Use the outlet in our custom class to get a reference to the UILabel in the cell
            lblMenuTitle.text = marketPlaceMenuArr[indexPath.row]
            
            
            //cell.backgroundColor = UIColor.cyan // make cell more visible in our example project
            marketPlaceMenuCell?.layer.cornerRadius = 5
            if indexPath.row == 0 {
                marketPlaceMenuCell.viewWithTag(11)?.backgroundColor = UIColor(red: CGFloat((191.0 / 255.0)), green: CGFloat((146.0 / 255.0)), blue: CGFloat((126.0 / 255.0)), alpha: 1.0)
            } else if indexPath.row == 1 {
                marketPlaceMenuCell.viewWithTag(11)?.backgroundColor = UIColor(red: CGFloat((110.0 / 255.0)), green: CGFloat((156.0 / 255.0)), blue: CGFloat((178.0 / 255.0)), alpha: 1.0)
            } else if indexPath.row == 2 {
                marketPlaceMenuCell.viewWithTag(11)?.backgroundColor = UIColor(red: CGFloat((165.0 / 255.0)), green: CGFloat((160.0 / 255.0)), blue: CGFloat((166.0 / 255.0)), alpha: 1.0)
            }
            
            var tempDict = [String:String]()
            tempDict["token"] = EpicLensConstantsVariables.accessToken
            marketPlaceDispatcher.GetMarketPlace(paramDict: tempDict)
            
            isAdsImg = true
            selectedAdsImgIndex = 0
            // Start Loaded
            loaderController.showEpcLoader(_sourceView: self)
            
            
            
            return marketPlaceMenuCell
        } else if collectionView.tag == 312 {//selectedID == 1 {
            
            let idstr = "MarketPlaceAdsCell"
            
            // get a reference to our storyboard cell
            let marketPlaceAdsCell : UICollectionViewCell! = collectionView.dequeueReusableCell(withReuseIdentifier: idstr, for: indexPath)
            
            // Use the outlet in our custom class to get a reference to the UI Elements in the cell
            let imgViewMarketPlaceAds = marketPlaceAdsCell.viewWithTag(3120) as! UIImageView
            
            if marketPlaceAdsArr[indexPath.row].mediaURL?.isEmpty == false && marketPlaceAdsArr[indexPath.row].mediaURL != "" {
                let escapedString = marketPlaceAdsArr[indexPath.row].mediaURL?.replacingOccurrences(of: " ", with: "%20")
                Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgViewMarketPlaceAds)
            } else {
                imgViewMarketPlaceAds.image = UIImage(named: "AppIcon")
            }
            
            return marketPlaceAdsCell
        } else if collectionView.tag == 40 {//selectedID == 2 {
            let idstr = "MarketPlaceClassifiedAdsCell"
            
            // get a reference to our storyboard cell
            let marketPlaceAdsCell : UICollectionViewCell! = collectionView.dequeueReusableCell(withReuseIdentifier: idstr, for: indexPath)
            
            // Use the outlet in our custom class to get a reference to the UI Elements in the cell
            let imgViewMarketPlaceClassifiedAds = marketPlaceAdsCell.viewWithTag(41) as! UIImageView
            let lblClassifiedTitle = marketPlaceAdsCell.viewWithTag(42) as! UILabel
            let lblClassifiedPrice = marketPlaceAdsCell.viewWithTag(43) as! UILabel
            let viewClassified = marketPlaceAdsCell.viewWithTag(44)
            
            
            imgViewMarketPlaceClassifiedAds.layer.cornerRadius = 25
            imgViewMarketPlaceClassifiedAds.layer.borderWidth = 1.0
            imgViewMarketPlaceClassifiedAds.layer.borderColor = UIColor.darkGray.cgColor
            imgViewMarketPlaceClassifiedAds.clipsToBounds = true
            
            viewClassified?.layer.borderWidth = 1.0
            viewClassified?.layer.borderColor = UIColor.darkGray.cgColor
            
            
            if marketPlaceClassifiedArr[indexPath.row].classifiedMediaURL?.isEmpty == false && marketPlaceClassifiedArr[indexPath.row].classifiedMediaURL != "" {
                let escapedString = marketPlaceClassifiedArr[indexPath.row].classifiedMediaURL?.replacingOccurrences(of: " ", with: "%20")
                Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgViewMarketPlaceClassifiedAds)
            } else {
                imgViewMarketPlaceClassifiedAds.image = UIImage(named: "AppIcon")
            }
            
            if (marketPlaceClassifiedArr[indexPath.row].classifiedTitle?.isEmpty)! {
                lblClassifiedTitle.isHidden =  true
            } else {
                lblClassifiedTitle.text =  marketPlaceClassifiedArr[indexPath.row].classifiedTitle!
            }
            
            if (marketPlaceClassifiedArr[indexPath.row].classifiedPrice?.isEmpty)! {
                lblClassifiedPrice.isHidden =  true
            } else {
                lblClassifiedPrice.text =  "$ " + marketPlaceClassifiedArr[indexPath.row].classifiedPrice!
            }
            
            
            return marketPlaceAdsCell
        } else {//if selectedID == 3 {
            let idstr = "MarketPlaceJobsAdsCell"
            
            // get a reference to our storyboard cell
            let marketPlaceJobsCell : UICollectionViewCell! = collectionView.dequeueReusableCell(withReuseIdentifier: idstr, for: indexPath)
            
            // Use the outlet in our custom class to get a reference to the UI Elements in the cell
            let viewFeature = marketPlaceJobsCell.viewWithTag(51)
            let imgViewMarketPlaceJobs = marketPlaceJobsCell.viewWithTag(52) as! UIImageView
            let lblJobTitle = marketPlaceJobsCell.viewWithTag(53) as! UILabel
            let lblCompanyName = marketPlaceJobsCell.viewWithTag(54) as! UILabel
            let lblJobsLocation = marketPlaceJobsCell.viewWithTag(55) as! UILabel
            let lblJobTimeAgo = marketPlaceJobsCell.viewWithTag(56) as! UILabel
            
            
            viewFeature?.layer.cornerRadius = 5
            viewFeature?.clipsToBounds = true
            
            
            if marketPlaceJobsArr[indexPath.row].companyLogo?.isEmpty == false && marketPlaceJobsArr[indexPath.row].companyLogo != "" {
                let escapedString = marketPlaceJobsArr[indexPath.row].companyLogo?.replacingOccurrences(of: " ", with: "%20")
                Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgViewMarketPlaceJobs)
            } else {
                imgViewMarketPlaceJobs.image = UIImage(named: "AppIcon")
            }
            
            if (marketPlaceJobsArr[indexPath.row].jobstitle?.isEmpty)! {
                lblJobTitle.isHidden =  true
            } else {
                lblJobTitle.text = marketPlaceJobsArr[indexPath.row].jobstitle!
            }
            
            if (marketPlaceJobsArr[indexPath.row].companyName?.isEmpty)! {
                lblCompanyName.isHidden =  true
            } else {
                lblCompanyName.text = marketPlaceJobsArr[indexPath.row].companyName!
            }
            
            if (marketPlaceJobsArr[indexPath.row].jobsLocation?.isEmpty)! {
                lblJobsLocation.isHidden =  true
            } else {
                lblJobsLocation.text = marketPlaceJobsArr[indexPath.row].jobsLocation!
            }
            
            if (marketPlaceJobsArr[indexPath.row].timeAgo?.isEmpty)! {
                lblJobTimeAgo.isHidden =  true
            } else {
                lblJobTimeAgo.text = marketPlaceJobsArr[indexPath.row].timeAgo!
            }
            
            
            return marketPlaceJobsCell
//        }  else {
//            let idstr = "MarketPlaceJobsAdsCell"
//
//            // get a reference to our storyboard cell
//            let marketPlaceJobsCell : UICollectionViewCell! = collectionView.dequeueReusableCell(withReuseIdentifier: idstr, for: indexPath)
//
//
//            return marketPlaceJobsCell
        }
    }
    
    
    
    // MARK:: UICollectionViewDelegate protocol
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collectionMarketPlaceMenu {
            let cellWidth = (self.view.frame.size.width-50)/3
            return CGSize(width: cellWidth, height: 35)
        } else if collectionView.tag == 312 {//selectedID == 1 {
            return CGSize(width: 75, height: 70)
        } else if collectionView.tag == 40 {//selectedID == 2 {
            let cellWidth = (self.view.frame.size.width-30)/2
            return CGSize(width: cellWidth, height: 70)
        } else if collectionView.tag == 50 {//if selectedID == 3 {
            return CGSize(width: 150, height: 240)
        }
        return CGSize(width: 0, height: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if collectionView == collectionMarketPlaceMenu {
            return UIEdgeInsetsMake(10.0, 15.0, 10.0, 15.0)
        } else if collectionView.tag == 312 {
            return UIEdgeInsetsMake(10.0, 10.0, 10.0, 10.0)
        } else if collectionView.tag == 40 {
            return UIEdgeInsetsMake(5.0, 10.0, 5.0, 10.0)
        } else if collectionView.tag == 50 {
            return UIEdgeInsetsMake(10.0, 10.0, 10.0, 10.0)
        }
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //print("selectedID Ads:: \(selectedID)")
        if collectionView == collectionMarketPlaceMenu {
            if indexPath.row == 0 {
                let storyboardMarketPlace = UIStoryboard(name: "MarketPlace", bundle: nil)
                let addAdvertisementVC = storyboardMarketPlace.instantiateViewController(withIdentifier: "AddAdvertisementVC") as! MarketPlaceAddAdvertisementController
                self.navigationController?.pushViewController(addAdvertisementVC, animated: false)
            } else if indexPath.row == 1 {
                let storyboardMarketPlace = UIStoryboard(name: "MarketPlace", bundle: nil)
                let addClassifiedVC = storyboardMarketPlace.instantiateViewController(withIdentifier: "AddClassifiedVC") as! MarketPlaceAddClassifiedController
                self.navigationController?.pushViewController(addClassifiedVC, animated: false)
            } else if indexPath.row == 2 {
                let storyboardMarketPlace = UIStoryboard(name: "MarketPlace", bundle: nil)
                let addJobsVC = storyboardMarketPlace.instantiateViewController(withIdentifier: "AddJobsVC") as! MarketPlaceAddJobsController
                self.navigationController?.pushViewController(addJobsVC, animated: false)
            }
        } else if collectionView.tag == 312 {//selectedID == 1 {
            isAdsImg = true
            selectedAdsImgIndex = indexPath.item
            print("selectedAdsImgIndex :: \(selectedAdsImgIndex)")
            tableMarketPlace.reloadData()
        } else if collectionView.tag == 40 {
            let storyboardMarketPlace = UIStoryboard(name: "MarketPlace", bundle: nil)
            let classifiedAdsDetailsVC = storyboardMarketPlace.instantiateViewController(withIdentifier: "ClassifiedAdsDetailsVC") as! ClassifiedAdsDetailsController
            classifiedAdsDetailsVC.classifiedAdsId = marketPlaceClassifiedArr[indexPath.row].id
            self.navigationController?.pushViewController(classifiedAdsDetailsVC, animated: true)
        } else if collectionView.tag == 50 {//selectedID == 1 {
            let storyboardMarketPlace = UIStoryboard(name: "MarketPlace", bundle: nil)
            let jobAdsDetailsVC = storyboardMarketPlace.instantiateViewController(withIdentifier: "JobsAdsDetailsVC") as! JobsAdDetailsController
            jobAdsDetailsVC.jobAdsId = marketPlaceJobsArr[indexPath.row].id
            self.navigationController?.pushViewController(jobAdsDetailsVC, animated: true)
        }
    }
    
}
