//
//  LensDeskDetailsController.swift
//  EpcLensProject
//
//  Created by Moumita Datta on 10/07/18.
//  Copyright © 2018 Moumita Datta. All rights reserved.
//

import UIKit

class LensDeskDetailsController: UIViewController, LensDeskDetailsProtocol {
    
    
    @IBOutlet var lblLensdeskDetailsHeader: UILabel!
    @IBOutlet var webViewLensdeskDetails: UIWebView!
    
    
    
    var lensDeskDetailsDispatcher = LensDeskDetailsDispatcher()
    var lensDeskDetailsArr = [LensdeskDetails]()
    
    
    var selectedLensDeskID : Int = -1
    
    
    //Mark:: AnimatedLoader
    let loaderController = EpcLensLoaderViewController(nibName: "EpcLensLoaderViewController", bundle: nil)
    
    
    
    //MARK:: UIView
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        lensDeskDetailsDispatcher.delegate = self
        var tempDict = [String:String]()
        tempDict["token"] = EpicLensConstantsVariables.accessToken
        tempDict["id"] = String(selectedLensDeskID)
        lensDeskDetailsDispatcher.GetLensDeskDetailsByID(paramDict: tempDict)
        
        
        webViewLensdeskDetails.backgroundColor = UIColor.clear
        webViewLensdeskDetails.isOpaque = false
        
        // Start Loaded
        loaderController.showEpcLoader(_sourceView: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:: IBAction
    @IBAction func btnBackDidTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:: LexiconDetailsProtocol
    func didUpdateControllerWithObjLensDeskDetails(lensDeskDetails:LensDeskDetailsModel) {
        DispatchQueue.main.async {
            if lensDeskDetails.data.sucess {
                self.lensDeskDetailsArr = lensDeskDetails.data.lensdeskdetails
                
                if let details = self.lensDeskDetailsArr[0].deskdetails {
                    if details.contains("<html>") {
                        self.webViewLensdeskDetails.loadHTMLString(details, baseURL: nil)
                    } else {
                        let modifiedString = "<html>" + details + "</html>"
                        self.webViewLensdeskDetails.loadHTMLString(modifiedString, baseURL: nil)
                    }
                }
                
                if (self.lensDeskDetailsArr[0].desktitle?.isEmpty)! {
                    self.lblLensdeskDetailsHeader.text = " "
                } else {
                    self.lblLensdeskDetailsHeader.text = self.lensDeskDetailsArr[0].desktitle
                }
                
                
                // Stop Loaded
                self.loaderController.hideEpcLoader()
            }
        }
    }
}
