//
//  MyLensListByCategoryController.swift
//  EpcLensProject
//
//  Created by Moumita Datta on 10/05/18.
//  Copyright © 2018 Moumita Datta. All rights reserved.
//

import UIKit

class MyLensListByCategoryController: UIViewController, MyLensListByCategoryProtocol {

    @IBOutlet weak var tableMyLensByCategory: UITableView!
    
    @IBOutlet weak var btnLenFlare: UIButton!
    @IBOutlet weak var btnMyProjects: UIButton!
    @IBOutlet weak var btnEPCSees: UIButton!
    @IBOutlet weak var btnMarketScape: UIButton!
    @IBOutlet weak var btnRadar: UIButton!
    @IBOutlet weak var btnMarketPlace: UIButton!
    @IBOutlet weak var btnLexicon: UIButton!
    
    var myLensByCategoryDispatcher = MyLensListByCategoryDispatcher()
    var myLensListByCategoryArr = [MyLensListByCategory]()
    
    //var slugValue :String = "lens_flare"
    
    
    //Mark:: AnimatedLoader
    let loaderController = EpcLensLoaderViewController(nibName: "EpcLensLoaderViewController", bundle: nil)
    
    
    
    //MARK:: UIView
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        
        self.tableMyLensByCategory.estimatedRowHeight = 95.0
        self.tableMyLensByCategory.rowHeight = UITableViewAutomaticDimension
        
        //self.tableMyLensByCategory.contentInset = UIEdgeInsetsMake(0, 0, 95, 0)
        
        
        myLensByCategoryDispatcher.delegate = self
        var tempDict = [String:String]()
        tempDict["token"] = EpicLensConstantsVariables.accessToken
        tempDict["slug"] = "lens_flare"
        myLensByCategoryDispatcher.GetMyLensListByCategory(paramDict: tempDict)
        
        
        
        
        // Start Loaded
        loaderController.showEpcLoader(_sourceView: self)
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:: IBAction
    @IBAction func btnLeftMenuDidTap(_ sender: Any) {
        let elDrawer = self.navigationController?.parent as! KYDrawerController
        elDrawer.setDrawerState(.opened, animated: true)
    }
    
    @IBAction func btnRightMenuDidTap(_ sender: UIButton) {
        
        //        let originalTransform = sender.transform
        //        let scaledTransform = originalTransform.scaledBy(x: 0.2, y: 0.2)
        //        let scaledAndTranslatedTransform = scaledTransform.translatedBy(x: 0.0, y: -250.0)
        //        UIView.animate(withDuration: 0.7, animations: {
        //            sender.transform = scaledAndTranslatedTransform
        //        })
        
        let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
        let objReportPopup = storyboardMain.instantiateViewController(withIdentifier: "RightMenuController") as! RightMenuController
        objReportPopup.showPopup(_controller: self)
    }
    
    @IBAction func btnMyLensFlipDidTap(_ sender: Any) {
        let storyboardMyLens = UIStoryboard(name: "MyLens", bundle: nil)
        let myLensListVC = storyboardMyLens.instantiateViewController(withIdentifier: "MyLensListVC") as! MyLensListController
        UIView.transition(with: (navigationController?.view)!, duration: 0.75, options: .transitionFlipFromRight, animations: {() -> Void in
            self.navigationController?.pushViewController(myLensListVC, animated: false)
        })
    }
    
    @IBAction func btnLensFlareDidTap(_ sender: Any) {
        myLensByCategoryDispatcher.delegate = self
        var tempDict = [String:String]()
        tempDict["token"] = EpicLensConstantsVariables.accessToken
        tempDict["slug"] = "lens_flare"
        myLensByCategoryDispatcher.GetMyLensListByCategory(paramDict: tempDict)
        //slugValue = "lens_flare"
        btnLenFlare.setImage(UIImage(named: "LensflareSelected"), for: UIControlState.normal)
        btnMyProjects.setImage(UIImage(named: "MyProjectsDeSelected"), for: UIControlState.normal)
        btnEPCSees.setImage(UIImage(named: "EpcSeesDeSelected"), for: UIControlState.normal)
        btnMarketScape.setImage(UIImage(named: "MarketScapeDeSelected"), for: UIControlState.normal)
        btnRadar.setImage(UIImage(named: "RadarDeSelected"), for: UIControlState.normal)
        btnMarketPlace.setImage(UIImage(named: "MarketPlaceDeSelected"), for: UIControlState.normal)
        btnLexicon.setImage(UIImage(named: "LexiconDeSelected"), for: UIControlState.normal)
        // Start Loaded
        loaderController.showEpcLoader(_sourceView: self)
    }
    
    @IBAction func btnMyProjectsDidTap(_ sender: Any) {
        myLensByCategoryDispatcher.delegate = self
        var tempDict = [String:String]()
        tempDict["token"] = EpicLensConstantsVariables.accessToken
        tempDict["slug"] = "ownership"
        myLensByCategoryDispatcher.GetMyLensListByCategory(paramDict: tempDict)
        //slugValue = "ownership"
        btnLenFlare.setImage(UIImage(named: "LensflareDeSelected"), for: UIControlState.normal)
        btnMyProjects.setImage(UIImage(named: "MyProjectsSelected"), for: UIControlState.normal)
        btnEPCSees.setImage(UIImage(named: "EpcSeesDeSelected"), for: UIControlState.normal)
        btnMarketScape.setImage(UIImage(named: "MarketScapeDeSelected"), for: UIControlState.normal)
        btnRadar.setImage(UIImage(named: "RadarDeSelected"), for: UIControlState.normal)
        btnMarketPlace.setImage(UIImage(named: "MarketPlaceDeSelected"), for: UIControlState.normal)
        btnLexicon.setImage(UIImage(named: "LexiconDeSelected"), for: UIControlState.normal)
        // Start Loaded
        loaderController.showEpcLoader(_sourceView: self)
    }
    
    @IBAction func btnEPCSeesDidTap(_ sender: Any) {
        var tempDict = [String:String]()
        tempDict["token"] = EpicLensConstantsVariables.accessToken
        tempDict["slug"] = "epc_sees"
        myLensByCategoryDispatcher.GetMyLensListByCategory(paramDict: tempDict)
        //slugValue = "epc_sees"
        btnLenFlare.setImage(UIImage(named: "LensflareDeSelected"), for: UIControlState.normal)
        btnMyProjects.setImage(UIImage(named: "MyProjectsDeSelected"), for: UIControlState.normal)
        btnEPCSees.setImage(UIImage(named: "EpcSeesSelected"), for: UIControlState.normal)
        btnMarketScape.setImage(UIImage(named: "MarketScapeDeSelected"), for: UIControlState.normal)
        btnRadar.setImage(UIImage(named: "RadarDeSelected"), for: UIControlState.normal)
        btnMarketPlace.setImage(UIImage(named: "MarketPlaceDeSelected"), for: UIControlState.normal)
        btnLexicon.setImage(UIImage(named: "LexiconDeSelected"), for: UIControlState.normal)
        // Start Loaded
        loaderController.showEpcLoader(_sourceView: self)
    }
    
    @IBAction func btnMarketScapeDidTap(_ sender: Any) {
        myLensByCategoryDispatcher.delegate = self
        var tempDict = [String:String]()
        tempDict["token"] = EpicLensConstantsVariables.accessToken
        tempDict["slug"] = ""
        myLensByCategoryDispatcher.GetMyLensListByCategory(paramDict: tempDict)
        //slugValue = ""
        btnLenFlare.setImage(UIImage(named: "LensflareDeSelected"), for: UIControlState.normal)
        btnMyProjects.setImage(UIImage(named: "MyProjectsDeSelected"), for: UIControlState.normal)
        btnEPCSees.setImage(UIImage(named: "EpcSeesDeSelected"), for: UIControlState.normal)
        btnMarketScape.setImage(UIImage(named: "MarketScapeSelected"), for: UIControlState.normal)
        btnRadar.setImage(UIImage(named: "RadarDeSelected"), for: UIControlState.normal)
        btnMarketPlace.setImage(UIImage(named: "MarketPlaceDeSelected"), for: UIControlState.normal)
        btnLexicon.setImage(UIImage(named: "LexiconDeSelected"), for: UIControlState.normal)
        // Start Loaded
        loaderController.showEpcLoader(_sourceView: self)
    }
    
    @IBAction func btnRadarDidTap(_ sender: Any) {
        myLensByCategoryDispatcher.delegate = self
        var tempDict = [String:String]()
        tempDict["token"] = EpicLensConstantsVariables.accessToken
        tempDict["slug"] = "post"
        myLensByCategoryDispatcher.GetMyLensListByCategory(paramDict: tempDict)
        //slugValue = "post"
        btnLenFlare.setImage(UIImage(named: "LensflareDeSelected"), for: UIControlState.normal)
        btnMyProjects.setImage(UIImage(named: "MyProjectsDeSelected"), for: UIControlState.normal)
        btnEPCSees.setImage(UIImage(named: "EpcSeesDeSelected"), for: UIControlState.normal)
        btnMarketScape.setImage(UIImage(named: "MarketScapeDeSelected"), for: UIControlState.normal)
        btnRadar.setImage(UIImage(named: "RadarSelected"), for: UIControlState.normal)
        btnMarketPlace.setImage(UIImage(named: "MarketPlaceDeSelected"), for: UIControlState.normal)
        btnLexicon.setImage(UIImage(named: "LexiconDeSelected"), for: UIControlState.normal)
        // Start Loaded
        loaderController.showEpcLoader(_sourceView: self)
    }
    
    @IBAction func btnMarketPlaceDidTap(_ sender: Any) {
        myLensByCategoryDispatcher.delegate = self
        var tempDict = [String:String]()
        tempDict["token"] = EpicLensConstantsVariables.accessToken
        tempDict["slug"] = "classified"
        myLensByCategoryDispatcher.GetMyLensListByCategory(paramDict: tempDict)
        //slugValue = "classified"
        btnLenFlare.setImage(UIImage(named: "LensflareDeSelected"), for: UIControlState.normal)
        btnMyProjects.setImage(UIImage(named: "MyProjectsDeSelected"), for: UIControlState.normal)
        btnEPCSees.setImage(UIImage(named: "EpcSeesDeSelected"), for: UIControlState.normal)
        btnMarketScape.setImage(UIImage(named: "MarketScapeDeSelected"), for: UIControlState.normal)
        btnRadar.setImage(UIImage(named: "RadarDeSelected"), for: UIControlState.normal)
        btnMarketPlace.setImage(UIImage(named: "MarketPlaceSelected"), for: UIControlState.normal)
        btnLexicon.setImage(UIImage(named: "LexiconDeSelected"), for: UIControlState.normal)
        // Start Loaded
        loaderController.showEpcLoader(_sourceView: self)
    }
    
    @IBAction func btnLexIconDidTap(_ sender: Any) {
        myLensByCategoryDispatcher.delegate = self
        var tempDict = [String:String]()
        tempDict["token"] = EpicLensConstantsVariables.accessToken
        tempDict["slug"] = "lexicon"
        myLensByCategoryDispatcher.GetMyLensListByCategory(paramDict: tempDict)
        //slugValue = "lexicon"
        btnLenFlare.setImage(UIImage(named: "LensflareDeSelected"), for: UIControlState.normal)
        btnMyProjects.setImage(UIImage(named: "MyProjectsDeSelected"), for: UIControlState.normal)
        btnEPCSees.setImage(UIImage(named: "EpcSeesDeSelected"), for: UIControlState.normal)
        btnMarketScape.setImage(UIImage(named: "MarketScapeDeSelected"), for: UIControlState.normal)
        btnRadar.setImage(UIImage(named: "RadarDeSelected"), for: UIControlState.normal)
        btnMarketPlace.setImage(UIImage(named: "MarketPlaceDeSelected"), for: UIControlState.normal)
        btnLexicon.setImage(UIImage(named: "LexiconSelected"), for: UIControlState.normal)
        // Start Loaded
        loaderController.showEpcLoader(_sourceView: self)
    }
    
    
    
    //MARK:: MyLensListByCategoryProtocol
    func didUpdateControllerWithObjMyLensListByCategory(myLensListByCategory: MyLensListByCategoryModel) {
        DispatchQueue.main.async {
            if myLensListByCategory.data.sucess {
                self.myLensListByCategoryArr = myLensListByCategory.data.mylensdetailsbyslug
                self.tableMyLensByCategory.delegate = self
                self.tableMyLensByCategory.dataSource = self
                self.tableMyLensByCategory.reloadData()
                // Stop Loaded
                self.loaderController.hideEpcLoader()
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension MyLensListByCategoryController : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myLensListByCategoryArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let idstr = "MyLensByCategoryCell"
        
        // get a reference to our storyboard cell
        var cell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: idstr)
        }
        
        // Use the outlet in our custom class to get a reference to the UI Elements in the cell
        let imgProfile = cell.viewWithTag(1) as! UIImageView
        let lblName = cell.viewWithTag(2) as! UILabel
        let lblShortDesc = cell.viewWithTag(3) as! UILabel
        
        
        if myLensListByCategoryArr[indexPath.row].icon?.isEmpty == false && myLensListByCategoryArr[indexPath.row].icon != "" {
            let escapedString = myLensListByCategoryArr[indexPath.row].icon?.replacingOccurrences(of: " ", with: "%20")
            Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgProfile)
        } else {
            imgProfile.image = UIImage(named: "AppIcon")
        } 
        
        if (myLensListByCategoryArr[indexPath.row].details?.isEmpty)! {
            lblName.text =  " "
        } else {
            lblName.text =  myLensListByCategoryArr[indexPath.row].details
        }
        
        
        
        if (myLensListByCategoryArr[indexPath.row].shortDesc?.isEmpty)! {
            lblShortDesc.text =  " "
        } else {
            lblShortDesc.text =  myLensListByCategoryArr[indexPath.row].shortDesc
        }
        
        
        
        //
        //                let lblAddress = cell.viewWithTag(4) as! UILabel
        //                lblAddress.text =  peopleListArr[indexPath.row].location
        //
        //                let btnContact = cell.viewWithTag(5) as! UIButton
        //                btnContact.addTarget(self, action:#selector(self.btnContactPressed(_sender:)), for: UIControlEvents.touchUpInside)
        //
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    /*func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        self.tableMyLensByCategory.estimatedRowHeight = 95.0
        return UITableViewAutomaticDimension
    }*/
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
}

