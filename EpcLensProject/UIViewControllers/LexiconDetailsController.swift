//
//  LexiconDetailsController.swift
//  EpcLensProject
//
//  Created by Moumita Datta on 31/05/18.
//  Copyright © 2018 Moumita Datta. All rights reserved.
//

import UIKit

class LexiconDetailsController: UIViewController, LexiconDetailsProtocol {
    //Navigation Bar
    @IBOutlet var lblNavigationTitle: UILabel!
    var navigationTitleStr : String = ""
    
    
    @IBOutlet weak var tableLexiconDetails: UITableView!
    
    
    
    var lexiconDetailsDispatcher = LexiconDetailsDispatcher()
    var lexiconDetailsArr = [LexiconDetailsResult]()
    
    
    var selectedLexiconID:Int = 0
    var selectedLexiconItemID:Int = 0
    
    //Mark:: AnimatedLoader
    let loaderController = EpcLensLoaderViewController(nibName: "EpcLensLoaderViewController", bundle: nil)
    
    
    
    //MARK:: UIView
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        lblNavigationTitle.text = navigationTitleStr
        
        self.tableLexiconDetails.rowHeight = UITableViewAutomaticDimension
        //  . self.tableLexiconDetails.estimatedRowHeight = 200.0
        
        lexiconDetailsDispatcher.delegate = self
        var tempDict = [String:String]()
        tempDict["token"] = EpicLensConstantsVariables.accessToken
        tempDict["id"] = String(selectedLexiconID)
        tempDict["itemid"] = String(selectedLexiconItemID)
        lexiconDetailsDispatcher.GetLexiconDetailsByCategory(paramDict: tempDict)
        
        
        // Start Loaded
        loaderController.showEpcLoader(_sourceView: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:: IBAction
    @IBAction func btnBackDidTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    //MARK:: LexiconDetailsProtocol
    func didUpdateControllerWithObjLexiconDetails(lexiconDetails: LexiconDetailsModel) {
        DispatchQueue.main.async {
            if lexiconDetails.data.sucess {
                self.lexiconDetailsArr = lexiconDetails.data.result
                self.tableLexiconDetails.delegate = self
                self.tableLexiconDetails.dataSource = self
                self.tableLexiconDetails.reloadData()
                // Stop Loaded
                self.loaderController.hideEpcLoader()
            }
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}



extension LexiconDetailsController : UITableViewDataSource, UITableViewDelegate {
    
    //MARK:: UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //print("lexiconDetailsArr.count :: \(lexiconDetailsArr.count)")
        if section == 0 {
            return 1
        } else {
            return lexiconDetailsArr.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            // get a reference to our storyboard cell
            var lexiconDetailsHeaderCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: "LexiconDetailsHeaderCell")!
            
            if lexiconDetailsHeaderCell == nil {
                lexiconDetailsHeaderCell = UITableViewCell(style: .default, reuseIdentifier: "LexiconDetailsHeaderCell")
            }
            
            // Use the outlet in our custom class to get a reference to the UI Elements in the cell
            let lblLexiconDetailsHeader = lexiconDetailsHeaderCell.viewWithTag(10) as! UILabel
            
            if lexiconDetailsArr.count > 0 {
                lblLexiconDetailsHeader.text = lexiconDetailsArr[0].title
            }
            
            return lexiconDetailsHeaderCell
        } else {
            let idstr = "LexiconDetailsTableCell"
            
            // get a reference to our storyboard cell
            var lexiconDetailsCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
            if lexiconDetailsCell == nil {
                lexiconDetailsCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
            }
            
            // Use the outlet in our custom class to get a reference to the UI Elements in the cell
            let lblLexiconDetails = lexiconDetailsCell.viewWithTag(25) as! UILabel
            lexiconDetailsCell.viewWithTag(20)?.layer.cornerRadius = 10.0
        
        
            if lexiconDetailsArr.count > 0 {
                lblLexiconDetails.attributedText = lexiconDetailsArr[indexPath.row].details?.attributedHtmlString
            }
            
        
            
            return lexiconDetailsCell
        }
    }
    
    
    //MARK:: UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 50
        } else {
            return UITableViewAutomaticDimension
        }
    }
    
    /*func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let lexiconDetailsHeaderCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: "LexiconDetailsHeaderCell")!
        
        
        //            let viewAddBtn = lexiconHeaderCell.viewWithTag(13)
        //            let viewTop = lexiconHeaderCell.viewWithTag(11)
        //            let viewDown = lexiconHeaderCell.viewWithTag(12)
//        let lblDetailsHeaderTitle = lexiconDetailsHeaderCell.viewWithTag(25) as! UILabel
//        
//        
//        
//        lblDetailsHeaderTitle.text = "new and trending"
//        lexiconDetailsHeaderCell.viewWithTag(14)?.isHidden = true
        
        return lexiconDetailsHeaderCell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }*/
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 15
    }
    
    
    
    //    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
    //        if indexPath.section == 0 {
    //            return 150
    //        } else {
    //            return 35
    //        }
    //    }
    
    
    /*func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
     return 85.0
     }*/
    
    
    
}
