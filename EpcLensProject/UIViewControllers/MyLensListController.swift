//
//  MyLensListController.swift
//  EpcLensProject
//
//  Created by Moumita Datta on 09/05/18.
//  Copyright © 2018 Moumita Datta. All rights reserved.
//

import UIKit

class MyLensListController: UIViewController, MyLensListProtocol {
    
    @IBOutlet weak var tableMyLens: UITableView!
    
    var myLensDispatcher = MyLensDispatcher()
    var myLensListArr = [MyLensList]()
    
    
    //Mark:: AnimatedLoader
    let loaderController = EpcLensLoaderViewController(nibName: "EpcLensLoaderViewController", bundle: nil)
    
    
    //MARK:: UIView
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        myLensDispatcher.delegate = self
        var tempDict = [String:String]()
        tempDict["token"] = EpicLensConstantsVariables.accessToken
        tempDict["page"] = "0"
        myLensDispatcher.GetMyLensList(paramDict: tempDict)
        
        // Start Loaded
        loaderController.showEpcLoader(_sourceView: self)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:: IBAction
    @IBAction func btnLeftMenuDidTap(_ sender: Any) {
        let elDrawer = self.navigationController?.parent as! KYDrawerController
        elDrawer.setDrawerState(.opened, animated: true)
    }
    
    @IBAction func btnRightMenuDidTap(_ sender: UIButton) {
        
        //        let originalTransform = sender.transform
        //        let scaledTransform = originalTransform.scaledBy(x: 0.2, y: 0.2)
        //        let scaledAndTranslatedTransform = scaledTransform.translatedBy(x: 0.0, y: -250.0)
        //        UIView.animate(withDuration: 0.7, animations: {
        //            sender.transform = scaledAndTranslatedTransform
        //        })
        
        let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
        let objReportPopup = storyboardMain.instantiateViewController(withIdentifier: "RightMenuController") as! RightMenuController
        objReportPopup.showPopup(_controller: self)
    }
    
    @IBAction func btnMyLensFlipDidTap(_ sender: Any) {
        let storyboardMyLens = UIStoryboard(name: "MyLens", bundle: nil)
        let myLensByCategoryVC = storyboardMyLens.instantiateViewController(withIdentifier: "MyLensListByCategoryVC") as! MyLensListByCategoryController
        UIView.transition(with: (navigationController?.view)!, duration: 0.75, options: .transitionFlipFromRight, animations: {() -> Void in
            self.navigationController?.pushViewController(myLensByCategoryVC, animated: false)
        })
    }
    
    
    
    //MARK:: MyLensListProtocol
    func didUpdateControllerWithObjMyLensList(myLensList:MyLensListModel) {
        DispatchQueue.main.async {
            if myLensList.data.sucess {
                self.myLensListArr = myLensList.data.mylenslist
                self.tableMyLens.delegate = self
                self.tableMyLens.dataSource = self
                self.tableMyLens.reloadData()
                // Stop Loaded
                self.loaderController.hideEpcLoader()
            }
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}



extension MyLensListController : UITableViewDataSource, UITableViewDelegate {
    //MARK:: UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myLensListArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let idstr = "MyLensCell"
        
        // get a reference to our storyboard cell
        var myLensCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
        if myLensCell == nil {
            myLensCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
        }
        
        // Use the outlet in our custom class to get a reference to the UI Elements in the cell
        let lblName = myLensCell.viewWithTag(3) as! UILabel
        let imgProfileThumbnail = myLensCell.viewWithTag(2) as! UIImageView
        let imgCategory = myLensCell.viewWithTag(1) as! UIImageView
        
        
        if (myLensListArr[indexPath.row].details?.isEmpty)! {
            lblName.text =  " "
        } else {
            lblName.text =  myLensListArr[indexPath.row].details
        }
        
        
        myLensCell.viewWithTag(2)?.layer.cornerRadius = (myLensCell.viewWithTag(2)?.frame.size.width)!/2
        myLensCell.viewWithTag(2)?.layer.borderColor = UIColor.gray.cgColor
        myLensCell.viewWithTag(2)?.layer.borderWidth = 2.0
        myLensCell.viewWithTag(2)?.clipsToBounds = true
        
        let profileImgUrlString = myLensListArr[indexPath.row].icon
        print("profileImgUrlString :: \(String(describing: profileImgUrlString))")
        
        if profileImgUrlString?.isEmpty == false && profileImgUrlString != "" {
            let escapedString = profileImgUrlString?.replacingOccurrences(of: " ", with: "%20")
            Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgProfileThumbnail)
        } else {
            imgProfileThumbnail.image = UIImage(named: "AppIcon")
        }
                
        
        if myLensListArr[indexPath.row].tableName.rawValue == "people" {
            imgCategory.image = UIImage(named: "top_people_icon")
        } else if myLensListArr[indexPath.row].tableName.rawValue == "company" {
            imgCategory.image = UIImage(named: "top_companies_icon")
        } else if myLensListArr[indexPath.row].tableName.rawValue == "project" {
            imgCategory.image = UIImage(named: "top_projects_icon")
        }
        
        
        return myLensCell
    }
    
    
    //MARK:: UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if myLensListArr[indexPath.row].tableName.rawValue == "project" {
            let storyboardPeople = UIStoryboard(name: "Project", bundle: nil)
            let projectVC = storyboardPeople.instantiateViewController(withIdentifier: "ProjectListVC") as! ProjectListController
            //projectVC.peopleId = myLensListArr[indexPath.row].id
            self.navigationController?.pushViewController(projectVC, animated: true)
        } else if myLensListArr[indexPath.row].tableName.rawValue == "company" {
            let storyboardCompany = UIStoryboard(name: "Company", bundle: nil)
            let companyDetailVC = storyboardCompany.instantiateViewController(withIdentifier: "CompanyDetailController") as! CompanyDetailController
            print("Company Id::\(String(describing: myLensListArr[indexPath.row].itemID))")
            companyDetailVC.companyId = myLensListArr[indexPath.row].itemID!
            companyDetailVC.selectedOptionMenuID = 0
            self.navigationController?.pushViewController(companyDetailVC, animated: true)            
        } else if myLensListArr[indexPath.row].tableName.rawValue == "people" {
            let storyboardPeople = UIStoryboard(name: "People", bundle: nil)
            let peopleVC = storyboardPeople.instantiateViewController(withIdentifier: "PeopleDetailController") as! PeopleDetailController
            //print("People Id::\(myLensListArr[indexPath.row])")
            peopleVC.peopleId = Int(myLensListArr[indexPath.row].itemID!)!
            self.navigationController?.pushViewController(peopleVC, animated: true)
        }
    }
}
