//
//  SearchController.swift
//  EpcLensProject
//
//  Created by BlueHorse MacBook Pro on 23/07/18.
//  Copyright © 2018 Moumita Datta. All rights reserved.
//

import UIKit

class SearchController: UIViewController {
    
    
    @IBOutlet var lblNavigationTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK:: IBAction
    @IBAction func btnBackDidTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}
