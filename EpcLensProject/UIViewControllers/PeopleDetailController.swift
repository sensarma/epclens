//
//  PeopleDetailController.swift
//  EpcLensProject
//
//  Created by Surajit Ghosh on 08/05/18.
//  Copyright © 2018 Sudipta Biswas. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation


class PeopleDetailController: UIViewController, PeopleDetailProtocol {
    
    var peopleDetailDispatcher = PeopleDetailDispatcher()
    var peopleDetailsArr = [PeopleDetail]()
    var peopleProjectArr = [PeopleProject]()
    var peopleGalleryArr = [PeopleGallery]()
    
    
    var objSelectedPeopleGalleryItem:PeopleGallery? = nil
    var peopleId : Int = -1
    private var isPlayVideo = false
    
    //var selectedGaleryID : Int = -1
    
    @IBOutlet weak var tablePeopleDetail: UITableView!
    
    //Mark:: AnimatedLoader
    let loaderController = EpcLensLoaderViewController(nibName: "EpcLensLoaderViewController", bundle: nil)
    
    //UIView
    override func viewDidLoad() {
        super.viewDidLoad()
        tablePeopleDetail.rowHeight = UITableViewAutomaticDimension
        tablePeopleDetail.estimatedRowHeight = 2000
        peopleDetailDispatcher.delegate = self
        var tempDict = [String:String]()
        tempDict["token"] = EpicLensConstantsVariables.accessToken
        tempDict["id"] = String(peopleId)
        peopleDetailDispatcher.GetPeopleDetail(paramDict:tempDict)
        
        // Start Loaded
        loaderController.showEpcLoader(_sourceView: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didUpdateControllerWithPeopleData(peopleDetailsData:PeopleDetailModel){
        DispatchQueue.main.async {
            if peopleDetailsData.data.sucess {
                self.peopleDetailsArr = peopleDetailsData.data.peopleDetails!
                self.peopleProjectArr = peopleDetailsData.data.peopleProject!
                self.peopleGalleryArr = peopleDetailsData.data.peopleresultArray!
                
                self.tablePeopleDetail.delegate = self
                self.tablePeopleDetail.dataSource = self
                self.tablePeopleDetail.reloadData()
                
                // Stop Loaded
                self.loaderController.hideEpcLoader()
            }
        }
    }
    
    //MARK:: IBAction
    @IBAction func btnBackDidTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:: UIButton Functions
    @objc func btnLinkedInTap(_ sender: Any) {
        UIApplication.shared.open(NSURL(string: "http://www.linkedin.com/")! as URL, options: [:], completionHandler: nil)
    }
}

extension PeopleDetailController : UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         if (indexPath.section == 0) {
            let idstr = "peoplecell"
            
            var cell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
            if cell == nil {
                cell = UITableViewCell(style: .default, reuseIdentifier: idstr)
            }
            
            //Declare the outlet in our custom class
            let viewBack = cell.viewWithTag(100)!
            let viewNameBack = cell.viewWithTag(105)!
            let lblName = cell.viewWithTag(1) as! UILabel
            let lblDesignation = cell.viewWithTag(2) as! UILabel
            let lblworkingFrom = cell.viewWithTag(3) as! UILabel
            let lblPhone = cell.viewWithTag(4) as! UILabel
            let lblEmail = cell.viewWithTag(5) as! UILabel
            let lblQuation = cell.viewWithTag(6) as! UILabel
            let lblNobelProjHeader = cell.viewWithTag(7) as! UILabel
            let lblNobelProj = cell.viewWithTag(8) as! UILabel
            let lblNobelStoryHeader = cell.viewWithTag(9) as! UILabel
            let lblNobelStory = cell.viewWithTag(10) as! UILabel
            let lblProjHeader = cell.viewWithTag(11) as! UILabel
            let collectionViewProject = cell.viewWithTag(12) as! UICollectionView
            
            
            
            let imgPeopleProfile = cell.viewWithTag(110) as! UIImageView
            let imgLogo = cell.viewWithTag(120) as! UIImageView
            let btnLinkedIn = cell.viewWithTag(130) as! UIButton
            
            imgLogo.layer.cornerRadius = 3.0
            imgLogo.clipsToBounds = true
            
            viewBack.layer.cornerRadius = 10.0
            viewBack.clipsToBounds = true
            
            viewNameBack.layer.cornerRadius = 10.0
            viewNameBack.clipsToBounds = true
            
            
            if peopleDetailsArr.count > 0 {
                if peopleDetailsArr[indexPath.item].img?.isEmpty == false && peopleDetailsArr[indexPath.item].img != "" {
                    let escapedString = peopleDetailsArr[indexPath.item].img?.replacingOccurrences(of: " ", with: "%20")
                    Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgPeopleProfile)
                } else {
                    imgPeopleProfile.image = UIImage(named: "AppIcon")
                }
                
                if peopleDetailsArr[indexPath.item].companyLogo?.isEmpty == false && peopleDetailsArr[indexPath.item].companyLogo != "" {
                    let escapedString = peopleDetailsArr[indexPath.item].companyLogo?.replacingOccurrences(of: " ", with: "%20")
                    Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgLogo)
                } else {
                    imgLogo.image = UIImage(named: "AppIcon")
                }
                
                if (peopleDetailsArr[indexPath.item].linkedinURL == nil) || (peopleDetailsArr[indexPath.item].linkedinURL! == "") || (peopleDetailsArr[indexPath.item].linkedinURL! == "(NULL)") || (peopleDetailsArr[indexPath.item].linkedinURL! == "<NULL>") || ((peopleDetailsArr[indexPath.item].linkedinURL! == "<null>") || (peopleDetailsArr[indexPath.item].linkedinURL! == "(null)") || (peopleDetailsArr[indexPath.item].linkedinURL! == "")) {
                    btnLinkedIn.isHidden = true
                } else {
                    btnLinkedIn.isHidden = false
                    btnLinkedIn.addTarget(self, action: #selector(btnLinkedInTap), for: .touchUpInside)
                }
                
                
                if (peopleDetailsArr[indexPath.item].peopleName == nil) || (peopleDetailsArr[indexPath.item].peopleName! == "") || (peopleDetailsArr[indexPath.item].peopleName! == "(NULL)") || (peopleDetailsArr[indexPath.item].peopleName! == "<NULL>") || ((peopleDetailsArr[indexPath.item].peopleName! == "<null>") || (peopleDetailsArr[indexPath.item].peopleName! == "(null)") || (peopleDetailsArr[indexPath.item].peopleName! == "")) {
                    lblName.text = " "
                } else {
                    lblName.text =  peopleDetailsArr[indexPath.item].peopleName
                }
                
                if (peopleDetailsArr[indexPath.item].designation == nil) || (peopleDetailsArr[indexPath.item].designation! == "") || (peopleDetailsArr[indexPath.item].designation! == "(NULL)") || (peopleDetailsArr[indexPath.item].designation! == "<NULL>") || ((peopleDetailsArr[indexPath.item].designation! == "<null>") || (peopleDetailsArr[indexPath.item].designation! == "(null)") || (peopleDetailsArr[indexPath.item].email! == "")) {
                    lblDesignation.text = " "
                } else {
                    lblDesignation.text =  peopleDetailsArr[indexPath.item].designation!
                }
                
                if (peopleDetailsArr[indexPath.item].workingFrom == nil) || (peopleDetailsArr[indexPath.item].workingFrom! == "") || (peopleDetailsArr[indexPath.item].workingFrom! == "(NULL)") || (peopleDetailsArr[indexPath.item].workingFrom! == "<NULL>") || ((peopleDetailsArr[indexPath.item].workingFrom! == "<null>") || (peopleDetailsArr[indexPath.item].workingFrom! == "(null)") || (peopleDetailsArr[indexPath.item].workingFrom! == "")) {
                    lblworkingFrom.text = " "
                } else {
                    lblworkingFrom.text =  peopleDetailsArr[indexPath.item].workingFrom!
                }
                
                
                if (peopleDetailsArr[0].phoneNo == nil) || (peopleDetailsArr[0].phoneNo! == "") || (peopleDetailsArr[0].phoneNo! == "(NULL)") || (peopleDetailsArr[0].phoneNo! == "<NULL>") || ((peopleDetailsArr[0].phoneNo! == "<null>") || (peopleDetailsArr[0].phoneNo! == "(null)") || (peopleDetailsArr[0].phoneNo! == "")) {
                    lblPhone.text = " "
                } else {
                    lblPhone.text =  peopleDetailsArr[0].phoneNo!
                }
                
                if (peopleDetailsArr[0].email == nil) || (peopleDetailsArr[0].email! == "") || (peopleDetailsArr[0].email! == "(NULL)") || (peopleDetailsArr[0].email! == "<NULL>") || ((peopleDetailsArr[0].email! == "<null>") || (peopleDetailsArr[0].email! == "(null)") || (peopleDetailsArr[0].email! == "")) {
                    lblEmail.text = " "
                } else {
                    lblEmail.text =  peopleDetailsArr[0].email!
                }
                
                //print("peopleDetailsArr[0].peopleQuotation? :: \(String(describing: peopleDetailsArr[0].peopleQuotation))")
                
                
                if (peopleDetailsArr[0].peopleQuotation == nil) || (peopleDetailsArr[0].peopleQuotation! == "") || (peopleDetailsArr[0].peopleQuotation! == "(NULL)") || (peopleDetailsArr[0].peopleQuotation! == "<NULL>") || ((peopleDetailsArr[0].peopleQuotation! == "<null>") || (peopleDetailsArr[0].peopleQuotation! == "(null)") || (peopleDetailsArr[0].peopleQuotation! == "")) {
                    lblQuation.text = " "
                } else {
                    lblQuation.text = peopleDetailsArr[0].peopleQuotation!
                }
                
                
                if (peopleDetailsArr[0].notableProject == nil) || (peopleDetailsArr[0].notableProject! == "") || (peopleDetailsArr[0].notableProject! == "(NULL)") || (peopleDetailsArr[0].notableProject! == "<NULL>") || ((peopleDetailsArr[0].notableProject! == "<null>") || (peopleDetailsArr[0].notableProject! == "(null)") || (peopleDetailsArr[0].notableProject! == "")) {
                    lblNobelProjHeader.isHidden = true
                    //lblNobelProjHeader.frame.height = 0.0
                    lblNobelProj.text = " "
                } else {
                    lblNobelProjHeader.isHidden = false
                    lblNobelProjHeader.text =  "notable projects:"
                    lblNobelProj.text = peopleDetailsArr[0].notableProject!
                }
                
                
                if (peopleDetailsArr[0].notableStory == nil) || (peopleDetailsArr[0].notableStory! == "") || (peopleDetailsArr[0].notableStory! == "(NULL)") || (peopleDetailsArr[0].notableStory! == "<NULL>") || ((peopleDetailsArr[0].notableStory! == "<null>") || (peopleDetailsArr[0].notableStory! == "(null)") || (peopleDetailsArr[0].notableStory! == "")) {
                    lblNobelStoryHeader.isHidden = true
                    lblNobelStory.text = " "
                } else {
                    lblNobelStoryHeader.isHidden = false
                    lblNobelStoryHeader.text =  "notable story:"
                    lblNobelStory.text = peopleDetailsArr[0].notableStory!
                }
                
                if peopleProjectArr.count == 0 {
                    lblProjHeader.isHidden = true
                } else {
                    lblProjHeader.isHidden = false
                    lblProjHeader.text =  "projects worked:"
                }
                
                
                collectionViewProject.dataSource = self
                collectionViewProject.delegate = self
                collectionViewProject.reloadData()
                //cell.setCollectionViewDataSourceDelegate(collectionView: collectionViewProject, dataSourceDelegate: self, forRow: indexPath.row)
                
            }
            return cell
        } else {
            let idstr = "imagevideocell"
            var cell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
            if cell == nil {
                cell = UITableViewCell(style: .default, reuseIdentifier: idstr)
            }
            
            //Declare the outlet in our custom class
            let collectionViewImageVideo = cell.viewWithTag(20) as! UICollectionView
            
            //let collImageVideo = cell.viewWithTag(20) as! UICollectionView
            //cell.setCollectionViewDataSourceDelegate(collectionView: collImageVideo, dataSourceDelegate: self, forRow: indexPath.row)
            
            collectionViewImageVideo.dataSource = self
            collectionViewImageVideo.delegate = self
            collectionViewImageVideo.reloadData()
            
            if self.isPlayVideo == true {
                // initialize the video player with the url
                let videoURL = URL(string: (self.objSelectedPeopleGalleryItem?.url)!)
                if videoURL != nil {
                    let player = AVPlayer(url: videoURL!)
                    let playerViewController = AVPlayerViewController()
                    playerViewController.player = player
                    self.present(playerViewController, animated: true) {
                        playerViewController.player!.play()
                    }
                    
                    /*let player = AVPlayer(url: videoURL!)
                    let playerLayer = AVPlayerLayer(player: player)
                    playerLayer.frame = self.view.bounds
                    self.view.layer.addSublayer(playerLayer)
                    player.play()
                     
                     
                    let player = AVPlayer(url: videoURL!)
                    let playerLayer = AVPlayerLayer(player: player)
                    playerLayer.frame = self.view.bounds
                    self.view.layer.addSublayer(playerLayer)
                    player.play()*/
                } else {
                    EpcLensController.sharedInstance.showAlert(_sourceController: self, _msg: EpcLensMessages.InVaild_VideoUrl)
                }
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.section == 0) {
            /*var ht : CGFloat = 0.0
            if peopleProjectArr.count == 0 {
                ht = UITableViewAutomaticDimension-80
            } else {
                ht = UITableViewAutomaticDimension
            }*/
            return UITableViewAutomaticDimension
        } else {
            return 150.0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension PeopleDetailController: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 12 {
             return peopleProjectArr.count
        } else {
            return peopleGalleryArr.count
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 12 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collprojcell", for: indexPath)
            
            let viewBack = cell.viewWithTag(2)!
            viewBack.layer.cornerRadius = 4.0
            viewBack.clipsToBounds = true
            
            if peopleProjectArr.count > 0 {
                let lblProjTitle = cell.viewWithTag(1) as! UILabel
                lblProjTitle.text =  peopleProjectArr[indexPath.item].projectName!
            }
            
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collimgvideocell", for: indexPath)
            
            let viewBack = cell.viewWithTag(3)!
            viewBack.layer.cornerRadius = 5.0
            viewBack.clipsToBounds = true
            
            //Declare the outlet in our custom class
            let imgProjPeople = cell.viewWithTag(111) as! UIImageView
            let imgVideoPeople = cell.viewWithTag(115) as! UIImageView
            let lblProjTitle = cell.viewWithTag(112) as! UILabel
            
            if peopleGalleryArr.count > 0 {
                if peopleGalleryArr[indexPath.row].contentType!.rawValue == "video" {
                    imgVideoPeople.isHidden = false
                    if peopleGalleryArr[indexPath.row].videoImg?.isEmpty == false && peopleGalleryArr[indexPath.item].videoImg != "" {
                        let escapedString = peopleGalleryArr[indexPath.item].videoImg?.replacingOccurrences(of: " ", with: "%20")
                        Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgProjPeople)
                    } else {
                        imgProjPeople.image = UIImage(named: "AppIcon")
                    }
                } else {
                    imgVideoPeople.isHidden = true
                    if peopleGalleryArr[indexPath.row].mediaURL?.isEmpty == false && peopleGalleryArr[indexPath.item].mediaURL != "" {
                        let escapedString = peopleGalleryArr[indexPath.item].mediaURL?.replacingOccurrences(of: " ", with: "%20")
                        Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgProjPeople)
                    } else {
                        imgProjPeople.image = UIImage(named: "AppIcon")
                    }
                }
                
                
                if (peopleGalleryArr[indexPath.item].title == nil) || (peopleGalleryArr[indexPath.item].title! == "") || (peopleGalleryArr[indexPath.item].title! == "(NULL)") || (peopleGalleryArr[indexPath.item].title! == "<NULL>") || ((peopleGalleryArr[indexPath.item].title! == "<null>") || (peopleGalleryArr[indexPath.item].title! == "(null)") || (peopleGalleryArr[indexPath.item].title! == "")) {
                    lblProjTitle.text = " "
                } else {
                    lblProjTitle.text =  peopleGalleryArr[indexPath.item].title!
                }
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 12 {
            return CGSize(width: (UIScreen.main.bounds.size.width-54)/2, height: 25)
        } else {
            return CGSize(width: 158, height: 117)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Collection view at row \(collectionView.tag) selected index path \(indexPath)")
        if collectionView.tag == 20 {
            self.objSelectedPeopleGalleryItem = self.peopleGalleryArr[indexPath.item]
            /*selectedGaleryID = (self.objSelectedPeopleGalleryItem?.peoplegalID)!
            print("selected Galery-ID:: ",String(selectedGaleryID))*/
            if peopleGalleryArr[indexPath.row].contentType!.rawValue == "video" {
                self.isPlayVideo = true
                let storyboardPeople = UIStoryboard(name: "People", bundle: nil)
                let videoVC = storyboardPeople.instantiateViewController(withIdentifier: "VideoViewController") as! VideoViewController
                videoVC.videoURLStr = self.peopleGalleryArr[indexPath.item].url!
                self.navigationController?.pushViewController(videoVC, animated: true)
            } else {
                EpcLensController.sharedInstance.showAlert(_sourceController: self, _msg: EpcLensMessages.IsValid_ImageUrl)
            }
        } else  if collectionView.tag == 12 {
            print("selected Project-ID:: \(String(describing: self.peopleProjectArr[indexPath.item].projectID))")
            
            let storyboardProject = UIStoryboard(name: "Project", bundle: nil)
            let projectVC = storyboardProject.instantiateViewController(withIdentifier: "ProjectListController") as! ProjectListController
            projectVC.projectID = self.peopleProjectArr[indexPath.item].projectID!
            self.navigationController?.pushViewController(projectVC, animated: true)
        }
    }
}



