//
//  PeopleViewController.swift
//  EpcLensProject
//
//  Created by Surajit Ghosh on 29/04/18.
//  Copyright © 2018 Sudipta Biswas. All rights reserved.
//

import UIKit

class PeopleViewController: UIViewController, PeopleListProtocol {
    
    var peopleDispatcher = PeopleDispatcher()
    var peopleListArr = [PeopleList]()
    @IBOutlet weak var tablePeopleList: UITableView!
    
    //Mark:: AnimatedLoader
    let loaderController = EpcLensLoaderViewController(nibName: "EpcLensLoaderViewController", bundle: nil)
    
    //MARK:: UIView
    override func viewDidLoad() {
        super.viewDidLoad()
        peopleDispatcher.delegate = self
        var tempDict = [String:String]()
        tablePeopleList.separatorStyle = .none
        tempDict["token"] = EpicLensConstantsVariables.accessToken
        tempDict["page"] = "0"
        peopleDispatcher.GetPeopleList(paramDict: tempDict)
        
        
        // Start Loaded
        loaderController.showEpcLoader(_sourceView: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func didupdateControllerWithPeopleList(peopleList:PeopleListModel) {        
        DispatchQueue.main.async {
            if peopleList.data.sucess {
                self.peopleListArr = peopleList.data.peopleList
                self.tablePeopleList.reloadData()
                
                // Stop Loaded
                self.loaderController.hideEpcLoader()
            }
        }
    }
    
    
    //MARK:: IBAction
    @IBAction func btnLeftMenuDidTap(_ sender: Any) {
        let elDrawer = self.navigationController?.parent as! KYDrawerController
        elDrawer.setDrawerState(.opened, animated: true)
    }
    
    @IBAction func btnRightMenuDidTap(_ sender: UIButton) {
        //let originalTransform = sender.transform
        //let scaledTransform = originalTransform.scaledBy(x: 0.2, y: 0.2)
        //let scaledAndTranslatedTransform = scaledTransform.translatedBy(x: 0.0, y: -250.0)
        //UIView.animate(withDuration: 0.7, animations: {
        //  sender.transform = scaledAndTranslatedTransform
        //})
        
        let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
        let objReportPopup = storyboardMain.instantiateViewController(withIdentifier: "RightMenuController") as! RightMenuController
        objReportPopup.showPopup(_controller: self)
    }
    
    @IBAction func menuClicked(_ sender: Any) {
        print(self.navigationController?.viewControllers ?? "")
        let elDrawer = self.navigationController?.parent as! KYDrawerController
        elDrawer.setDrawerState(.opened, animated: true)
    }
    
    @objc func btnContactPressed(_sender:UIButton) {
        print("contact pressed")
    }
    
}

extension PeopleViewController : UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return peopleListArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let idstr = "peoplecell"
        
        var cell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: idstr)
        }
        
        // Use the outlet in our custom class to get a reference to the UI Elements in the cell
        let lblName = cell.viewWithTag(1) as! UILabel
        let imgPeopleProfile = cell.viewWithTag(2) as! UIImageView
        let lblDesignation = cell.viewWithTag(3) as! UILabel
        let lblAddress = cell.viewWithTag(4) as! UILabel
        let lblJobTypes = cell.viewWithTag(5) as! UILabel
        let viewBack = cell.viewWithTag(6)!
        let viewNameBack = cell.viewWithTag(7)!
        let viewJobTypesBack = cell.viewWithTag(8)!
        
        lblName.text =  peopleListArr[indexPath.row].peopleName
        
        imgPeopleProfile.layer.cornerRadius = 33.5
        imgPeopleProfile.layer.borderColor = UIColor.darkGray.cgColor
        imgPeopleProfile.layer.borderWidth = 1.0
        imgPeopleProfile.clipsToBounds = true
        
        if peopleListArr[indexPath.row].img?.isEmpty == false && peopleListArr[indexPath.row].img != "" {
            let escapedString = peopleListArr[indexPath.row].img?.replacingOccurrences(of: " ", with: "%20")
            Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgPeopleProfile)
        } else {
            imgPeopleProfile.image = UIImage(named: "AppIcon")
        }
        
        if (peopleListArr[indexPath.row].designation == nil) || (peopleListArr[indexPath.row].designation! == "") || (peopleListArr[indexPath.row].designation! == "(NULL)") || (peopleListArr[indexPath.row].designation! == "<NULL>") || ((peopleListArr[indexPath.row].designation! == "<null>") || (peopleListArr[indexPath.row].designation! == "(null)") || (peopleListArr[indexPath.row].designation! == "")) {
            lblDesignation.text = " "
        } else {
            lblDesignation.text =  peopleListArr[indexPath.row].designation ?? "" + " at " +  peopleListArr[indexPath.row].companyName!
        }
        
        if (peopleListArr[indexPath.row].location == nil) || (peopleListArr[indexPath.row].location! == "") || (peopleListArr[indexPath.row].location! == "(NULL)") || (peopleListArr[indexPath.row].location! == "<NULL>") || ((peopleListArr[indexPath.row].location! == "<null>") || (peopleListArr[indexPath.row].location! == "(null)") || (peopleListArr[indexPath.row].location! == "")) {
            lblAddress.text = " "
        } else {
            lblAddress.text =  peopleListArr[indexPath.row].location ?? "no address"
        }
        
        if (peopleListArr[indexPath.row].jobTypes == nil) || (peopleListArr[indexPath.row].jobTypes! == "") || (peopleListArr[indexPath.row].jobTypes! == "(NULL)") || (peopleListArr[indexPath.row].jobTypes! == "<NULL>") || ((peopleListArr[indexPath.row].jobTypes! == "<null>") || (peopleListArr[indexPath.row].jobTypes! == "(null)") || (peopleListArr[indexPath.row].jobTypes! == "")) {
            lblJobTypes.text = " "
        } else {
            lblJobTypes.text =  peopleListArr[indexPath.row].jobTypes
        }
        
        //btnContact.addTarget(self, action:#selector(self.btnContactPressed(_sender:)), for: UIControlEvents.touchUpInside)
        viewJobTypesBack.layer.cornerRadius = 5.0
        viewJobTypesBack.clipsToBounds = true
        viewJobTypesBack.layer.borderColor = UIColor.darkGray.cgColor
        viewJobTypesBack.layer.borderWidth = 0.5
        
        viewBack.layer.cornerRadius = 10.0
        viewBack.clipsToBounds = true
        
        viewNameBack.layer.cornerRadius = 10.0
        viewNameBack.clipsToBounds = true
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 133.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboardPeople = UIStoryboard(name: "People", bundle: nil)
        let peopleVC = storyboardPeople.instantiateViewController(withIdentifier: "PeopleDetailController") as! PeopleDetailController
        peopleVC.peopleId = peopleListArr[indexPath.row].id
        self.navigationController?.pushViewController(peopleVC, animated: true)
    }
}

