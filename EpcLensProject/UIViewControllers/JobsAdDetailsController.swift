//
//  JobsAdDetailsController.swift
//  EpcLensProject
//
//  Created by Moumita Datta on 04/07/18.
//  Copyright © 2018 Moumita Datta. All rights reserved.
//

import UIKit

class JobsAdDetailsController: UIViewController, JobAdsDetailsProtocol {
    
    @IBOutlet weak var tableJobAdsDetails: UITableView!
    @IBOutlet var constraintTableBottom: NSLayoutConstraint!
    
    
    var jobDetailsDispatcher = MarketPlaceJobAdsDetailsDispatcher()
    var jobDetailsArr = [JobDetails]()
    var isSavedJob: Int = 0
    
    
    var jobAdsId:Int = 0
    var isGalleryImgCalled : Bool = false
    var selectedAdsImgIndex : Int = -1
    
    //Mark:: AnimatedLoader
    let loaderController = EpcLensLoaderViewController(nibName: "EpcLensLoaderViewController", bundle: nil)
    
    
    
    //MARK:: UIView
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        jobDetailsDispatcher.delegate = self
        var tempDict = [String:String]()
        tempDict["token"] = EpicLensConstantsVariables.accessToken
        tempDict["id"] = String(jobAdsId)
        jobDetailsDispatcher.GetMarketPlaceJobAdsDetails(paramDict: tempDict)
        
        
        // Start Loaded
        loaderController.showEpcLoader(_sourceView: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWasShown), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeHidden), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- Keyboard show hide notification
    @objc func keyboardWasShown(aNotification: NSNotification) {
        if let keyboardSize = (aNotification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            self.constraintTableBottom.constant = -keyboardSize.height
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillBeHidden(aNotification: NSNotification) {
        self.constraintTableBottom.constant = 0.0
        self.view.layoutIfNeeded()
    }
    
    //MARK:: IBAction
    @IBAction func btnBackDidTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:: JobAdsDetailsProtocol
    func didUpdateControllerWithObjMarketPlaceJobAdsDetails(jobAdsDetails: MarketPlaceJobAdsDetailsModel) {
        DispatchQueue.main.async {
            if jobAdsDetails.data.sucess {
                self.jobDetailsArr = jobAdsDetails.data.jobs!
                self.isSavedJob = jobAdsDetails.data.isSavedJob as Int
                
                self.tableJobAdsDetails.delegate = self
                self.tableJobAdsDetails.dataSource = self
                self.tableJobAdsDetails.reloadData()
                // Stop Loaded
                self.loaderController.hideEpcLoader()
            }
        }
    }
    
}


extension JobsAdDetailsController : UITableViewDataSource, UITableViewDelegate {
    
    //MARK:: UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else if section == 1 {
            return 1
        } else if section == 2 {
            return 1
        } else if section == 3 {
            return 1
        } else if section == 4 {
            return 3
        } else if section == 5 {
            return 1
        } else if section == 6 {
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.section == 0) {
            let idstr = "JobsAdsTitleTableCell"
            
            // get a reference to our storyboard cell
            var jobAdsTitleCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
            if jobAdsTitleCell == nil {
                jobAdsTitleCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
            }
            
            
            //Declare the outlet in our custom class
            let viewJobAds = jobAdsTitleCell.viewWithTag(10)
            let viewTitleJobAds = jobAdsTitleCell.viewWithTag(11)
            let imgViewJobAds = jobAdsTitleCell.viewWithTag(15) as! UIImageView
            
            let lblJobTitle = jobAdsTitleCell.viewWithTag(12) as! UILabel
            let lblJobLocation = jobAdsTitleCell.viewWithTag(13) as! UILabel
            let lblJobsCompanyName = jobAdsTitleCell.viewWithTag(14) as! UILabel
            let lblJobsPoster = jobAdsTitleCell.viewWithTag(16) as! UILabel
            let lblJobsType = jobAdsTitleCell.viewWithTag(17) as! UILabel
            let lblIsSavedJob = jobAdsTitleCell.viewWithTag(18) as! UILabel
            let viewIsSavedJob = jobAdsTitleCell.viewWithTag(19)
            
            //print("classifiedDetailsArr :: \(classifiedDetailsArr)")
            
            viewJobAds?.layer.cornerRadius = 10
            viewJobAds?.clipsToBounds = true
            viewTitleJobAds?.layer.cornerRadius = 10
            viewTitleJobAds?.clipsToBounds = true
            
            
            
            if jobDetailsArr.count > 0 {
                if (jobDetailsArr[0].jobsTitle?.isEmpty)! {
                    lblJobTitle.text = " "
                } else {
                    lblJobTitle.text = jobDetailsArr[indexPath.row].jobsTitle!
                }
                
                imgViewJobAds.layer.cornerRadius = 5.0
                imgViewJobAds.clipsToBounds = true
                
                if jobDetailsArr[indexPath.row].companyLogo?.isEmpty == false && jobDetailsArr[0].companyLogo != "" {
                    let escapedString = jobDetailsArr[0].companyLogo?.replacingOccurrences(of: " ", with: "%20")
                    Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgViewJobAds)
                } else {
                    imgViewJobAds.image = UIImage(named: "AppIcon")
                }
                
                if (jobDetailsArr[0].jobsLocation?.isEmpty)! {
                    lblJobLocation.text = " "
                } else {
                    lblJobLocation.text = jobDetailsArr[0].jobsLocation!
                }
                
                if (jobDetailsArr[0].companyName?.isEmpty)! {
                    lblJobsCompanyName.text = " "
                } else {
                    lblJobsCompanyName.text = jobDetailsArr[0].companyName!
                }
                
                if (jobDetailsArr[0].jobPostername?.isEmpty)! {
                    lblJobsPoster.text = " "
                } else {
                    lblJobsPoster.text = jobDetailsArr[0].jobPostername!
                }
                
                if (jobDetailsArr[0].jobTypeLabel?.isEmpty)! {
                    lblJobsType.text = " "
                } else {
                    lblJobsType.text = jobDetailsArr[indexPath.row].jobTypeLabel!
                }
                
                
                if isSavedJob == 0 {
                    lblIsSavedJob.text = "save this job"
                    viewIsSavedJob?.backgroundColor = UIColor.black
                    
                } else {
                    lblIsSavedJob.text = "saved job"
                    viewIsSavedJob?.backgroundColor = UIColor(red: CGFloat((255.0 / 255.0)), green: CGFloat((165.0 / 255.0)), blue: CGFloat((3.0 / 255.0)), alpha: 1.0)
                }
            }
            
            //jobAdsTitleCell.selectionStyle = UITableViewCellSelectionStyle.none
            return jobAdsTitleCell
        }  else if (indexPath.section == 1) {
            let idstr = "JobAdsDescriptionTableCell"
            
            // get a reference to our storyboard cell
            var jobAdsDescriptionCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
            if jobAdsDescriptionCell == nil {
                jobAdsDescriptionCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
            }
            
            //Declare the outlet in our custom class
            let lblJobAdsDescription = jobAdsDescriptionCell.viewWithTag(22) as! UILabel
            
            
            if jobDetailsArr.count > 0 {
                if (jobDetailsArr[0].jobsDescription?.isEmpty)! {
                    lblJobAdsDescription.text = " "
                } else {
                    lblJobAdsDescription.attributedText = jobDetailsArr[0].jobsDescription!.attributedHtmlString
                }
            }
            
            jobAdsDescriptionCell.selectionStyle = UITableViewCellSelectionStyle.none
            return jobAdsDescriptionCell
        } else if (indexPath.section == 2) {
            let idstr = "JobAdsSummaryTableCell"
            
            // get a reference to our storyboard cell
            var jobAdsSummaryCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
            if jobAdsSummaryCell == nil {
                jobAdsSummaryCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
            }
            
            //Declare the outlet in our custom class
            let lblJobAdsLocation = jobAdsSummaryCell.viewWithTag(31) as! UILabel
            let lblJobAdsType = jobAdsSummaryCell.viewWithTag(32) as! UILabel
            let lblJobAdsSalary = jobAdsSummaryCell.viewWithTag(33) as! UILabel
            let lblJobAdsPosted = jobAdsSummaryCell.viewWithTag(34) as! UILabel
            let lblJobAdsIndustries = jobAdsSummaryCell.viewWithTag(35) as! UILabel
            
            
            if jobDetailsArr.count > 0 {
                if (jobDetailsArr[0].jobsLocation?.isEmpty)! {
                    lblJobAdsLocation.text = " "
                } else {
                    lblJobAdsLocation.text = jobDetailsArr[0].jobsLocation!
                }
                
                if (jobDetailsArr[0].jobTypeLabel?.isEmpty)! {
                    lblJobAdsType.text = " "
                } else {
                    lblJobAdsType.text = jobDetailsArr[0].jobTypeLabel!
                }
                
                if (jobDetailsArr[0].jobsSalary?.isEmpty)! {
                    lblJobAdsSalary.text = " "
                } else {
                    lblJobAdsSalary.text = jobDetailsArr[0].jobsSalary! + " $/hour "
                }
                
                if (jobDetailsArr[0].jobsPublishedAt?.isEmpty)! {
                    lblJobAdsPosted.text = " "
                } else {
                    lblJobAdsPosted.text = jobDetailsArr[0].jobsPublishedAt!
                }
                
                if (jobDetailsArr[0].industryNames?.isEmpty)! {
                    lblJobAdsIndustries.text = " "
                } else {
                    lblJobAdsIndustries.text = jobDetailsArr[0].industryNames!
                }
            }
            
            jobAdsSummaryCell.selectionStyle = UITableViewCellSelectionStyle.none
            return jobAdsSummaryCell
        } else if (indexPath.section == 3) {
            let idstr = "JobAdsPosterInfoTableCell"
            
            // get a reference to our storyboard cell
            var jobAdsSummaryCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
            if jobAdsSummaryCell == nil {
                jobAdsSummaryCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
            }
            
            //Declare the outlet in our custom class
            let lblJobAdsPosterName = jobAdsSummaryCell.viewWithTag(42) as! UILabel
            let lblJobAdsJobsDesignation = jobAdsSummaryCell.viewWithTag(43) as! UILabel
            let lblJobAdsPosterlocation = jobAdsSummaryCell.viewWithTag(44) as! UILabel
            let imgViewJobAdsPoster = jobAdsSummaryCell.viewWithTag(45) as! UIImageView
            
            imgViewJobAdsPoster.layer.cornerRadius = 35
            imgViewJobAdsPoster.clipsToBounds = true
            
            
            if jobDetailsArr.count > 0 {
                if jobDetailsArr[indexPath.row].jobPosterimage?.isEmpty == false && jobDetailsArr[0].jobPosterimage != "" {
                    let escapedString = jobDetailsArr[0].jobPosterimage?.replacingOccurrences(of: " ", with: "%20")
                    Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgViewJobAdsPoster)
                } else {
                    imgViewJobAdsPoster.image = UIImage(named: "AppIcon")
                }
                
                if (jobDetailsArr[0].jobPostername?.isEmpty)! {
                    lblJobAdsPosterName.text = " "
                } else {
                    lblJobAdsPosterName.text = jobDetailsArr[0].jobPostername!
                }
                
                if (jobDetailsArr[0].jobPosterdesignation?.isEmpty)! {
                    lblJobAdsJobsDesignation.text = " "
                } else {
                    lblJobAdsJobsDesignation.text = jobDetailsArr[0].jobPosterdesignation!
                }
                
                if (jobDetailsArr[0].jobPosterlocation?.isEmpty)! {
                    lblJobAdsPosterlocation.text = " "
                } else {
                    lblJobAdsPosterlocation.text = jobDetailsArr[0].jobPosterlocation!
                }
            }
            
            jobAdsSummaryCell.selectionStyle = UITableViewCellSelectionStyle.none
            return jobAdsSummaryCell
        } else if (indexPath.section == 4) {
            let idstr = "JobAdsApplierInfoTableCell"
            
            // get a reference to our storyboard cell
            var classifiedAdsInfoCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
            if classifiedAdsInfoCell == nil {
                classifiedAdsInfoCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
            }
            
            //Declare the outlet in our custom class
            let imgviewClassifiedAdsContactInfo = classifiedAdsInfoCell.viewWithTag(52) as! UIImageView
            let txtFieldClassifiedAdsContactInfo = classifiedAdsInfoCell.viewWithTag(53) as! UITextField
            
            
            classifiedAdsInfoCell.viewWithTag(51)?.layer.cornerRadius = 10.0
            classifiedAdsInfoCell.viewWithTag(51)?.clipsToBounds = true
            classifiedAdsInfoCell.viewWithTag(51)?.layer.borderColor = UIColor.black.cgColor
            classifiedAdsInfoCell.viewWithTag(51)?.layer.borderWidth = 0.5
            
           
            if indexPath.row == 0 {
                imgviewClassifiedAdsContactInfo.image = UIImage(named: "MarketPlace_Person")
                txtFieldClassifiedAdsContactInfo.placeholder = "your name"
            } else if indexPath.row == 1 {
                imgviewClassifiedAdsContactInfo.image = UIImage(named: "MarketPlace_Phone")
                txtFieldClassifiedAdsContactInfo.placeholder = "phone number e.g.(204) 234-5034"
            } else if indexPath.row == 2 {
                imgviewClassifiedAdsContactInfo.image = UIImage(named: "MarketPlace_Write_Message")
                txtFieldClassifiedAdsContactInfo.placeholder = "message"
            }
            txtFieldClassifiedAdsContactInfo.delegate = self
            
            
            classifiedAdsInfoCell.selectionStyle = UITableViewCellSelectionStyle.none
            
            return classifiedAdsInfoCell
        } else if (indexPath.section == 5) {
            let idstr = "JobAdsSaveCopyTableCell"
            
            // get a reference to our storyboard cell
            var classifiedAdsSaveCopyCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
            if classifiedAdsSaveCopyCell == nil {
                classifiedAdsSaveCopyCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
            }
            
            return classifiedAdsSaveCopyCell
        } else {
            let idstr = "JobAdsSendRequestBtnTableCell"
            
            // get a reference to our storyboard cell
            var jobAdsBtnCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
            if jobAdsBtnCell == nil {
                jobAdsBtnCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
            }
            
            //Declare the outlet in our custom class
            //let viewClassifiedAdsBtnTop = classifiedAdsBtnCell.viewWithTag(70)
            let viewClassifiedAdsBtn = jobAdsBtnCell.viewWithTag(75)
            let btnclassifiedAdsSendRequest = jobAdsBtnCell.viewWithTag(71) as! UIButton
            
            
            viewClassifiedAdsBtn?.layer.cornerRadius = 10
            viewClassifiedAdsBtn?.clipsToBounds = true
            
            btnclassifiedAdsSendRequest.layer.cornerRadius = 10.0
            
            //classifiedAdsBtnCell.selectionStyle = UITableViewCellSelectionStyle.none
            return jobAdsBtnCell
        }
    }
    
    
    //MARK:: UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.section == 0) {
            return 185
        } else if (indexPath.section == 1) {
            return UITableViewAutomaticDimension
        } else if (indexPath.section == 2) {
            return 160
        } else if (indexPath.section == 3) {
            return 115
        } else if (indexPath.section == 4) {
            return 45
        } else if (indexPath.section == 5) {
            return 45
        } else {
            return 75
        }
    }


}

extension JobsAdDetailsController : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.tableJobAdsDetails.scrollToRow(at: IndexPath.init(row: 2, section: 4), at: .bottom, animated: true)
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let cell = textField.superview?.superview?.superview?.superview as? UITableViewCell
        guard let _cell = cell else {
            return false
        }
            let indexpath = self.tableJobAdsDetails.indexPath(for: _cell)
            if indexpath?.section == 4 {
                textField.resignFirstResponder()
                if indexpath?.row == 0 {
                    let cellTemp = self.tableJobAdsDetails.cellForRow(at: IndexPath.init(row: 1, section: 4))
                    let textfieldCell = cellTemp?.viewWithTag(53) as! UITextField
                    textfieldCell.becomeFirstResponder()
                    
                } else if indexpath?.row == 1 {
                    let cellTemp = self.tableJobAdsDetails.cellForRow(at: IndexPath.init(row: 2, section: 4))
                    let textfieldCell = cellTemp?.viewWithTag(53) as! UITextField
                    textfieldCell.becomeFirstResponder()
                } else {
                    textField.resignFirstResponder()
                }
            }
        
        textField.resignFirstResponder()
        return true
    }
}
