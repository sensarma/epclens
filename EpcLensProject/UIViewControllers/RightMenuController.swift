//
//  RightMenuController.swift
//  EpcLensProject
//
//  Created by Surajit Ghosh on 01/05/18.
//  Copyright © 2018 Sudipta Biswas. All rights reserved.
//

import UIKit

class RightMenuController: UIViewController{

    @IBOutlet weak var imageVwback: UIImageView!
    
    @IBOutlet weak var tblRightMenu: UITableView!
    
    @IBOutlet weak var consTblWidth: NSLayoutConstraint!
    
    var menuItems  = [Menu]()
    
    //MARK:: UIView
    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configurePopup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        consTblWidth.constant = 0
        UIView.animate(withDuration: 1) {
            self.consTblWidth.constant = 247
        }
    }
    
    private func configurePopup() {
        tblRightMenu.bounces = false
        tblRightMenu.separatorStyle = .none
        imageVwback.isUserInteractionEnabled = true
        tblRightMenu.allowsSelection = true
        tblRightMenu.isUserInteractionEnabled = true
        self.view.backgroundColor = UIColor.clear
        self.createMenu()
        tblRightMenu.reloadData()
    }
    
    func createMenu() {
        var menu = Menu()
        menu.name = "projects"
        menu.image = "dash_lens_flare_icon"
        menu.isSelected = false
        menuItems.append(menu)
        
        menu = Menu()
        menu.name = "companies"
        menu.image = "dash_my_projects_icon"
        menu.isSelected = false
        menuItems.append(menu)
        
        menu = Menu()
        menu.name = "people"
        menu.image = "dash_epc_sees_icon"
        menu.isSelected = false
        menuItems.append(menu)
        
        menu = Menu()
        menu.name = "projistics"
        menu.image = "dash_marketscape_icon"
        menu.isSelected = false
        menuItems.append(menu)
        
        menu = Menu()
        menu.name = "lens together"
        menu.image = "dash_my_lens_icon"
        menu.isSelected = false
        menuItems.append(menu)
        
        menu = Menu()
        menu.name = "together"
        menu.image = "dash_radar_icon"
        menu.isSelected = false
        menuItems.append(menu)
        
    }
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

 
    
    // MARK:- Show popup
    func showPopup(_controller:UIViewController) {
        self.view.backgroundColor = UIColor.clear
        _controller.addChildViewController(self)
        _controller.view.addSubview(self.view)
//        self.view.alpha = 0.0
//        UIView.animate(withDuration: 0.4, delay: 0.0, options: .curveEaseInOut, animations: {
//            self.view.alpha = 1.0
//        }) { (completed) in
//
//        }
        
    }
    
    // MARK:- Remove popup
    func removePopUp() {
        UIView.animate(withDuration: 0.4, delay: 0.0, options: .curveEaseInOut, animations: {
            self.view.alpha = 0.0
        }) { (completed) in
            if completed {
                DispatchQueue.main.async {
                    self.view.removeFromSuperview()
                    self.removeFromParentViewController()
                }
            }
        }
    }
    
    // MARK:- Background Touch
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch: UITouch = touches.first!
        if (touch.view == imageVwback){
            print("touchesBegan | This is an ImageView")
            self.removePopUp()
        }else{
            print("touchesBegan | This is not an ImageView")
        }
    }

}

extension RightMenuController : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 54
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let idstr = "menucell"
            var cell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
            if cell == nil {
                cell = UITableViewCell(style: .default, reuseIdentifier: idstr)
            }
            
            let imgViewHexagon = cell.viewWithTag(1) as! UIImageView
            imgViewHexagon.image = UIImage.init(named:"haxagone-white" )
            
            // dash_gray-dark
            
            let imgViewHexagonInner = cell.viewWithTag(2) as! UIImageView
            imgViewHexagonInner.image = UIImage.init(named: menuItems[indexPath.row].image)
            
            let imgLblBack = cell.viewWithTag(3) as! UIImageView
            imgLblBack.image = UIImage.init(named:"yellow-layer")
            imgLblBack.isHidden = true
            
            let lblMenu = cell.viewWithTag(4) as! UILabel
            lblMenu.text =  menuItems[indexPath.row].name
        
            return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let elDrawer = self.navigationController?.parent as! KYDrawerController
        switch indexPath.row {
        case 0:
            let storyboardProject = UIStoryboard(name: "Project", bundle: nil)
            let dashboardViewController = storyboardProject.instantiateViewController(withIdentifier: "ProjectListController") as! ProjectListController
            let navController = UINavigationController.init(rootViewController: dashboardViewController)
            navController.isNavigationBarHidden = true
            elDrawer.mainViewController = navController
            elDrawer.setDrawerState(.closed, animated: true)
        case 1:
            let storyboardCompany = UIStoryboard(name: "Company", bundle: nil)
            let dashboardViewController = storyboardCompany.instantiateViewController(withIdentifier: "CompanyListController") as! CompanyListController
            let navController = UINavigationController.init(rootViewController: dashboardViewController)
            navController.isNavigationBarHidden = true
            elDrawer.mainViewController = navController
            elDrawer.setDrawerState(.closed, animated: true)
        case 2:
            let storyboardPeople = UIStoryboard(name: "People", bundle: nil)
            let dashboardViewController = storyboardPeople.instantiateViewController(withIdentifier: "PeopleViewController") as! PeopleViewController
            let navController = UINavigationController.init(rootViewController: dashboardViewController)
            navController.isNavigationBarHidden = true
            elDrawer.mainViewController = navController
            elDrawer.setDrawerState(.closed, animated: true)
        case 3:
            let dashboardViewController = self.storyboard?.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
            dashboardViewController.view.backgroundColor = UIColor.yellow
            let navController = UINavigationController.init(rootViewController: dashboardViewController)
            navController.isNavigationBarHidden = true
            elDrawer.mainViewController = navController
            elDrawer.setDrawerState(.closed, animated: true)
        case 4:
            let dashboardViewController = self.storyboard?.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
            dashboardViewController.view.backgroundColor = UIColor.yellow
            let navController = UINavigationController.init(rootViewController: dashboardViewController)
            navController.isNavigationBarHidden = true
            elDrawer.mainViewController = navController
            elDrawer.setDrawerState(.closed, animated: true)
        case 5:
            let dashboardViewController = self.storyboard?.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
            dashboardViewController.view.backgroundColor = UIColor.yellow
            let navController = UINavigationController.init(rootViewController: dashboardViewController)
            navController.isNavigationBarHidden = true
            elDrawer.mainViewController = navController
            elDrawer.setDrawerState(.closed, animated: true)
        default:
            self.removePopUp()
        }
        
    }
}
