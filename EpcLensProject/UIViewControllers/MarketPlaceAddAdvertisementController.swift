//
//  MarketPlaceAddAdvertisementController.swift
//  EPCLens
//
//  Created by Moumita Datta on 27/03/18.
//  Copyright © 2018 Moumita Datta. All rights reserved.
//

import UIKit
import QuartzCore


class MarketPlaceAddAdvertisementController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextViewDelegate  {
    
    @IBOutlet var btnUploadAds: UIButton!
    @IBOutlet var imgUploadAds: UIImageView!
    
    @IBOutlet var btnCreateAdsType: UIButton!
    @IBOutlet var imgCreateAdsType: UIImageView!
    
    
    
    @IBOutlet var uploadAdsView: UIView!
    @IBOutlet var createAdsTypeView: UIView!
    
    
    var imagePicker = UIImagePickerController()
    //UploadAds
    @IBOutlet var viewImgBrowsser: UIView!
    @IBOutlet var imgViewBrowse: UIImageView!
    @IBOutlet var downloadView: UIView!
    @IBOutlet var acceptableFormatLbl: UILabel!
    @IBOutlet var imageSizeLbl: UILabel!
    
    
    
    @IBOutlet var btn$99: UIButton!
    @IBOutlet var img$99: UIImageView!
    
    @IBOutlet var btn$199: UIButton!
    @IBOutlet var img$199: UIImageView!
    
    @IBOutlet var btn$389: UIButton!
    @IBOutlet var img$389: UIImageView!
    
    
    //CreateAdsType
    @IBOutlet var btnImage: UIButton!
    @IBOutlet var imgViewImage: UIImageView!
    
    @IBOutlet var btnVideo: UIButton!
    @IBOutlet var imgViewVideo: UIImageView!
    
    @IBOutlet var btnBoth: UIButton!
    @IBOutlet var imgViewBoth: UIImageView!
    
    @IBOutlet var adsTextView: UITextView!
    
    //MARK:: UIView
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        let yourViewBorder = CAShapeLayer()
        yourViewBorder.strokeColor = UIColor.darkGray.cgColor
        yourViewBorder.lineDashPattern = [5, 5] //[width,gap]
        yourViewBorder.frame = viewImgBrowsser.bounds
        yourViewBorder.fillColor = nil
        yourViewBorder.path = UIBezierPath(rect: viewImgBrowsser.bounds).cgPath
        viewImgBrowsser.layer.addSublayer(yourViewBorder)
        viewImgBrowsser.layer.cornerRadius = 10.0
        viewImgBrowsser.clipsToBounds = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:: UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        imgViewBrowse.image = image
        dismiss(animated: true) {() -> Void in }
    }
    
    //MARK:: UITextViewDelegate
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (adsTextView.text == "\n") {
            adsTextView.resignFirstResponder()
            return false
        }        
        return true
    }
    
    
    //MARK:: IBAction
    @IBAction func btnBackDidTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnUploadAdsDidTap(_ sender: Any) {
        uploadAdsView.isHidden = false
        createAdsTypeView.isHidden = true
        
        imgUploadAds.image = UIImage(named: "MarketPlace_Selected_Circle")!
        imgCreateAdsType.image = UIImage(named: "MarketPlace_Circle")!
    }
    
    @IBAction func btnCreateAdsTypeDidTap(_ sender: Any) {
        uploadAdsView.isHidden = true
        createAdsTypeView.isHidden = false
        
        imgUploadAds.image = UIImage(named: "MarketPlace_Circle")!
        imgCreateAdsType.image = UIImage(named: "MarketPlace_Selected_Circle")!
    }
    
    
    // UploadAds
    @IBAction func btnBrowseDidTap(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum;
            imagePicker.allowsEditing = false
            
            self.present(imagePicker, animated: true, completion: nil)
        }
        downloadView.isHidden = true
        acceptableFormatLbl.isHidden = true
        imageSizeLbl.isHidden = true
    }
    
    @IBAction func btn$99DidTap(_ sender: Any) {
        img$99.image = UIImage(named: "MarketPlace_Selected_Circle")!
        img$199.image = UIImage(named: "MarketPlace_Circle")!
        img$389.image = UIImage(named: "MarketPlace_Circle")!
    }
    
    @IBAction func btn$199DidTap(_ sender: Any) {
        img$99.image = UIImage(named: "MarketPlace_Circle")!
        img$199.image = UIImage(named: "MarketPlace_Selected_Circle")!
        img$389.image = UIImage(named: "MarketPlace_Circle")!
    }
    
    @IBAction func btn$389DidTap(_ sender: Any) {
        img$99.image = UIImage(named: "MarketPlace_Circle")!
        img$199.image = UIImage(named: "MarketPlace_Circle")!
        img$389.image = UIImage(named: "MarketPlace_Selected_Circle")!
    }
    
    
    // CreateAdsType
    @IBAction func btnImageDidTap(_ sender: Any) {
        imgViewImage.image = UIImage(named: "MarketPlace_Selected_Circle")!
        imgViewVideo.image = UIImage(named: "MarketPlace_Circle")!
        imgViewBoth.image = UIImage(named: "MarketPlace_Circle")!
    }
    
    @IBAction func btnVideoDidTap(_ sender: Any) {
        imgViewImage.image = UIImage(named: "MarketPlace_Circle")!
        imgViewVideo.image = UIImage(named: "MarketPlace_Selected_Circle")!
        imgViewBoth.image = UIImage(named: "MarketPlace_Circle")!
    }
    
    @IBAction func btnBothDidTap(_ sender: Any) {
        imgViewImage.image = UIImage(named: "MarketPlace_Circle")!
        imgViewVideo.image = UIImage(named: "MarketPlace_Circle")!
        imgViewBoth.image = UIImage(named: "MarketPlace_Selected_Circle")!
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
