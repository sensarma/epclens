//
//  SideMenuController.swift
//  SJSwiftNavigationController
//
//  Created by Mac on 12/19/16.
//  Copyright © 2016 Sumit Jagdev. All rights reserved.
//

import UIKit

class SideMenuController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var menuTableView : UITableView!
   
    var menuItems  = [Menu]()
    
    
    
    //MARK:: UIView
    override func viewDidLoad() {
        super.viewDidLoad()
        //menuTableView.bounces = false
        // Do any additional setup after loading the view.
        menuTableView.separatorStyle = .none
        self.createMenu()
    }
    
    func createMenu() {
        var menu = Menu()
        menu = Menu()
        menu.name = "dashboard"
        menu.image = "dash_dashboard_icon"
        menu.isSelected = false
        menuItems.append(menu)
        
        menu = Menu()
        menu.name = "lens flare"
        menu.image = "dash_lens_flare_icon"
        menu.isSelected = false
        menuItems.append(menu)
        
        menu = Menu()
        menu.name = "my projects"
        menu.image = "dash_my_projects_icon"
        menu.isSelected = false
        menuItems.append(menu)
        
        menu = Menu()
        menu.name = "epc sees"
        menu.image = "dash_epc_sees_icon"
        menu.isSelected = false
        menuItems.append(menu)
        
        menu = Menu()
        menu.name = "marketscape"
        menu.image = "dash_marketscape_icon"
        menu.isSelected = false
        menuItems.append(menu)
        
        menu = Menu()
        menu.name = "my lens"
        menu.image = "dash_my_lens_icon"
        menu.isSelected = false
        menuItems.append(menu)
        
        menu = Menu()
        menu.name = "radar(news)"
        menu.image = "dash_radar_icon"
        menu.isSelected = false
        menuItems.append(menu)
        
        menu = Menu()
        menu.name = "marketplace"
        menu.image = "dash_marketplace_icon"
        menu.isSelected = false
        menuItems.append(menu)
        
        menu = Menu()
        menu.name = "lexicon"
        menu.image = "dash_lexicon_icon"
        menu.isSelected = false
        menuItems.append(menu)
        
        menu = Menu()
        menu.name = "my account"
        menu.image = "dash_account_icon"
        menu.isSelected = false
        menuItems.append(menu)
        
        menu = Menu()
        menu.name = "lens desk"
        menu.image = "lens_desk_link_icon"
        menu.isSelected = false
        menuItems.append(menu)
        
        menu = Menu()
        menu.name = "logout"
        menu.image = "log_icon"
        menu.isSelected = false
        menuItems.append(menu)
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        menuTableView.allowsSelection = true
        menuTableView.isUserInteractionEnabled = true
        self.view.backgroundColor = UIColor.clear
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //print("Left-Side Menu :: ",menuItems.count )
        return menuItems.count + 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         if indexPath.row == 0{
            return 84
         }else{
            return 54
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let idstr = "menuheadercell"
            var cell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
            if cell == nil {
                cell = UITableViewCell(style: .default, reuseIdentifier: idstr)
            }
            
//            let imgViewHexagon = cell.viewWithTag(1) as! UIImageView
//            imgViewHexagon.image = UIImage.init(named:"haxagone-white")
//
//
            let lblMenu = cell.viewWithTag(2) as! UILabel
            print("UserName :: ",String(describing:EpicLensConstantsVariables.userDetailsData?.userName))
            lblMenu.text =  EpicLensConstantsVariables.userDetailsData?.subcriptionName
//
//            let lblMenu = cell.viewWithTag(3) as! UILabel
//            lblMenu.text =  menuItems[indexPath.row-1].name
//
//            let lblMenu = cell.viewWithTag(4) as! UILabel
//            lblMenu.text =  menuItems[indexPath.row-1].name
//
//            let imgViewHexagon = cell.viewWithTag(5) as! UIImageView
//            imgViewHexagon.image = UIImage.init(named:"haxagone-white")
            
            return cell
        } else {
            
            let idstr = "menucell"
            var cell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
            if cell == nil {
                cell = UITableViewCell(style: .default, reuseIdentifier: idstr)
            }
            
            let imgViewHexagon = cell.viewWithTag(1) as! UIImageView
            imgViewHexagon.image = UIImage.init(named:"haxagone-white" )
            
           // dash_gray-dark
            
            let imgViewHexagonInner = cell.viewWithTag(2) as! UIImageView
            imgViewHexagonInner.image = UIImage.init(named: menuItems[indexPath.row-1].image)
            
            let imgLblBack = cell.viewWithTag(3) as! UIImageView
            imgLblBack.image = UIImage.init(named:"yellow-layer")
            imgLblBack.isHidden = true
            
            let lblMenu = cell.viewWithTag(4) as! UILabel
            lblMenu.text =  menuItems[indexPath.row-1].name
            
            
            return cell
        }
       
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        let elDrawer = self.navigationController?.parent as! KYDrawerController
        switch indexPath.row {
        case 1:
            let dashboardViewController = self.storyboard?.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
            dashboardViewController.view.backgroundColor = UIColor.red
            let navController = UINavigationController.init(rootViewController: dashboardViewController)
            navController.isNavigationBarHidden = true
            elDrawer.mainViewController = navController
            elDrawer.setDrawerState(.closed, animated: true)
        case 2:
            let storyboardLensflare = UIStoryboard(name: "Lensflare", bundle: nil)
            let lensFlareController = storyboardLensflare.instantiateViewController(withIdentifier: "LensFlareVC") as! LensFlareController
            //LensFlareController.view.backgroundColor = UIColor.yellow
            //UserDefaults.standard.set(true, forKey: "isLensflare")
            
            let navController = UINavigationController.init(rootViewController: lensFlareController)
            navController.isNavigationBarHidden = true
            elDrawer.mainViewController = navController
            elDrawer.setDrawerState(.closed, animated: true)
        case 3:
            let dashboardViewController = self.storyboard?.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
            dashboardViewController.view.backgroundColor = UIColor.red
            let navController = UINavigationController.init(rootViewController: dashboardViewController)
            navController.isNavigationBarHidden = true
            elDrawer.mainViewController = navController
            elDrawer.setDrawerState(.closed, animated: true)
        case 4:
            let storyboardLensflare = UIStoryboard(name: "EPCSees", bundle: nil)
            let epcSeesListController = storyboardLensflare.instantiateViewController(withIdentifier: "EPCSeesListVC") as! EPCSeesListController
            //epcSeesListController.view.backgroundColor = UIColor.yellow
            let navController = UINavigationController.init(rootViewController: epcSeesListController)
            navController.isNavigationBarHidden = true
            elDrawer.mainViewController = navController
            elDrawer.setDrawerState(.closed, animated: true)
        case 5:
            let dashboardViewController = self.storyboard?.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
            dashboardViewController.view.backgroundColor = UIColor.red
            let navController = UINavigationController.init(rootViewController: dashboardViewController)
            navController.isNavigationBarHidden = true
            elDrawer.mainViewController = navController
            elDrawer.setDrawerState(.closed, animated: true)
        case 6:
            let storyboardMyLens = UIStoryboard(name: "MyLens", bundle: nil)
            let myLensController = storyboardMyLens.instantiateViewController(withIdentifier: "MyLensListVC") as! MyLensListController
            //myLensController.view.backgroundColor = UIColor.yellow
            let navController = UINavigationController.init(rootViewController: myLensController)
            navController.isNavigationBarHidden = true
            elDrawer.mainViewController = navController
            elDrawer.setDrawerState(.closed, animated: true)
        case 8:
            let storyboardMarketPlace = UIStoryboard(name: "MarketPlace", bundle: nil)
            let marketPlaceController = storyboardMarketPlace.instantiateViewController(withIdentifier: "MarketPlaceVC") as! MarketPlaceController
            let navController = UINavigationController.init(rootViewController: marketPlaceController)
            navController.isNavigationBarHidden = true
            elDrawer.mainViewController = navController
            elDrawer.setDrawerState(.closed, animated: true)
        case 9:
            let storyboardLexicon = UIStoryboard(name: "Lexicon", bundle: nil)
            let lexiconListController = storyboardLexicon.instantiateViewController(withIdentifier: "LexiconListVC") as! LexiconListController
            let navController = UINavigationController.init(rootViewController: lexiconListController)
            navController.isNavigationBarHidden = true
            elDrawer.mainViewController = navController
            elDrawer.setDrawerState(.closed, animated: true)
        case 10:
            let storyboardMyAccount = UIStoryboard(name: "MyAccount", bundle: nil)
            let myAccountVC = storyboardMyAccount.instantiateViewController(withIdentifier: "MyAccountVC") as! MyAccountController
            myAccountVC.selectedID = 1
            
            let navController = UINavigationController.init(rootViewController: myAccountVC)
            navController.isNavigationBarHidden = true
            elDrawer.mainViewController = navController
            elDrawer.setDrawerState(.closed, animated: true)
        case 11:
            let storyboardLensDesk = UIStoryboard(name: "LensDesk", bundle: nil)
            let lensDeskVC = storyboardLensDesk.instantiateViewController(withIdentifier: "LensDeskVC") as! LensDeskController
            lensDeskVC.selectedID = 3
            
            let navController = UINavigationController.init(rootViewController: lensDeskVC)
            navController.isNavigationBarHidden = true
            elDrawer.mainViewController = navController
            elDrawer.setDrawerState(.closed, animated: true)
            
            
        default:
            elDrawer.setDrawerState(.closed, animated: true)
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

class Menu {
    var name = ""
    var image = ""
    var badge = 0
    var isSelected = false
}

extension CGFloat {
    public static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}
extension UIColor {
    public static func randomColor() -> UIColor {
        // If you wanted a random alpha, just create another
        // random number for that too.
        return UIColor(red:   .random(),
                       green: .random(),
                       blue:  .random(),
                       alpha: 1.0)
    }
}

