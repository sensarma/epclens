//
//  MarketPlaceAddJobsController.swift
//  EpcLensProject
//
//  Created by Moumita Datta on 02/07/18.
//  Copyright © 2018 Moumita Datta. All rights reserved.
//

import UIKit

class MarketPlaceAddJobsController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:: IBAction
    @IBAction func btnBackDidTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
