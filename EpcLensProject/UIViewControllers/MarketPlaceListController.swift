//
//  MarketPlaceListController.swift
//  EpcLensProject
//
//  Created by Moumita Datta on 20/06/18.
//  Copyright © 2018 Moumita Datta. All rights reserved.
//

import UIKit

class MarketPlaceListController: UIViewController, ClassifiedAdsListingProtocol, JobsListingProtocol {
    
    @IBOutlet weak var tableMarketPlaceList: UITableView!
    
    
    var classifiedAdsListDispatcher = MarketPlaceClassifiedAdsListingDispatcher()
    var classifiedAdsListArr = [ClassifiedAdsList]()
    
    var jobsListDispatcher = MarketPlaceJobsListingDispatcher()
    var jobsAdsListArr = [JobsList]()
    
    var isClassifiedAds : Int = 0
    
    
    //Mark:: AnimatedLoader
    let loaderController = EpcLensLoaderViewController(nibName: "EpcLensLoaderViewController", bundle: nil)
    
    
    
    //MARK:: UIView
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if isClassifiedAds == 0 {
            classifiedAdsListDispatcher.delegate = self
            var tempDict = [String:String]()
            tempDict["token"] = EpicLensConstantsVariables.accessToken
            tempDict["page"] = "0"
            classifiedAdsListDispatcher.GetMarketPlaceClassifiedAdsListing(paramDict: tempDict)
        } else {
            jobsListDispatcher.delegate = self
            var tempDict = [String:String]()
            tempDict["token"] = EpicLensConstantsVariables.accessToken
            tempDict["page"] = "0"
            jobsListDispatcher.GetMarketPlaceJobsListing(paramDict: tempDict)
        }
        
        
        // Start Loaded
        loaderController.showEpcLoader(_sourceView: self)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:: IBAction
    @IBAction func btnBackDidTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:: ClassifiedAdsListingProtocol
    func didUpdateControllerWithObjMarketPlaceClassifiedAdsListing(classifiedAdsListing: MarketPlaceClassifiedAdsListModel) {
        DispatchQueue.main.async {
            if classifiedAdsListing.data.sucess {
                self.classifiedAdsListArr = classifiedAdsListing.data.classifiedlist!
                self.tableMarketPlaceList.delegate = self
                self.tableMarketPlaceList.dataSource = self
                self.tableMarketPlaceList.reloadData()
                // Stop Loaded
                self.loaderController.hideEpcLoader()
            }
        }
    }
    
    //MARK:: JobsListingProtocol
    func didUpdateControllerWithObjMarketPlaceJobsListing(jobsListing: MarketPlaceJobsListModel) {
        DispatchQueue.main.async {
            if jobsListing.data.sucess {
                self.jobsAdsListArr = jobsListing.data.joblist!
                self.tableMarketPlaceList.delegate = self
                self.tableMarketPlaceList.dataSource = self
                self.tableMarketPlaceList.reloadData()
                // Stop Loaded
                self.loaderController.hideEpcLoader()
            }
        }
    }
}


extension MarketPlaceListController : UITableViewDataSource, UITableViewDelegate {
    
    //MARK:: UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isClassifiedAds == 0 {
            return classifiedAdsListArr.count
        } else {
            return jobsAdsListArr.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isClassifiedAds == 0 {
            let idstr = "ClassifiedAdsListTableCell"
            
            // get a reference to our storyboard cell
            var classifiedAdsListCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
            if classifiedAdsListCell == nil {
                classifiedAdsListCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
            }
            
            
            //Declare the outlet in our custom class
            let imgViewClassifiedAds = classifiedAdsListCell.viewWithTag(25) as! UIImageView
            //let viewClassifiedAds = classifiedAdsListCell.viewWithTag(21)
            let lblClassifiedAdsTitle = classifiedAdsListCell.viewWithTag(22) as! UILabel
            let lblClassifiedAdsCategory = classifiedAdsListCell.viewWithTag(23) as! UILabel
            let lblClassifiedAdsLocation = classifiedAdsListCell.viewWithTag(24) as! UILabel
            let lblClassifiedAdsShortDesc = classifiedAdsListCell.viewWithTag(26) as! UILabel
            let viewClassifiedAdsType = classifiedAdsListCell.viewWithTag(27)
            let lblClassifiedAdsType = classifiedAdsListCell.viewWithTag(28) as! UILabel
            
            classifiedAdsListCell.viewWithTag(20)?.layer.cornerRadius = 10.0
            classifiedAdsListCell.viewWithTag(20)?.clipsToBounds = true
            classifiedAdsListCell.viewWithTag(21)?.layer.cornerRadius = 10.0
            classifiedAdsListCell.viewWithTag(21)?.clipsToBounds = true
            imgViewClassifiedAds.layer.cornerRadius = 5.0
            imgViewClassifiedAds.clipsToBounds = true
            viewClassifiedAdsType?.layer.cornerRadius = 5.0
            viewClassifiedAdsType?.clipsToBounds = true
            
            if (classifiedAdsListArr[indexPath.row].title?.isEmpty)! {
                lblClassifiedAdsTitle.text = " "
            } else {
                lblClassifiedAdsTitle.text = classifiedAdsListArr[indexPath.row].title!
            }
            
            let isClassifiedAdsType : String = classifiedAdsListArr[indexPath.row].isFeatured!
            
            if isClassifiedAdsType == "yes" {
                viewClassifiedAdsType?.isHidden = false
                lblClassifiedAdsType.text = "featured"
            } else {
                viewClassifiedAdsType?.isHidden = true
            }
            
            if classifiedAdsListArr[indexPath.row].mediaURL == nil {
                imgViewClassifiedAds.image = UIImage(named: "AppIcon")
            } else if let imgUrl = URL.init(string: classifiedAdsListArr[indexPath.row].mediaURL!){
                Manager.shared.loadImage(with: imgUrl, into: imgViewClassifiedAds)
            }
            
            if (classifiedAdsListArr[indexPath.row].optionLabel?.isEmpty)! {
                lblClassifiedAdsCategory.text =  " "
            } else {
                lblClassifiedAdsCategory.text = "category : " + classifiedAdsListArr[indexPath.row].optionLabel!
            }
            
            if (classifiedAdsListArr[indexPath.row].location?.isEmpty)! {
                lblClassifiedAdsLocation.text = " "
            } else {
                lblClassifiedAdsLocation.text =  classifiedAdsListArr[indexPath.row].location!
            }
            
            if (classifiedAdsListArr[indexPath.row].shortDesc?.isEmpty)! {
                lblClassifiedAdsShortDesc.text = " "
            } else {
                lblClassifiedAdsShortDesc.text = classifiedAdsListArr[indexPath.row].shortDesc!
            }
            
            return classifiedAdsListCell
        } else {
            let idstr = "JobsListTableCell"
            
            // get a reference to our storyboard cell
            var jobsListCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
            if jobsListCell == nil {
                jobsListCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
            }
            
            
            //Declare the outlet in our custom class
            let imgViewJobs = jobsListCell.viewWithTag(35) as! UIImageView
            //let viewJobs = jobsListCell.viewWithTag(31)
            let lblJobsTitle = jobsListCell.viewWithTag(32) as! UILabel
            let lblJobsType = jobsListCell.viewWithTag(33) as! UILabel
            let lblJobsLocation = jobsListCell.viewWithTag(34) as! UILabel
            let lblJobsCompanyName = jobsListCell.viewWithTag(36) as! UILabel
            let lblJobsPoster = jobsListCell.viewWithTag(38) as! UILabel
            
            jobsListCell.viewWithTag(30)?.layer.cornerRadius = 10.0
            jobsListCell.viewWithTag(30)?.clipsToBounds = true
            jobsListCell.viewWithTag(31)?.layer.cornerRadius = 10.0
            jobsListCell.viewWithTag(31)?.clipsToBounds = true
            imgViewJobs.layer.cornerRadius = 5.0
            imgViewJobs.clipsToBounds = true
            
            if (jobsAdsListArr[indexPath.row].jobstitle?.isEmpty)! {
                lblJobsTitle.text = " "
            } else {
                lblJobsTitle.text = jobsAdsListArr[indexPath.row].jobstitle!
            }
            
            if jobsAdsListArr[indexPath.row].companyLogo?.isEmpty == false && jobsAdsListArr[indexPath.row].companyLogo != "" {
                let escapedString = jobsAdsListArr[indexPath.row].companyLogo?.replacingOccurrences(of: " ", with: "%20")
                Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgViewJobs)
            } else {
                imgViewJobs.image = UIImage(named: "AppIcon")
            }
            
            
            if (jobsAdsListArr[indexPath.row].jobTypeLabel?.isEmpty)! {
                lblJobsType.text = " "
            } else {
                lblJobsType.text = jobsAdsListArr[indexPath.row].jobTypeLabel!
            }
            
            if (jobsAdsListArr[indexPath.row].jobsLocation?.isEmpty)! {
                lblJobsLocation.text = " "
            } else {
                lblJobsLocation.text =  jobsAdsListArr[indexPath.row].jobsLocation!
            }
            
            if (jobsAdsListArr[indexPath.row].companyName?.isEmpty)! {
                lblJobsCompanyName.text = " "
            } else {
                lblJobsCompanyName.text = jobsAdsListArr[indexPath.row].companyName!
            }
            
            if (jobsAdsListArr[indexPath.row].jobPoster?.isEmpty)! {
                lblJobsPoster.text = " "
            } else {
                lblJobsPoster.text = jobsAdsListArr[indexPath.row].jobPoster!
            }
            
            return jobsListCell
        }
    }
    
    
    //MARK:: UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if isClassifiedAds == 0 {
            return 150
        } else {
            return 155
        }
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        // get a reference to our storyboard cell
        let marketPlaceListHeaderCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: "MarketPlaceListHeaderCell")!
        
        //Declare the outlet in our custom class
        let lblHeaderTitle = marketPlaceListHeaderCell.viewWithTag(10) as! UILabel
        
        if isClassifiedAds == 0 {
            lblHeaderTitle.font = lblHeaderTitle.font.withSize(16)
            lblHeaderTitle.text = "classified ads"
            
        } else {
            lblHeaderTitle.font = lblHeaderTitle.font.withSize(16)
            lblHeaderTitle.text = "job ads"
        }
        return marketPlaceListHeaderCell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isClassifiedAds == 0 {
            let storyboardMarketPlace = UIStoryboard(name: "MarketPlace", bundle: nil)
            let classifiedAdsDetailsVC = storyboardMarketPlace.instantiateViewController(withIdentifier: "ClassifiedAdsDetailsVC") as! ClassifiedAdsDetailsController
            classifiedAdsDetailsVC.classifiedAdsId = classifiedAdsListArr[indexPath.row].id
            self.navigationController?.pushViewController(classifiedAdsDetailsVC, animated: true)
        } else {
            let storyboardMarketPlace = UIStoryboard(name: "MarketPlace", bundle: nil)
            let jobAdsDetailsVC = storyboardMarketPlace.instantiateViewController(withIdentifier: "JobsAdsDetailsVC") as! JobsAdDetailsController
            jobAdsDetailsVC.jobAdsId = jobsAdsListArr[indexPath.row].jobsID
            self.navigationController?.pushViewController(jobAdsDetailsVC, animated: true)
        }
    }
}
