//
//  LensDeskController.swift
//  EpcLensProject
//
//  Created by Moumita Datta on 25/06/18.
//  Copyright © 2018 Moumita Datta. All rights reserved.
//

import UIKit

class LensDeskController: UIViewController, LensDeskListProtocol {
    
    @IBOutlet weak var tableLensDesk: UITableView!
    
    var lensDeskDispatcher = LensDeskDispatcher()
    var lensDeskMenuListArr = [LensDeskMenuList]()
    var lensDeskListByMenuArr = [LensDeskDetailsByCategory]()
    
    //var objSelectedItem:LexiconsMenuList? = nil
    
    
    var selectedID : Int = -1
    private var isFirstTimeLoadMenuDetails = false
    
    
    //Mark:: AnimatedLoader
    let loaderController = EpcLensLoaderViewController(nibName: "EpcLensLoaderViewController", bundle: nil)
    
    
    
    //MARK:: UIView
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
//
//                lexiconDispatcher.delegate = self
//                var tempDict = [String:String]()
//                tempDict["token"] = EpicLensConstantsVariables.accessToken
//                lexiconDispatcher.GetLexiconMenuList(paramDict: tempDict)
//
//        
//        selectedID = 1
//        let indexPath = IndexPath(item: 0, section: 0) //Here the first value is set to the first index of the collection view
//        self.postCategoryCollectionView.selectItem(at: indexPath, animated: true, scrollPosition: [])
//
        
        
        
        lensDeskDispatcher.delegate = self
        var tempDict = [String:String]()
        tempDict["token"] = EpicLensConstantsVariables.accessToken
        lensDeskDispatcher.GetLensDeskMenuList(paramDict: tempDict)
        
        
        // Start Loaded
        loaderController.showEpcLoader(_sourceView: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:: IBAction
    @IBAction func btnLeftMenuDidTap(_ sender: Any) {
        let elDrawer = self.navigationController?.parent as! KYDrawerController
        elDrawer.setDrawerState(.opened, animated: true)
    }
    
    @IBAction func btnRightMenuDidTap(_ sender: UIButton) {
        let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
        let objReportPopup = storyboardMain.instantiateViewController(withIdentifier: "RightMenuController") as! RightMenuController
        objReportPopup.showPopup(_controller: self)
    }
    
    
    
    //MARK:: LensDeskListProtocol
    func didUpdateControllerWithObjLensDeskMenuList(lensDeskMenuList: LensDeskMenuListModel) {
        DispatchQueue.main.async {
            if lensDeskMenuList.data.sucess {
                self.lensDeskMenuListArr = lensDeskMenuList.data.lensdesklist!
                
                self.tableLensDesk.delegate = self
                self.tableLensDesk.dataSource = self
                self.tableLensDesk.reloadData()
                
                if self.isFirstTimeLoadMenuDetails == false && self.lensDeskMenuListArr.count > 0 {
                    self.isFirstTimeLoadMenuDetails = true
                    self.selectedID = self.lensDeskMenuListArr[0].id
                    var tempDict = [String:String]()
                    tempDict["token"] = EpicLensConstantsVariables.accessToken
                    tempDict["id"] = String(self.selectedID)
                    self.lensDeskDispatcher.GetLensDeskListDetailsByMenu(paramDict: tempDict)
                } else {
                    // Stop Loaded
                    self.loaderController.hideEpcLoader()
                }
            }
        }
    }
    
    
    
    func didUpdateControllerWithObjLensDeskListDetailsByMenu(lensDeskListDetailsByMenu: LensDeskDetailsByMenuModel) {
        DispatchQueue.main.async {
            if lensDeskListDetailsByMenu.data.sucess {
                //self.lensDeskListByMenuArr.removeAll()
                self.lensDeskListByMenuArr = lensDeskListDetailsByMenu.data.lensdeskdetailsbycat
                
                self.tableLensDesk.delegate = self
                self.tableLensDesk.dataSource = self
                self.tableLensDesk.reloadData()
                // Stop Loaded
                self.loaderController.hideEpcLoader()
            }
        }
    }
    
}


extension LensDeskController : UITableViewDataSource, UITableViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    //MARK:: UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            print("lensDeskListByMenuArr.count :: \(lensDeskListByMenuArr.count)")
            return lensDeskListByMenuArr.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (indexPath.section == 0) {
            let idstr = "LensDeskMenuTableCell"
            
            // get a reference to our storyboard cell
            var lensDeskMenuCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
            if lensDeskMenuCell == nil {
                lensDeskMenuCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
            }
            
            // Use the outlet in our custom class to get a reference to the UI Elements in the cell
            let collectionLensDeskMenu = lensDeskMenuCell.viewWithTag(1) as! UICollectionView
            
            collectionLensDeskMenu.dataSource = self
            collectionLensDeskMenu.delegate = self
            collectionLensDeskMenu.reloadData()
            
            lensDeskMenuCell.selectionStyle = UITableViewCellSelectionStyle.none
            return lensDeskMenuCell
        } else {
            let idstr = "LensDeskTableCell"
            
            // get a reference to our storyboard cell
            var lensDeskCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: idstr)!
            if lensDeskCell == nil {
                lensDeskCell = UITableViewCell(style: .default, reuseIdentifier: idstr)
            }
            
            
            // Use the outlet in our custom class to get a reference to the UI Elements in the cell
            let lblDetails = lensDeskCell.viewWithTag(4) as! UILabel
            
            if lensDeskListByMenuArr.count > 0 {
                lblDetails.text = lensDeskListByMenuArr[indexPath.row].desktitle
            }
            
            
            if (indexPath.row == (lensDeskListByMenuArr.count-1)) {
                lensDeskCell.viewWithTag(32)?.layer.cornerRadius = 10
            }
            
            
            lensDeskCell.selectionStyle = UITableViewCellSelectionStyle.none
            return lensDeskCell
        }
    }
    
    
    //MARK:: UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 110
        } else {
            return 35
        }
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 {
            let lensDeskHeaderCell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: "LensDeskHeaderView")!
            
            
            let lblHeaderTitle = lensDeskHeaderCell.viewWithTag(13) as! UILabel
            
            if lensDeskMenuListArr.count > 0 {
                if selectedID == 3 {
                    lblHeaderTitle.text = lensDeskMenuListArr[0].title
                } else if selectedID == 4 {
                    lblHeaderTitle.text = lensDeskMenuListArr[1].title
                } else if selectedID == 5 {
                    lblHeaderTitle.text = lensDeskMenuListArr[2].title
                } else if selectedID == 6 {
                    lblHeaderTitle.text = lensDeskMenuListArr[3].title
                } else if selectedID == 7 {
                    lblHeaderTitle.text = lensDeskMenuListArr[4].title
                }
            }
            
            return lensDeskHeaderCell
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if (section == 0) {
            return 0
        } else if (section == 1) {
            return 35
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 15
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboardLensDesk = UIStoryboard(name: "LensDesk", bundle: nil)
        let lensDeskDetailsVC = storyboardLensDesk.instantiateViewController(withIdentifier: "LensDeskDetailsVC") as! LensDeskDetailsController
        lensDeskDetailsVC.selectedLensDeskID = lensDeskListByMenuArr[indexPath.row].id
        self.navigationController?.pushViewController(lensDeskDetailsVC, animated: false)
    }
    
    
    //    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
    //        if indexPath.section == 0 {
    //            return 150
    //        } else {
    //            return 35
    //        }
    //    }
    
    
    /*func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
     return 85.0
     }*/
    
    /*func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //print("Menu ::",lexiconMenuResultArr[indexPath.row].type!)
        if lexiconMenuResultArr[indexPath.row].type == "video" {
            print("VideoList id :: \(String(describing: self.lexiconMenuResultArr[indexPath.item].id))")
            let storyboardLensflare = UIStoryboard(name: "Lensflare", bundle: nil)
            let lensFlareVC = storyboardLensflare.instantiateViewController(withIdentifier: "LensFlareVC") as! LensFlareController
            lensFlareVC.id = lexiconMenuResultArr[indexPath.item].id
            lensFlareVC.isLensFlareLoadType = 2
            //UserDefaults.standard.set(false, forKey: "isLensflare")
            self.navigationController?.pushViewController(lensFlareVC, animated: false)
        } else if lexiconMenuResultArr[indexPath.row].lexiconcategoryTitle == "encyclopedia" {
            let storyboardLensflare = UIStoryboard(name: "Lexicon", bundle: nil)
            let lexiconDetailsVC = storyboardLensflare.instantiateViewController(withIdentifier: "LexiconDetailsVC") as! LexiconDetailsController
            lexiconDetailsVC.selectedLexiconID = 2
            lexiconDetailsVC.selectedLexiconItemID = lexiconMenuResultArr[indexPath.item].id
            lexiconDetailsVC.navigationTitleStr = lexiconMenuResultArr[indexPath.row].lexiconcategoryTitle!//"encyclopedia"
            self.navigationController?.pushViewController(lexiconDetailsVC, animated: false)
        } else if lexiconMenuResultArr[indexPath.row].lexiconcategoryTitle == "glossary" {
            let storyboardLensflare = UIStoryboard(name: "Lexicon", bundle: nil)
            let lexiconDetailsVC = storyboardLensflare.instantiateViewController(withIdentifier: "LexiconDetailsVC") as! LexiconDetailsController
            lexiconDetailsVC.selectedLexiconID = 3
            lexiconDetailsVC.selectedLexiconItemID = lexiconMenuResultArr[indexPath.row].id
            lexiconDetailsVC.navigationTitleStr = "glossary"
            self.navigationController?.pushViewController(lexiconDetailsVC, animated: false)
        } else if lexiconMenuResultArr[indexPath.row].lexiconcategoryTitle == "regulations" {
            let storyboardLensflare = UIStoryboard(name: "Lexicon", bundle: nil)
            let lexiconDetailsVC = storyboardLensflare.instantiateViewController(withIdentifier: "LexiconDetailsVC") as! LexiconDetailsController
            //print("selectedLexiconItemID :: ",lexiconMenuResultArr[indexPath.item].id)
            lexiconDetailsVC.selectedLexiconID = 4
            lexiconDetailsVC.selectedLexiconItemID = lexiconMenuResultArr[indexPath.row].id
            lexiconDetailsVC.navigationTitleStr = "regulations"
            self.navigationController?.pushViewController(lexiconDetailsVC, animated: false)
        } else if lexiconMenuResultArr[indexPath.row].lexiconcategoryTitle == "tools" {
            let storyboardLensflare = UIStoryboard(name: "Lexicon", bundle: nil)
            let lexiconDetailsVC = storyboardLensflare.instantiateViewController(withIdentifier: "LexiconDetailsVC") as! LexiconDetailsController
            lexiconDetailsVC.selectedLexiconID = 5
            lexiconDetailsVC.selectedLexiconItemID = lexiconMenuResultArr[indexPath.row].id
            lexiconDetailsVC.navigationTitleStr = "tools"
            self.navigationController?.pushViewController(lexiconDetailsVC, animated: false)
        }
    }*/
    
    
    // MARK:: UICollectionViewDataSource protocol
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("lensDeskMenuListArr count:: ",lensDeskMenuListArr.count)
        return lensDeskMenuListArr.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let idstr = "LensDeskMenuCell"
        
        // get a reference to our storyboard cell
        let lensDeskMenuCell : UICollectionViewCell! = collectionView.dequeueReusableCell(withReuseIdentifier: idstr, for: indexPath)
        
        // Use the outlet in our custom class to get a reference to the UI Elements in the cell
        let lblMenuTitle = lensDeskMenuCell.viewWithTag(2) as! UILabel
        let imgMenuThumbnail = lensDeskMenuCell.viewWithTag(3) as! UIImageView
        let lblMenuTitle1 = lensDeskMenuCell.viewWithTag(4) as! UILabel
        
        
        var token = lensDeskMenuListArr[indexPath.row].title?.components(separatedBy: " ")
        lblMenuTitle.text = token![0]
        
        
        
        let menuImgUrlString = lensDeskMenuListArr[indexPath.row].image
        
        if menuImgUrlString?.isEmpty == false && menuImgUrlString != "" {
            let escapedString = menuImgUrlString?.replacingOccurrences(of: " ", with: "%20")            
            Manager.shared.loadImage(with: URL.init(string: escapedString!)!, into: imgMenuThumbnail)
        } else {
            imgMenuThumbnail.image = UIImage(named: "AppIcon")
        }
        
        /// Text Color Change based on Selection
        if selectedID == lensDeskMenuListArr[indexPath.row].id {
            lblMenuTitle.textColor = UIColor.black
            lblMenuTitle1.textColor = UIColor.black
        } else {
            lblMenuTitle.textColor = UIColor.white
            lblMenuTitle1.textColor = UIColor.white
        }
        
        /// background Color Change of Items
        if indexPath.row == 0 {
            lensDeskMenuCell?.backgroundColor = UIColor(red: CGFloat((191.0 / 255.0)), green: CGFloat((146.0 / 255.0)), blue: CGFloat((126.0 / 255.0)), alpha: 1.0)
            lblMenuTitle1.text = "info"
        } else if indexPath.row == 1 {
            lensDeskMenuCell?.backgroundColor = UIColor(red: CGFloat((110.0 / 255.0)), green: CGFloat((156.0 / 255.0)), blue: CGFloat((178.0 / 255.0)), alpha: 1.0)
            lblMenuTitle1.text = "info"
        } else if indexPath.row == 2 {
            lensDeskMenuCell?.backgroundColor = UIColor(red: CGFloat((140.0 / 255.0)), green: CGFloat((161.0 / 255.0)), blue: CGFloat((179.0 / 255.0)), alpha: 1.0)
            lblMenuTitle1.text = "info"
        } else if indexPath.row == 3 {
            lensDeskMenuCell?.backgroundColor = UIColor(red: CGFloat((175.0 / 255.0)), green: CGFloat((163.0 / 255.0)), blue: CGFloat((149.0 / 255.0)), alpha: 1.0)
            lblMenuTitle1.text = "info"
        } else {
            lensDeskMenuCell?.backgroundColor = UIColor(red: CGFloat((165.0 / 255.0)), green: CGFloat((160.0 / 255.0)), blue: CGFloat((166.0 / 255.0)), alpha: 1.0)
            lblMenuTitle1.text = "interest"
        }
        
        return lensDeskMenuCell!
    }
    
    
    // MARK:: UICollectionViewDelegate protocol
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth = (self.view.frame.size.width-50)/3
        return CGSize(width: cellWidth, height: 35)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("objSelectedItem :: \(String(describing: self.lensDeskMenuListArr[indexPath.item]))")
        selectedID = self.lensDeskMenuListArr[indexPath.item].id
        
        
        var tempDict = [String:String]()
        tempDict["token"] = EpicLensConstantsVariables.accessToken
        tempDict["id"] = String(selectedID)
        lensDeskDispatcher.GetLensDeskListDetailsByMenu(paramDict: tempDict)
        
        tableLensDesk.reloadData()
    }
    
}
