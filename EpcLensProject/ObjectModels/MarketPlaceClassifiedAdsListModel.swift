// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? JSONDecoder().decode(Welcome.self, from: jsonData)

import Foundation

struct MarketPlaceClassifiedAdsListModel: Codable {
    let data: ClassifiedAdsDataClass
    let errNode: ErrNodeClassifiedAds
}

struct ClassifiedAdsDataClass: Codable {
    let sucess: Bool
    let totalPage: Int?
    let classifiedlist: [ClassifiedAdsList]?
    
    enum CodingKeys: String, CodingKey {
        case sucess
        case totalPage = "total_page"
        case classifiedlist
    }
}

struct ClassifiedAdsList: Codable {
    let id: Int
    let title, slug, mediaURL: String?
    let price: Double?
    let shortDesc, location, isFeatured, startDate: String?
    let optionLabel: String?
    
    enum CodingKeys: String, CodingKey {
        case id, title, slug
        case mediaURL = "mediaUrl"
        case price, shortDesc, location, isFeatured, startDate, optionLabel
    }
}

struct ErrNodeClassifiedAds: Codable {
    let errMsg, errCode: String
}
