//
//  LoginResponseModel.swift
//  EpcLensProject
//
//  Created by Sudipta Biswas on 19/04/18.
//  Copyright © 2018 Sudipta Biswas. All rights reserved.
//

import UIKit
import Foundation

struct LoginResponseModel: Codable {
    let data: LoginResponse
    let errNode: ErrNodeLogin
}

struct LoginResponse: Codable {
    let verified: Bool
    let accessToken: String?
    
    enum CodingKeys: String, CodingKey {
        case verified
        case accessToken = "access_token"
    }
}

struct ErrNodeLogin: Codable {
    let errMsg, errCode: String?
}
