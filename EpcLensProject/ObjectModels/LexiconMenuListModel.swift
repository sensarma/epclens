// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? JSONDecoder().decode(Welcome.self, from: jsonData)


import Foundation

struct LexiconMenuListModel: Codable {
    let data: LexiconMenuDataClass
    let errNode: ErrNodeLexicon
}

struct LexiconMenuDataClass: Codable {
    let sucess: Bool
    let lexiconslist: [LexiconsMenuList]
}

struct LexiconsMenuList: Codable {
    let id: Int
    let title, image: String?
}

struct ErrNodeLexicon: Codable {
    let errMsg, errCode: String
}


/*struct LexiconListModel: Codable {
    let data: LexiconDataClass
    let errNode: ErrNodeLexicon
}

struct LexiconDataClass: Codable {
    let success: String
    let newTrending: [LexiconNewTrending]
    let helpFinish: [LexiconHelpFinish]
    
    enum CodingKeys: String, CodingKey {
        case success
        case newTrending = "new_trending"
        case helpFinish = "help_finish"
    }
}

struct LexiconHelpFinish: Codable {
    let id, text1: String
}

struct LexiconNewTrending: Codable {
    let id, text: String
}

struct ErrNodeLexicon: Codable {
    let errCode: Int
    let errMsg: String
}*/
