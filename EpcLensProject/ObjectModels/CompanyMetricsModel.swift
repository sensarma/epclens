// To parse the JSON, add this file to your project and do:
//
//   let companyMetricsModel = try? JSONDecoder().decode(CompanyMetricsModel.self, from: jsonData)

import Foundation

struct CompanyMetricsModel: Codable {
    let data: CompanyMetricsData
    let errNode: ErrNodeCompanyMetrics
}

struct  CompanyMetricsData: Codable {
    let sucess: Bool
    let companyDetails: [CompanyDetailMetrics]?
    let companyClientale: [CompanyClientaleMetrics]?
    let companyServicePortfolio: [CompanyServicePortfolio]?
    
    enum CodingKeys: String, CodingKey {
        case sucess
        case companyDetails = "company_details"
        case companyClientale, companyServicePortfolio
    }
}

struct CompanyClientaleMetrics: Codable {
    let companyID: Int?
    let image: String?
    
    enum CodingKeys: String, CodingKey {
        case companyID = "company_id"
        case image
    }
}

struct CompanyDetailMetrics: Codable {
    let contractrisk, annualBudget, revnue, projectSoftware: String?
    let isnetRegistrationReference, projectphaseName, opectypeName: String?
}

struct CompanyServicePortfolio: Codable {
    let id, companyID, serviceID: Int?
    let serviceText, createdAt, updatedAt: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case companyID = "companyId"
        case serviceID = "serviceId"
        case serviceText, createdAt, updatedAt
    }
}

struct ErrNodeCompanyMetrics: Codable {
    let errMsg, errCode: String
}
