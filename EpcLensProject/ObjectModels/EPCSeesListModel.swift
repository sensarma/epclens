// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? JSONDecoder().decode(Welcome.self, from: jsonData)

import Foundation

struct EPCSeesListModel: Codable {
    let data: EPCSeesListDataClass
    let errNode: ErrNodeEPCSeesList
}

struct EPCSeesListDataClass: Codable {
    let sucess: Bool
    let totalPage: Int
    let itemList: [EPCSeesItemList]
    
    enum CodingKeys: String, CodingKey {
        case sucess
        case totalPage = "total_page"
        case itemList = "item_list"
    }
}

struct EPCSeesItemList: Codable {
    let id: Int
    let title, image, description: String?
}

struct ErrNodeEPCSeesList: Codable {
    let errMsg, errCode: String
}

