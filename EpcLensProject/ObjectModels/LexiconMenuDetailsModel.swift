// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? JSONDecoder().decode(Welcome.self, from: jsonData)

import Foundation

struct LexiconMenuDetailsModel: Codable {
    let data: LexiconMenuDetailsDataClass
    let errNode: ErrNodeLexiconMenuDetails
}

struct LexiconMenuDetailsDataClass: Codable {
    let sucess: Bool
    let result: [LexiconMenuResult]?
    let helpus: [LexiconMenuHelpus]?
}

struct LexiconMenuResult: Codable {
    let id: Int
     let title, details, type: String?
    let lexiconcategoryTitle: String?
}

struct LexiconMenuHelpus: Codable {
    let id: Int
    let title, details, type: String?
    let lexiconcategoryTitle: String?
    //let lexiconcategoryTitle: LexiconcategoryTitle
}


enum LexiconcategoryTitle: String, Codable {
    case encyclopedia = "encyclopedia"
    case glossary = "glossary"
    case tools = "tools"
}

struct ErrNodeLexiconMenuDetails: Codable {
    let errMsg, errCode: String
}
