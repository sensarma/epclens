// To parse the JSON, add this file to your project and do:
//
//   let companyDetailsModel = try? JSONDecoder().decode(CompanyDetailsModel.self, from: jsonData)

import Foundation

struct CompanyDetailsModel: Codable {
    let data: CompanyDetailsData
    let errNode: ErrNodeCompanyDetails
}

struct CompanyDetailsData: Codable {
    let sucess: Bool
    let companyDetails: [CompanyDetailNode]?
    
    enum CodingKeys: String, CodingKey {
        case sucess
        case companyDetails = "company_details"
    }
}

struct CompanyDetailNode: Codable {
    let id: Int?
    let name, ownership, ftesBusinessUnit, companyBanner: String?
    let logo, projectphaseName, opectypeName, companyServiceType: String?
}

struct ErrNodeCompanyDetails: Codable {
    let errMsg, errCode: String
}
