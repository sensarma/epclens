// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? JSONDecoder().decode(Welcome.self, from: jsonData)

import Foundation

struct LensDeskDetailsByMenuModel: Codable {
    let data: LensDeskDetailsByMenuDataClass
    let errNode: ErrNodeLensDeskDetailsByMenu
}

struct LensDeskDetailsByMenuDataClass: Codable {
    let sucess: Bool
    let lensdeskdetailsbycat: [LensDeskDetailsByCategory]
}

struct LensDeskDetailsByCategory: Codable {
    let id: Int
    let desktitle, lensdeskcategoryTitle: String?
}


struct ErrNodeLensDeskDetailsByMenu: Codable {
    let errMsg, errCode: String
}

