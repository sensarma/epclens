//
//  UserdetailsResponseModel.swift
//  EpcLensProject
//
//  Created by Sudipta Biswas on 19/04/18.
//  Copyright © 2018 Sudipta Biswas. All rights reserved.
//

import UIKit
import Foundation

struct UserdetailsResponseModel: Codable {
    let data: UserDetailsData
    let errNode: ErrNodeUserDetails
}

struct UserDetailsData: Codable {
    let success: Bool
    let userName, userDesignation: String?
    let subcriptionName: String?
    let userPic, subscriptionImg: String?
    
    enum CodingKeys: String, CodingKey {
        case success
        case userName = "user_name"
        case userDesignation = "user_designation"
        case subcriptionName = "subcription_name"
        case userPic = "user_pic"
        case subscriptionImg = "subscription_img"
    }
}

struct ErrNodeUserDetails: Codable {
    let errMsg, errCode: String?
}

