// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? JSONDecoder().decode(Welcome.self, from: jsonData)

import Foundation

struct MyLensListByCategoryModel: Codable {
    let data: MyLensByCategoryDataClass
    let errNode: ErrNodeMyLensByCategory
}

struct MyLensByCategoryDataClass: Codable {
    let sucess: Bool
    let mylensdetailsbyslug: [MyLensListByCategory]
}

struct MyLensListByCategory: Codable {
    let id: Int
    let tableName, url, details, icon: String?
    let shortDesc: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case tableName = "table_name"
        case url, details, icon, shortDesc
    }
}

struct ErrNodeMyLensByCategory: Codable {
    let errMsg, errCode: String
}
