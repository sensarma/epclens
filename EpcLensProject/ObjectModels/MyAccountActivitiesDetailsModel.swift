// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? JSONDecoder().decode(Welcome.self, from: jsonData)

import Foundation

struct MyAccountActivitiesDetailsModel: Codable {
    let data: ActivitiesDetailsDataClass
    let errNode: ErrNodeActivitiesDetails
}

struct ActivitiesDetailsDataClass: Codable {
    let sucess: Bool
    
    let advertisement: [AdvertisementResults]?
    let rader: [ClassifiedResults]?
    let jobs: [JobResults]?
    let lexicon: [LexiconResults]?
    let classified: [ClassifiedResults]?
}

struct AdvertisementResults: Codable {
    let id: Int
    let title, mediaType, mediaURL, shortDesc: String?
    let companyID, moduleID: Int?
    
    enum CodingKeys: String, CodingKey {
        case id, title, mediaType
        case mediaURL = "mediaUrl"
        case shortDesc
        case companyID = "companyId"
        case moduleID = "module_id"
    }
}

struct ClassifiedResults: Codable {
    let id: Int
    let title: String?
    let price: Double?
    let isFeatured: String?
    let slug, mediaURL, shortDesc: String?
    let companyID, moduleID: Int?
    let description, industry: String?
    
    enum CodingKeys: String, CodingKey {
        case id, title, price, isFeatured, slug
        case mediaURL = "mediaUrl"
        case shortDesc
        case companyID = "companyId"
        case moduleID = "module_id"
        case description, industry
    }
}

struct JobResults: Codable {
    let id: Int
    let title, isFeatured, shortDesc: String?
    let companyID: Int?
    let companyLogo, ownership, location, timeAgo: String?
    let moduleID: Int?
    
    enum CodingKeys: String, CodingKey {
        case id, title, isFeatured, shortDesc
        case companyID = "companyId"
        case companyLogo, ownership, location, timeAgo
        case moduleID = "module_id"
    }
}

struct LexiconResults: Codable {
    let id: Int
    let title: String?
    let moduleID: Int?
    
    enum CodingKeys: String, CodingKey {
        case id, title
        case moduleID = "module_id"
    }
}

struct ErrNodeActivitiesDetails: Codable {
    let errMsg, errCode: String
}
