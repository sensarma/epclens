// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? JSONDecoder().decode(Welcome.self, from: jsonData)

import Foundation

struct MarketPlaceJobsListModel: Codable {
    let data: JobsListDataClass
    let errNode: ErrNodeJobsList
}

struct JobsListDataClass: Codable {
    let sucess: Bool
    let totalPage: Int
    let joblist: [JobsList]?
    
    enum CodingKeys: String, CodingKey {
        case sucess
        case totalPage = "total_page"
        case joblist
    }
}

struct JobsList: Codable {
    let jobsID: Int
    let jobstitle, jobsSlug, jobsLocation, companyName: String?
    let companyLogo, jobPoster, jobTypeLabel: String?
    
    enum CodingKeys: String, CodingKey {
        case jobsID = "jobsId"
        case jobstitle, jobsSlug, jobsLocation, companyName, companyLogo, jobPoster, jobTypeLabel
    }
}

struct ErrNodeJobsList: Codable {
    let errMsg, errCode: String
}
