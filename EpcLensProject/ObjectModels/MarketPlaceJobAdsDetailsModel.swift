// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? JSONDecoder().decode(Welcome.self, from: jsonData)

import Foundation

struct MarketPlaceJobAdsDetailsModel: Codable {
    let data: JobAdsDetailsDataClass
    let errNode: ErrNodeJobAdsDetails
}

struct JobAdsDetailsDataClass: Codable {
    let sucess: Bool
    let isSavedJob: Int
    let jobs: [JobDetails]?
    
    enum CodingKeys: String, CodingKey {
        case sucess
        case isSavedJob = "is_saved_job"
        case jobs
    }
}

struct JobDetails: Codable {
    let jobsID: Int
    let jobsTitle, jobsDescription, jobsSalary, jobsSlug: String?
    let jobsDesignation, jobsLocation, jobsUpdatedBy, companyName: String?
    let companyOwnership, companyLogo, companyBanner, industryNames: String?
    let jobPosteremail, jobPostername, jobPosterdesignation, jobPosterlocation: String?
    let jobPosterimage, jobTypeLabel, jobsPublishedAt: String?
    
    enum CodingKeys: String, CodingKey {
        case jobsID = "jobsId"
        case jobsTitle, jobsDescription, jobsSalary, jobsSlug, jobsDesignation, jobsLocation, jobsUpdatedBy, companyName, companyOwnership, companyLogo, companyBanner, industryNames, jobPosteremail, jobPostername, jobPosterdesignation, jobPosterlocation, jobPosterimage, jobTypeLabel, jobsPublishedAt
    }
}

struct ErrNodeJobAdsDetails: Codable {
    let errMsg, errCode: String
}
