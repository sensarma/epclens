// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? JSONDecoder().decode(Welcome.self, from: jsonData)

import Foundation

struct PeopleDetailModel: Codable {
    let data: PeopleDetailDataClass
    let errNode: ErrNodePeopleDetail
}

struct PeopleDetailDataClass: Codable {
    let sucess: Bool
    let peopleDetails: [PeopleDetail]?
    let peopleProject: [PeopleProject]?
    let peopleresultArray: [PeopleGallery]?
    
    enum CodingKeys: String, CodingKey {
        case sucess
        case peopleDetails = "people_details"
        case peopleProject = "people_project"
        case peopleresultArray
    }
}

struct PeopleDetail: Codable {
    let id: Int?
    let linkedinURL, workingFrom, companyName, peopleName: String?
    let designation, location, img, peopleQuotation: String?
    let email, phoneNo, companyLogo, notableProject: String?
    let notableStory: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case linkedinURL = "linkedinUrl"
        case workingFrom, companyName
        case peopleName = "people_name"
        case designation, location, img
        case peopleQuotation = "people_quotation"
        case email
        case phoneNo = "phone_no"
        case companyLogo
        case notableProject = "notable_project"
        case notableStory = "notable_story"
    }
}

struct PeopleProject: Codable {
    let projectName, projectSlug: String?
    let projectID: Int?
    let projectDesignation, projectStatus, projectValuation, projectCompletionPercentage: String?
    let status: String?
    
    enum CodingKeys: String, CodingKey {
        case projectName, projectSlug
        case projectID = "projectId"
        case projectDesignation, projectStatus, projectValuation, projectCompletionPercentage, status
    }
}

struct PeopleGallery: Codable {
    let contentType: ContentType?
    let title, mediaURL, videoImg: String?
    let peoplegalID: Int?
    let url: String?
    
    enum CodingKeys: String, CodingKey {
        case contentType
        case title = "Title"
        case mediaURL = "mediaUrl"
        case videoImg = "video_img"
        case peoplegalID = "peoplegalId"
        case url = "Url"
    }
}

enum ContentType: String, Codable {
    case image = "image"
    case video = "video"
}

struct ErrNodePeopleDetail: Codable {
    let errMsg, errCode: String
}
