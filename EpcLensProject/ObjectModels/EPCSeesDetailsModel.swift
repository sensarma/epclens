// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? JSONDecoder().decode(Welcome.self, from: jsonData)

import Foundation

struct EPCSeesDetailsModel: Codable {
    let data: EPCSeesDetailsDataClass
    let errNode: ErrNodeEPCSeesDetails
}

struct EPCSeesDetailsDataClass: Codable {
    let sucess: Bool
    let itemList: [EPCSeesDetailsItemList]?
    
    enum CodingKeys: String, CodingKey {
        case sucess
        case itemList = "item_list"
    }
}

struct EPCSeesDetailsItemList: Codable {
    let id: Int?
    let videoTitle, publishedDate, videoImg, videoURL: String?
    let videoUrl1, videoDescription: String?
    let sponsorsBy, featuredBy: [By]?
    
    enum CodingKeys: String, CodingKey {
        case id
        case videoTitle = "video_title"
        case publishedDate
        case videoImg = "video_img"
        case videoURL = "video_url"
        case videoUrl1 = "video_url1"
        case videoDescription = "video_description"
        case sponsorsBy = "sponsors_by"
        case featuredBy = "featured_by"
    }
}

struct By: Codable {
    let compLogo: String?
    let galleryID, companyID: Int?
    let slug: String?
    
    enum CodingKeys: String, CodingKey {
        case compLogo
        case galleryID = "galleryId"
        case companyID = "company_id"
        case slug
    }
}

struct ErrNodeEPCSeesDetails: Codable {
    let errMsg, errCode: String
}
