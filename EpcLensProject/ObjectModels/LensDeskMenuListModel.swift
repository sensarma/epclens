// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? JSONDecoder().decode(Welcome.self, from: jsonData)

import Foundation

struct LensDeskMenuListModel: Codable {
    let data: LensDeskMenuDataClass
    let errNode: ErrNodeLensDeskMenu
}

struct LensDeskMenuDataClass: Codable {
    let sucess: Bool
    let lensdesklist: [LensDeskMenuList]?
}

struct LensDeskMenuList: Codable {
    let id: Int
    let title, image: String?
}

struct ErrNodeLensDeskMenu: Codable {
    let errMsg, errCode: String
}

