// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? JSONDecoder().decode(Welcome.self, from: jsonData)

import Foundation

struct LensFlareListModel: Codable {
    let data: LensFlareListDataClass
    let errNode: ErrNodeLensFlareList
}

struct LensFlareListDataClass: Codable {
    let sucess: Bool
    let itemList: [LensFlareItemList]?
    
    enum CodingKeys: String, CodingKey {
        case sucess
        case itemList = "item_list"
    }
}

struct LensFlareItemList: Codable {
    let id: Int
    let videoTitle, dateTime: String?
    let videoURL, videoUrl1: String?
    let galleryID: Int?
    let type: TypeEnum?
    let videoImg: String?
    let videoDescription: String?
    let sponsorsBy: [SponsorsBy]?
    let featuredBy: [FeaturedBy]?
    
    enum CodingKeys: String, CodingKey {
        case id
        case videoTitle = "video_title"
        case dateTime = "date_time"
        case videoURL = "video_url"
        case videoUrl1 = "video_url1"
        case galleryID = "galleryId"
        case type
        case videoImg = "video_img"
        case videoDescription = "video_description"
        case sponsorsBy = "sponsors_by"
        case featuredBy = "featured_by"
    }
}

struct SponsorsBy: Codable {
    let compLogo: String?
    let galleryID, companyID: Int?
    let slug: String?
    
    enum CodingKeys: String, CodingKey {
        case compLogo
        case galleryID = "galleryId"
        case companyID = "company_id"
        case slug
    }
}

struct FeaturedBy: Codable {
    let compLogo: String?
    let galleryID, companyID: Int?
    let slug: String?
    
    enum CodingKeys: String, CodingKey {
        case compLogo
        case galleryID = "galleryId"
        case companyID = "company_id"
        case slug
    }
}

enum TypeEnum: String, Codable {
    case video = "video"
}

struct ErrNodeLensFlareList: Codable {
    let errMsg, errCode: String
}
