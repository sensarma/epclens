// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? JSONDecoder().decode(Welcome.self, from: jsonData)

import Foundation

struct MarketPlaceModel: Codable {
    let data: MarketPlaceDataClass
    let errNode: ErrNodeMarketPlace
}

struct MarketPlaceDataClass: Codable {
    let sucess: Bool
    let adver: [AdverMarketPlaceList]?
    let jobs: [JobsMarketPlaceList]?
    let classified: [ClassifiedMarketPlaceList]?
}

struct AdverMarketPlaceList: Codable {
    let id: Int
    let title, mediaType, mediaURL, shortDesc: String?
    let companySlug: String?
    
    enum CodingKeys: String, CodingKey {
        case id, title, mediaType
        case mediaURL = "mediaUrl"
        case shortDesc, companySlug
    }
}

struct ClassifiedMarketPlaceList: Codable {
    let id: Int
    let classifiedSlug, classifiedTitle, classifiedMediaURL, classifiedPrice: String?
    let classifiedIsFeatured: String?
    
    enum CodingKeys: String, CodingKey {
        case id, classifiedSlug, classifiedTitle
        case classifiedMediaURL = "classifiedMediaUrl"
        case classifiedPrice, classifiedIsFeatured
    }
}

struct JobsMarketPlaceList: Codable {
    let id: Int
    let jobstitle, jobsSlug, jobsDesignation, jobsType: String?
    let jobsIsFeatured, jobsLocation, jobsCreatedAt, timeAgo: String?
    let jobsUpdatedBy, companyName, companyOwnership, companyLogo: String?
}

struct ErrNodeMarketPlace: Codable {
    let errMsg, errCode: String
}
