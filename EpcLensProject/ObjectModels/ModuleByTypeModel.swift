// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? JSONDecoder().decode(Welcome.self, from: jsonData)

import Foundation

struct ModuleByTypeModel: Codable {
    let data: ModuleByTypeDataClass
    let errNode: ErrNodeModuleByType
}

struct ModuleByTypeDataClass: Codable {
    let success: Bool
    let msg: String?
}

struct ErrNodeModuleByType: Codable {
    let errMsg, errCode: String
}
