// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? JSONDecoder().decode(Welcome.self, from: jsonData)

import Foundation

struct MarketPlaceClassifiedAdsDetailsModel: Codable {
    let data: ClassifiedAdsDetailsDataClass
    let errNode: ErrNodeClassifiedAdsDetails
}

struct ClassifiedAdsDetailsDataClass: Codable {
    let sucess: Bool
    let isSavedClassified: Int
    let classifieddetails: [ClassifiedDetails]?
    let defaultGalleries: [DefaultGallery]?
    let featuredClassified: [FeaturedClassified]?
    
    enum CodingKeys: String, CodingKey {
        case sucess
        case isSavedClassified = "is_saved_classified"
        case classifieddetails, defaultGalleries
        case featuredClassified = "featured_classified"
    }
}

struct ClassifiedDetails: Codable {
    let description, fax, profilePicture, contactNo: String?
    let countryCode, contactNo1, countryCode1, emailID: String?
    let title, slug, mediaURL: String?
    let price: Double?
    let shortDesc, location, isFeatured, startDate: String?
    let optionLabel: String?
    
    enum CodingKeys: String, CodingKey {
        case description, fax, profilePicture, contactNo, countryCode, contactNo1, countryCode1
        case emailID = "emailId"
        case title, slug
        case mediaURL = "mediaUrl"
        case price, shortDesc, location, isFeatured, startDate, optionLabel
    }
}

struct DefaultGallery: Codable {
    let galleryThumbnail, thumbnailShort, galleryType, youtubeIframe: String?
    let classifiedID: Int?
    
    enum CodingKeys: String, CodingKey {
        case galleryThumbnail, thumbnailShort, galleryType
        case youtubeIframe = "youtube_iframe"
        case classifiedID = "classifiedId"
    }
}

struct FeaturedClassified: Codable {
    let classifiedIsFeatured, classifiedMediaURL: String?
    let classifiedPrice: Int?
    let classifiedSlug, classifiedTitle: String?
    
    enum CodingKeys: String, CodingKey {
        case classifiedIsFeatured
        case classifiedMediaURL = "classifiedMediaUrl"
        case classifiedPrice, classifiedSlug, classifiedTitle
    }
}

struct ErrNodeClassifiedAdsDetails: Codable {
    let errMsg, errCode: String
}
