// To parse the JSON, add this file to your project and do:
//
//   let companyOverViewModel = try? JSONDecoder().decode(CompanyOverViewModel.self, from: jsonData)

import Foundation

struct CompanyOverViewModel: Codable {
    let data: CompanyOverView
    let errNode: ErrNodeOverView
}

struct CompanyOverView: Codable {
    let sucess: Bool
    let companyDetails: [CompanyDetailOverView]?
    
    enum CodingKeys: String, CodingKey {
        case sucess
        case companyDetails = "company_details"
    }
}

struct CompanyDetailOverView: Codable {
    let id: Int
    let satoffAddresslat, satoffAddresslng, operegAddresslat, operegAddresslng: String?
    let phoneNo, countryCode, email, headquarterAddress: String?
    let website, businessDesc, missionStatement, officeLocation: String?
    let operatingRegions: String?
    let yearOfStartingOps,presidentCeo, presidentCeoImage, parentCompany, naicsCode: String?
    let subsidiries, affilations, projectphaseName, opectypeName: String?
    
    enum CodingKeys: String, CodingKey {
        case id, satoffAddresslat, satoffAddresslng, operegAddresslat, operegAddresslng, phoneNo, countryCode, email, headquarterAddress, website, businessDesc, missionStatement, officeLocation
        case operatingRegions = "OperatingRegions"
        case yearOfStartingOps, presidentCeo, presidentCeoImage, parentCompany, naicsCode, subsidiries, affilations, projectphaseName, opectypeName
    }
}

struct ErrNodeOverView: Codable {
    let errMsg, errCode: String
}
