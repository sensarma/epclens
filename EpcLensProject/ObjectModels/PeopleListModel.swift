// To parse the JSON, add this file to your project and do:
//
//   let peopleListModel = try? JSONDecoder().decode(PeopleListModel.self, from: jsonData)

import Foundation

struct PeopleListModel: Codable {
    let data: PeopleListDataClass
    let errNode: ErrNodePeopleList
}

struct PeopleListDataClass: Codable {
    let sucess: Bool
    let totalPage: Int
    let peopleList: [PeopleList]
    
    enum CodingKeys: String, CodingKey {
        case sucess
        case totalPage = "total_page"
        case peopleList = "people_list"
    }
}

struct PeopleList: Codable {
    let id: Int
    let companyName, jobTypes, peopleName, designation: String?
    let location, img: String?
    
    enum CodingKeys: String, CodingKey {
        case id, companyName, jobTypes
        case peopleName = "people_name"
        case designation, location, img
    }
}

struct ErrNodePeopleList: Codable {
    let errMsg, errCode: String
}

