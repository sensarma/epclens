// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? JSONDecoder().decode(Welcome.self, from: jsonData)

import Foundation

struct CompanyOperationModel: Codable {
    let data: CompanyOperationDataClass
    let errNode: ErrNodeCompanyOperation
}

struct CompanyOperationDataClass: Codable {
    let sucess: Bool
    let companyDetails: [CompanyOperationDetail]?
    let companyprojects: [CompanyOperationProject]?
    let companyStory: [CompanyOperationStory]?
    let resultArray: [OperationResultArray]?
    
    enum CodingKeys: String, CodingKey {
        case sucess
        case companyDetails = "company_details"
        case companyprojects, companyStory, resultArray
    }
}

struct CompanyOperationDetail: Codable {
    let missionStatement, companySize, ftesBusinessUnit: String?
    let noOfStuff, totalStaff: Int?
    let registeredLisencedProducts, projectphaseName, opectypeName: String?
}

struct CompanyOperationStory: Codable {
    let id, companyID: Int?
    let storyTitle, storyDesc, createdAt, updatedAt: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case companyID = "companyId"
        case storyTitle, storyDesc, createdAt, updatedAt
    }
}

struct CompanyOperationProject: Codable {
    let id: Int?
    let slug, projectStatus, name, projectCompletionPercentage: String?
}

struct OperationResultArray: Codable {
    let title, type, description, mediaURL: String?
    let thumbnailShort, thumbnailURL, source: String?
    let compnygalID: Int?
    
    enum CodingKeys: String, CodingKey {
        case title, type, description
        case mediaURL = "mediaUrl"
        case thumbnailShort
        case thumbnailURL = "thumbnailUrl"
        case source
        case compnygalID = "compnygalId"
    }
}

struct ErrNodeCompanyOperation: Codable {
    let errMsg, errCode: String
}

