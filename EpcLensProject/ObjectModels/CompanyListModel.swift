// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? JSONDecoder().decode(Welcome.self, from: jsonData)

import Foundation

struct CompanyListModel: Codable {
    let data: CompanyListDataClass
    let errNode: ErrNodeCompanyList
}

struct CompanyListDataClass: Codable {
    let sucess: Bool
    let totalPage: Int
    let companyList: [CompanyList]?
    
    enum CodingKeys: String, CodingKey {
        case sucess
        case totalPage = "total_page"
        case companyList = "company_list"
    }
}

struct CompanyList: Codable {
    let id: Int
    let name: String?
    let currentPercentage, peakPercentage: Int?
    let projectphaseName: [ProjectphaseName]?
    let peopleName, peopleDesignation: String?
    let opectypeName: String?
    let noOfStuff: Int?
    let serviceType: ServiceType?
    let totalStaff: Int?
    let companySize: String?
    let logo: String?
    let revnue: Int?
    let slug: String?
    let compReg: [CompReg]?
    let projectPahseDetailsHTML: ProjectPahseDetailsHTML?
    let companyCategory: [CompanyCategory]?
    
    enum CodingKeys: String, CodingKey {
        case id, name, currentPercentage, peakPercentage, projectphaseName, peopleName, peopleDesignation, opectypeName, noOfStuff, serviceType, totalStaff, companySize, logo, revnue, slug, compReg
        case projectPahseDetailsHTML = "projectPahseDetailsHtml"
        case companyCategory
    }
}

struct CompReg: Codable {
    let regionName: String
    
    enum CodingKeys: String, CodingKey {
        case regionName = "region_name"
    }
}

struct CompanyCategory: Codable {
    let categoryName: String
    let companyID, isChecked: Int
    
    enum CodingKeys: String, CodingKey {
        case categoryName = "category_name"
        case companyID = "companyId"
        case isChecked = "is_checked"
    }
}

enum ProjectPahseDetailsHTML: String, Codable {
    case oepc = "oepc"
}

struct ProjectphaseName: Codable {
    let optionLabel: OptionLabel
    let isChecked: Int
    
    enum CodingKeys: String, CodingKey {
        case optionLabel
        case isChecked = "is_checked"
    }
}

enum OptionLabel: String, Codable {
    case construction = "Construction"
    case engineering = "Engineering"
    case ownership = "Ownership"
    case procurement = "Procurement"
}

enum ServiceType: String, Codable {
    case operations = "operations"
    case resourceDevelopment = "resource development"
    case services = "services"
}

struct ErrNodeCompanyList: Codable {
    let errMsg, errCode: String
}
