//
//  ProjectListModel.swift
//  EpcLensProject
//
//  Created by Mohammad Ishtiyak on 27/11/18.
//  Copyright © 2018 Moumita Datta. All rights reserved.
//

import UIKit
import Foundation

struct ProjectListModel: Codable {
    let data: ProjectListDataClass
    let errNode: ErrNodeProjectList
}

struct ProjectListDataClass: Codable {
    let sucess: Bool
    let totalPage: Int
    let projectList: [ProjectList]
    
    enum CodingKeys: String, CodingKey {
        case sucess
        case totalPage = "total_page"
        case projectList
    }
}

struct ProjectList: Codable {
    let projectID: Int
    let projectSlug, projectName: String
    let projectImage: String
    let siteImage, processImage, geographyImage, statusImage: String
    let ownerID: Int?
    let companyID, serviceTypeID: Int
    let desc, shortDesc: String
    let location, lat, long, specification: String?
    let projectStatus: ProjectStatus
    let projectValuation, projectCompletionPercentage: String
    let status: Status
    let companyName, companyOwnership: String?
    let companyLogo: String?
    let projectServiceTypeID: Int
    let projectServiceTypeName: ProjectServiceTypeName
    let projectServiceTypeIcon: String
    
    enum CodingKeys: String, CodingKey {
        case projectID = "projectId"
        case projectSlug, projectName, projectImage, siteImage, processImage, geographyImage, statusImage
        case ownerID = "ownerId"
        case companyID = "companyId"
        case serviceTypeID = "serviceTypeId"
        case desc, shortDesc, location, lat, long, specification, projectStatus, projectValuation, projectCompletionPercentage, status, companyName, companyOwnership, companyLogo
        case projectServiceTypeID = "projectServiceTypeId"
        case projectServiceTypeName, projectServiceTypeIcon
    }
}

enum ProjectServiceTypeName: String, Codable {
    case construction = "construction"
    case electrical = "electrical"
    case testProjectServiceType = "Test project service type"
}

enum ProjectStatus: String, Codable {
    case execution = "execution"
    case pendingApproval = "pending approval"
    case plan = "plan"
}

enum Status: String, Codable {
    case active = "active"
}

struct ErrNodeProjectList: Codable {
    let errMsg, errCode: String
}
