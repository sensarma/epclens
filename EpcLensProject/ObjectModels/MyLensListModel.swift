// To parse the JSON, add this file to your project and do:
//
//   let MyLensListModel = try? JSONDecoder().decode(CompanyListModel.self, from: jsonData)

import Foundation

struct MyLensListModel: Codable {
    let data: MyLensDataClass
    let errNode: ErrNodeMyLensList
}

struct MyLensDataClass: Codable {
    let sucess: Bool
    let mylenslist: [MyLensList]
}

struct MyLensList: Codable {
    let id: Int
    let itemID: String?
    let tableName: MyLensCategory
    let url, details, icon, shortDesc: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case itemID = "itemId"
        case tableName = "table_name"
        case url, details, icon, shortDesc
    }
}

enum MyLensCategory: String, Codable {
    case company = "company"
    case people = "people"
    case project = "project"
}

struct ErrNodeMyLensList: Codable {
    let errMsg, errCode: String
}

