// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? JSONDecoder().decode(Welcome.self, from: jsonData)

import Foundation

struct LensDeskDetailsModel: Codable {
    let data: LensDeskDetailsDataClass
    let errNode: ErrNodeLensDeskDetails
}

struct LensDeskDetailsDataClass: Codable {
    let sucess: Bool
    let lensdeskdetails: [LensdeskDetails]
}

struct LensdeskDetails: Codable {
    let id: Int
    let desktitle, deskdetails, lensdeskcategoryTitle: String?
}

struct ErrNodeLensDeskDetails: Codable {
    let errMsg, errCode: String
}
