// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? JSONDecoder().decode(Welcome.self, from: jsonData)

import Foundation

struct LexiconDetailsModel: Codable {
    let data: LexiconDetailsDataClass
    let errNode: ErrNodeLexiconDetails
}

struct LexiconDetailsDataClass: Codable {
    let sucess: Bool
    let result: [LexiconDetailsResult]
}

struct LexiconDetailsResult: Codable {
    let id: Int
    let title, publishedDate, description, mediaURL, details, lexiconcategoryTitle: String?
    //let title, publishedDate, description, mediaURL: String
    let thumbnailShort: String?
    let sponsorsBy: [LexiconDetailsSponsorsBy]?
    let projectData: [ProjectDatum]?
    let peopleData: [PeopleDatum]?
    let peopleFrom: String?
    
    enum CodingKeys: String, CodingKey {
        case id, title, publishedDate, description, details, lexiconcategoryTitle
        case mediaURL = "mediaUrl"
        case thumbnailShort
        case sponsorsBy = "sponsors_by"
        case projectData, peopleData, peopleFrom
    }
}

struct LexiconDetailsSponsorsBy: Codable {
    let compLogo: String?
    let galleryID, companyID: Int?
    let slug: String?
    
    enum CodingKeys: String, CodingKey {
        case compLogo
        case galleryID = "galleryId"
        case companyID = "company_id"
        case slug
    }
}

struct PeopleDatum: Codable {
    let compLogo: String?
    let galleryID, companyID: Int?
    let slug: String?
    
    enum CodingKeys: String, CodingKey {
        case compLogo
        case galleryID = "galleryId"
        case companyID = "company_id"
        case slug
    }
}

struct ProjectDatum: Codable {
    let projects: String?
}

struct ErrNodeLexiconDetails: Codable {
    let errMsg, errCode: String
}



