//
//  EpcLensLoaderViewController.swift
//  GifLoader
//
//  Created by Sudipta Biswas on 12/07/18.
//  Copyright © 2018 Sudipta Biswas. All rights reserved.
//

import UIKit
import Gifu

class EpcLensLoaderViewController: UIViewController {

    @IBOutlet weak var gifLoader: GIFImageView!
    var sourceControleller:UIViewController? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // TODO:- Loading Gif animation on loader willAppear
        gifLoader.animate(withGIFNamed: "EPC-hexa") {
            print("Start Loading")
        }
    }
    
    // MARK:- Show Epc Loader on the Source Controller
    func showEpcLoader(_sourceView:UIViewController) {
        self.sourceControleller = _sourceView
        _sourceView.addChildViewController(self)
        self.view.backgroundColor = UIColor.clear
        _sourceView.view.addSubview(self.view)
        self.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
    }
    
    // MARK:- Hide Epc Loader from Source Controller
    func hideEpcLoader() {
        DispatchQueue.main.async {
            self.gifLoader.stopAnimating()
            self.view.removeFromSuperview()
            self.removeFromParentViewController()
        }
    }
   

}
