//
//  SBFileManager.swift
//  EpcLensProject
//
//  Created by Sudipta Biswas on 1/26/18.
//  Copyright © 2018 Sudipta Biswas. All rights reserved.
//

import UIKit

class SBFileManager: NSObject {
    
    var sourceController:UIViewController? = nil
    
    override init() {
        super.init()
        
    }
    
    // MARK:- Get Document Directory path
    func GetDocumentDirectory()->String {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        print(documentsPath)
        return documentsPath;
    }
    
    // MARK:- Create a directory in documentdirectory
    func createFolder(_foldername:String) -> Bool {
        
        let folderPath = GetDocumentDirectory().stringByAppendingPathComponent(path: _foldername)
        if !FileManager.default.fileExists(atPath: folderPath) {
            do {
                try FileManager.default.createDirectory(atPath: folderPath, withIntermediateDirectories: true, attributes: nil)
            } catch let error as NSError {
                NSLog("Unable to create directory \(error.debugDescription)")
            }
            return true
        }
        return false
    }
    
    func createFolderWithPath(_folderpath:String) -> Bool {
        
        let folderPath = _folderpath
        if !FileManager.default.fileExists(atPath: folderPath) {
            do {
                try FileManager.default.createDirectory(atPath: folderPath, withIntermediateDirectories: true, attributes: nil)
            } catch let error as NSError {
                NSLog("Unable to create directory \(error.debugDescription)")
            }
            return true
        }
        return false
    }
    
    // MARK:- Create sub directories
    func createSubFolders(_directory:String,_arrFolder:[String]) -> Bool {
        
        for folderName in _arrFolder {
            let folderPath = GetDocumentDirectory().stringByAppendingPathComponent(path: _directory).stringByAppendingPathComponent(path: folderName)
            if (!FileManager.default.fileExists(atPath: folderPath)) {
                do {
                    try FileManager.default.createDirectory(atPath: folderPath, withIntermediateDirectories: true, attributes: nil)
                } catch let error as NSError {
                    NSLog("Unable to create directory \(error.debugDescription)")
                }
            }
        }
        return true
    }
    
    
    func isFileExist(_directory:String) -> Bool {
        let filePath = GetDocumentDirectory().stringByAppendingPathComponent(path: _directory)
        if (FileManager.default.fileExists(atPath: filePath)) {
            return true;
        } else {
            return false
        }
    }
    
    func isFileExistAtPath(_filePath:String) -> Bool {
        if (FileManager.default.fileExists(atPath: _filePath)) {
            return true;
        } else {
            return false
        }
    }
    
    // MARK:- Save image to Document Directory
    func saveImage(_img:UIImage,_imgFilename:String) {
        
        let imgData = UIImageJPEGRepresentation(_img, 0.5)
        let FileName = _imgFilename+".jpeg"
        let imagePath = GetDocumentDirectory().stringByAppendingPathComponent(path: FileName)
        do {
            try imgData?.write(to: URL.init(fileURLWithPath: imagePath))
        } catch {
            print(error)
        }
    }
    
    func saveImage(_img:UIImage,_sourceDirectory:String) -> (Bool,String) {
        let imgData = UIImagePNGRepresentation(_img)
        let imgName = Date().TimeStamp()+".png"
        let directoryPath = GetDocumentDirectory().stringByAppendingPathComponent(path: _sourceDirectory)
        let imagePath = directoryPath.stringByAppendingPathComponent(path: imgName)
        do {
            try imgData?.write(to: URL.init(fileURLWithPath: imagePath))
            return (true,imgName)
        } catch {
            print(error)
        }
        return (false,"")
    }
    
    func saveImageInPath(_img:UIImage,_sourcePath:String) -> (Bool) {
        let imgData = UIImagePNGRepresentation(_img)
        let imagePath = _sourcePath
        do {
            try imgData?.write(to: URL.init(fileURLWithPath: imagePath))
            return (true)
        } catch {
            print(error)
        }
        return (false)
    }
    
    func saveVideo(_videoUrl:NSURL,_sourceDirectory:String) -> (Bool,String) {
        let videoName = Date().TimeStamp()+".mov"
        let directoryPath = GetDocumentDirectory().stringByAppendingPathComponent(path: _sourceDirectory)
        let videoPath = directoryPath.stringByAppendingPathComponent(path: videoName)
        do {
            let movieData = try NSData(contentsOf: _videoUrl as URL, options: .mappedIfSafe)
            try movieData.write(to: URL.init(fileURLWithPath: videoPath))
            return (true,videoName)
        } catch {
            print(error)
        }
        return (false,"")
    }
    
    func saveVideoData(_videoData:Data,_sourceDirectory:String) -> (Bool,String) {
        let videoName = Date().TimeStamp()+".mov"
        let directoryPath = GetDocumentDirectory().stringByAppendingPathComponent(path: _sourceDirectory)
        let videoPath = directoryPath.stringByAppendingPathComponent(path: videoName)
        do {
            let movieData = _videoData
            try movieData.write(to: URL.init(fileURLWithPath: videoPath))
            return (true,videoName)
        } catch {
            print(error)
        }
        return (false,"")
    }
    
    // MARK:- Save Note to Directory
    func saveNote(text: String, fileNamed: String, folder: String) -> (Bool,String) {
        guard let writePath = NSURL(fileURLWithPath: GetDocumentDirectory()).appendingPathComponent(folder) else { return (false,"") }
        let fileName = fileNamed + ".txt"
        let file = writePath.appendingPathComponent(fileName)
        try? text.write(to: file, atomically: false, encoding: String.Encoding.utf8)
        return (true,fileName)
    }
    
    // MARK:- Create Folder name in Documentdirectory
    func createFolderNameFromTag(_objString:String) -> String {
        let notAllowedCharactersSet = CharacterSet.init(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_0123456789").inverted
        let filtered = (_objString.components(separatedBy: notAllowedCharactersSet) as NSArray).componentsJoined(by: "")
        if filtered != "" {
            return filtered
        } else {
            return "Folder_\(Date().TimeStamp())"
        }
        //_objString.replacingOccurrences(of: ".", with: "").replacingOccurrences(of: " ", with: "_")
    }
    
    // MARK:- Delete Tag and all it's contents
    func deleteDirectoryWithAllContents(_directory:String) -> Bool {
        let dirPath = GetDocumentDirectory()
        let tempDirPath = dirPath.stringByAppendingPathComponent(path: _directory)
        do {
            let directoryContents: NSArray = try FileManager.default.contentsOfDirectory(atPath: tempDirPath) as NSArray
            if directoryContents.count > 0 {
                for path_ in directoryContents {
                    let fullPath = tempDirPath.stringByAppendingPathComponent(path: path_ as! String)
                    try! FileManager.default.removeItem(atPath: fullPath)
                }
                try! FileManager.default.removeItem(atPath: tempDirPath)
                
            }
        } catch {
            print(error)
        }
        return true
    }
    
    func deleteDirectoryAllContents(_directory:String) -> Bool {
        let dirPath = GetDocumentDirectory()
        let tempDirPath = dirPath.stringByAppendingPathComponent(path: _directory)
        do {
            let directoryContents: NSArray = try FileManager.default.contentsOfDirectory(atPath: tempDirPath) as NSArray
            if directoryContents.count > 0 {
                for path_ in directoryContents {
                    let fullPath = tempDirPath.stringByAppendingPathComponent(path: path_ as! String)
                    try! FileManager.default.removeItem(atPath: fullPath)
                }
            }
        } catch {
                print(error)
        }
        return true
    }
    
    func deleteFile(_filePath:String) -> Bool {
        do {
            if _filePath != "" && FileManager.default.fileExists(atPath: _filePath) {
                try! FileManager.default.removeItem(atPath: _filePath)
            } else {
                return false
            }
        }
        return true
    }
    
    // MARK:- Rename Directory
    func renameDirectory(_formDirectory:String,_toDirectory:String) {
        do {
            let path = GetDocumentDirectory()
            let originPath = path.stringByAppendingPathComponent(path: _formDirectory)
            let destinationPath = path.stringByAppendingPathComponent(path: _toDirectory)
            try FileManager.default.moveItem(at: URL.init(fileURLWithPath: originPath), to: URL.init(fileURLWithPath: destinationPath))
        } catch {
            print(error)
        }
    }
    
    // MARK:- Get All Contents of directory
    func getAllContentsOfDocumentDirectory() -> NSArray {
        let path = GetDocumentDirectory()
        do {
            let directoryContents: NSArray = try FileManager.default.contentsOfDirectory(atPath: path) as NSArray
            return directoryContents
        } catch {
            print(error)
        }
        return []
        
    }
    
    func GetAllDirectoryAllContents(_directory:String) -> [String] {
        
        let dirPath = GetDocumentDirectory()
        var arrofFiles = [String]()
        let tempDirPath = dirPath.stringByAppendingPathComponent(path: _directory)
        do {
            let directoryContents: NSArray = try FileManager.default.contentsOfDirectory(atPath: tempDirPath) as NSArray
            if directoryContents.count > 0 {
                for path_ in directoryContents {
                    let fullPath = tempDirPath.stringByAppendingPathComponent(path: path_ as! String)
                    arrofFiles.append(fullPath)
                }
            }}catch{
                print(error)
        }
        return arrofFiles
    }
    
}

extension String {
    func stringByAppendingPathComponent(path: String) -> String {
        let nsSt = self as NSString
        return nsSt.appendingPathComponent(path)
    }
}


