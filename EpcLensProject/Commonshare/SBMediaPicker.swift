//
//  SBMediaPicker.swift
//  EpcLensProject
//
//  Created by Sudipta Biswas on 1/26/18.
//  Copyright © 2018 Sudipta Biswas. All rights reserved.
//

import UIKit
import MobileCoreServices

class SBMediaPicker: NSObject,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    private var imagePicker:UIImagePickerController = UIImagePickerController()
    public var sourceController:UIViewController = UIViewController()
    public var image:UIImage = UIImage()
    public var videoURL:NSURL = NSURL()
    public var selectorFunction:Selector? = nil
    
    override init() {
        super.init()
        
    }

    func pickImage() {
        
        let alertController = UIAlertController(title: "Pick Image", message: "Please choose location", preferredStyle: .alert)
        let actionPhotolibrary = UIAlertAction(title: "Photo Library", style: .default) { (action) in
            self.imagePicker.mediaTypes = [kUTTypeImage as String]
            alertController.dismiss(animated: true, completion: nil)
            self.pickFromPhotoLibrary()
        }
        let actionCamera = UIAlertAction(title: "Device Camera", style: .default) { (action) in
            self.imagePicker.mediaTypes = [kUTTypeImage as String]
            alertController.dismiss(animated: true, completion: nil)
            self.pickFromCamera()
        }
        let actionCancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        alertController.addAction(actionPhotolibrary)
        alertController.addAction(actionCamera)
        alertController.addAction(actionCancel)
        self.sourceController.present(alertController, animated: true, completion: nil)
        
    }
    
    func pickVideo() {
        
        let alertController = UIAlertController(title: "Pick Video", message: "Please choose location", preferredStyle: .alert)
        let actionPhotolibrary = UIAlertAction(title: "Photo Library", style: .default) { (action) in
            self.imagePicker.mediaTypes = [kUTTypeMovie as String]
            alertController.dismiss(animated: true, completion: nil)
           self.pickFromPhotoLibrary()
        }
        let actionCamera = UIAlertAction(title: "Device Camera", style: .default) { (action) in
            self.imagePicker.mediaTypes = [kUTTypeMovie as String]
            alertController.dismiss(animated: true, completion: nil)
            self.pickFromCamera()
        }
        let actionCancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        alertController.addAction(actionPhotolibrary)
        alertController.addAction(actionCamera)
        alertController.addAction(actionCancel)
        self.sourceController.present(alertController, animated: true, completion: nil)
        
    }
    
    
    private func pickFromPhotoLibrary(){
        self.imagePicker.delegate = self;
        self.imagePicker.allowsEditing = false
        self.imagePicker.sourceType = .photoLibrary
        self.sourceController.present(self.imagePicker, animated: true, completion: nil)
    }
    
    private func pickFromCamera(){
        self.imagePicker.delegate = self;
        self.imagePicker.allowsEditing = false
        self.imagePicker.sourceType = .camera
        self.sourceController.present(self.imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.imagePicker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if (picker.mediaTypes == [kUTTypeImage as String]){
            
            let _image = info[UIImagePickerControllerOriginalImage] as! UIImage
            self.image = _image
            
        } else {
            let _videoURL = info["UIImagePickerControllerMediaURL"] as? NSURL
            self.videoURL = _videoURL!
        }
        self.imagePicker.dismiss(animated: true, completion: nil)
        callFunction()
  
    }
    
    private func callFunction() {
        let meth = class_getInstanceMethod(object_getClass(self.sourceController), self.selectorFunction!)
        let imp = method_getImplementation(meth!)
        typealias ClosureType = @convention(c) (AnyObject, Selector) -> Void
        let callbackFunction : ClosureType = unsafeBitCast(imp, to: ClosureType.self)
        callbackFunction(self.sourceController,self.selectorFunction!)
    }
    
}
