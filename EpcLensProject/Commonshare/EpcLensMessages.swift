//
//  EpcLensMessages.swift
//  EpcLensProject
//
//  Created by Sudipta Biswas on 1/27/18.
//  Copyright © 2018 Sudipta Biswas. All rights reserved.
//

import UIKit

class EpcLensMessages{
    static let NoInternetConnection = "No network available on your device. Please check and try again later."
    
    static let login_InvalidEmail = "Please enter valid email."
    static let login_blankPassword = "Password field cannot be left blank. Please enter password."
    static let login_credentialInvalid = "You have entered incorrect credentials. Please enter valid credentials to proceed"
    
    static let login_forgotpassMessage = "Please enter your email id to retrieve password"
    static let login_passwordMinimumLength = "Password minimum 6 characters in length."
    static let login_SignupOtp = "Please check your text messages and input the OTP here. You will get an email afterwards with password. You can then copy and paste the password from email on login page. You are able to change your password from profile once you are logged in with your account"
    static let login_otpBlank = "Please enter OTP to proceed"
    static let TryagainLater = "Please try again later"
    
    static let Signup_fname = "Please enter First Name"
    static let Signup_lname = "Please enter Last Name"
    static let Signup_email = "Please enter Email."
    static let Signup_country = "Please select Country"
    static let Signup_phone = "Please enter Phone Number"
    static let Signup_InvalidOtp = "Please enter correct OTP to proceed"
    
    static let ChooseLocation_ProperAddress = "Please choose proper location"
    
    
    static let InVaild_VideoUrl = "Url not found 404 Error"
    static let IsValid_ImageUrl = "click item is image"
    //static let NotFound_VideoUrl = "Url not found 404 Error"
    
    
    static let EventPost_ExccedPerformanceDate = "You can not add more than 25 Performance Dates."
    
    static let Enter_Event_Name = "Please enter event name.";
    static let Enter_Result_Name = "Please enter result name.";
    static let Enter_Group_Name = "Please enter group name.";
    
    static let Enter_Added_Money = "Please enter added money.";
    static let Enter_Performance_Open_Date_Time = "Please enter performance start date and time.";
    static let Enter_Performance_Open_Date = "Please enter performance start date.";
    static let Enter_Performance_Open_Time = "Please enter performance start time.";
    static let Enter_Performance_Close_Date = "Please enter performance end date.";
    static let Enter_Performance_Close_Time = "Please enter performance end time.";
    static let Enter_Slack_Date = "Please enter slack date.";
    static let Enter_Slack_Time = "Please enter slack time.";
    static let Enter_Open_Date = "Please enter start date.";
    static let Enter_Open_Time = "Please enter start time.";
    static let Enter_Close_Date = "Please enter close date.";
    static let Enter_Close_Time = "Please enter close time.";
    static let Enter_Event_Desc = "Please enter event description.";
    static let Enter_Event_YourData = "Please enter your data.";
    static let Enter_Event_Category = "Please choose category.";
    static let Enter_Event_Loaction = "Please choose location.";
    static let Enter_Event_Image_OR_TEXT = "You can upload text or image.Please select any one option.";
    static let Enter_POST_SUCCESS = "New event created successfully. You can go back to list to see info."
    
    
    
    
    static let Enter_Image_Text_Result = "Please enter description or upload image.";
    static let Enable_Locaion_Service = "Please enable location service and try again.";
    
    static let Enter_From_Address = "Please enter From info";
    static let Enter_Livesin_Address = "Please enter Lives In info";
    static let Enter_Marriedto = "Please enter married to person";
    static let Enter_First_Name = "Please enter first name";
    static let Enter_Last_Name = "Please enter last name";
    
    static let ContactUs_ContactName = "Please enter contact name"
    static let ContactUs_email = "Please enter email"
    static let ContactUs_phone = "Please enter phone number"
    static let ContactUs_remarks = "Please enter your remarks"
    
    static let Logout_Reach = "Are you sure you want to logout from Reach?"
    
    static let ChangePass_Enter_Current_Pass = "Please enter your current password.";
    static let ChangePass_Enter_New_Pass = "Please enter new password.";
    static let ChangePass_Enter_Confirm_Pass = "Please enter confirm password.";
    static let ChangePass_Enter_New_Confirm_Pass = "New password and confirm password is not matching. Please check.";
    
    static let EditPro_Success = "Your profile has been successfully updated.";
    static let EditPro_Change_Pic = "You have changed profile picture.Do you want to update it.";
    
    static let Interest_SureToDelete = "Are you sure you want to delete this interest?"
    
    static let CommonParseEorrorMsg = "Something went wrong. please try again later."
    
    
    static let Rlist_Post_Title = "Please enter post title.";
    static let Enter_contact_name = "Please enter contact name.";
    
    static let Rlist_Post_Desc = "Please enter description";
    static let Enter_Remarks = "Please enter remarks.";
    
    static let Rlist_email = "Please enter email."
    static let Rlist_phone = "Please enter phone number."
    static let Rlist_ZipCode = "Please enter zip code."
    static let Rlist_ValidUrl = "Please enter valid url."
    
    static let ShowAddress_locationAccess = "Please give access to location service."
    static let ShowAddress_enableLocation = "Location services are not enabled. Please enable it."
    
    static let Map_Start_date = "Please enter start date.";
    static let Map_End_date = "Please enter end date.";
    static let Map_Start_date_End_date = "Start date should be less than end date.";
    static let Location_Service_Enable = "Please enable location service to pick location.";
    
    static let ActivityReportActivity = "You have successfully submitted your report against activity."
    static let ActivityReportFailed = "Your report has not been submitted successfully. Please try again later."
    static let ActivityShare = "Are you sure you want to share this activity?"
    
    static let ActivityShareSuccess = "You have successfully shared this activity."
    static let ActivityShareFailed = "Something wrong happens when you try to share this activity. Please try to share it later."
    
    static let ActivityPostSuccess = "You have successfully posted."
    static let ActivityPostFailed = "Something wrong happens when you try to post your activity. Please try to post it later."
    
    static let RlistPostExternalUrlInfo = "Have posted this item in another website already, e.g. Facebook? Please enter the link here and we will link this Item to that post"
    
}

class EpcLensShortText{
    static let Reach = "Reach"
    static let loading = "Loading..."
    static let ForgotPass = "Forgot Password"
    static let Submit = "Submit"
    static let Cancel = "Cancel"
    static let ShareOnFb = "Want to share in Facebook"
    static let Add = "Add"
    static let Ok = "Ok"
    static let Save = "Save"
    static let ResendOtp = "Resend OTP"
    static let OtpSignUp = "Complete Signup with OTP"
    static let Done = "Done"
    static let Sure = "Sure"
    static let SelectDate = "Select Date"
    static let SelectTime = "Select Time"
    static let SelectDateTime = "Select Date and Time"
    static let Logout = "Logout"
    static let AssociateTitle = "R Associates"
    static let ChooseType = "Please choose type"
    
    static let Image = "Image"
    static let Text = "Text"
    static let ActivityReport = "Report Activity"
    static let ActivityShare = "Share Activity"
    static let ActivityPost = "Post Activity"
    
    static let ExternalURl = "External URL"
    
}


class EpcLensDateFormat {
    static let Dateformat1 = "MM/dd/yyyy"
    static let Dateformat2 = "yyyy-MM-dd"
    static let Dateformat3 = "dd-MMM-yyyy"
    static let Timeformat1 = "HH:mm"
    static let DateTimeFormat1 = "yyyy-MM-dd HH:mm"
    static let DateTimeFormat2 = "yyyy-MM-dd HH:mm:ss"
    static let DateTimeFormat3 = "dd MMM yyyy HH:mm:ss"
    static let DateTimeFormat4 = "yyyy-MM-dd'T'HH:mm:ssZZZ"
    static let DateTimeFormat5 = "MMM-dd-yyyy 'at' HH:mm:ss"
}

class EpcLensPlaceholder {
    
    static let EmailId = "Email"
    static let Otp = "OTP"
    static let Entername = "Enter Name"
}
