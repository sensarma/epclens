//
//  EpcLensController.swift
//  EpcLensProject
//
//  Created by Sudipta Biswas on 1/26/18.
//  Copyright © 2018 Sudipta Biswas. All rights reserved.
//

import UIKit
import SystemConfiguration
import Agrume
import CoreLocation
import AVFoundation

class EpcLensController {
    static let sharedInstance = EpcLensController()
    
    
    // MARK:- Check location service
    func isLocationPermissionGranted() -> Bool
    {
        guard CLLocationManager.locationServicesEnabled() else { return false }
        return [.authorizedAlways, .authorizedWhenInUse].contains(CLLocationManager.authorizationStatus())
    }
    
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func isValid(_ email: String) -> Bool {
        let emailRegEx = "(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"+"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"+"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"+"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"+"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"+"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"+"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        
        let emailTest = NSPredicate(format:"SELF MATCHES[c] %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    func canOpenURL(string: String?) -> Bool {
        guard let urlString = string else {return false}
        guard let url = NSURL(string: urlString) else {return false}
        if !UIApplication.shared.canOpenURL(url as URL) {return false}
        
        //
        let regEx = "((https|http)://)((\\w|-)+)(([.]|[/])((\\w|-)+))+"
        let predicate = NSPredicate(format:"SELF MATCHES %@", argumentArray:[regEx])
        return predicate.evaluate(with:string)
    }
    
    func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
    
    func getThumbnailFrom(path: URL) -> UIImage? {
        do {
            let asset = AVURLAsset(url: path , options: nil)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            imgGenerator.appliesPreferredTrackTransform = true
            let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(0, 1), actualTime: nil)
            let thumbnail = UIImage(cgImage: cgImage)
            return thumbnail
            
        } catch let error {
            print("*** Error generating thumbnail: \(error.localizedDescription)")
            return nil
        }
    }
    
    // MARK:- Json String
    func JSONStringify(value: AnyObject,prettyPrinted:Bool = false) -> String{
        
        if JSONSerialization.isValidJSONObject(value) {
            
            do{
                let data = try JSONSerialization.data(withJSONObject: value, options: JSONSerialization.WritingOptions(rawValue: 0))
                if let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                    return string as String
                }
            }catch {
                
                print("error")
                //Access error here
            }
            
        }
        return ""
        
    }
    
    // MARK:- Show alert message
    func showAlert(_sourceController:UIViewController,_msg:String){
        
        let alertController = UIAlertController(title: "EpcLens", message: _msg, preferredStyle: .alert);
        let actionOk = UIAlertAction(title: "Ok", style: .cancel) { (action) in
            
        }
        alertController.addAction(actionOk)
        _sourceController.present(alertController, animated: true, completion: nil)
    }
    
    // MARK:- Show alert message
    func showAlert(_sourceController:UIViewController,_msg:String, withCompletion:@escaping () -> Void){
        
        let alertController = UIAlertController(title: "EpcLens", message: _msg, preferredStyle: .alert);
        let actionOk = UIAlertAction(title: "Ok", style: .cancel) { (action) in
            withCompletion()
        }
        alertController.addAction(actionOk)
        _sourceController.present(alertController, animated: true, completion: nil)
    }
    func showAlert(_sourceController:UIViewController,_msg:String, onTitle:String, onYes:@escaping()->Void,cancelTitle:String, onCancel:@escaping () -> Void){
        
        let alertController = UIAlertController(title: "EpcLens", message: _msg, preferredStyle: .alert);
        if onTitle.count > 0{
            let actionOk = UIAlertAction(title: onTitle, style: .default) { (action) in
                onYes()
            }
             alertController.addAction(actionOk)
        }
        if cancelTitle.count > 0{
            let actionCancel = UIAlertAction(title: cancelTitle, style: .cancel) { (action) in
                onCancel()
            }
            alertController.addAction(actionCancel)
        }
       
        _sourceController.present(alertController, animated: true, completion: nil)
    }
    
    // MARK:- Show Toast message on screen
    func showToastMesage(_sourceController:UIViewController,_msg:String) {
        _sourceController.view.makeToast(_msg, duration: 2.0, position: .center, title: nil, image: nil)
    }
    
    // MARK:- Preview Image
    func showImagePreview(_img:UIImage = #imageLiteral(resourceName: "RNoimage"),_imglink:String = "",_controller:UIViewController) {
        let imgTemp = _img
        if _imglink != "" {
            
        }
        let agrume = Agrume(image: imgTemp, backgroundBlurStyle: UIBlurEffectStyle.light, backgroundColor: UIColor.black)
        agrume.showFrom(_controller)
    }
    
    // MARK:- Time Ago
    public func timeAgoSince(_ date: Date) -> String {
        
        let calendar = Calendar.current
        let now = Date()
        let unitFlags: NSCalendar.Unit = [.second, .minute, .hour, .day, .weekOfYear, .month, .year]
        let components = (calendar as NSCalendar).components(unitFlags, from: date, to: now, options: [])
        
        if let year = components.year, year >= 2 {
            return "\(year) years ago"
        }
        
        if let year = components.year, year >= 1 {
            return "Last year"
        }
        
        if let month = components.month, month >= 2 {
            return "\(month) months ago"
        }
        
        if let month = components.month, month >= 1 {
            return "Last month"
        }
        
        if let week = components.weekOfYear, week >= 2 {
            return "\(week) weeks ago"
        }
        
        if let week = components.weekOfYear, week >= 1 {
            return "Last week"
        }
        
        if let day = components.day, day >= 2 {
            return "\(day) days ago"
        }
        
        if let day = components.day, day >= 1 {
            return "Yesterday"
        }
        
        if let hour = components.hour, hour >= 2 {
            return "\(hour) hours ago"
        }
        
        if let hour = components.hour, hour >= 1 {
            return "An hour ago"
        }
        
        if let minute = components.minute, minute >= 2 {
            return "\(minute) minutes ago"
        }
        
        if let minute = components.minute, minute >= 1 {
            return "A minute ago"
        }
        
        if let second = components.second, second >= 3 {
            return "\(second) seconds ago"
        }
        
        return "Just now"
        
    }

    
}

